import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { FormGroupDirective } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { DocumentViewer } from '@ionic-native/document-viewer';
import { SecureStorage } from '@ionic-native/secure-storage';
import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';
import { FileOpener } from '@ionic-native/file-opener';
import { CallNumber } from '@ionic-native/call-number';
import { CodePush } from '@ionic-native/code-push';
import { EmailComposer } from '@ionic-native/email-composer';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from '../interceptors/auth.interceptor';
import { LoadingInterceptor } from '../interceptors/loading.interceptor';

import { EffectsModule } from '@ngrx/effects';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';



import { MyApp } from './app.component';
import { TestComponent } from '../components/test/test.component';
import { ComponentModule } from '../components';
import { ModalModule } from '../modals';
import { NotificationsModule } from '../pages/notifications/notifications.module';

import { ConfigService } from '../config/configProvider';
import { LoadingService } from '../modules/loading.provider';
import { StorageService } from '../config/storageProvider';
import { APIService } from '../modules/serviceProvider';
import { NavigationService } from './navigation';

import { LoginModule } from '../pages/login/login.module';
import { HomeModule } from '../pages/home/home.module';
import { AOHModule } from '../pages/aoh/aoh.module';
import { BasketModule } from '../pages/basket/basket.module';
import { SearchModule } from '../pages/search/search.module';
import { LiftingsModule } from '../pages/liftings/liftings.module';
import { PlaygroundModule } from '../pages/playground/playground.module';
import { ContractDetailsModule } from '../pages/contracts/subPages/contractDetails/contractDetails.module';
import { LoadIdsModule } from '../pages/loadids/loadids.module';
import { UsefulInformationModule } from '../pages/usefulInformation/usefulInformation.module';
import { PricesModule } from '../pages/prices/prices.module';
import { UserModule } from '../pages/user/user.module';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { rootReducer } from '../modules/root.reducer';
import { metaReducers } from '../modules/storeEnchancers';
import { effects } from '../modules/effects';
import { HSSEModule } from '../pages/hsse/hsse.module';


export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/locales/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    TestComponent,
    // ConnectFormDirective,
  ],
  imports: [
    BrowserModule,
    ComponentModule,
    ModalModule,
    LoginModule,
    HomeModule,
    AOHModule,
    BasketModule,
    SearchModule,
    HSSEModule,
    LiftingsModule,
    PlaygroundModule,
    ContractDetailsModule,
    NotificationsModule,
    HttpClientModule,
    HttpModule,
    NotificationsModule,
    LoadIdsModule,
    UsefulInformationModule,
    PricesModule,
    UserModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: (createTranslateLoader),
          deps: [HttpClient]
      }}),
    StoreModule.forRoot(rootReducer, { metaReducers }),
    EffectsModule.forRoot(effects),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
    }),
    IonicModule.forRoot(MyApp, {
      mode: 'md'
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StorageService,
    StatusBar,
    SplashScreen,
    ConfigService,
    NavigationService,
    LoadingService,
    APIService,
    InAppBrowser,
    DocumentViewer,
    SecureStorage,
    File,
    FileTransfer,
    FileOpener,
    FormGroupDirective,
    CallNumber,
    CodePush,
    EmailComposer,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: LoadingInterceptor,
    //   multi: true
    // },
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule {}
