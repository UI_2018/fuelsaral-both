import { Injectable } from '@angular/core';

import { Store } from '@ngrx/store';

import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { TranslateService } from '@ngx-translate/core';


import { select } from '../utils/storeUtils';
import { customersAsArray, selectedCustomer } from '../pages/customers/customers.selectors';

import { LoadingService } from '../modules/loading.provider';

import { getResourceUrl } from '../utils/cordova.helpers.js';

@Injectable()
export class NavigationService {
    nav;
    constructor(
        private store: Store<any>,
        private loadingService: LoadingService,
        private transfer: FileTransfer,
        private file: File,
        private fileOpener: FileOpener,
        private translateService: TranslateService) {
    }

    setNav(nav) {
        this.nav = nav;
    }

    followLink(link, params = {}) {
        if (['contracts', 'aoh'].indexOf(link) !== -1) {
            if (!select(this.store, selectedCustomer)) {
                this.nav.setRoot('customers', { target: link });
                return;
            } else if (select(this.store, customersAsArray).length > 1) {
                this.nav.setPages([{ page: 'customers', params: { target: link } }, { page: link }]);
                return;
            } else {
                this.nav.setRoot(link, params);
            }
        } else {
            this.nav.setRoot(link, params);
        }
    }

    push(link, params = {}) {
        this.nav.push(link, params);
    }

    openRemotePDF(url, name) {
        if (!window['cordova']) {
            this.loadingService.showLoader();

            setTimeout(() => {
                this.loadingService.hideLoader();
                alert('Only supported on device');
            }, 1000);
        } else {
            this.loadingService.showLoader();
            const fileTransfer: FileTransferObject = this.transfer.create();
            fileTransfer.download(url, this.file.dataDirectory + name).then((entry) => {
              this.loadingService.hideLoader();
              this.fileOpener.open(entry.toURL(), 'application/pdf')
                .then(() => console.log('File is opened'))
                .catch(e => console.log('Error opening file', e));
            }, (error) => {
                this.loadingService.hideLoader();
              // handle error
            });
        }

    }

    openPDF(url) {
        this.fileOpener.open(getResourceUrl(url), 'application/pdf');
    }


    openFile(url, displayName = null) {
        if (window['cordova']) {
            var lastIndex = url.lastIndexOf('/');
            const path = url.substr(0, lastIndex)
            const fileName = url.substr(lastIndex + 1)
            displayName = displayName ? this.translateService.instant(displayName) + '.pdf' : null
            const base = window['cordova'].file.applicationDirectory + 'www' + path
            if(displayName) {
                let theMove = this.file.copyFile(base, fileName, window['cordova'].file.tempDirectory, displayName);
                this.fileOpener.open(window['cordova'].file.tempDirectory + displayName, 'application/pdf')
            } else {
                this.fileOpener.open(getResourceUrl(url), 'application/pdf');
            }
            // this.fileOpener.open(getResourceUrl(path + '/pdf.pdf'), 'application/pdf');
        } else {
            window.open(url);
        }
    }
}
