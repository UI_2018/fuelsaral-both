import { Component, ViewChild } from '@angular/core';
import { MenuController, Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TranslateService } from '@ngx-translate/core';

import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import moment from 'moment';
import { CodePush } from '@ionic-native/code-push';

import { StorageService } from '../config/storageProvider';


import { getMenuOptions, user } from '../modules/user.selectors';
import { logout  } from '../modules/user.reducer';
import { viewDidEnter } from '../modules/router/router.reducer';

import { showSidebar } from '../modules/router/router.selectors';

import { LoginPage } from '../pages/login/login';


import { NavigationService } from './navigation';
import { LoadingService } from '../modules/loading.provider';
interface AppState {
  count: number;
}

interface User {
  fullName: string;
}
const startingHash = window.location.hash || '#/login';
// window.location.hash = ''
@Component({
  templateUrl: 'app.html',
  styles: ['./app.scss']
})
export class MyApp {
  count: Observable<number>;
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  navigation: any[];

  navigation$;

  showSidebar$;

  user: Observable<User>;

  codePushVersion;


  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    private store: Store<AppState>,
    public splashScreen: SplashScreen,
    public menuCtrl: MenuController,
    public storage: StorageService,
    public translate: TranslateService,
    private navigate: NavigationService,

    private codePush: CodePush,
    private loadingService: LoadingService

  ) {
    translate.setDefaultLang('en');
    if (window['cordova']) {
      translate.use('de');
      moment.lang('de');
    } else {
      translate.use('en');
      moment.lang('en');
      // translate.use('en');
      // moment.use('en');
    }
    this.initializeApp();

    this.navigation$ = this.store.select(getMenuOptions);
    this.showSidebar$ = this.store.select(showSidebar);
    this.user = this.store.select(user);

    this.count = store.pipe(select('count'));
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // if(window['cordova'] && false) {
      //   this.codePush.sync({updateDialog: true, deploymentKey: 'FS2S_bYlYg-6IsYMrG68xC8gyFp-r1ghIZnP7'})
      //     .subscribe((syncStatus) => {
      //       switch(syncStatus) {
      //         case SyncStatus.DOWNLOADING_PACKAGE:
      //           // alert('downloading')
      //           this.loadingService.showLoader()
      //           break
      //         case SyncStatus.UPDATE_INSTALLED:
      //           // alert('installed')
      //           setTimeout(() => {
      //             this.loadingService.hideLoader()
      //             alert('Please restart app to load new version')
      //             window.location.reload()
      //           }, 3000)
      //           break
      //         case SyncStatus.INSTALLING_UPDATE:
      //           // alert('installing')
      //           this.loadingService.showLoader()
      //           break;
      //         case SyncStatus.ERROR:
      //           // alert('error')
      //           setTimeout(() => {
      //             this.loadingService.hideLoader()
      //             alert('Please restart app to load new version')
      //             window.location.reload()
      //           }, 3000)
      //           break
      //       }
      //     });
      //   // this.codePush.getCurrentPackage()
      //   //   .then((update) => {
      //   //     if (!update) {
      //   //         console.log("No updates have been installed");
      //   //         return;
      //   //     }

      //   //     this.codePushVersion = update.appVersion
      //   //     if (update.isFirstRun && update.description) {
      //   //         // Display a "what's new?" modal
      //   //     }
      //   //   });

      // }
      this.navigate.setNav(this.nav);
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      if (window['cordova']) {
        this.nav.setRoot('login-page');
      }

      // this.storage.init()
      //   .then(() => {
          // window.location.hash = startingHash
      //     this.splashScreen.hide();
      //   })
      setTimeout(() => {
        // if(!window.location.hash) {
        //   this.store.dispatch(viewDidEnter({
        //     data: null,
        //     name: 'LoginPage'
        //   }));
        // }

        this.nav.viewWillEnter.subscribe((view) => {
          this.store.dispatch(viewDidEnter({
            data: view.data,
            name: view.name
          }));
        });
      }, 10);


    });
  }

  openPage(page) {
    this.menuCtrl.close();
    this.navigate.followLink(page.link);
    // if(this.nav.getActive().id !== page.link) {
      // this.nav.setRoot(page.link);
    // }

    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario

  }

  logout() {
    this.menuCtrl.close();
    this.nav.setRoot('login-page');
    this.store.dispatch(logout())
  }
}
