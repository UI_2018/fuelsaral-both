import { Pipe, PipeTransform } from '@angular/core';

import moment from 'moment';
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({ name: 'timeFromDate' })
export class TimeFromDatePipe implements PipeTransform {
  transform(date: Date): string {

    return moment(date).format('HH:mm');
  }
}
