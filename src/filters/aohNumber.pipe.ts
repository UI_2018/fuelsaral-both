import { Pipe, PipeTransform } from '@angular/core';


function formatNumber(number, options) {
  /** @type {Array.<string>} */
  const result = (options['fraction'] ? number.toFixed(options['fraction']) :
                                      '' + number).split('.');
  return options['prefix'] +
      result[0].replace(/\B(?=(\d{3})+(?!\d))/g, options['grouping']) +
      (result[1] && options.fraction != 0 ? options['decimal'] + result[1] : '');
}

@Pipe({ name: 'aohNumber' })
export class AOHNumber implements PipeTransform {
  transform(number, decimalPlaces) {
    if (!number) {
      return '';
    }
    number = + number
      .toString()
      .replace(/\,/g, ''); // Some numbers might already be formatted as english strings
    return formatNumber(Number(number), {
      'decimal': ',',
      'grouping': '.',
      'fraction': decimalPlaces == null ? decimalPlaces : decimalPlaces,
      'prefix': '',
    });
  }
}
