import { Pipe, PipeTransform } from '@angular/core';

import moment from 'moment';
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({ name: 'dayDiff' })
export class DayDiffPipe implements PipeTransform {
  transform(date, date2) {
    const input = moment(date);
    const date2Moment = moment(date2);
    return input.from(date2Moment, true)

    // const diff = input.diff(date2Moment, 'days');

    // return Math.abs(diff);
  }
}
