import { Pipe, PipeTransform } from '@angular/core';


@Pipe({ name: 'i18nPlural' })
export class TranslatePlural implements PipeTransform {
  transform(key, count = 0) {
    if (count == 0) {
        key += '.none';
    } else if (count == 1) {
        key += '.singular';
    } else if (count > 1) {
        key += '.plural';
    }

    return key;
  }
}
