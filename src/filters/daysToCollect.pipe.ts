import { Pipe, PipeTransform } from '@angular/core';

import moment from 'moment';
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({ name: 'daysToCollect' })
export class DaysToCollectPipe implements PipeTransform {
  transform(date) {
    const input = moment(date);
    const today = moment();

    const diff = input.diff(today, 'days');



    return diff;
  }
}
