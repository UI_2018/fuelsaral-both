import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import moment from 'moment';
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({ name: 'countdown' })
export class CountDownPipe implements PipeTransform {

  constructor(private translateService: TranslateService) {
  }

  transform(value: number): string {

    const oneHour = 1000 * 60 * 60;
    const fiveMins = 1000 * 60 * 5;
    const oneMin = 1000 * 60;
    const duration = moment.duration(value, 'ms');

    if (value <= oneMin) {
      return moment.utc(duration.asMilliseconds()).format('s') + ' ' + this.translateService.instant('COUNT_DOWN.SECONDS');
    } else if (value <= fiveMins) {
      return moment.utc(duration.asMilliseconds()).format('m:ss') + ' ' + this.translateService.instant('COUNT_DOWN.MINUTES');
    } else if (value <= oneHour) {
      return moment.utc(duration.asMilliseconds()).format('m') + ' ' + this.translateService.instant('COUNT_DOWN.MINUTES');
    } else {
      return duration.humanize();
    }

  }
}
