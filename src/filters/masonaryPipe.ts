import { Pipe, PipeTransform } from '@angular/core';
import { range } from 'lodash';

/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({ name: 'masonary' })
export class MasonaryPipe implements PipeTransform {
  transform(list, numColumns = 2) {
    const v = numColumns > 1 ? range(numColumns) : [0];
    return v.reduce((prev, col) => {
        const newItems = list.reduce((prev, item, i) => {
            if (i % numColumns === col) {
                return [...prev, item];
            }
            return prev;
        }, []);
        return [...prev, ...newItems];
    }, []);
  }
}
