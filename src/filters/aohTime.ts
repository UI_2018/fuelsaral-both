import { Pipe, PipeTransform } from '@angular/core';

import moment from 'moment';
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({ name: 'aohTime' })
export class AOHTime implements PipeTransform {
  transform(date) {
    if (!date) {
      return '';
    }
    return moment(date).format('hh:mm');
  }
}
