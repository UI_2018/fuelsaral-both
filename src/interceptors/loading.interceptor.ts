import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { tap } from 'rxjs/operators';

import { Store } from '@ngrx/store';

import { loadingItem, loadingItemComplete } from '../modules/loading.reducer';

@Injectable()
export class LoadingInterceptor implements HttpInterceptor {

  constructor(public store: Store<any>) {
  }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.store.dispatch(loadingItem(request.url));
    console.log(request)
    return next.handle(request).delay(100)
      .pipe(tap((event) => {
        this.store.dispatch(loadingItemComplete(request.url));
        return event;
      }));
      //   if(event instanceof HttpResponse) {
      //     this.store.dispatch(loadingItemComplete(request.url))
      //   }
      // });
  }
}
