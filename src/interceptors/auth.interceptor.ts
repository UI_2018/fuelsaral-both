import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Store } from '@ngrx/store';
import { getResourceUrl } from '../utils/cordova.helpers.js';

import { Environments } from '../config/config';

import qs from 'qs';

const alterUrl = (url, env) => {
  if (url.indexOf('locales') !== -1) {
    return url;
  }
  // return url
  if (env.mock) {
    if (url.indexOf('Connections') !== -1) {
      // return window['cordova'].file.applicationDirectory + 'www/assets/mocks/f1/LOGIN.json'
      return getResourceUrl('/assets/mocks/f1/LOGIN.json');
    } else {
      const parts = url.split('?');
      const query = qs.parse(parts[1]);
      // return window['cordova'].file.applicationDirectory + `www/assets/mocks/f1/${parts[0]}/${query.mode}.json`
      return getResourceUrl(`/assets/mocks/f1/${parts[0]}/${query.mode}.json`);
    }
  } else {
    if (url.indexOf('Connections') !== -1) {
      // return env.api.protocol + env.api.uri + env.api.authUrl + env.api.appId + url;
      // return env.api.protocol + env.api.uri + env.api.authUrl + env.api.appId + url;
      return '/' + env.api.authUrl + env.api.appId + url;
    }
    //  / Read?mode=GETUSER
    const fullUrl = env.api.protocol + env.api.uri + env.api.appId + env.api.service + url;
    console.log(fullUrl)
    return fullUrl
  }
};

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    private config;

    private env;
    constructor(public store: Store<any>) {
    this.store.select('config')
        .subscribe((v) => {
            this.config = v;
            this.env = Environments[this.config.environment];
        });
  }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({
      url: alterUrl(request.url, this.env),
      // method: this.env.mock ? 'GET' : request.method,
      // setHeaders: {
      //   Something: this.config.environment
      // }
    });
    return next.handle(request);
  }
}
