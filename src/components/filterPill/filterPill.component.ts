import { Component, EventEmitter, Input, Output } from '@angular/core';

const template = `
<div (click)="handleDismiss()" class="row align-center">
    <div>{{label}}</div>
    <aoh-icon icon="close"></aoh-icon>
<div>
`;

@Component({
    selector: 'filter-pill',
    template,
    styles: ['./filterPill.scss']
})
export class FilterPill {
    @Input() label;
    @Input() item;

    @Output() onDismiss = new EventEmitter();

    handleDismiss() {
        this.onDismiss.emit(this.item);
    }
}
