import { AfterViewInit, Directive, ElementRef, HostListener, Input } from '@angular/core';

const getWidth = (element) => {
    let style = element.currentStyle || window.getComputedStyle(element),
    width = element.offsetWidth, // or use style.width
    margin = parseFloat(style.marginLeft) + parseFloat(style.marginRight),
    padding = parseFloat(style.paddingLeft) + parseFloat(style.paddingRight),
    border = parseFloat(style.borderLeftWidth) + parseFloat(style.borderRightWidth);

    return width + margin - padding + border;
};

@Directive({
 selector: '[homeContainerResize]'
})
export class HomeContainerResize implements AfterViewInit {

    @Input('homeContainerResize') itemWidth;

    parent;
    element;
    constructor(private el: ElementRef) {

    }

    ngAfterViewInit() {
        this.parent = this.el.nativeElement.parentElement;
        this.element = this.el.nativeElement;

        this.onResize({});
    }
    @HostListener('window:resize', ['$event'])
    onResize(event) {
        const availableWidth = this.parent.clientWidth;
        const firstChild = this.element.firstElementChild;

        const itemWidth = getWidth(firstChild);


        const numItems = Math.floor(availableWidth / itemWidth);
        this.element.style.width = `${numItems * itemWidth}px`;
    }
}
