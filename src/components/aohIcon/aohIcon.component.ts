import { Component, Input } from '@angular/core';


const template = `
<div class="aoh-icon {{icon}}"></div>
`;

@Component({
    template,
    selector: 'aoh-icon',
    styles: ['./aohIcon.component.scss']
})
export class AOHIcon {
    @Input() icon: string;
    constructor() {}
}
