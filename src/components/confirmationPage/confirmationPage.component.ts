import { Component, Input } from '@angular/core';

import { NavController } from 'ionic-angular';

const template = `
<aoh-icon *ngIf="showClose" icon="close" (click)="onClose()"></aoh-icon>
<div class="tick"></div>
<ng-content></ng-content>
`;

@Component({
    selector: 'confirmation-page',
    template,
    styles: []
})
export class ConfirmationPage {
    @Input() pageSubtitle;
    @Input() showClose = true;
    constructor(private navCtrl: NavController) {

    }
    onClose() {
        this.navCtrl.pop();
    }
}
