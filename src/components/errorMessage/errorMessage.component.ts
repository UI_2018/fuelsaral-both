import { Component, Input } from '@angular/core';

const template = `
<div [ngClass]="{error: error}">{{message}}</div>
`;

@Component({
    selector: 'error-message',
    template,
})
export class ErrorMessage {
    @Input() error;
    @Input() message;
}
