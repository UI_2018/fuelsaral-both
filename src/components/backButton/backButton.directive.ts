// An autoresize directive that works with ion-textarea in Ionic 2
// Usage example: <ion-textarea autoresize [(ngModel)]="body"></ion-textarea>
// Based on https://www.npmjs.com/package/angular2-autosize

import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[backButton]' // Attribute selector
})
export class BackButton {


  constructor(public element: ElementRef) {
  }
  ngOnInit(): void {
    this.adjust();
  }
  adjust(): void {
        setTimeout(() => {
            const el = this.element.nativeElement;
            const backButton = el.querySelector('.back-button');
            backButton.classList.add('show-back-button');
            const menuButton = el.querySelector('.bar-button-menutoggle');
            menuButton.classList.add('hidden');
        }, 100);
  }

}
