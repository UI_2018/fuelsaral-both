import { Component, ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import moment from 'moment';
import { range } from 'lodash';

const numberOfWeeks = 6;
@Component({
    selector: 'date-selector',
    templateUrl: './dateSelector.html',
    styles: ['./dateSelector.scss']
})
export class DateSelector {
    @ViewChild(Slides) slides: Slides;
    selectedDate = (moment().subtract('days', 2));

    startDate = (moment().subtract('weeks', 1).set('day', 0));
    viewingMonth = 6;
    viewingYear = 2018;

    dateRows = [];

    dummy = range(20);
    constructor() {
        const firstOfMonth = moment()
            .date(1)
            .month(this.viewingMonth)
            .year(this.viewingYear);

        this.dateRows = range(numberOfWeeks).map(weekNum => {
            return range(7).map((day) => {
                return moment(this.startDate).add('days', (weekNum * 7) + day);
            });
        });
        const today = moment();
        const todayDayOfWeek = today.day();
    }

    get activeMonth() {
        return this.dateRows[2][0].month();
    }
    get activeMonthName() {
        return this.dateRows[2][0].format('MMMM');
    }
    isInActiveMonth(input) {
        return input.month() === this.activeMonth;
    }
    selectDate(date) {
        this.selectedDate = date;
    }

    slideChanged() {
        const currentIndex = this.slides.getActiveIndex();
        console.log('Current index is', currentIndex);
    }


}
