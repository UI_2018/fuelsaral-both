import { Component, Input, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NavigationService } from '../../app/navigation';
// import headerStyle    from './header.component.css';


@Component({
  selector: 'home-tile',
  templateUrl: './homeTile.component.html',
  styles: ['./homeTile.component.scss']
})
export class HomeTile implements OnInit {

  @Input() tile: string;

  constructor(public navCtrl: NavController, public navService: NavigationService) { }

  ngOnInit() {
  }

}
