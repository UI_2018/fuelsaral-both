import { Component, Input } from '@angular/core';

const template = `
<ng-container *ngFor="let step of steps; let i = index">
<div class="step">
<div class="step--details">
    <div class="step--number" [ngClass]="{active: i <= activeStep }"><div class="step--number-inner">{{i + 1}}</div></div>
    <div class="step--title" [ngClass]="{active: i <= activeStep }">{{step.title | translate}}</div>
</div>
</div>
<div class="step--bar" *ngIf="i !== steps.length - 1" [ngClass]="{active: i < activeStep }"></div>


</ng-container>
`;
const steps = [
    { title: 'Stepa' },
    { title: 'Step b' },
    { title: 'Step c' },
];

@Component({
    selector: 'progress-stepper',
    styles: ['./progressStepper.scss'],
    template,
})
export class ProgressStepper {
    @Input('steps') steps;
    @Input('activeStep') activeStep;
}
