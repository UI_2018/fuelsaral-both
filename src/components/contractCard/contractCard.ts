import { Component, Input } from '@angular/core';
import { ModalController, NavController } from 'ionic-angular';

import { ShareModal } from '../../modals/share/share.component';

const template = `
<div class="row align-center">
    <div class="flex-1">
        <div class="contract-header">{{contract.MATERIAL_DESCRIPTION}}</div>
        <div class="contract-location">
            <aoh-icon icon="location"></aoh-icon>
            <span>{{contract.PLANT_DESCRIPTION}}</span>
        </div>
    </div>
    <div>
        <badge *ngIf="contract.isOpen" [label]="'CONTRACT_ITEM.BADGE_OPEN' | translate"></badge>
        <badge *ngIf="!contract.isOpen" [label]="'CONTRACT_ITEM.BADGE_CLOSED' | translate"></badge>
    </div>
</div>

<div class="contract-details">
    <details-item label="CONTRACT_ITEM.CONTRACT_ID" value="{{contract.CONTRACT_NUMBER}}"></details-item>
    <details-item label="CONTRACT_ITEM.CONTRACT_VOLUME" value="{{contract.TARGET_QUANTITY | aohNumber}} {{contract.UOM_EXT}}"></details-item>
    <details-item label="CONTRACT_ITEM.OPEN_VOLUME" value="{{contract.OPEN_QUANTITY | aohNumber}} {{contract.UOM_EXT}}"></details-item>
</div>

<lifting-bar [value]="contract.LIFTED_PERC"></lifting-bar>

<div class="btn-footer">
    <button [disabled]="!contract.id" class="btn-primary filled" (click)="openDetails()">{{'CONTRACT_ITEM.VIEW_DETAILS' | translate}}</button>
    <button class="btn-primary filled" (click)="openShare()">{{'CONTRACT_ITEM.SHARE' | translate}}</button>
</div>
`;

@Component({
    selector: 'contract-card',
    template,
})export class ContractCard {
    @Input('contract') contract;
    constructor(private navCtrl: NavController, private modalCtrl: ModalController) {

    }
    openDetails(contract) {
        this.navCtrl.push('contract-details', this.contract);
    }

    openShare() {
        this.modalCtrl.create(ShareModal, { contracts: [this.contract] }, { cssClass: 'modal-full' }).present();
    }
}
