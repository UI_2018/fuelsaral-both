import { AfterViewInit, Directive, ElementRef, HostListener, Input } from '@angular/core';
import { throttle } from 'lodash';
import { Store } from '@ngrx/store';
import { setWidth } from '../../modules/device/device.reducer';


@Directive({
 selector: '[device]',
})
export class DeviceDirective implements AfterViewInit {

    @Input('device') itemWidth;

    currentClass;
    numberOfColumns = 1;

    parent;
    element;
    handleResize;
    constructor(private el: ElementRef, private store: Store<any>) {
    }

    ngAfterViewInit() {
        this.parent = this.el.nativeElement.parentElement;
        this.element = this.el.nativeElement;


        this.handleResize = throttle((value) => {
            this.store.dispatch(setWidth(value));
        }, 1000);
        this.onResize({});

    }
    @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (!this.parent) {
            return;
        }
        const availableWidth = this.parent.clientWidth;
        if (this.handleResize) {
            this.handleResize(availableWidth);
        }
        // alert(availableWidth)
        return;
        //
        // let numItems = Math.floor(availableWidth / this.itemWidth);
        // numItems = numItems <= 4 ? numItems : 4;
        // numItems = numItems <= 0 ? 1 : numItems;
        // this.numberOfColumns = numItems;
        // const newClassName = `columns-${numItems}`;
        // if (this.currentClass !== newClassName) {
        //     if (this.currentClass) {
        //         this.element.classList.remove(this.currentClass);
        //     }
        //     this.element.classList.add(newClassName);
        //     this.currentClass = newClassName;
        // }
    }
}
