import { AfterViewInit, Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[formFocus]'
})
export class FormFocusDirective implements AfterViewInit {

  constructor(private el: ElementRef) {
  }

  ngAfterViewInit() {
      setTimeout(() => {
        if (this.el.nativeElement.children[0]) {
          this.el.nativeElement.children[0].focus();
        } else {
          this.el.nativeElement.focus();
        }
      }, 530);

  }
}
