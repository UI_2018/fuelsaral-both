import { Component } from '@angular/core';
import { Store } from '@ngrx/store';


import { getMenuOptions } from '../../modules/user.selectors';
import { NavigationService } from '../../app/navigation';

@Component({
    selector: 'icon-sidebar',
    templateUrl: './iconSidebar.component.html',
    styles: ['./iconSidebar.component.scss']
})
export class IconSidebar {
    navigation$;
    constructor(private store: Store<any>, private navService: NavigationService) {
        this.navigation$ = this.store.select(getMenuOptions);
    }
    openPage(p) {
        this.navService.followLink(p.link);
    }
}
