import { Component, Input } from '@angular/core';
import { ModalController } from 'ionic-angular';

import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';

import { LoadingService } from '../../modules/loading.provider';
import { NavigationService } from '../../app/navigation';

import { MakeAClaimModal } from '../../modals/makeAClaim/makeAClaim.component';

@Component({
    selector: 'lifting-card',
    templateUrl: './liftingCard.component.html',
})
export class LiftingCard {
    @Input('lifting') lifting;

    constructor(private modalCtrl: ModalController,
                private transfer: FileTransfer,
                private file: File,
                private fileOpener: FileOpener,
                private navService: NavigationService,
                private loadingService: LoadingService
    ) {}

    openMakeAClaim() {
        this.modalCtrl.create(MakeAClaimModal, null, {
            cssClass: 'modal-full'
        }).present();
    }

    openInvoice() {
        this.navService.openFile('/assets/pdf/invoice.pdf');
        // if(!window['cordova']) {
        //     this.loadingService.showLoader()

        //     setTimeout(()=> {
        //         this.loadingService.hideLoader()
        //         alert('Only supported on device')
        //     }, 1000)
        // } else {
        //     this.loadingService.showLoader()
        //     const url = 'http://www.pdf995.com/samples/pdf.pdf';
        //     const fileTransfer: FileTransferObject = this.transfer.create();
        //     fileTransfer.download(url, this.file.dataDirectory + 'file.pdf').then((entry) => {
        //       console.log('download complete: ' + entry.toURL());
        //       this.loadingService.hideLoader()
        //       this.fileOpener.open(entry.toURL(), 'application/pdf')
        //         .then(() => console.log('File is opened'))
        //         .catch(e => console.log('Error opening file', e));
        //     }, (error) => {
        //         this.loadingService.hideLoader()
        //       // handle error
        //     });
        // }

    }
}
