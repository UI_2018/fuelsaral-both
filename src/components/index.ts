import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeTile } from './homeTile/homeTile.component';
import { AOHIcon } from './aohIcon/aohIcon.component';
import { ErrorMessage } from './errorMessage/errorMessage.component';
import { FormFocusDirective } from './formFocus/formFocus.directive';
import { Autoresize } from './autoResize/autoResize.directive';
import { FilterPill } from './filterPill/filterPill.component';
import { DetailsBox } from './detailsBox/detailsBox';
import { DetailsItem } from './detailsItem/detailsItem';
import { DismissPageHeader } from './dismissPageHeader/dismissPageHeader';
import { CardSection } from './cardSection/cardSection';
import { ContractCard } from './contractCard/contractCard';
import { Badge } from './badge/badge';
import { LiftingBar } from './liftingBar/liftingBar';
import { TabBar } from './tabBar/tabBar';
import { HomeContainerResize } from './homeContainerResize/homeContainerResize';
import { MasonaryContainer } from './masonaryContainer/masonaryContainer';
import { DeviceDirective } from './device/deviceDirective';
import { PricingCountDown } from './pricingCountDown/pricingCountDown.component';
import { LiftingCard } from './liftingCard/liftingCard.component';
import { SelectionList } from './selectionList/selectionList.component';
import { ConfirmationPage } from './confirmationPage/confirmationPage.component';
import { IconSidebar } from './iconSidebar/iconSidebar.component';
import { MasonaryPipe } from '../filters/masonaryPipe';
import { SearchDetails } from './searchDetails/searchDetails';
import { LoadingIndicator } from './loadingIndicator/loadingIndicator';
import { BackButton } from './backButton/backButton.directive';
import { NotificationIcon } from './notificationIcon/notificationIcon';
import { ProgressStepper } from './progressStepper/progressStepper';

import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from 'ionic-angular';
0;

import { CountDownPipe } from '../filters/countDown.pipe';
import { TimeFromDatePipe } from '../filters/timeFromDate.pips';
import { DaysToCollectPipe } from '../filters/daysToCollect.pipe';
import { DayDiffPipe } from '../filters/dayDiff';
import { AOHDate } from '../filters/aohDate.pipe';
import { AOHTime } from '../filters/aohTime';
import { AOHNumber } from '../filters/aohNumber.pipe';
import { TranslatePlural } from '../filters/i18nPlural';
@NgModule({
  imports: [TranslateModule, CommonModule, IonicModule],
  declarations: [
    HomeTile,
    AOHIcon,
    CountDownPipe,
    TimeFromDatePipe,
    ErrorMessage,
    FormFocusDirective,
    Autoresize,
    FilterPill,
    DetailsBox,
    DetailsItem,
    DismissPageHeader,
    CardSection,
    ContractCard,
    Badge,
    LiftingBar,
    TabBar,
    HomeContainerResize,
    DaysToCollectPipe,
    PricingCountDown,
    DayDiffPipe,
    AOHDate,
    LiftingCard,
    SelectionList,
    ConfirmationPage,
    SearchDetails,
    LoadingIndicator,
    IconSidebar,
    MasonaryPipe,
    MasonaryContainer,
    DeviceDirective,
    BackButton,
    AOHTime,
    AOHNumber,
    TranslatePlural,
    NotificationIcon,
    ProgressStepper,
  ],
  exports: [
    HomeTile,
    AOHIcon,
    TranslateModule,
    CountDownPipe,
    TimeFromDatePipe,
    ErrorMessage,
    FormFocusDirective,
    Autoresize,
    FilterPill,
    DetailsBox,
    DetailsItem,
    DismissPageHeader,
    CardSection,
    ContractCard,
    Badge,
    LiftingBar,
    TabBar,
    HomeContainerResize,
    DaysToCollectPipe,
    PricingCountDown,
    DayDiffPipe,
    AOHDate,
    LiftingCard,
    SelectionList,
    ConfirmationPage,
    IconSidebar,
    MasonaryPipe,
    SearchDetails,
    LoadingIndicator,
    MasonaryContainer,
    DeviceDirective,
    BackButton,
    AOHTime,
    AOHNumber,
    TranslatePlural,
    NotificationIcon,
    ProgressStepper,
  ]

})
export class ComponentModule {}
