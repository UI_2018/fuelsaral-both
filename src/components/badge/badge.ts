import { Component, Input } from '@angular/core';

const template = `
<div>{{label}}</div>
`;

@Component({
    selector: 'badge',
    template,
    styles: ['./badge.scss']
})
export class Badge {
    @Input() label;
}
