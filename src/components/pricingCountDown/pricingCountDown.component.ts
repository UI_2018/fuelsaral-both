import { Component, EventEmitter, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import moment from 'moment';

import { aohTimer, creditPosition } from '../../pages/aoh/aoh.selectors';

@Component({
    selector: 'pricing-count-down',
    templateUrl: './pricingCountDown.component.html',
    styles: ['./pricingCountDown.scss']
})
export class PricingCountDown {
    interval;
    private timeRemaining = 100;
    private periodEndTime: Date;

    creditPosition$;

    remainingListener;

    @Output() onSizeChange = new EventEmitter();

    @Output() onCountdownComplete = new EventEmitter();


    private isOpen: Boolean = false;

    private hasEmittedComplete = false;
    constructor(private store: Store<any>) {
        this.remainingListener = this.store.select(aohTimer)
            .subscribe((timer) => {
                if (timer) {

                  const date = moment(
                    new Date(timer.EV_PERIOD_END_DATE).getTime() +
                    new Date(timer.EV_PERIOD_END_TIME).getTime()
                  );

                  this.hasEmittedComplete = false;
                    this.setTimeRemaining(
                        date.toDate(),
                        moment().toDate()
                    );
                }

            });
        this.creditPosition$ = this.store.select(creditPosition);
    }

    setTimeRemaining(endTime, systemTime) {
        this.periodEndTime = endTime;
        const msRemaining = endTime.getTime() - systemTime.getTime();
        this.timeRemaining = msRemaining;
    }
    handleInterval() {
        // this.timeRemaining
        this.timeRemaining = this.timeRemaining - 1000;
        // When we reach zero stop the countdown
        if (this.timeRemaining <= 0) {
            if (!this.hasEmittedComplete) {
                this.hasEmittedComplete = true;
                this.onCountdownComplete.emit();
            }

            // this.interval.unsubscribe()
            this.timeRemaining = 0;
        } else {
            this.hasEmittedComplete = false;
        }
    }

    toggleOpen() {
        this.isOpen = !this.isOpen;
        if (this.onSizeChange) {
            this.onSizeChange.emit();
        }
    }
    ngOnInit() {
        this.interval = Observable.interval(1000)
            .subscribe(this.handleInterval.bind(this));
    }
    ngOnDestroy() {
        if (this.interval) {
            clearInterval(this.interval);
        }
        this.remainingListener.unsubscribe();
    }

    onResize($event) {
        this.onSizeChange.emit();
    }
}
