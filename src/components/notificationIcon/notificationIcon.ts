import { Component, Input } from '@angular/core';

import { Store } from '@ngrx/store';

import { notificationCount } from '../../pages/notifications/notifications.selectors';

const template = `
<div class="notification-icon {{size}}">
<aoh-icon [icon]="icon"></aoh-icon>
<div class="count" *ngIf="count$ | async">{{count$ | async}}</div>
</div>
`;

@Component({
    selector: 'notification-icon',
    template,
    styles: ['./notification.scss']
})
export class NotificationIcon {
    @Input('size') size;

    count$;

    constructor(private store: Store<any>) {
        this.count$ = this.store.select(notificationCount);
    }

    get icon() {
        return this.size == 'lg' ? 'notification home-tile' : 'notification';
    }

}
