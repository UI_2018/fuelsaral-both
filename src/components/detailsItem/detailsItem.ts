import { Component, Input } from '@angular/core';

const template = `
<div class="details-label {{labelClass}}">{{label | translate}}</div>
<div class="details-value">{{value}}<div>
`;

@Component({
    selector: 'details-item',
    template,
    styles: ['./detailsItem.scss']
})
export class DetailsItem {
    @Input() label;

    @Input() labelClass;
    @Input() value;
}
