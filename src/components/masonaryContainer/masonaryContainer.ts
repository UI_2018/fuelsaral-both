import { AfterViewInit, Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
 selector: '[masonaryContainer]',
})
export class MasonaryContainer implements AfterViewInit {

    @Input('masonaryContainer') itemWidth;

    currentClass;
    numberOfColumns = 1;

    parent;
    element;
    constructor(private el: ElementRef) {
    }

    ngAfterViewInit() {
        this.parent = this.el.nativeElement.parentElement;
        this.element = this.el.nativeElement;

        this.onResize({});
    }
    @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (!this.parent) {
            return;
        }
        const availableWidth = this.parent.clientWidth;
        let numItems = Math.floor(availableWidth / this.itemWidth);
        numItems = numItems <= 4 ? numItems : 4;
        numItems = numItems <= 0 ? 1 : numItems;
        this.numberOfColumns = numItems;
        const newClassName = `columns-${numItems}`;
        if (this.currentClass !== newClassName) {
            if (this.currentClass) {
                this.element.classList.remove(this.currentClass);
            }
            this.element.classList.add(newClassName);
            this.currentClass = newClassName;
        }
    }
}
