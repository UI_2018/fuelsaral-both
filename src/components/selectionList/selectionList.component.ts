import { Component, Input, Renderer2, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { AlertController } from 'ionic-angular';

export const EPANDED_TEXTAREA_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => SelectionList),
  multi: true,
};

@Component({
  selector: 'selection-list',
  providers: [EPANDED_TEXTAREA_VALUE_ACCESSOR],
  templateUrl: './selectionList.component.html',
  styles: ['./selectionList.component.scss']
})
export class SelectionList implements ControlValueAccessor {
  @Input() options;
  @Input() label;
  innerValue = [];

  selectedKeys = {};
  onChange;
  onTouched;

  writeValue(value: any): void {
    this.innerValue = value;
    if (value) {
      this.setSelectedKeys();
    }
  }

  setSelectedKeys() {
    this.selectedKeys = this.innerValue.reduce((prev, next) => {
      prev[next.title] = true;
      return prev;
    }, {});
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    // const div = this.textarea.nativeElement;
    // const action = isDisabled ? 'addClass' : 'removeClass';
    // this.renderer[action](div, 'disabled');
  }

  constructor(private renderer: Renderer2, private alertCtrl: AlertController) {
  }

  onFieldChange($event) {
    this.setFieldValue($event.target.value);
  }

  onCheck($event, option, checked) {
    const newState = $event.value;

    // Make sure that the checkbox has actually changed state
    if (newState === this.isChecked(option)) {
      return;
    }
    if (newState) {
      this.setFieldValue([
        ...this.innerValue,
        option,
      ]);
    } else {
      this.setFieldValue(
        this.innerValue.filter((i) => {
          return i.title !== option.title;
        })
      );
    }
  }

  setFieldValue(value) {
    this.innerValue = value;
    this.setSelectedKeys();
    this.onChange(value);
    this.onTouched(value);
  }

  isChecked(option) {
    if (!this.innerValue) {
      return false;
    }
    return this.selectedKeys[option.title] || false;
  }
}

// <textarea-expanded [formControl]="control"></textarea-expanded>
