import { Component, EventEmitter, Input, Output  } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';


import { select } from '../../utils/storeUtils';
import { Store } from '@ngrx/store';

const template = `
<details-box [footerActions]="footerActions">
    <details-item *ngFor="let field of fieldsToShow" [label]="field.label" [value]="getValue(field)"></details-item>
</details-box>
`;

// import { fields } from '../../pages/search/searchConfig'
const values = {
    'contractNo': '',
    'customerReference': '',
    'contractType': 'any',
    'contractStatus': 'any',
    'terminal': 'any',
    'date': {
        'startDate': '2018-07-23',
        'endDate': '2018-07-23'
    }
};

@Component({
    selector: 'search-details',
    template,
    styles: []
})
export class SearchDetails {
    @Input('values') values;
    @Input('fields') fields;
    footerActions = [{
        label: 'CONTRACTS_SEARCH.CHANGE',
        handler: () => {
            this.onChange.emit();
        }
    }];
    @Output() onChange = new EventEmitter();

    fieldsToShow;
    constructor(private store: Store<any>, private translateService: TranslateService) {
        this.setFieldsToShow();
    }
    ngOnChanges(changes) {
        if (changes.values) {
            this.setFieldsToShow();
        }
        // changes.prop contains the old and the new value...
    }
    setFieldsToShow() {
        if (!this.values) {
            return;
        }
        this.fieldsToShow = this.fields.filter((field) => {
            const value = this.values[field.name];
            return value && value !== 'any';
        });
    }

    getValue(field) {
        const value = this.values[field.name];
        if (field.type === 'select' || field.type === 'select-date') {
            const options = select(this.store, field.options);
            const selectedValue = options.find((option) => {
                return option.value === value;
            });
            if(selectedValue && selectedValue.title) {
                return field.translate ? this.translateService.instant(selectedValue.title) : selectedValue.title
            }
            return field.displayFn ? field.displayFn(value) : '';
        }
        return value;
    }
}
