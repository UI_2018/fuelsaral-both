import { Component } from '@angular/core';
import moment from 'moment';
import { range } from 'lodash';
import Swiper from 'swiper';
import { Subject } from 'rxjs';


const numberOfWeeks = 1000;
@Component({
    selector: 'date-selector',
    templateUrl: './dateSelector.html',
    styles: ['./dateSelector.scss', './swiper.scss']
})
export class DateSelector {

    selectedDate = (moment().subtract('days', 2));

    startDate = (moment().subtract('weeks', 200).set('day', 0));
    viewingMonth = 6;
    viewingYear = 2018;

    dateRows = [];
    virtualDateRows = {};

    virtualDateRows$ =  new Subject();

    currentSlide = numberOfWeeks / 2;

    swiper;
    constructor() {

    }
    ngOnInit() {
        console.log(Swiper);
        const self = this;
        this.dateRows = this.getMonthDateRows(5, 2018);
        // setTimeout(() => {
            // this.swiper = new Swiper ('.swiper-container', {
            //     // Optional parameters
            //     direction: 'vertical',
            //     loop: false,
            //     initialSlide: this.currentSlide,
            //     slidesPerView: 6,
            //     freeMode: true,
            //     virtual: {
            //         slides: this.dateRows,
            //         renderExternal(data) {
            //             self.virtualDateRows$.next(data)
            //             // assign virtual slides data
            //             // this.virtualDateRows = data;
            //           },
            //     }

            //   })
        // }, 1000)
    }

    getMonthDateRows(month, year) {
        const firstOfMonth = moment().month(month).year(year).date(1);
        const numberDaysInMonth = firstOfMonth.daysInMonth();
        const startDay = firstOfMonth.day();

        const numWeeksToShow = Math.floor(((numberDaysInMonth + startDay) / 7)) + 1;

        const dateRows = range(numWeeksToShow)
            .map((weekNum) => {
                return range(7).map((day) => {
                    const dayDiff = (day - startDay) + weekNum * 7;
                    if (dayDiff >= 0 && dayDiff < numberDaysInMonth) {
                        return moment(firstOfMonth).add('days', (day - startDay) + weekNum * 7);
                    }
                    return null;
                });

            });

        return dateRows;
    }

    selectDate(date) {
        this.selectedDate = date;
    }

    isSameDate(date) {
        if (!date) {
            return false;
        }
        return date.isSame(this.selectedDate, 'day');
    }
}
