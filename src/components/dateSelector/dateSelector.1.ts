import { Component, ElementRef, ViewChild } from '@angular/core';
import moment from 'moment';
import { range } from 'lodash';
import Swiper from 'swiper';
import { Subject } from 'rxjs';


const numberOfWeeks = 1000;
@Component({
    selector: 'date-selector',
    templateUrl: './dateSelector.html',
    styles: ['./dateSelector.scss', './swiper.scss']
})
export class DateSelector {
    @ViewChild('slideContainer') slideContainer: ElementRef;

    selectedDate = (moment().subtract('days', 2));

    startDate = (moment().subtract('weeks', 200).set('day', 0));
    viewingMonth = 6;
    viewingYear = 2018;

    dateRows = [];
    virtualDateRows = {};

    virtualDateRows$ =  new Subject();

    currentSlide = numberOfWeeks / 2;

    swiper;
    constructor() {

        const firstOfMonth = moment()
            .date(1)
            .month(this.viewingMonth)
            .year(this.viewingYear);

        this.virtualDateRows$.subscribe(v => {
            this.virtualDateRows = v;
        });


        this.dateRows = range(numberOfWeeks).map(weekNum => {
            return range(7).map((day) => {
                return moment(this.startDate).add('days', (weekNum * 7) + day);
            });
        });

    }
    ngOnInit() {
        console.log(Swiper);
        const self = this;
        // setTimeout(() => {
        this.swiper = new Swiper ('.swiper-container', {
                // Optional parameters
                direction: 'vertical',
                loop: false,
                initialSlide: this.currentSlide,
                slidesPerView: 6,
                freeMode: true,
                virtual: {
                    slides: this.dateRows,
                    renderExternal(data) {
                        self.virtualDateRows$.next(data);
                        // assign virtual slides data
                        // this.virtualDateRows = data;
                      },
                }

              });
        // }, 1000)
    }

    updateRows() {
        const currentIndex = this.swiper.activeIndex;

        if (currentIndex > this.currentSlide) {
            const needToAppend = currentIndex + 8 > this.swiper.slides.length;
            if (needToAppend) {
                this.swiper.virtual.appendSlide('a');
            }

            console.log('forwards');
        }
        console.log(currentIndex);

        this.currentSlide = currentIndex;
    }

    prependIfRequired() {
        const currentIndex = this.swiper.snapIndex;
        const numSlides = this.swiper.virtual.slides.length;
        if (currentIndex === 1) {
            this.swiper.virtual.prependSlide('a');
        }
    }

    get activeMonth() {
        const activeIndex = this.swiper.activeIndex;
        return this.dateRows[activeIndex + 3][0].month();
    }
    get activeMonthName() {
        const activeIndex = this.swiper.activeIndex;
        return this.dateRows[activeIndex + 3][0].format('MMMM');
    }
    isInActiveMonth(input) {
        return input.month() === this.activeMonth;
    }

    selectDate(date) {
        this.selectedDate = date;
    }
}
