import { Component, EventEmitter, Input, Output } from '@angular/core';

const template = `
<ion-segment [(ngModel)]="selected" (ionChange)="segmentChanged($event)">
    <ion-segment-button *ngFor="let option of options" [value]="option.value">
        {{option.title | translate}}
    </ion-segment-button>
</ion-segment>
`;

@Component({
    selector: 'tab-bar',
    template,
    styles: ['./tabBar.scss']
})
export class TabBar {
    @Output('onChange') onChange = new EventEmitter();

    @Input('options') options = [];
    @Input('value') value;
    selected = true;
    ngOnInit() {
        if (this.value) {
            this.selected = this.value;
        }
    }
    ngOnChanges(changes) {
        if(changes.value) {
            if(changes.value.currentValue !== this.selected) {
                this.selected = changes.value.currentValue
            }
        }
    }
    segmentChanged() {
        this.onChange.emit(this.selected);
    }
}
