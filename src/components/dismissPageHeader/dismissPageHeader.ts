import { Component, Input } from '@angular/core';

import { NavController } from 'ionic-angular';

const template = `
<aoh-icon icon="close" (click)="onClose()"></aoh-icon>
<div>
    <div class="page-title">{{pageTitle | translate}}</div>
    <div class="page-subtitle">{{pageSubtitle}}</div>
</div>
`;

@Component({
    selector: 'dismiss-page-header',
    template,
    styles: ['./dismissPageHeader.scss']
})
export class DismissPageHeader {
    @Input() pageTitle;
    @Input() pageSubtitle;

    constructor(private navCtrl: NavController) {

    }
    onClose() {
        this.navCtrl.pop();
    }
}
