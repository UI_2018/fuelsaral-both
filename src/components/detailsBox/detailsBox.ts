import { Component, Input } from '@angular/core';

const template = `
<ng-content></ng-content>
<div class="row flex-end">
    <button class="btn btn-secondary" *ngFor="let item of footerActions" (click)="item.handler($event, item)">{{ item.label | translate}}</button>
</div>
`;

@Component({
    selector: 'details-box',
    template,
    styles: ['./detailsBox.scss']
})
export class DetailsBox {
    @Input('footerActions') footerActions;
}
