import { Component, Input } from '@angular/core';

const template = `
<div class="row align-center">
<div class="flex-1">
<div class="bar" [ngStyle]="{width: percentage}" [ngClass]="{tooLarge: this.isTooLarge}"></div>
</div>
<div class="percentage" [ngClass]="{tooLarge: this.isTooLarge}">{{value | aohNumber: 0}} %</div>
</div>
`;

@Component({
    selector: 'lifting-bar',
    template,
})
export class LiftingBar {
    @Input() value;

    get percentage(): string {
        return this.value <= 100 ? (this.value) + '%' : '100%';
    }

    get isTooLarge() {
        return this.value >= 100;
    }


}
