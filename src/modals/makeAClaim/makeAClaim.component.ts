import { Component } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';

import { ModalController, ViewController } from 'ionic-angular';

import { MakeAClaimConfirmationModal } from '../makeAClaimConfirmation/makeAClaimConfirmation.component';

import { Store } from '@ngrx/store';

const claimReasons = [
    { title: 'MAKE_A_CLAIM.REASON_1', value: 1 },
    { title: 'MAKE_A_CLAIM.REASON_2', value: 2 },
    { title: 'MAKE_A_CLAIM.REASON_3', value: 3 },
    { title: 'MAKE_A_CLAIM.REASON_4', value: 4 },
];

@Component({
    selector: 'make-a-claim',
    templateUrl: './makeAClaim.component.html',
    styles: ['./makeAClaim.component.scss']
})
export class MakeAClaimModal {

    form;
    claimReasons = claimReasons;

    activeStep = 0;
    constructor(
        public viewCtrl: ViewController,
        public store: Store<any>,
        private fb: FormBuilder,

        private modalCtrl: ModalController,
    ) {
        this.form = this.fb.group({
            claimReason: new FormControl([], null),
            justification: new FormControl(null, null),
            email: new FormControl(null, null),
            phone: new FormControl(null, null)
        });
    }
    dismiss() {
        this.viewCtrl.dismiss();
    }


    get hasNextStep(): boolean {
        return this.activeStep <= 1;
    }

    get hasPreviousStep(): boolean {
        return this.activeStep > 0;
    }

    get formIsValid() {
        const value = this.form.value;
        if (this.activeStep === 0) {
            return value.claimReason.length;
        }
        if (this.activeStep === 1) {
            return value.justification;
        }
        if (this.activeStep === 2) {
            return value.email && value.phone;
        }
    }


    onNextStep() {
        this.activeStep ++;
    }

    onPreviousStep() {
        this.activeStep --;
    }

    onSubmit() {
        this.viewCtrl.dismiss();
        this.modalCtrl.create(MakeAClaimConfirmationModal).present();
    }
}
