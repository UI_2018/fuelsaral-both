import { Component, ViewChild } from '@angular/core';
import { DateTime, ViewController } from 'ionic-angular';
import moment from 'moment';

import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormControl }   from '@angular/forms';



@Component({
    selector: 'date-range-selector',
    templateUrl: './dateRangeSelector.html',
    styles: ['./dateRangeSelector.scss']
})
export class DateRangeSelector {
    @ViewChild('startDate') startDateInput: DateTime;
    selection;

    pickerOptions = {
        buttons: [{
          text: this.translateService.instant('CLEAR'),
          handler: () => {
            //   this.onClick()

          }
        }]
      };

    // we may use Angular's date pipe instead
    dayNames = moment.weekdays();
    dayShortNames = moment.weekdaysShort();
    monthNames = moment.months();
    monthShortNames = moment.monthsShort();

    constructor(private translateService: TranslateService, private viewCtrl: ViewController, private fb: FormBuilder) {
        const today = moment().format('YYYY-MM-DD');
        this.selection = this.fb.group({
            startDate: new FormControl(today, null),
            endDate: new FormControl(today, null)
        });
    }
    ngOnInit() {
        setTimeout(() => {
            this.startDateInput.open();
        }, 20);
    }

    submit() {
        this.viewCtrl.dismiss(this.selection.value);
    }
}
