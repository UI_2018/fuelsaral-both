import { Component } from '@angular/core';

import { ViewController } from 'ionic-angular';

import { Store } from '@ngrx/store';

import { ConfigService } from '../../config/configProvider';
import { setEnvironment } from '../../modules/config.reducer';

@Component({
    templateUrl: './environmentModal.component.html',
})
export class EnvironmentModal {
    private selectedEnv: string;
    constructor(
        public viewCtrl: ViewController,
        public configService: ConfigService,
        public store: Store<any>,
    ) {
        this.store.select('config', 'environment')
            .subscribe(v => this.selectedEnv = v);
    }
    dismiss() {
        this.store.dispatch(new setEnvironment(this.selectedEnv));
        this.viewCtrl.dismiss();
    }
}
