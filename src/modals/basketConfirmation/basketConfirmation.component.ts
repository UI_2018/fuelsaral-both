import { Component } from '@angular/core';

import { NavParams, ViewController } from 'ionic-angular';


@Component({
    selector: 'basketConfirmation-confirmation',
    templateUrl: './basketConfirmation.component.html',
    styles: ['./basketConfirmation.component.scss']
})
export class BasketConfirmationModal {
    private email: string;
    constructor(
        public viewCtrl: ViewController,
        private navParams: NavParams
    ) {
        this.email = navParams.get('email');
    }
    dismiss() {
        this.viewCtrl.dismiss();
    }
}
