import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';


import { EnvironmentModal } from './environment/environmentModal.component';
import { ShareModal } from './share/share.component';
import { ShareConfirmationModal } from './shareConfirmation/shareConfirmation.component';
import { MakeAClaimModal } from './makeAClaim/makeAClaim.component';
import { MakeAClaimConfirmationModal } from './makeAClaimConfirmation/makeAClaimConfirmation.component';
import { BasketConfirmationModal } from './basketConfirmation/basketConfirmation.component';

import { DateRangeSelector } from './dateRangeSelector/dateRangeSelector';

import { ComponentModule } from '../components/';


@NgModule({
    imports: [
        IonicPageModule.forChild(EnvironmentModal),
        IonicPageModule.forChild(DateRangeSelector),
        IonicPageModule.forChild(ShareModal),
        IonicPageModule.forChild(ShareConfirmationModal),
        IonicPageModule.forChild(MakeAClaimModal),
        IonicPageModule.forChild(MakeAClaimConfirmationModal),
        IonicPageModule.forChild(BasketConfirmationModal),
        ComponentModule,
    ],
    declarations: [
        EnvironmentModal,
        DateRangeSelector,
        ShareModal,
        ShareConfirmationModal,
        MakeAClaimModal,
        MakeAClaimConfirmationModal,
        BasketConfirmationModal,
    ],
    entryComponents: [
        EnvironmentModal,
        DateRangeSelector,
        ShareModal,
        ShareConfirmationModal,
        MakeAClaimModal,
        MakeAClaimConfirmationModal,
        BasketConfirmationModal,
    ],

})
export class ModalModule {}
