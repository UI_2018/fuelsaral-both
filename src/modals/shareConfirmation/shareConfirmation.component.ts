import { Component } from '@angular/core';

import { NavParams, ViewController } from 'ionic-angular';

@Component({
    selector: 'share-confirmation',
    templateUrl: './shareConfirmation.component.html',
    styles: ['./shareConfirmation.component.scss']
})
export class ShareConfirmationModal {
    private email: string;
    constructor(
        public viewCtrl: ViewController,
        private navParams: NavParams
    ) {
        this.email = navParams.get('email');
    }
    dismiss() {
        this.viewCtrl.dismiss();
    }
}
