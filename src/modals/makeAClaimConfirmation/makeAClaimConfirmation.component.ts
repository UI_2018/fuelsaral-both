import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import { ViewController } from 'ionic-angular';

import { Store } from '@ngrx/store';

@Component({
    selector: 'make-a-claim-confirmation',
    templateUrl: './makeAClaimConfirmation.component.html',
})
export class MakeAClaimConfirmationModal {
    constructor(
        public viewCtrl: ViewController,
        public store: Store<any>,
        private fb: FormBuilder,
    ) {
    }
    dismiss() {
        this.viewCtrl.dismiss();
    }
}
