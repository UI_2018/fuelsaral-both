import { Component } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';

import { ModalController, NavParams, ViewController } from 'ionic-angular';
import { ShareConfirmationModal } from '../shareConfirmation/shareConfirmation.component';

import { Store } from '@ngrx/store';

const dummyContracts = [
    { title: 'Some contract', value: 'value1' },
    { title: 'Some contract 2', value: 'value2' }
];

const validateEmail = (control: FormControl) => {
    const email = control.value;
    if (!email) {
        return { email: true };
    }
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const isValid = re.test(control.value.toLowerCase());
    return isValid ? null : { email: true };
};

const atLeastOneSelected = (control: FormControl) => {
    return control.value.length ? null : { noItemSelected: true };
};

@Component({
    selector: 'share',
    templateUrl: './share.component.html',
    styles: ['./share.component.scss']
})
export class ShareModal {
    form;
    contracts = dummyContracts;
    constructor(
        public viewCtrl: ViewController,
        public store: Store<any>,
        private fb: FormBuilder,
        private modalCtrl: ModalController,
        private navParams: NavParams,
    ) {
        const contracts = navParams.get('contracts');

        this.contracts = contracts.map((contract) => {
            return {
                ...contract,
                title: contract.MATERIAL_DESCRIPTION,
                value: contract.MATERIAL_DESCRIPTION, // TODO: correct value
            };
        });

        this.form = this.form = this.fb.group({
            email: new FormControl(null, [validateEmail]),
            contracts: new FormControl([], [atLeastOneSelected])
        }, {});

        if (this.contracts.length === 1) {
            this.form.patchValue({
                contracts: [...this.contracts]
            });
        }

    }
    share() {
        this.viewCtrl.dismiss();
        this.modalCtrl.create(ShareConfirmationModal, this.form.value).present();
    }
}
