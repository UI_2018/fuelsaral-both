import { Injectable } from '@angular/core';

import { Environments } from './config';

interface Api {
    protocol: string;
    uri: string;
    appId: string;
    service: string;
    authUrl: string;

}
interface Environment {
    api: Api;
}

@Injectable()
export class ConfigService {
    env: Environment;
    envName: string;
    envs: string[] = Object.keys(Environments);

    constructor() {
        this.setEnv('test_internal');
    }

    setEnv(env: string) {
        if (!Environments[env]) {
            console.error('INVALID ENVIRONMENT');
        }
        this.envName = env;
        this.env = Environments[env];
    }

    get isMock(): boolean {
        return false;
    }

}
