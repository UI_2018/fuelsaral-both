import { Injectable } from '@angular/core';



@Injectable()
class DummyStorage {
    create(name) {
        // const self = this;
        // return this;
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(self);
            }, 100);
        });
    }
    set(key, value) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                localStorage.setItem(key, value);
                resolve();
            }, 100);
        });
    }
    get(key) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                const x = localStorage.getItem(key);
                resolve({ key, value: x });
            }, 100);
        });
    }

    keys() {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(Object.keys(localStorage));
            });
        });
    }
}

class StorageItem {
    constructor() {

    }

    get value(): string {
        return;
    }

}


@Injectable()
export class StorageService {
    storage;
    private secureStorage;
    constructor() {
        // this.secureStorage.create('my_store_name')
        //     .then((storage: SecureStorageObject) => {
        //         this.storage = storage
        //         this.storage.set('test', 'waffle').then()
        //     })
    }

    init() {
        return new Promise((resolve) => {
            resolve();
        });
        // return new Promise((resolve, reject) => {
        //     const dummyStorage = new DummyStorage()
        //     this.secureStorage = dummyStorage

        //     dummyStorage.keys()
        //         .then((keys) => {
        //             return Promise.all(
        //                 keys.map((key) => dummyStorage.get(key))
        //             )
        //         })
        //         .then((values) => {
        //             this.storage = values.reduce((prev, next) => {
        //                 prev[next.key] = next.value
        //                 return prev
        //             }, {})
        //             return true
        //         }).then(resolve)
        // })
    }

    getValue(key) {
        return this.storage[key];
    }
    setValue(key, value) {
        this.secureStorage.set(key, value);
        return this.storage[key] = value;
    }

}
