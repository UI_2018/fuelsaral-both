export const Environments = {
    local: {
        mock : true,
        // show/hide debug messages / options
        debug: true,
        pdfUrl: '/mocks/test.pdf'
    },
    dev: {
        api: {
            protocol: 'https://',
            uri: 'mobile-a59a70163.hana.ondemand.com:443/',
            appId: 'com.bp.csp.fuels/',
            authUrl: 'odata/applications/latest/',
            service: 'irj/servlet/prt/portal/prtroot/bp.com~bpdc~fvc~css~data~service.',
            // length ajax call
            timeout: 5 * 60 * 1000,
            authTimeout: 15 * 1000
        },
        analyticsId: 'UA-44971642-30',
        // show/hide debug messages / options
        debug: true,
        pdfUrl: 'irj/servlet/prt/portal/prtroot/bp.com~bpdc~fvc~css~data~service.GetPDFData'
    },
    test_internal: {
        api: {
            protocol: 'https://',
            uri: 'mobile-ac9181cf2.hana.ondemand.com:443/',
            appId: 'com.bp.csp.fuels.internal/',
            authUrl: 'odata/applications/latest/',
            service: 'irj/servlet/prt/portal/prtroot/bp.com~bpdc~fvc~css~data~service.',
            // length ajax call
            timeout: 5 * 60 * 1000,
            authTimeout: 15 * 1000
        },
        analyticsId: 'UA-44971642-31',
        // show/hide debug messages / options
        debug: true,
        pdfUrl: 'irj/servlet/prt/portal/prtroot/bp.com~bpdc~fvc~css~data~service.GetPDFData'
    },
    test_external: {
        default: true,
        api: {
            protocol: 'https://',
            uri: 'mobile-ac9181cf2.hana.ondemand.com:443/',
            appId: 'com.bp.csp.fuels.external/',
            authUrl: 'odata/applications/latest/',
            service: 'irj/servlet/prt/portal/prtroot/bp.com~bpdc~fvc~css~data~service.',
            // length ajax call
            timeout: 5 * 60 * 1000,
            authTimeout: 15 * 1000
        },
        analyticsId: 'UA-44971642-31',
        // show/hide debug messages / options
        debug: false,
        pdfUrl: 'irj/servlet/prt/portal/prtroot/bp.com~bpdc~fvc~css~data~service.GetPDFData'
    },
    prod_internal: {
        api: {
            protocol: 'https://',
            uri: 'mobile-a292390f8.hana.ondemand.com:443/',
            appId: 'com.bp.csp.fuels.internal/',
            authUrl: 'odata/applications/latest/',
            service: 'irj/servlet/prt/portal/prtroot/bp.com~bpdc~fvc~css~data~service.',
            // length ajax call
            timeout: 5 * 60 * 1000,
            authTimeout: 15 * 1000
        },
        analyticsId: 'UA-44971642-32',
        // show/hide debug messages / options
        debug: false,
        pdfUrl: 'irj/servlet/prt/portal/prtroot/bp.com~bpdc~fvc~css~data~service.GetPDFData'
    },
    prod_external: {
        api: {
            protocol: 'https://',
            uri: 'mobile-a292390f8.hana.ondemand.com:443/',
            appId: 'com.bp.csp.fuels.external/',
            authUrl: 'odata/applications/latest/',
            service: 'irj/servlet/prt/portal/prtroot/bp.com~bpdc~fvc~css~data~service.',
            // length ajax call
            timeout: 5 * 60 * 1000,
            authTimeout: 15 * 1000
        },
        analyticsId: 'UA-44971642-32',
        // show/hide debug messages / options
        debug: false,
        pdfUrl: 'irj/servlet/prt/portal/prtroot/bp.com~bpdc~fvc~css~data~service.GetPDFData'
    }
};
