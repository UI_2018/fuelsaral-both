interface SalesArea {
    'SALES_ORG': string;
    'DIST_CHANNEL': string;
    'DIVISION': string;
    'INCLUDE_QUOTES': string;
    'MERGE_FLAG': string;
}

interface Url {
    id: string;
    url: string;
}

interface ControlFlags {
    'CAN_DUPLICATE': string;
    'SHOW_LOAD_ID_ADDON': string;
    'CRE_SOLDTO_CUST_NUMBER': string;
    'CAN_SAVE_DRAFT': string;
    'OVR_SHOW_PARAM_OTYPE': string;
    'SHOW_LIFTING_WINDOW': string;
    'CRE_BP_SEARCH_DEFAULT': string;
    'CRE_PRELOAD': string;
    'CRE_BP_SEARCH_TOGGLE': string;
    'CAN_EDIT': string;
    'HIDE_LOADING_TIME': string;
    'USE_DELIVERY_POINT_LABEL': string;
    'CAN_CREATE_MULTIPLE': string;
    'OVR_SHOW_PARAM_CUST': string;
    'OVR_EXCLUDE_OWN_ORDERS': string;
    'SHOW_DRIVER_VEHICLE': string;
    'CRE_BP_SEARCH_SOLDTO': string;
}

interface Role {
    'external_url': string;
    'name': string;
}

interface User {
    'T_SALES_AREAS': SalesArea[];
    't_sales_areas_count': number;
    'E_TOKEN': string;
    'EV_USER_TYPE': string;
    'IMPORTPARAMS': object;
    'T_PLANT_DETAILS': object[];
    'EV_SHOW_REPORT_CUST_SEARCH': string;
    'T_URLS': Url[];
    'E_LOCALE': string;
    'EV_PARTNER_NUMBER': string;
    'EV_SHOW_INVOICE_DOWNLOAD': string;
    'E_EXT': boolean;
    'EV_PARTNER_COUNT': number;
    'EV_HIDE_MARKETING_REGION': string;
    'E_SUBBRAND': '';
    'E_MWVERSION': 'FBS14.3.1';
    'EV_SHIPTO_SEARCH': '';
    'ES_ORDER_CONTROL_FLAGS': ControlFlags;
    'EV_PARTNER_FUNCTION': 'AG';
    'T_ROLES': Role[];
    'E_LASTNAME': string;
    'E_FIRSTNAME': string;
    't_plant_details_count': number;
    'EV_MULTI_PARTNER': string;
    'EV_TRUCK_NUMBER': string;
    'E_BRAND': string;
    'EV_CONTACT_PERSON': string;
}

// class UserModel implements User {

// }
