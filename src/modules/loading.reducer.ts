
const initialState = {
    count: 0,
};

export const LOADING_ITEM = 'LOADING_ITEM';
export const LOADING_ITEM_COMPLETE = 'LOADING_ITEM_COMPLETE';

export const loadingItem = (url) => {
    return {
        type: LOADING_ITEM,
        payload: url,
    };
};
export const loadingItemComplete = (url) => {
    return {
        type: LOADING_ITEM_COMPLETE,
        payload: url,
    };
};
export function loadingReducer(state = initialState, { type, payload }) {
    switch (type) {
        case LOADING_ITEM:
            console.log('LOADING STARTING:', payload);
            return {
                ...state,
                count: state.count + 1
            };
        case LOADING_ITEM_COMPLETE:
            console.log('LOADING COMPLETE:', payload);
            const newCount = state.count - 1;
            return {
                ...state,
                count: newCount <= 0 ? 0 : newCount
            };
        default:
            return state;
    }
}
