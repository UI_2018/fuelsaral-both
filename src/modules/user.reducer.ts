import { createAction } from '../utils/storeUtils'
export const LOGIN = 'LOGIN';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';

export const LOGOUT = 'LOGOUT';

const initialState = {
    loggedIn: false,
};

export const logout = createAction(LOGOUT)

export function userReducer(state: object = initialState, action) {
  switch (action.type) {
    case LOGIN:
      return state;

    case LOGIN_SUCCESS:
      return { ...action.payload };

    case LOGIN_FAILURE:
      return 0;

    default:
      return state;
  }
}
