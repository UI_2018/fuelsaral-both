import { AuthEffects } from './user.effects';
import { ConnectFormEffects } from './forms/connectForm.effects';

import { AOHEffects } from '../pages/aoh/aoh.effects';

import { ContractsEffects } from '../pages/contracts/contracts.effects';
import { CustomersEffects } from '../pages/customers/customers.effects';
import { LiftingsEffects } from '../pages/liftings/liftings.effects';
import { LoadIdsEffects } from '../pages/loadids/loadids.effects';
import { PricesEffects } from '../pages/prices/prices.effects';

export const effects = [
    AuthEffects,
    ConnectFormEffects,
    AOHEffects,
    ContractsEffects,
    CustomersEffects,
    LiftingsEffects,
    LoadIdsEffects,
    PricesEffects,
];
