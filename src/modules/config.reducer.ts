// counter.ts
import { Action } from '@ngrx/store';

export const SET_ENVIRONMENT = 'SET_ENVIRONMENT';

export class setEnvironment implements Action {
    readonly type = SET_ENVIRONMENT;

    constructor(public payload) {}
}

export const Actions = {
    setEnvironment
};

const initialState = ({
    environment: 'test_internal'
    // environment: 'local'
});

export function configReducer(state = initialState, action) {
  switch (action.type) {
    case SET_ENVIRONMENT:
        return {
            ...state,
            environment: action.payload,
        };
    default:
      return state;
  }
}
