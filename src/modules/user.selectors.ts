import { createSelector } from '@ngrx/store';

import { currentRouteId } from '../modules/router/router.selectors';

export const user = (state) => {
    return state['userReducer'];
};

const menuConfig = [
    { title: 'NAVIGATION.HOME', icon: 'home', link: 'home', tile: false },
    { title: 'NAVIGATION.AOH', icon: 'prices', link: 'aoh', tile: true },
    { title: 'NAVIGATION.PRICES', icon: 'prices', link: 'prices', tile: true },
    { title: 'NAVIGATION.CONTRACTS', icon: 'contract', link: 'contracts', tile: true },
    { title: 'NAVIGATION.LIFTINGS', icon: 'liftings', link: 'liftings', tile: true },
    { title: 'NAVIGATION.NOTIFICATIONS', notification: true, icon: 'notification', link: 'notifications', tile: true },
    { title: 'NAVIGATION.HSSE', icon: 'hsse', link: 'hsse', divider: true, tile: true },
    { title: 'NAVIGATION.LOAD_IDS', icon: 'load-ids', link: 'loadids', tile: false },
    { title: 'NAVIGATION.USEFUL_INFORMATION', icon: 'ui', link: 'useful-information', divider: true, tile: false },
];
export const getMenuOptions = createSelector(
    user, currentRouteId,
    (user, currentRouteId) => {
        return menuConfig.map((m) => {
            return {
                ...m,
                active: m.link === currentRouteId
            };
        });
    }
);

export const getHomeTileOptions = createSelector(
    user,
    (user) => {
        return menuConfig.filter(i => i.tile);
    }
);
