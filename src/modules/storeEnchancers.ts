import { ActionReducer, ActionReducerMap, MetaReducer } from '@ngrx/store';

import { localStorageSync } from 'ngrx-store-localstorage';

import { rootReducer } from './root.reducer';

const reducers: ActionReducerMap<any> = rootReducer;

export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return localStorageSync({
      keys: [
        'userReducer',
        // 'config',
        'basket',
        // 'aoh',
        // 'customers',
        // {aoh: {filter: ['contracts', 'prices']}},
        'router',
        { prices: { filter: ['viewing'] } },

        // 'liftings',
        // 'contracts',
        'forms'],
      rehydrate: true,
    })(reducer);
}

// console.log all actions
export function debug(reducer: ActionReducer<any>): ActionReducer<any> {
  return function(state, action) {
    // console.log('state', state);
    // console.log('action', action);

    return reducer(state, action);
  };
}

export const metaReducers: MetaReducer<any>[] = [localStorageSyncReducer, debug];
