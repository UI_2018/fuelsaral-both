import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';


import { Store } from '@ngrx/store';
import { timer } from 'rxjs/observable/timer';

import { Subject } from 'rxjs';

import { loadingCount } from './loading.selector';

@Injectable()
export class LoadingService {
    private currentCount = 0;
    private loader;

    isLoading = new Subject();
    constructor(private store: Store<any>, private loadingCtrl: LoadingController, private translateService: TranslateService) {
        this.store.select(loadingCount)
            .debounce((v) => {
                if (v && !this.currentCount) {
                    return timer(1);
                }
                return timer(200);
            })
            .subscribe((v) => {
                this.currentCount = v;
                this.isLoading.next(!!this.currentCount);
            });
    }

    showLoader() {
        if (!this.loader) {
            this.loader = this.loadingCtrl.create({
                content: this.translateService.instant('LOADING.LOGIN')
              });
            this.loader.present();
        }
    }
    hideLoader() {
        if (this.loader) {
            this.loader.dismiss();
            this.loader = null;
        }
    }

}
