import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';

import { App, LoadingController, NavController } from 'ionic-angular';

import { APIService } from './serviceProvider';


import * as CONSTANTS from './user.reducer';

@Injectable()
export class AuthEffects {
  private loader: any;
  // Listen for the 'LOGIN' action
  @Effect()
  login$: Observable<Action> = this.actions$.pipe(
    ofType(CONSTANTS.LOGIN),
    mergeMap(action => {
        this.loader = this.loadingCtrl.create({
          content: this.translateService.instant('LOADING.LOGIN')
        });
        this.loader.present();
        return this.api.authenticate(action['payload'])
          .pipe(
            mergeMap(data => this.api.login()),

            map(data => {
              this.loader.dismiss();
              this.navCtrl.setRoot('home');
              return { type: 'LOGIN_SUCCESS', payload: data['data'] };
            }),
            catchError((e) => {
              this.loader.dismiss();
              return of({ type: 'LOGIN_FAILED' });
            })
          );
    })
  );

  @Effect()
  logout$: Observable<Action> = this.actions$.pipe(
    ofType(CONSTANTS.LOGOUT),
    mergeMap(action => {
        // this.navCtrl.setRoot('home')
        return this.api.logout().
        pipe(
          map(data => {
              this.navCtrl.setRoot('login-page');
              return { type: 'LOGOUT_SUCCESS', payload: data };
          }),
          // If request fails, dispatch failed action
          catchError((e) => {
            return of({ type: 'LOGOUT_FAILED' });
          })
        );
    })
  );

  constructor(private http: HttpClient, private api: APIService, private actions$: Actions, private app: App, public loadingCtrl: LoadingController, private translateService: TranslateService) {
    this.loader = this.loadingCtrl.create({
      content: this.translateService.instant('LOADING.LOGIN')
    });
  }
  get navCtrl(): NavController {
    return this.app.getRootNav();
  }
}
