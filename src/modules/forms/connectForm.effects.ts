import { Injectable } from '@angular/core';

import { from } from 'rxjs/observable/from';
import { formErrorAction, formSuccessAction } from './forms.reducer';
import { of } from 'rxjs/observable/of';

import { Store } from '@ngrx/store';
import { Actions, Effect } from '@ngrx/effects';

@Injectable()
export class ConnectFormEffects {
  constructor(private store: Store<any>,
              private actions$: Actions) {
  }

  @Effect() addStory$ = this.actions$
    .ofType('ADD_STORY')
    .switchMap(action =>
      of({})
        .switchMap(story => (from([{
          type: 'ADD_STORY_SUCCESS'
        },                         formSuccessAction('newStory')])))
        .catch(err => {
            return of(formErrorAction('newStory', err));
        })
    );
}
