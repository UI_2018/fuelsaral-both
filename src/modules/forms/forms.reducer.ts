const FORM_SUBMIT_SUCCESS = 'FORM_SUBMIT_SUCCESS';
const FORM_SUBMIT_ERROR = 'FORM_SUBMIT_ERROR';
const UPDATE_FORM = 'UPDATE_FORM';

const updateForm = (payload) => {
    return {
        type: UPDATE_FORM,
        payload,
    };
};

export const formErrorAction = (payload, err) => {
    return {
        type: 'FORM_SUBMIT_ERROR',
        payload
    };
};
export const formSuccessAction = (payload) => {
    return {
        type: 'FORM_SUBMIT_SUCCESS',
        payload
    };
};

const initialState = {
    someForm: {
        test: 'fromStore',
        items: [
            { name: 'billy' },
            { name: 'billy' },
            { name: 'something' }
        ],
        terminals: [
            { title: 'option1' }
        ]
    },
    newStory: {
      title: 'asdasd',
      description: ''
    },
    contactUs: {
      email: '',
      message: ''
    }
  };

export function forms(state = initialState, action) {
      switch (action.type) {
          case UPDATE_FORM:
          return { ...state, [action.payload.path]: action.payload.value };
          default:
            return state;
      }
  }
