import { Directive, EventEmitter, Input, Output } from '@angular/core';
import { FormGroupDirective } from '@angular/forms';

import { Store } from '@ngrx/store';
import { Actions } from '@ngrx/effects';
import { Subscription } from 'rxjs/Subscription';




@Directive({
    selector: '[connectForm]'
})
export class ConnectFormDirective {
    @Input('connectForm') path: string;

    @Input() debounce = 300;
    @Output() error = new EventEmitter();
    @Output() success = new EventEmitter();
    @Output() buildForm = new EventEmitter();
    formChange: Subscription;
    formSuccess: Subscription;
    formError: Subscription;

    constructor(private formGroupDirective: FormGroupDirective,
                private store: Store<any>,
                private actions$: Actions, ) {


        }
    ngOnInit() {
            const sub = this.store.select(state => state.forms[this.path]).take(1).subscribe(formValue => {
                if (formValue) {
                    this.buildForm.emit(formValue);
                    this.formGroupDirective.form.patchValue(formValue);
                }
            });
            this.formChange = this.formGroupDirective.form.valueChanges
            .subscribe(value => {
                this.store.dispatch({
                    type: 'UPDATE_FORM',
                    payload: {
                        value,
                        path: this.path, // newStory
                    }
                });
            });
            this.formSuccess = this.actions$
                .ofType('FORM_SUBMIT_SUCCESS')
                .filter(({ payload }: any) => payload.path === this.path)
                .subscribe(() => {
                    this.formGroupDirective.form.reset();
                    this.success.emit();
                });

            this.formError = this.actions$
                .ofType('FORM_SUBMIT_ERROR')
                .filter(({ payload }: any) => payload.path === this.path)
                .subscribe(({ payload }: any) => this.error.emit(payload.error));
            // Update the form value based on the state

        }
    ngOnDestroy() {
            this.formChange.unsubscribe();
            this.formError.unsubscribe();
            this.formSuccess.unsubscribe();
        }
    }
