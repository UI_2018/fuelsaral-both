import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Headers } from '@angular/http';

import { map, tap, catchError } from 'rxjs/operators';
import qs from 'qs';
import { keyBy, isObject, isDate, isString, forEach } from 'lodash';

import { ConfigService } from '../config/configProvider';
import { StorageService } from '../config/storageProvider';

function encodeUriQuery(val, pctEncodeSpaces?) {
  return encodeURIComponent(val).
             replace(/%40/gi, '@').
             replace(/%3A/gi, ':').
             replace(/%24/g, '$').
             replace(/%2C/gi, ',').
             replace(/%3B/gi, ';').
             replace(/%20/g, (pctEncodeSpaces ? '%20' : '+'));
}

// exposed from ionic.bundle.js
function serializeValue(v) {
  if (isObject(v)) {
    return isDate(v) ? v.toISOString() : JSON.stringify(v);
  }
  return v;
}

function customTransformRequest(obj) {
  // not all calls have data
  if (!obj){
    return;
  }
    var params;
    // FILTER
    // convert from object to array of property/value objects <rolls eyes/>
    if (obj.filter && !isString(obj.filter)){
        params = [];

        forEach(obj.filter, function (val = '', key){
            params.push({
                property: key,
                // only run encodeUriQuery on individual fields and not arrays ...
                value: val.length ? val : encodeUriQuery(val)
            });
        }, params);

        // obj.filter = encodeUriQuery(serializeValue(params));
        obj.filter = serializeValue(params);

    }

    // INPUTPARAMS
    if (obj.inputparams && !isString(obj.inputparams)){
        // yes this is how it should be !!!
        obj.inputparams = '[' + serializeValue(obj.inputparams) + ']';
        // obj.inputparams = encodeUriQuery(obj.inputparams);
    }

    var str = [];
    for(var p in obj)
    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    return str.join("&");
}

@Injectable()
export class APIService {

  private ApplicationConnectionId: string;
  private SecurityToken: string;

  private baseAuth: string;

  private userName: string;
  constructor(private http: HttpClient, private configService: ConfigService, private storage: StorageService) {

  }

  getUsername() {
    if (this.configService.isMock) {
      return this.userName || 'f1';
    }
    return this.userName;
  }

  getServiceUrl(serviceCall, mode) {
    const queryString = qs.stringify({
      mode,
    });

    // return this.configService.env.api.protocol + this.configService.env.api.uri + '/' + this.configService.env.api.appId + this.configService.env.api.service + serviceCall + '?' + queryString
    return serviceCall + '?' + queryString;
  }

  getAuthUrl() {
    return 'Connections';
  }

  getDeAuthUrl(connectionId) {
    return 'Connections(\'' + connectionId + '\')';
  }

  getHeaders(opts = {}, auth?) {
    const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      ...opts,
    }
    if(auth) {
      headers['Authorization'] = this.baseAuth//'Basic UG9ydGFsVHN0RjAyOndlbGNvbWUy'
    }
    const h = new HttpHeaders(headers);
    return h
  }

  authenticate({ username, password }) {
    this.userName = username;

    const url = this.getAuthUrl();
    this.baseAuth = 'Basic ' + btoa(`${username}:${password}`);
    const headers = this.getHeaders(null, true);
    return this.http.post(url, {
      'DeviceType': 'android'
    }, {headers}).pipe(
      tap((resp) => {
        this.ApplicationConnectionId = resp['d'].ApplicationConnectionId;
      })
    );
  }

  login() {
    // https://mobile-ac9181cf2.hana.ondemand.com/odata/applications/latest/com.bp.csp.fuels.internal/Connections
    // https://mobile-ac9181cf2.hana.ondemand.com/odata/applications/latest/com.bp.csp.fuels.internal/Connections
    const url = this.getServiceUrl('Read', 'GETUSER');
    const headers = this.getHeaders({
      // 'X-SMP-APPCID': undefined,
      'X-SMP-APPCID': this.ApplicationConnectionId,
    }, true);

    return this.http.post(url, {}, { headers })
      .pipe(
        map((d: any) => {
          const { data } = d;

          this.SecurityToken = data.E_TOKEN
          data.flags = data.T_ROLES.reduce((prev, role) => {
            const name = role.name.split('_cpr_').pop();
            prev[name] = true;
            return prev;
          }, {});
          d.data = data;
          d.data.fullName = `${d.data.E_FIRSTNAME}  ${d.data.E_LASTNAME}`;
          return d;
        }),
        catchError((err, caught) => {
          return null;
        })
      );
  }


  logout() {
    const url = this.getDeAuthUrl(this.ApplicationConnectionId);
    const headers = this.getHeaders();
    return this.http.delete(url, { headers })
      .pipe(
        tap((resp) => {
          this.ApplicationConnectionId = undefined;
        })
      );
  }

  aoh() {
    const filter = {
      "IV_CUSTOMER_NUMBER": "12097786",
      "IV_SALES_ORG": "DE01",
      "IV_DIST_CHANNEL": "05",
      "IV_DIVISION": "01",
    }
    const headers = this.getHeaders({
      Accept: '*/*',
      "Security-Token": this.SecurityToken,
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });

    let data: any = {
      filter,
    };
    data = customTransformRequest(data)

    const url = this.getServiceUrl('Read', 'OFFERPRICE');
    // const headers = this.getHeaders()
    return this.http.post(url, data, {headers});
  }

  flexiContracts() {
    const url = this.getServiceUrl('Search', 'FLEXICONTRACT');
    return this.http.get(url);
  }

  contractDetails() {
    // Note this should go to flexicontract instead
    const url = this.getServiceUrl('Search', 'FLEXICONTRACT_DETAILS');
    return this.http.get(url);
  }

  timer() {
    const filter = {
      "IV_CUSTOMER_NUMBER": "12097786",
      "IV_SALES_ORG": "DE01",
      "IV_DIST_CHANNEL": "05",
      "IV_DIVISION": "01",
      "IV_OMR": ""
    }
    const headers = this.getHeaders({
      Accept: '*/*',
      "Security-Token": this.SecurityToken,
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });

    let data: any = {
      filter,
    };
    data = customTransformRequest(data)

    const url = this.getServiceUrl('Write', 'CHECKTIME');
    return this.http.post(url, data, {headers});
  }

  customers() {
    // [{
      // "property":"T_SALES_AREAS","value":"[{\"SALES_ORG\":\"NZ0A\",\"DIST_CHANNEL\":\"05\",\"DIVISION\":\"01\"}]"}
      // ,{"property":"IV_MULTI_PARTNER","value":" "},
      // {"property":"IV_PARTNER_FUNCTION","value":"AG"},
      // {"property":"IV_USER_TYPE","value":"KU"},
      // {"property":"IV_PARTNER_NUMBER","value":"0012046432"},
      // {"property":"IV_GET_SALES_AREA_DETAILS","value":"X"},
      // {"property":"IV_LIMIT","value":"999"}]

    const filter = {
      T_SALES_AREAS: "[{\"SALES_ORG\":\"DE01\",\"DIST_CHANNEL\":\"05\",\"DIVISION\":\"01\"}]",
      IV_MULTI_PARTNER: "X",
      IV_PARTNER_FUNCTION: "",
      IV_USER_TYPE: "AT",
      IV_PARTNER_NUMBER: "",
      IV_GET_SALES_AREA_DETAILS: "X",
      IV_LIMIT: "999",
    }
    const headers = this.getHeaders({
      Accept: '*/*',
      "Security-Token": this.SecurityToken,
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });

    let data: any = {
      filter,
    };
    data = customTransformRequest(data)
    const url = this.getServiceUrl('Search', 'CUSTOMER');
    return this.http.post(url, data, {headers}).pipe(
      map((resp: any) => {
        const out =  keyBy(resp.data.T_BUSINESS_PARTNERS, 'BP_NUMBER');
        return out;
      })
    );
  }

  creditPosition() {
    const url = this.getServiceUrl('Read', 'CREDITPOSITION');
    return this.http.get(url);
  }

  getContracts() {
    const filter = 
    {
      "IV_CUSTOMER_NUMBER": "12097786",
      "IV_SALES_ORG": "DE01",
      "IV_DIST_CHANNEL": "05",
      "IV_DIVISION": "01",
      "IV_LIMIT": "999",
      "IV_DATE_FROM": "2018-04-01T00:00:00",
      "IV_DATE_TO": "2018-09-20T00:00:00",
    }
    const headers = this.getHeaders({
      Accept: '*/*',
      "Security-Token": this.SecurityToken,
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });

    let data: any = {
      filter,
    };
    data = customTransformRequest(data)
    const url = this.getServiceUrl('Search', 'CONTRACTITEMS');
    return this.http.post(url, data, {headers})
  }

  getContractDetails(contractId) {
    const hasOwnData = !!contractId;
    const url = this.getServiceUrl('Read', (hasOwnData) ? ('CONTRACTITEMS-' + contractId) : 'CONTRACTITEMS');
    return this.http.get(url);
  }

  getLiftings() {
    // const filter = [
    //   {"property":"IV_LIMIT","value":99999},
    //   {"property":"IV_FROM_DATE","value":"2018-05-01T00:00:00"},
    //   {"property":"IV_TO_DATE","value":"2018-09-20T00:00:00"},
    //   {"property":"T_SALES_AREAS","value":"[{\"SALES_ORG\":\"DE01\",\"DIST_CHANNEL\":\"05\",\"DIVISION\":\"01\"}]"},
    //   {"property":"IV_MULTI_PARTNER","value":"X"}
    // ]
    const filter = {
      IV_LIMIT: 99999,
      IV_FROM_DATE: "2018-05-01T00:00:00",
      IV_TO_DATE: "2018-09-20T00:00:00",
      T_SALES_AREAS: "[{\"SALES_ORG\":\"DE01\",\"DIST_CHANNEL\":\"05\",\"DIVISION\":\"01\"}]",
      IV_MULTI_PARTNER: "X"
    }

    let data: any = {
      filter,
    };

    data = customTransformRequest(data)

    const url = this.getServiceUrl('Search', 'INVOICES');
    const headers = this.getHeaders({
      Accept: '*/*',
      "Security-Token": this.SecurityToken,
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    })
    return this.http.post(url, data, {headers});
  }

  loadIds() {
    let data: any = {
      filter: {
        "IV_MULTI_PARTNER": "X",
        "IV_PARTNER_FUNCTION": "LF",
        "IV_PARTNER_NUMBER": "0006901189",
        "IV_LIMIT": 99999
      }
    };

    data = customTransformRequest(data)

    const headers = this.getHeaders({
      // 'X-SMP-APPCID': undefined,
      'X-SMP-APPCID': this.ApplicationConnectionId,
    });

    const url = this.getServiceUrl('Search', 'LOADIDS');
    

    return this.http.post(url, data, { headers })
  }

  getContractSearchStatusTypes() {
    const url = this.getServiceUrl('Search', 'LOADIDS');
    return this.http.get(url)
      .pipe(
        tap((resp) => {
          return {
            statuses: [{ title: 'remote', value: 1 }],
            types: [{ title: 'renite', value: 1 }]
          };
        })
      );
  }
  getContractSearchGroups() {
    const url = this.getServiceUrl('Search', 'LOADIDS');
    return this.http.get(url)
      .pipe(
        tap((resp) => {
          return {
            groups: [{ title: 'remote', value: 1 }],
          };
        })
      );
  }
  getContractSearchPlants() {
    const url = this.getServiceUrl('Search', 'LOADIDS');
    return this.http.get(url)
      .pipe(
        tap((resp) => {
          return {
            plants: [{ title: 'remote', value: 1 }],
          };
        })
      );
  }

  getCurrentPrices() {
    const filter = {
      IV_CUSTOMER_NUMBER: "12097786",
      IV_SALES_ORG: "DE01",
      IV_DIST_CHANNEL: "05",
      IV_DIVISION: "01"
    }
    let data: any = {
      filter,
    };
    data = customTransformRequest(data)

    const headers = this.getHeaders({
      Accept: '*/*',
      "Security-Token": this.SecurityToken,
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    })
    const url = this.getServiceUrl('Report', 'CURRENTPRICE');
    return this.http.post(url, data, { headers })
    ;
  }

  getHistoricPrices() {
    const filter = {
      // IV_CUSTOMER_NUMBER: "12097786",
      IV_CUSTOMER_NUMBER: "12128636",
      IV_SALES_ORG: "DE01",
      IV_DIST_CHANNEL: "05",
      IV_DIVISION: "01"
    }
    let data: any = {
      filter,
    };
    data = customTransformRequest(data)

    const headers = this.getHeaders({
      Accept: '*/*',
      "Security-Token": this.SecurityToken,
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    })
    const url = this.getServiceUrl('Report', 'HISTORICPRICE');
    return this.http.post(url, data, { headers })
  }
}
