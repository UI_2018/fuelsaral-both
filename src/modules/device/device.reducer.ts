import { createAction } from '../../utils/storeUtils';

const initialState = {
    width: 200
};

export const SET_WIDTH = 'SET_WIDTH';
export const setWidth = createAction(SET_WIDTH);

export function deviceReducer(state = initialState, { type, payload }) {
    switch (type) {
        case SET_WIDTH:
            return {
                ...state,
                width: payload
            };
        default:
            return state;
    }
}
