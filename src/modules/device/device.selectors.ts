import { createSelector } from '@ngrx/store';

export const isTablet = (state) => {
    return state.device.width >= 767;
};
export const virtualList = (selector) => {
    return createSelector(selector, isTablet, (items: any = [], isTablet) => {
        const numberColumns = isTablet ? 2 : 1;
        const output = items.reduce((prev, next, i) => {
            const rem = i % numberColumns;
            if (!rem) {
                prev.push([next]);
            } else {
                prev[prev.length - 1].push(next);
            }
            return prev;
        }, []);
        return {
            items: output,
            count: items.length
        };
    });
};
