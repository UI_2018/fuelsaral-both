const initialState = {
    currentParams: {},
    currentRoute: ''
};

const VIEW_DID_ENTER = 'VIEW_DID_ENTER';

export const viewDidEnter = (payload) => {
    return {
        type: VIEW_DID_ENTER,
        payload
    };
};

export const routerReducer = (state = initialState, action) => {
    switch (action.type) {
        case VIEW_DID_ENTER:
            return {
                currentRoute: action.payload.name,
                currentParams: action.payload.data,
                id: action.payload.id,
            };
        default:
            return state;
    }
};
