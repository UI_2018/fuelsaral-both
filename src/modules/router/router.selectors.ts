import { createSelector } from '@ngrx/store';


export const currentParams = (state) => {
    return state['router'].currentParams;
};

export const currentRouteName = (state) => {
    return state['router'].currentRoute;
};

export const currentRouteId = (state) => {
    return state['router'].id;
};

export const showSidebar = createSelector(
    currentRouteName,
    (route) => {
        return [
            'ContractDetailsPage',
            'DetailsPage',
            'LoginPage',
            'UserPage',
        ].indexOf(route) == -1;
    }
);
