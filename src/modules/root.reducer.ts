import { counterReducer } from '../modules/counter.reducer';
import { userReducer } from './user.reducer';
import { configReducer } from './config.reducer';
import { basketReducer } from '../pages/basket/basket.reducer';
import { forms } from './forms/forms.reducer';
import { aohReducer } from '../pages/aoh/aoh.reducer';
import { contractsReducer } from '../pages/contracts/contracts.reducer';
import { routerReducer } from './router/router.reducer';
import { loadingReducer } from './loading.reducer';
import { notificationsReducer } from '../pages/notifications/notifications.reducer';
import { liftingsReducer } from '../pages/liftings/liftings.reducer';
import { customersReducer } from '../pages/customers/customers.reducer';
import { loadIdsReducer } from '../pages/loadids/loadids.reducer';
import { priceReducer } from '../pages/prices/prices.reducer';
import { deviceReducer } from '../modules/device/device.reducer';
export const rootReducer = {
    router: routerReducer,
    counterReducer,
    userReducer,
    forms,
    config: configReducer,
    aoh: aohReducer,
    contracts: contractsReducer,
    basket: basketReducer,
    loading: loadingReducer,
    notifications: notificationsReducer,
    liftings: liftingsReducer,
    customers: customersReducer,
    loadids: loadIdsReducer,
    prices: priceReducer,
    device: deviceReducer,
};
