import { Store } from '@ngrx/store';

export function getState(store: Store<any>): any {
    let state;

    store.take(1).subscribe(s => state = s);

    return state;
}

export function select(store, selector) {
    return selector(getState(store));
}

export const createAction = (type) => {
    return (payload = {}) => ({
        type,
        payload,
    });
};
