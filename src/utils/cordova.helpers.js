export const getResourceUrl = (url) => {
    if(window['cordova']) {
        return  window['cordova'].file.applicationDirectory + 'www' + url
    } else {
        return url
    }
}