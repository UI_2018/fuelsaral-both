import { Component } from '@angular/core';
import { FormBuilder, FormControl }   from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

import { IonicPage } from 'ionic-angular';
import { Store } from '@ngrx/store';

import { AlertController, NavParams, ViewController } from 'ionic-angular';




import { fields } from './searchConfig';



@IonicPage({
    name: 'search'
})
@Component({
    templateUrl: './search.html',
    styles: ['./search.scss']
})
export class SearchPage {
    group = 'any';

    fields = fields;
    fieldData;
    form;
    onSearch;

    constructor(private alertCtrl: AlertController, private fb: FormBuilder, public viewCtrl: ViewController, private store: Store<any>, private navParams: NavParams, private translateService: TranslateService) {
        this.fields = this.navParams.get('fields').map((i) => ({
          ...i,
          label: this.translateService.instant(i.label)
        }));
        this.onSearch = this.navParams.get('onSearch');

        const values = this.navParams.get('values');
        this.form = this.fb.group(this.fields.reduce((prev, field) => {
            // If there is no value passed in then use the default for that field
            const value = values[field.name] || field.value;
            prev[field.name] = new FormControl(value, null);
            return prev;
        }, {}));
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    performSearch() {
        this.viewCtrl.dismiss();


        setTimeout(() => {
            this.onSearch(this.form.value);

        }, 10);

    }
}
