import { NgModule } from '@angular/core';

import { TranslateModule } from '@ngx-translate/core';

import { IonicPageModule } from 'ionic-angular';

import { SearchPage } from './search';

import { FilterSelect } from './components/filterSelect/filterSelect.component';

@NgModule({
    declarations: [
        SearchPage,
        FilterSelect
    ],
    imports: [
        TranslateModule,
        IonicPageModule.forChild(SearchPage)
    ],
    entryComponents: [
        SearchPage
    ]
})
export class SearchModule {}
