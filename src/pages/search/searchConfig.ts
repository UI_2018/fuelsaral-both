import { DateRangeSelector } from '../../modals/dateRangeSelector/dateRangeSelector';
import { getReferenceSet } from '../loadids/loadIds.selectors';
import { getReferenceSet as getReferenceSetContracts } from '../contracts/contracts.selectors';
import { getReferenceSet as getReferenceSetLiftings } from '../liftings/liftings.selectors';
import moment, { isMoment } from 'moment';

export const dateOptions = [
    { title: 'DATERANGE.ANY_TIME', value: 'any' },
    { title: 'DATERANGE.YESTERDAY', value: {title: 'yesterday', startDate: moment().subtract('1', 'day'), endDate: moment().add('1', 'year')} },
    { title: 'DATERANGE.LAST_WEEK', value: {title: 'last week', startDate: moment().subtract('1', 'week'), endDate: moment().add('1', 'year')} },
    { title: 'DATERANGE.LAST_MONTH', value: {title: 'last month', startDate: moment().subtract('1', 'month'), endDate: moment().add('1', 'year')} },
    { title: 'DATERANGE.CUSTOM_RANGE', modal: DateRangeSelector }
];

const returnStatic = (set) => {
    return () => set;
};

export const fields = [
    { type: 'text', label: 'SEARCH.CONTRACT_NO', name: 'CONTRACT_NUMBER', value: '' },
    { type: 'text', label: 'SEARCH.CUSTOMER_REFERENCE', name: 'customerReference', value: '' },
    { type: 'select', label: 'SEARCH.PRODUCT_GROUP', placeholder: 'SEARCH.ANY_PRODUCT_GROUP', name: 'MATERIAL_GROUP_ID', value: null, options: getReferenceSetContracts('MATERIAL_GROUP_ID', 'MATERIAL_GROUP_DESCRIPTION', 'MATERIAL_GROUP_ID') },
    { type: 'select', label: 'SEARCH.PRODUCT', placeholder: 'SEARCH.ANY_PRODUCT', name: 'MATERIAL_ID', value: null, options: getReferenceSetContracts('MATERIAL_ID', 'MATERIAL_DESCRIPTION', 'MATERIAL_ID') },
    { type: 'select', label: 'SEARCH.TERM_TYPE', placeholder: 'SEARCH.ANY_TERM_TYPE', name: 'CONTRACT_TYPE', value: null, options: getReferenceSetContracts('CONTRACT_TYPE', 'CONTRACT_TYPE_DESC', 'CONTRACT_TYPE') },
    { type: 'select', label: 'SEARCH.CONTRACT_STATUS', placeholder: 'SEARCH.ANY_CONTRACT_STATUS', name: 'STATUS_ID', value: null, options: getReferenceSetContracts('STATUS_ID', 'STATUS_DESCRIPTION', 'STATUS_ID') },
    { type: 'select', label: 'SEARCH.TERMINAL', placeholder: 'SEARCH.ANY_TERMINAL', name: 'Terminal', value: null, options: getReferenceSetContracts('PLANT_ID', 'PLANT_DESCRIPTION', 'PLANT_ID') },
    {type: 'select-date', label: 'SEARCH.DATE',  name: 'DATE_STARTED', value: 'any', options: returnStatic(dateOptions), translate: true, displayFn: (value) => {
        if(value.startDate || value.endDate) {
            return [value.startDate, value.endDate].join(' - ');
        }
        if(value.title) {
            return value.title
        }
    }}
];

export const contractsSearch = {
    fields,
};

export const liftingsFields = [
    { type: 'text', label: 'SEARCH.CONTRACT_NO', name: 'CONTRACT_NUMBER', value: '' },
    // { type: 'text', label: 'SEARCH.INVOICE_NO', name: 'invoiceNo', value: '' },
    // { type: 'text', label: 'SEARCH.ORDER_NO', name: 'orderNo', value: '' },
    { type: 'text', label: 'SEARCH.CUSTOMER_REFERENCE', name: 'CONTRACT_ITEM_NO', value: '' },
    { type: 'select', label: 'SEARCH.PRODUCT_GROUP', placeholder: 'SEARCH.ANY_PRODUCT_GROUP', name: 'MATERIAL_GROUP_ID', value: null, options: getReferenceSetLiftings('MATERIAL_GROUP_ID', 'MATERIAL_GROUP_DESCRIPTION', 'MATERIAL_GROUP_ID') },
    { type: 'select', label: 'SEARCH.PRODUCT', placeholder: 'SEARCH.ANY_PRODUCT', name: 'MATERIAL_DESCRIPTION', value: null, options: getReferenceSetLiftings('PRODUCT_ID', 'MATERIAL_DESCRIPTION', 'PRODUCT_ID') },
    { type: 'select', label: 'SEARCH.TERMINAL', placeholder: 'SEARCH.ANY_TERMINAL', name: 'PLANT_ID', value: null, options: getReferenceSetLiftings('PLANT_ID', 'DELIVERY_POINT_NAME', 'PLANT_ID') },
    {type: 'select-date', label: 'SEARCH.DATE', name: 'LIFTING_DATE', value: 'any', options: returnStatic(dateOptions), translate: true, displayFn: (value) => {
        return [value.startDate, value.endDate].join(' - ');
    }}
];

export const liftingsSearch = {
    fields: liftingsFields,
};

export const loadIdsFields = [
    { type: 'select', label: 'SEARCH.MATERIAL', placeholder: 'SEARCH.ANY_MATERIAL', name: 'MATERIAL_NUMBER', value: null, options: getReferenceSet('MATERIAL_NUMBER', 'MATERIAL_DESCRIPTION', 'MATERIAL_NUMBER') },
    { type: 'select', label: 'SEARCH.TERM_TYPE', placeholder: 'SEARCH.ANY_TERM_TYPE', name: 'CONTRACT_TYPE', value: null, options: getReferenceSet('CONTRACT_TYPE', 'CONTRACT_TYPE_DESC', 'CONTRACT_TYPE') },
    { type: 'select', label: 'SEARCH.TERMINAL', placeholder: 'SEARCH.ANY_TERMINAL', name: 'PLANT_NUMBER', value: null, options: getReferenceSet('PLANT_NUMBER', 'PLANT_NAME', 'PLANT_NUMBER') },
];

export const loadIdsSearch = {
    fields: loadIdsFields,
};
