import { Component, Input, Renderer2, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { Store } from '@ngrx/store';

import { TranslateService } from '@ngx-translate/core';

import { AlertController, ModalController } from 'ionic-angular';


export const EPANDED_TEXTAREA_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => FilterSelect),
  multi: true,
};

@Component({
  selector: 'filter-select',
  providers: [EPANDED_TEXTAREA_VALUE_ACCESSOR],
  templateUrl: './filterSelect.component.html',
  styles: ['./filterSelect.component.scss']
})
export class FilterSelect implements ControlValueAccessor {
  @Input() field = { translate: false, displayFn: null, options: null, label: null, placeholder: null };
  options;
  innerValue;
  onChange;
  onTouched;

  subscription;

  writeValue(value: any): void {
    this.innerValue = value;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    // const div = this.textarea.nativeElement;
    // const action = isDisabled ? 'addClass' : 'removeClass';
    // this.renderer[action](div, 'disabled');
  }

  constructor(
    private renderer: Renderer2,
    private alertCtrl: AlertController,
    private modalCtrl: ModalController,
    private store: Store<any>,
    private translateService: TranslateService) {


  }

  ngOnInit() {
      this.subscription = this.store.select(this.field.options)
        .subscribe((v) => {
          this.options = v;
        });
  }

  onFieldChange($event) {
    this.setFieldValue($event.target.value);
  }

  setFieldValue(value) {
    this.innerValue = value;
    this.onChange(value);
    this.onTouched(value);
  }

  get displayValue() {
    if (!this.options) {
      return '';
    }
    const selectedValue = this.options.find((o) => o.value === this.innerValue);
    if (selectedValue) {
      return this.field.translate ? this.translateService.instant(selectedValue.title) : selectedValue.title;
    }
    if (!selectedValue && this.innerValue && this.field.displayFn) {
      return this.field.displayFn(this.innerValue);
    }
    return this.translateService.instant(this.field.placeholder) || '';
  }

  showSelect() {
    const alert = this.alertCtrl.create();
    const title = this.translateService.instant(this.field.label);

    alert.setTitle(title);

    if (this.field.placeholder) {
      const placeholderInput = {
        type: 'radio',
        label: this.translateService.instant(this.field.placeholder),
        value: undefined,
        checked: !this.innerValue,
      };
      alert.addInput(placeholderInput);
    }


    const inputs = this.options.map((o) => {
      return {
        type: 'radio',
        label: this.field.translate ? this.translateService.instant(o.title) : o.title,
        value: o.value,
        checked: o.value === this.innerValue,
        handler: () => {
          if (o.modal) {
            alert.dismiss();
            const modal = this.modalCtrl.create(o.modal);
            modal.onDidDismiss(data => {
              this.setFieldValue(data);
            });
            modal.present();
          }
        }
      };
    });

    inputs.forEach((i) => {
      alert.addInput(i);
    });

    alert.addButton(this.translateService.instant('CANCEL'));
    alert.addButton({
      text: this.translateService.instant('OK'),
      handler: (data: any) => {
        this.setFieldValue(data);
      }
    });

    alert.present();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}

// <textarea-expanded [formControl]="control"></textarea-expanded>
