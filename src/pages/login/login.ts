import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController } from 'ionic-angular';
import { Store } from '@ngrx/store';

import { InAppBrowser } from '@ionic-native/in-app-browser';

import { Login } from '../../modules/user.actions';

import { EnvironmentModal } from '../../modals/environment/environmentModal.component';

import { StorageService } from '../../config/storageProvider';

// const initialUser = {
//   username: 'PortalTstF02',
//   password: 'welcome2'
// }
// const initialUser = {
//   username: 'PortalTstF02',
//   password: 'welcome2'
// };
const initialUser = {
  username: 'PORTALTSTF86',
  password: 'welcome2'
};

@IonicPage({
    name: 'login-page'
})
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  styles: ['./login.scss']
})
export class LoginPage {

  user: object = initialUser;

  constructor(
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public store: Store<{}>,
    public inAppBrowser: InAppBrowser,

    public storage: StorageService) {

  }

  login() {
    this.store.dispatch(new Login(this.user));
  }

  swipeEvent(e) {
    if (e.overallVelocityX >= 0) {
        return;
    }
    const environmentModal = this.modalCtrl.create(EnvironmentModal);
    environmentModal.present();
  }

  forgottenPassword() {
    const browser = this.inAppBrowser.create('https://pscuat.bpglobal.com/PwdSelfCare/dialog/appl/bp/wflow/main', '_system');
    // browser.show()

  }

  loginTest() {
    const $ = window['$']
    $.ajax({
      type: "POST",
      url: '/odata/applications/latest/com.bp.csp.fuels.internal/Connections',
      // url: 'http://postb.in/V8B2par0',
      data: JSON.stringify({"DeviceType": "android"}),
      dataType: 'json',
      
      headers: {
        "authorization": "Basic UG9ydGFsVHN0RjAyOndlbGNvbWUy",
        "Content-Type": "application/json",
        "Accept": "application/json"
      }
    }).then((p) => {
      debugger
    });
  }
}
