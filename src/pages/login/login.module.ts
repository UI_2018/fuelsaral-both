import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { LoginPage } from './login';

import { ComponentModule } from '../../components';

@NgModule({
    declarations: [
        LoginPage
    ],
    imports: [
        IonicPageModule.forChild(LoginPage),
        ComponentModule
    ],
    entryComponents: [
        LoginPage
    ]
})
export class LoginModule {}
