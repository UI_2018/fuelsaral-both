import { createSelector } from '@ngrx/store';
import { sumBy } from 'lodash';
export const basketItems = (state) => {
    return state['basket'].items;
};

export const basketItemsWithSubTotal = createSelector(
    basketItems,
    (basketItems = []) => {
        return basketItems.map((item) => {
            return {
                ...item,
                subTotal: (+item.quantity / 100) * +item.material.CUSTOMER_PRICE
            };
        });
    }
);

export const basketItemIds = createSelector(
    basketItems,
    (basketItems) => {
        return basketItems.reduce((prev, item) => {
            prev[item.id] = true;
            return prev;
        }, {});
    }
);

export const basketTotal = createSelector(
    basketItemsWithSubTotal,
    (basketItems) => {
        const total = sumBy(basketItems, 'subTotal');
        return total;
    }
);


export const basketCount = createSelector(
    basketItems,
    (basketItems) => {
        if (!basketItems) {
            return 0;
        }
        return basketItems.length;
    }
);
// export const selectFeatureCount = createSelector(
//   selectFeature,
//   (state: FeatureState) => state.counter
// );
