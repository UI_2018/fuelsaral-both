import { Component, ViewChild } from '@angular/core';
import { Content, IonicPage, ModalController } from 'ionic-angular';
import { FormBuilder, FormControl } from '@angular/forms';

import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';


import { actions } from './basket.reducer';
import { basketCount , basketItemsWithSubTotal, basketTotal } from './basket.selectors';

import { ShareModal } from '../../modals/share/share.component';

import { NavigationService } from '../../app/navigation';

const mustBeTrue = (control: FormControl) => {
    return control.value ? null : { mustBeTrue: true };
};
@IonicPage({
    name: 'basket',
    defaultHistory: ['aoh']
})
@Component({
    templateUrl: './basket.html',
    styles: ['./basket.scss']
})
export class BasketPage {
    private items$: Observable<object[]>;
    private basketTotal$: Observable<number>;
    private basketCount$: Observable<number>;
    private basketConfirmationForm;

    private isSubmitted = false;
    @ViewChild(Content) content: Content;

    constructor(private store: Store<any>, private fb: FormBuilder, private modalCtrl: ModalController, private navService: NavigationService) {
        this.items$ = store.pipe(select(basketItemsWithSubTotal));
        this.basketTotal$ = store.pipe(select(basketTotal));
        this.basketCount$ = store.pipe(select(basketCount));

        this.basketConfirmationForm = this.fb.group({
            confirmed: new FormControl(false, mustBeTrue)
        });
    }



    addItem() {
        this.store.dispatch(new actions.AddItem({}));
    }

    onFooterUpdate() {
        this.content.resize();
    }

    onSubmit() {
        this.isSubmitted = true;
        setTimeout(() => {
            this.store.dispatch(new actions.EmptyBasket({}));
        }, 10);
        // this.modalCtrl.create(BasketConfirmationModal).present()
    }

    navigate(link) {
        this.navService.followLink(link);
    }

    openTerms(event) {
        event.stopPropagation();
        this.navService.openFile('/assets/pdf/terms.pdf');
    }

    openShare() {
        this.modalCtrl.create(ShareModal, { contracts: [{
            MATERIAL_DESCRIPTION: 'Neuer Kontrakt'
        }] }, { cssClass: 'modal-full' }).present();
    }
}
