import { Action } from '@ngrx/store';


export const ADD_ITEM = 'ADD_ITEM';
export const REMOVE_ITEM = 'REMOVE_ITEM';
export const UPDATE_ITEM = 'UPDATE_ITEM';

export const EMPTY_BASKET = 'EMPTY_BASKET';

export class AddItem implements Action {
  readonly type = ADD_ITEM;

  constructor(public payload) {}
}

export class RemoveItem implements Action {
  readonly type = REMOVE_ITEM;

  constructor(public payload) {}
}

export class UpdateItem implements Action {
  readonly type = UPDATE_ITEM;

  constructor(public payload) {}
}

export class EmptyBasket implements Action {
  readonly type = EMPTY_BASKET;

  constructor(public payload) {}
}

export const actions = {
  AddItem,
  RemoveItem,
  UpdateItem,
  EmptyBasket
};

interface Item {
  price: number;
  quantity: number;
}

interface State {
  items: Item[];
}

const initialState = {
  items: [],
  // items: [],
  total: 0,
};

export interface CustomAction extends Action {
  type: string;
  payload?: any;
  }
export function basketReducer(state: State = initialState, action: CustomAction) {
  switch (action.type) {
    case ADD_ITEM:
      action.payload.id = action.payload.material.id;
      return {
        ...state,
        items: [...(state.items), ...action.payload]
      };
    case REMOVE_ITEM:
      const { items } = state;
      const id = action.payload;
      return {
        ...state,
        items: items.filter((i) => i['id'] !== id)
      };
    case UPDATE_ITEM:
      const { items: updateItems } = state;
      const { id: itemId, quantity } = action.payload;
      return {
        ...state,
        items: updateItems.map((i) => {
          if (i['id'] === itemId) {
            return {
              ...i,
              quantity
            };
          }
          return i;
        })
      };
    case EMPTY_BASKET:
      return {
        ...state,
        items: []
      };
    default:
      return state;
  }
}
