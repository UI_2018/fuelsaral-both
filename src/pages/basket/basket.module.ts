import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { BasketPage } from './basket';

import { BasketItem } from './components/basketItem/basketItem';
import { BasketIcon } from './components/basketIcon/basketIcon.component';

import { UpdateItemModal } from './modals/updateItem/updateItem';
import { AddItemModal } from './modals/addItem/addItem';

import { ComponentModule } from '../../components';

@NgModule({
    declarations: [
        BasketPage,
        BasketItem,
        BasketIcon,
        UpdateItemModal,
        AddItemModal,
    ],
    imports: [
        IonicPageModule.forChild(BasketPage),
        ComponentModule,
    ],
    entryComponents: [
        BasketPage,
        UpdateItemModal,
        AddItemModal,
        BasketIcon
    ],
    exports: [
        BasketIcon,
        UpdateItemModal
    ]
})
export class BasketModule {}
