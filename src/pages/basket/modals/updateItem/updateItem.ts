import { Component } from '@angular/core';

import { NavParams, ViewController } from 'ionic-angular';


@Component({
    templateUrl: './updateItem.html',
})
export class UpdateItemModal {
    private item;
    constructor(
        public viewCtrl: ViewController,
        private params: NavParams
    ) {
        this.item = this.params.get('item');
    }
    dismiss() {
        this.viewCtrl.dismiss();
    }
}
