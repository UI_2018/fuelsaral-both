import { Component } from '@angular/core';

import { NavParams, ViewController } from 'ionic-angular';
import { FormBuilder, FormControl }   from '@angular/forms';

const quantityValidator = (min, max, rounding) => {
    return (control: FormControl) => {
        let { value } = control;
        value = +value;
        const errors = {};
        if (value < min) {
            errors['tooSmall'] = true;
        }
        if (value > max && max !== 0) {
            errors['tooLarge'] = true;
        }
        if ((value % rounding) !== 0) {
            errors['rounding'] = true;
        }
        return Object.keys(errors).length ? errors : null;
    };
};

@Component({
    templateUrl: './addItem.html',
})
export class AddItemModal {
    private material;
    private onConfirm;

    private labels;
    private materialForm;
    constructor(
        public viewCtrl: ViewController,
        private params: NavParams,
        private fb: FormBuilder,
    ) {
        this.material = this.params.get('material');
        this.onConfirm = this.params.get('onConfirm');
        this.labels = this.params.get('labels');

        const item = this.params.get('item');
        const initialQuantity = item ? item.quantity : null;

        this.materialForm = this.fb.group({
            quantity: this.fb.control(initialQuantity, quantityValidator(+this.material.MIN_QUANTITY, +this.material.MAX_QUANTITY, +this.material.ROUNDING_QUANTITY)),
        });
    }

    break() {
    }
    addItem() {
        const { quantity } = this.materialForm.value;
        this.onConfirm({
            material: this.material,
            quantity,
        });
        this.dismiss();
    }
    dismiss() {
        this.viewCtrl.dismiss();
    }
}
