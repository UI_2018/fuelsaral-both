import { Component, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';

import { AlertController, ModalController } from 'ionic-angular';

import { AddItemModal } from '../../modals/addItem/addItem';

import { actions } from '../../basket.reducer';

@Component({
    selector: 'basket-item',
    templateUrl: './basketItem.component.html'
})
export class BasketItem {
    @Input() private item;
    @Input() private index: number;
    constructor(private store: Store<any>, public modalCtrl: ModalController, private alertCtrl: AlertController, private translateService: TranslateService) {

    }
    removeItem() {
        const alert = this.alertCtrl.create({
            title: this.translateService.instant('BASKET.REMOVE_ALERT_TITLE'),
            message: this.translateService.instant('BASKET.REMOVE_ALERT_MESSAGE'),
            buttons: [
              {
                text: this.translateService.instant('CANCEL'),
                handler: () => {
                }
              },
              {
                text: this.translateService.instant('BASKET.REMOVE_ALERT_OK_BUTTON'),
                handler: () => {
                    this.store.dispatch(new actions.RemoveItem(this.item.material.id));
                }
              }
            ]
          });

        alert.present();
    }

    openUpdateItem(item) {
        const updateItemModal = this.modalCtrl.create(AddItemModal, {
            material: this.item.material,
            item: this.item,
            onConfirm: this.updateItem.bind(this),
            labels: {
                title: 'BASKET.UPDATE_ITEM',
                confirm: 'BASKET.UPDATE_CONFIRM'
            }
        });
        updateItemModal.present();
    }

    updateItem({ quantity, material }) {
        this.store.dispatch(new actions.UpdateItem({
            id: this.item.material.id,
            quantity,
        }));
    }


}
