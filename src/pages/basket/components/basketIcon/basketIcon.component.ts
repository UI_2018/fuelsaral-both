import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';

import { NavController } from 'ionic-angular';

import { basketCount } from '../../basket.selectors';

const template = `
<div>
    <aoh-icon icon="basket" (click)="showBasket()"></aoh-icon>
    <span *ngIf="itemCount$ | async" class="count" (click)="showBasket()">{{itemCount$ | async}}</span>
</div>
`;
@Component({
    selector: 'basket-icon',
    template
})
export class BasketIcon {
    private itemCount$: Observable<number>;

    constructor(public navCtrl: NavController, private store: Store<any>) {
        this.itemCount$ = this.store.pipe(select(basketCount));
    }

    showBasket() {
        this.navCtrl.push('basket');
    }
}
