import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { FormBuilder, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';


const languages = [
    { title: 'English', code: 'en' },
    { title: 'German', code: 'de' }
];

@IonicPage({
    name: 'user',
    segment: 'user'
})
@Component({
    templateUrl: './user.html',
})
export class UserPage {
    languages = languages;
    selectedLanguage = null;
    form;
    constructor(private fb: FormBuilder, private translate: TranslateService) {
        this.form = this.fb.group({
            language: new FormControl(null, null),
            email: new FormControl(null, null),
            phone: new FormControl(null, null),
        });
    }

    onSet() {
        const values = this.form.value;
        if (values.language) {
            this.translate.use(values.language);
        }
    }
}
