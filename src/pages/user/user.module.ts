import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { UserPage } from './user';
import { ComponentModule } from '../../components';


@NgModule({
    declarations: [
        UserPage
    ],
    imports: [
        IonicPageModule.forChild(UserPage),
        ComponentModule,
    ]
})
export class UserModule {}
