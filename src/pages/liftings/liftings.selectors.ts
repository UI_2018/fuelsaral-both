import { createSelector } from '@ngrx/store';
import { keys, some, uniqBy, every, keyBy } from 'lodash';
import moment from 'moment'
window['moment'] = moment
import { liftingsFields  as liftingsSearchConfigData } from '../search/searchConfig'
export const liftings = (state) => {
    return state.liftings.items;
};


export const liftingsSearch = (state) => {
    return state['liftings'].search || {};
};

export const liftingsSearchConfig = () => {
  return keyBy(liftingsSearchConfigData, 'name')
}

export const isShowingLiftingsSearch = createSelector(
    liftingsSearch,
    (search) => {
        return some(keys(search), (searchKey) => {
            return search[searchKey] && search[searchKey] !== 'any';
        });
    }
);

export const visibleLiftings = createSelector(
  liftings, liftingsSearch, isShowingLiftingsSearch, liftingsSearchConfig,
  (liftings, search, isShowingSearch, liftingsSearchConfig) => {
    if (isShowingSearch) {
      const searchKeys = keys(search);
      return liftings.filter((lifting) => {
          return every(searchKeys, (key) => {
              if(!search[key]) {
                  return true
              } else if(liftingsSearchConfig[key].type === 'select-date') {
                const value = search[key]
                if(value.startDate && moment(lifting[key]).startOf('day').isBefore(moment(value.startDate))) {
                  return false
                }
                if(value.endDate && moment(lifting[key]).startOf('day').isAfter(moment(value.endDate))) {
                  return false
                }
                return true
              }
              return search[key] === lifting[key];
          });
      });
    }
    return liftings;
  }
)

export const getReferenceSet = (type, titleKey, valueKey) => {
  return createSelector(
    liftings,
    (liftings) => {
      const items = uniqBy(liftings, type);
      return items.map((i) => {
        return {
          ...i,
          title: i[titleKey],
          value: i[valueKey]
        };
      });
    }
  );
};
