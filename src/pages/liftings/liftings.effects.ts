import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import { APIService } from '../../modules/serviceProvider';

import { GET_LIFTINGS, getLiftingsSuccess } from './liftings.reducer';


@Injectable()
export class LiftingsEffects {

  @Effect()
  liftings$: Observable<Action> = this.actions$.pipe(
    ofType(GET_LIFTINGS),
    mergeMap(action => {
      return this.api.getLiftings()
        .pipe(
          map((resp: any) => {
            return getLiftingsSuccess(resp);
          })
        );
    })
  );

  constructor(
    private api: APIService,
    private actions$: Actions,
    private store: Store<any>
  ) {
  }

}
