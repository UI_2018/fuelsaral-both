import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController } from 'ionic-angular';
import { Store } from '@ngrx/store';

import { SearchPage } from '../search/search';


import { liftingsSearch } from '../search/searchConfig';

import { isShowingLiftingsSearch, visibleLiftings, liftingsSearch as liftingsSearchSelector } from './liftings.selectors';
import { getLiftings, setLiftingsSearch } from './liftings.reducer';

const tabOptions = [
  { title: 'LIFTINGS.OPEN', value: true },
  { title: 'CONTRACTS.OVERDUE', value: false }
];

@IonicPage({
    name: 'liftings'
})
@Component({
  selector: 'liftings',
  templateUrl: 'liftings.html',
  styles: ['./liftings.scss']
})
export class LiftingsPage {
  liftingsSearch = liftingsSearch;
  showingSearchResults$;
  liftings$;
  searchValues;
  tabOptions = tabOptions;

  constructor(
    public store: Store<{}>,
    private navCtrl: NavController,
    private modalCtrl: ModalController) {
      this.liftings$ = this.store.select(visibleLiftings);
      this.showingSearchResults$ = this.store.select(isShowingLiftingsSearch);
      this.store.select(liftingsSearchSelector)
        .subscribe((v) => {
          this.searchValues = v;
        });
    this.store.dispatch(getLiftings());
  }

  openSearch() {
    this.modalCtrl.create(SearchPage, {
      fields: liftingsSearch.fields,
      onSearch: this.onSearch.bind(this),
      values: this.searchValues
    }, {
      cssClass: 'modal-right'
    }).present();
  }

  onChangeSearch() {
    this.openSearch();
  }

  onSearch(values) {
    this.store.dispatch(setLiftingsSearch(values));
  }
}
