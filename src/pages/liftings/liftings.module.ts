import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { LiftingsPage } from './liftings';

import { ComponentModule } from '../../components';
@NgModule({
    declarations: [
        LiftingsPage
    ],
    imports: [
        IonicPageModule.forChild(LiftingsPage),
        ComponentModule
    ],
    entryComponents: [
        LiftingsPage
    ]
})
export class LiftingsModule {}
