import {createAction} from "../../utils/storeUtils";

const initialState = {
  items: [],
  search: {}
};

export const GET_LIFTINGS = 'GET_LIFTINGS';
export const GET_LIFTINGS_SUCCESS = 'GET_LIFTINGS_SUCCESS';

export const SET_LIFTINGS_SEARCH = 'SET_LIFTINGS_SEARCH';

export const getLiftings = createAction(GET_LIFTINGS);
export const getLiftingsSuccess = createAction(GET_LIFTINGS_SUCCESS);

export const setLiftingsSearch = (payload) => ({
  type: SET_LIFTINGS_SEARCH,
  payload
});

export function liftingsReducer(state = initialState, {type, payload}) {
  switch (type) {
    case SET_LIFTINGS_SEARCH:
      return {
        ...state,
        search: payload,
      };
    case GET_LIFTINGS_SUCCESS:
      return {
        ...state,
        items: payload
      };
    case SET_LIFTINGS_SEARCH:
      return {
        ...state,
        search: payload,
      };
    default:
      return state;
  }
}
