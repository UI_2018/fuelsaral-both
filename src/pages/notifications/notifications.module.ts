import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { NotificationsPage } from './notifications';

import { ComponentModule } from '../../components';

import { NotificationDetailsModule } from './subPages/notificationDetails/notificationDetails.module';
import { NotificationSettingsModule } from './subPages/notificationSettings/notificationSettings.module';

@NgModule({
    declarations: [
        NotificationsPage,
    ],
    imports: [
        IonicPageModule.forChild(NotificationsPage),
        NotificationDetailsModule,
        NotificationSettingsModule,
        ComponentModule
    ],
    entryComponents: [
        NotificationsPage
    ]
})
export class NotificationsModule {}
