const initialState = {
    items: {
        '1': {
            id: '1',
            hidden: false,
            createdAt: 'Thur Sep 20 2018 16:46:45 GMT+0100',
            title: 'Aktualisierte Preise verfügbar',
            content: `Bitte beachten Sie, dass im Menüpunkt „Preise“ aktualisierte Preise zur Verfügung stehen.`
        },
        '2': {
            id: '2',
            hidden: false,
            createdAt: 'Wed Sep 19 2018 16:46:45 GMT+0100',
            title: 'Eingeschränkte SOK 98 (Super Plus) Verladung in Lingen',
            content: `Aufgrund einer Tankrevision steht ab dem 29.09.18, 14:00 Uhr bis 01.10.18 nach Tankfreigabe (voraussichtlich gegen 11:00 Uhr) kein SOK98 für die Tankwagenverladung zur Verfügung.

            Bitte informieren Sie die Speditionen über diese Einschränkung.`
        },
        '3': {
            id: '3',
            createdAt: 'Sun Sep 16 2018 16:46:45 GMT+0100',
            read: false,
            title: 'TL Gustavsburg: Sonderöffnungszeiten Oktober',
            content: `Dienstag, 02.10.2018 bis 22:00 Uhr letzte Einfahrt

            Samstag, 06.10.2018 um 13:00 Uhr letzte Einfahrt

            Mittwoch,  31.10.2018 bis 22:00 Uhr letzte Einfahrt`
        },
        '4': {
            id: '4',
            createdAt: 'Mon Sep 10 2018 16:46:45 GMT+0100',
            title: 'TL Koeln: Keine Verladung am 22.09.2018',
            read: false,
            content: `Am Samstag, den 22.09.2018, führt das TL Koeln Umbauarbeiten an den Füllbühnen durch. Um die Sicherheit gewährleisten zu können, ist es nötig die Verladung an diesem Tag einzustellen. Letzte Einfahrt für TKWs ist um 5:00 Uhr.
            `
        }
    }
};


export const DISMISS_NOTIFICATION = 'DISMISS_NOTIFICATION';
export const READ_NOTIFICATION = 'READ_NOTIFICATION';
export const dismissNotification = (payload) => ({
    type: DISMISS_NOTIFICATION,
    payload,
});

export const readNotification = (payload) => ({
    type: READ_NOTIFICATION,
    payload
});

export function notificationsReducer(state = initialState, { type, payload }) {
    switch (type) {
        case DISMISS_NOTIFICATION:
            return {
                ...state,
                items: {
                    ...state.items,
                    [payload]: {
                        ...state.items[payload],
                        hidden: true
                    }
                },
            };
        case READ_NOTIFICATION:
            return {
                ...state,
                items: {
                    ...state.items,
                    [payload]: {
                        ...state.items[payload],
                        read: true
                    }
                },
            };
        default:
            return state;
    }
}
