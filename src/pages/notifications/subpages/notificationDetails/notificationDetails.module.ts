import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { NotificationDetailsPage } from './notificationDetails';

import { ComponentModule } from '../../../../components';
@NgModule({
    declarations: [
        NotificationDetailsPage
    ],
    imports: [
        IonicPageModule.forChild(NotificationDetailsPage),
        ComponentModule
    ],
    entryComponents: [
        NotificationDetailsPage
    ]
})
export class NotificationDetailsModule {}
