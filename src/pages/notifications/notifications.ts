import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Store } from '@ngrx/store';

import { notificationItemsAsArray } from './notifications.selectors';

import { dismissNotification } from './notifications.reducer';

@IonicPage({
  name: 'notifications'
})
@Component({
  selector: 'notifications',
  templateUrl: 'notifications.html',
  styles: ['./notifications.scss']
})
export class NotificationsPage {
  selectedItem: any;
  icons: string[];
  items$;

  constructor(public navCtrl: NavController, public navParams: NavParams, private store: Store<any>) {
    this.items$ = store.select(notificationItemsAsArray);
  }

  itemTapped(event, item) {
    this.navCtrl.push('notification-details', item);
  }

  openSettings() {
    this.navCtrl.push('notification-settings');
  }

  dismiss(event, item) {
    this.store.dispatch(dismissNotification(item.id));
  }
}
