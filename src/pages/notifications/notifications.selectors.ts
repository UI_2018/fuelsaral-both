import { createSelector } from '@ngrx/store';

import { toArray } from 'lodash';

import { currentParams } from '../../modules/router/router.selectors';

export const notificationItems = (state) => {
    return state['notifications'].items;
};

export const notificationItemsAsArray = createSelector(
    notificationItems,
    (items) => {
        return toArray(items).filter(i => !i.hidden);
    }
);

export const notification = createSelector(
    currentParams, notificationItems,
    (params, items) => {
        return items[params.id];
    }
);

export const notificationCount = createSelector(
    notificationItemsAsArray,
    (n) => {
        return n.reduce((prev, next) => {
            if (!next.read) {
                prev = prev + 1;
            }
            return prev;
        }, 0);
    }
);
