import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

import { Store } from '@ngrx/store';

import { notification } from '../../notifications.selectors';

import { readNotification } from '../../notifications.reducer';

@IonicPage({
  name: 'notification-details',
  segment: 'notifcations/:id',
  defaultHistory: ['notifications'],
})
@Component({
  selector: 'notification-details',
  templateUrl: 'notificationDetails.html',
  styles: ['./notificationDetails.scss']
})
export class NotificationDetailsPage {
  details$;
  constructor(public navCtrl: NavController, private store: Store<any>) {
    this.details$ = this.store.select(notification);

    this.details$.first(v => v).subscribe(v => {
      this.store.dispatch(readNotification(v.id));
    });
  }
}
