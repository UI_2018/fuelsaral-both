import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';


import { Store } from '@ngrx/store';

const fields = [
    { title: 'NOTIFICATIONS.SMS', key: 'SMS' },
    { title: 'NOTIFICATIONS.EMAIL', key: 'EMAIL' },
    { title: 'NOTIFICATIONS.PUSH', description: 'NOTIFICATIONS.PUSH_DESCRIPTION' }
];

const getFields = () => {
    return fields.map(f => {
        return {...f}
    })
}

const config = [

    {
        title: 'NOTIFICATIONS.PRICE_CHANGE',
        description: 'NOTIFICATIONS.PRICE_CHANGE_DESCRIPTION',
        fields: getFields(),
    },
    {
        title: 'NOTIFICATIONS.CONTRACT_CONFIRMATION',
        description: 'NOTIFICATIONS.CONTRACT_CONFIRMATION_DESCRIPTION',
        fields: getFields(),
    },
    {
        title: 'NOTIFICATIONS.CONTRACT_NOT_FULFILLED',
        description: 'NOTIFICATIONS.CONTRACT_NOT_FULFILLED_DESCRIPTION',
        fields: getFields(),
    },
    {
        title: 'NOTIFICATIONS.LOW_IN_CONTRACT',
        description: 'NOTIFICATIONS.LOW_IN_CONTRACT_DESCRIPTION',
        fields: getFields(),
    },
    {
        title: 'NOTIFICATIONS.TERMINAL_EVENTS',
        description: 'NOTIFICATIONS.TERMINAL_EVENTS_DESCRIPTION',
        fields: getFields(),
    }
];

@IonicPage({
  name: 'notification-settings',
  defaultHistory: ['notifications'],
})
@Component({
  selector: 'notification-settings',
  templateUrl: 'notificationSettings.html',
  styles: ['./notificationSettings.scss']
})
export class NotificationSettingsPage {
  config = config;

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      private store: Store<any>,
      private alertCtrl: AlertController,
      private translateService: TranslateService) {
  }
  toggleItem(event, field) {
      this.doPrompt(event, field)
  }

  doPrompt(event, field) {
    event.preventDefault()
    event.stopPropagation()


    if(!field.checked || !field.key) {
      return
    }
    const keys = {
      'EMAIL': {
        TITLE: 'NOTIFICATIONS.EMAIL_TITLE',
        DESCRIPTION: 'NOTIFICATIONS.EMAIL_MESSAGE',
        PLACEHOLDER: 'NOTIFICATIONS.EMAIL_ADDRESS'
      },
      'SMS': {
        TITLE: 'NOTIFICATIONS.SMS_TITLE',
        DESCRIPTION: 'NOTIFICATIONS.SMS_MESSAGE',
        PLACEHOLDER: 'NOTIFICATIONS.SMS_PHONE_NUMBER'
      }
    }[field.key]
    let alert = this.alertCtrl.create({
      title:  this.translateService.instant(keys.TITLE),
      message:  this.translateService.instant(keys.DESCRIPTION),
      inputs: [
        {
          name: 'title',
          placeholder:  this.translateService.instant(keys.PLACEHOLDER)
        },
      ],
      buttons: [
        {
          text: this.translateService.instant('NOTIFICATIONS.CANCEL'),
          handler: () => {
            field.checked = false
          }
        },
        {
          text: this.translateService.instant('NOTIFICATIONS.SAVE'),
          handler: () => {
            field.checked = true
          }
        }
      ]
    });

    alert.present();
  }
}
