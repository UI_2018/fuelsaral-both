import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { NotificationSettingsPage } from './notificationSettings';

import { ComponentModule } from '../../../../components';

@NgModule({
    declarations: [
        NotificationSettingsPage,
    ],
    imports: [
        IonicPageModule.forChild(NotificationSettingsPage),
        ComponentModule
    ],
    entryComponents: [
        NotificationSettingsPage
    ]
})
export class NotificationSettingsModule {}
