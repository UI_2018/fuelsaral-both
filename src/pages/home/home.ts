import { Component } from '@angular/core';

import { IonicPage, NavController } from 'ionic-angular';

import { Store } from '@ngrx/store';

import { getHomeTileOptions } from '../../modules/user.selectors';

const tiles = [
  { title: 'Aoh', icon: 'prices', link: 'aoh' },
  { title: 'Contracts', icon: 'contract', link: 'contracts' },
  { title: 'Liftings', icon: 'liftings', link: 'home' },
  { title: 'Notifications', icon: 'notification', link: 'home' },
  { title: 'HSSE', icon: 'hsse', link: 'hsse' }
];

@IonicPage({
  name: 'home'
})
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private tiles = tiles;
  tiles$;
  constructor(public navCtrl: NavController, private store: Store<any>) {
    this.tiles$ = this.store.select(getHomeTileOptions);
  }

}
