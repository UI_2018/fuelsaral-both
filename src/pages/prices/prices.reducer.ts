import { createAction } from '../../utils/storeUtils';

import { removeDuplicateRecentFilters } from '../aoh/aoh.reducer';

const initialState = {
  viewing: 'history',
  prices: [],
  recentFilters: [],
  selectedFilter: null,
};

export const SET_VIEWING = 'SET_VIEWING';
export const SET_PRICE_FILTER = 'SET_PRICE_FILTER';

export const PRICES_LOAD_SUCCESS = 'PRICES_LOAD_SUCCESS';
export const setViewing = createAction(SET_VIEWING);
export const pricesLoadSuccess = createAction(PRICES_LOAD_SUCCESS);
export const setPriceFilter = createAction(SET_PRICE_FILTER);

export function priceReducer(state = initialState, { type, payload }) {
    switch (type) {
      case SET_VIEWING:
        return {
          ...state,
          viewing: payload
        };
      case PRICES_LOAD_SUCCESS:
        return {
          ...state,
          prices: payload
        };
      case SET_PRICE_FILTER:
        let newRecentFilters = state.recentFilters;
        // If there are no filters selected then don't push to recent list
        // If there are more than 5 filters in the history then remove the last
        if (payload.storeAsRecent && (payload.products.length || payload.plants.length)) {
            newRecentFilters = [payload, ...newRecentFilters];
        }
        newRecentFilters = removeDuplicateRecentFilters(newRecentFilters);
        return {
            ...state,
            selectedFilter: payload,
            recentFilters: newRecentFilters
        };
      default:
        return state;
    }
}
