import { Component, Input } from '@angular/core';

@Component({
    selector: 'price-item',
    templateUrl: './priceItem.html'
})
export class PriceItem {
    @Input('price') price;
}
