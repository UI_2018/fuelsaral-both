import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Store } from '@ngrx/store';

import { NavigationService } from '../../app/navigation';

import { historyPrices, prices, pricesFilterOptions, pricesSelectedFilter, pricesViewing, recents } from './prices.selectors';
import { setPriceFilter, setViewing } from './prices.reducer';

import { select } from '../../utils/storeUtils';

const tabOptions = [
  { title: 'PRICES.CURRENT', value: 'current' },
  { title: 'PRICES.HISTORY', value: 'history' }
];

@IonicPage({
  name: 'prices'
})
@Component({
  selector: 'prices',
  templateUrl: 'prices.html',
  styles: ['./prices.scss']
})
export class PricesPage {
  tabOptions = tabOptions;

  prices$;
  historyPrices$;
  viewing$;
  selectedFilter;

  subscription;

  constructor(public navCtrl: NavController, public navParams: NavParams, private store: Store<any>, private navService: NavigationService) {
    this.prices$ = this.store.select(prices);
    this.historyPrices$ = this.store.select(historyPrices);
    this.subscription = this.store.select(pricesSelectedFilter)
      .subscribe((v) => {
        this.selectedFilter = v;
      });
    this.viewing$ = this.store.select(pricesViewing);
  }

  ionViewDidLoad() {
    const viewing = select(this.store, pricesViewing) || 'current';
    this.store.dispatch(setViewing(viewing));
  }

  onTabChange(value) {
    this.store.dispatch(setViewing(value));
  }

  removePlantFilter(item) {
    const newFilters = {
      plants: this.selectedFilter.plants.filter((p) => p.title !== item.title),
      products: this.selectedFilter.products
    };
    this.store.dispatch(setPriceFilter(newFilters));
  }

  removeProductFilter(item) {
    const newFilters = {
      products: this.selectedFilter.products.filter((p) => p.title !== item.title),
      plants: this.selectedFilter.plants
    };
    this.store.dispatch(setPriceFilter(newFilters));
  }
  openFilter() {
    this.navCtrl.push('filter', {
      options: pricesFilterOptions,
      recents,
      onSetFilter: setPriceFilter,
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
