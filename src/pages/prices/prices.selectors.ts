import { createSelector } from '@ngrx/store';
import { groupBy, some } from 'lodash';

import { getOptions } from '../aoh/aoh.selectors';

export const prices = (state) => {
    return state['prices'].prices || [];
};

export const recents = (state) => {
    return state['prices'].recentFilters || [];
};


export const pricesSelectedFilter = (state) => {
    return state['prices'].selectedFilter || null;
};

export const pricesViewing = (state) => {
    return state['prices'].viewing;
};
export const pricesFilterOptions = createSelector(
    prices,
    (prices) => {
        if (!prices.length) {
            return {};
        }
        return {
            PLANTS: getOptions(prices, 'PLANT_NAME', 'PLANT_NAME', 'PLANT'),
            PRODUCTS: getOptions(prices, 'MATERIAL_DESCRIPTION', 'MATERIAL_DESCRIPTION', 'MATERIAL_ID')
        };
    }
);

export const historyPrices = createSelector(
    prices, pricesSelectedFilter,
    (prices, selectedFilter) => {
        if (selectedFilter) {
            prices = prices.filter((price) => {
                let matchesPlant = true;
                let matchesProduct = true;
                if (selectedFilter.plants.length) {
                    matchesPlant = some(selectedFilter.plants, (plant) => {
                        return price.PLANT === plant.PLANT;
                    });
                }
                if (selectedFilter.products.length) {
                    matchesProduct = some(selectedFilter.products, (product) => {
                        return price.MATERIAL_ID === product.MATERIAL_ID;
                    });
                }
                return matchesPlant && matchesProduct;
            });
        }

        const pricesByDate = groupBy(prices, 'CREATION_DATE');

        let output = [];
        for (const date in pricesByDate) {
            output.push({
                date,
                prices: pricesByDate[date]
            });
        }
        output = output.sort(function compare(a, b) {
            a = new Date(a.date);
            b = new Date(b.date);
            return a > b ? -1 : a < b ? 1 : 0;
          });


        return output;
    }
);
