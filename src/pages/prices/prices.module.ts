import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { PricesPage } from './prices';

import { ComponentModule } from '../../components';
import { PriceItem } from './components/priceItem/priceItem';

@NgModule({
    declarations: [
        PricesPage,
        PriceItem,
    ],
    imports: [
        IonicPageModule.forChild(PricesPage),
        ComponentModule
    ],
    entryComponents: [
        PricesPage
    ]
})
export class PricesModule {}
