import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import { SET_VIEWING, pricesLoadSuccess } from './prices.reducer';

import { APIService } from '../../modules/serviceProvider';

@Injectable()
export class PricesEffects {
  private loader: any;
  // Listen for the 'LOGIN' action
  @Effect()
  prices$: Observable<Action> = this.actions$.pipe(
    ofType(SET_VIEWING),
    mergeMap((action: any) => {
        if (action.payload === 'current') {
          return this.api.getCurrentPrices()
              .pipe(
                  map((resp: any) => {
                      return pricesLoadSuccess(resp.data.T_MATERIAL_DETAILS);
                  })
              );
            }
        return this.api.getHistoricPrices()
          .pipe(
              map((resp: any) => {
                  return pricesLoadSuccess(resp.data.T_HISTORICAL_MATERIAL_DETAILS);
              })
          );
      })
    );
        // return this.api.authenticate(action['payload'])
        //   .pipe(
        //     mergeMap(data => this.api.login()),

        //     map(data => {
        //       this.loader.dismiss()
        //       this.navCtrl.setRoot('home')
        //       return { type: 'LOGIN_SUCCESS', payload: data['data'] }
        //     }),
        //     catchError((e) => {
        //       this.loader.dismiss()
        //       return of({ type: 'LOGIN_FAILED' })
        //     })
        //   )



  constructor(private http: HttpClient, private api: APIService, private actions$: Actions) {
  }
}
