import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, ViewController } from 'ionic-angular';
import { Store } from '@ngrx/store';

import { SearchPage } from '../search/search';
import { contractsSearch as contractsSearchAction, getContracts, setFilter } from './contracts.reducer';


import { contractSearch, contractsWithFlags, isShowingContractSearch, visibleContracts, contractFilter } from './contracts.selectors';
import { contractsSearch } from '../search/searchConfig';

const tabOptions = [
  { title: 'CONTRACTS.OPEN', value: true },
  { title: 'CONTRACTS.ALL', value: false }
];

@IonicPage({
    name: 'contracts'
})
@Component({
  selector: 'contracts',
  templateUrl: 'contracts.html',
  styles: ['./contracts.scss']
})
export class ContractsPage {
  contractsSearch = contractsSearch;
  tabOptions = tabOptions;
  showingSearchResults$;
  searchValues;
  contracts;
  filter$
  constructor(
    public store: Store<{}>,
    private navCtrl: NavController,
    private viewCtrl: ViewController,
    private modalCtrl: ModalController) {
      this.store.dispatch(getContracts({}));
      this.store.select(contractSearch).subscribe((v) => {
        this.searchValues = v;
      });
      this.store.select(visibleContracts).subscribe((v) => {
        this.contracts = v;
      });
      this.filter$ = this.store.select(contractFilter)
      this.showingSearchResults$ = this.store.select(isShowingContractSearch);
  }

  openSearch() {
    this.modalCtrl.create(SearchPage, {
      fields: contractsSearch.fields,
      onSearch: this.onSearch.bind(this),
      values: this.searchValues
    },{
      cssClass: 'modal-right'
    }).present();
  }

  onChangeSearch() {
    this.openSearch();
  }

  handleFilterChange(value) {
    this.store.dispatch(setFilter({ isOpen: value }));
  }
  onSearch(values) {
    this.store.dispatch(setFilter({ isOpen: false }));
    this.store.dispatch(contractsSearchAction(values));
  }
}
