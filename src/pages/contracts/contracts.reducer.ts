import { keyBy } from 'lodash';

const initialState = {
    contracts: {},
    details: {},
    search: {

    },
    filter: {
        isOpen: true
    }
};

const createAction = (type) => {
    return (payload = {}) => ({
        type,
        payload,
    });
};
export const SET_SEARCH = 'SET_SEARCH';

export const CONTRACTS_SEARCH = 'CONTRACTS_SEARCH';
export const CONTRACTS_SEARCH_SUCCESS = 'CONTRACTS_SEARCH_SUCCESS';

export const GET_CONTRACTS = 'GET_CONTRACTS';
export const GET_CONTRACTS_SUCCESS = 'GET_CONTRACTS_SUCCESS';

export const GET_CONTRACT = 'GET_CONTRACT';
export const GET_CONTRACT_SUCCESS = 'GET_CONTRACT_SUCCESS';

export const SET_FILTER = 'SET_FILTER';

export const setSearch = createAction(SET_SEARCH);
export const getContracts = createAction(GET_CONTRACTS);
export const getContractsSuccess = createAction(GET_CONTRACTS_SUCCESS);

export const getContract = createAction(GET_CONTRACT);
export const getContractSuccess = createAction(GET_CONTRACT_SUCCESS);

export const contractsSearch = createAction(CONTRACTS_SEARCH);
export const contractsSearchSuccess = createAction(CONTRACTS_SEARCH_SUCCESS);

export const setFilter = createAction(SET_FILTER);

export function contractsReducer(state = initialState, action) {
    switch (action.type) {
        case CONTRACTS_SEARCH_SUCCESS:
            return {
                ...state,
                search: action.payload
            };
        case GET_CONTRACT_SUCCESS:
            const {
                ES_CONTRACT_ITEM: details,
                ES_DELIVERY_POINT: delivery,
                ES_SOLD_TO: soldTo,
                T_LIFTINGS: liftings,
            } = action.payload;
            const contractId = details.CONTRACT_NUMBER;
            return {
                ...state,
                details: {
                    ...state.details,
                    [contractId]: {
                        details,
                        delivery,
                        soldTo,
                        liftings
                    }
                }
            };
        case GET_CONTRACTS_SUCCESS:
            const contracts = action.payload.T_CONTRACT_ITEMS.map((contract) => {
                return {
                    ...contract,
                    id: contract.CONTRACT_NUMBER
                };
            });
            return {
                ...state,
                contracts: keyBy(contracts, 'id')
            };
        case SET_FILTER:
            return {
                ...state,
                filter: action.payload,
            };
        default:
            return state;
    }
}
