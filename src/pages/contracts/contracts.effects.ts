import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import { App, LoadingController } from 'ionic-angular';

import { APIService } from '../../modules/serviceProvider';

import {
  CONTRACTS_SEARCH,
  GET_CONTRACT,
  GET_CONTRACTS,
  contractsSearchSuccess,
  getContractSuccess,
  getContractsSuccess,
} from './contracts.reducer';

@Injectable()
export class ContractsEffects {
  private loader: any;
  @Effect()
  getContracts$: Observable<Action> = this.actions$.pipe(
    ofType(GET_CONTRACTS),
    mergeMap(action => {
        return this.api.getContracts()
          .pipe(
            map((resp: any) => {
              return getContractsSuccess(resp.data);
            })
          );
    })
  );

  @Effect()
  getContract$: Observable<Action> = this.actions$.pipe(
    ofType(GET_CONTRACT),
    mergeMap((action: any) => {
        return this.api.getContractDetails(action.payload.contractId)
          .pipe(
            map((resp: any) => {
              return getContractSuccess(resp.data);
            })
          );
    })
  );

  @Effect()
  contractsSearch$: Observable<Action> = this.actions$.pipe(
    ofType(CONTRACTS_SEARCH),
    map((action: any) => {
      return contractsSearchSuccess(action.payload);
    })
  );



  constructor(private http: HttpClient, private api: APIService, private actions$: Actions, private app: App, public loadingCtrl: LoadingController) {
  }
}
