import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { ContractsPage } from './contracts';

import { ComponentModule } from '../../components';
@NgModule({
    declarations: [
        ContractsPage
    ],
    imports: [
        IonicPageModule.forChild(ContractsPage),
        ComponentModule
    ],
    entryComponents: [
        ContractsPage
    ]
})
export class ContractsModule {}
