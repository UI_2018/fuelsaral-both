import { Component } from '@angular/core';

import { Store } from '@ngrx/store';

import { getContract } from '../../contracts.reducer';

import { IonicPage, NavParams } from 'ionic-angular';

import { contract } from '../../contracts.selectors';

@IonicPage({
    name: 'contract-details',
    segment: 'contracts/:id',
    defaultHistory: ['contracts'],
})
@Component({
    templateUrl: './contractDetails.html',
    styles: ['./contractDetails.scss']
})
export class ContractDetailsPage {
    contract;
    constructor(private store: Store<any>, private navParams: NavParams) {
        this.store.dispatch(getContract({contractId: this.navParams.get("CONTRACT_NUMBER")}));
        this.store.select(contract)
            .subscribe((v) => {
                this.contract = v;
            });
    }

}
