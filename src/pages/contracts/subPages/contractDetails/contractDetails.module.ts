import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { ComponentModule } from '../../../../components';
import { ContractDetailsPage } from './contractDetails';

@NgModule({
    declarations: [
        ContractDetailsPage,
    ],
    imports: [
        IonicPageModule.forChild(ContractDetailsPage),
        ComponentModule,
    ],
    entryComponents: [
        ContractDetailsPage
    ]
})
export class ContractDetailsModule {}
