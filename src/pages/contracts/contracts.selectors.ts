import { createSelector } from '@ngrx/store';
import { keys, some, toArray, uniqBy, every, keyBy } from 'lodash';
import moment from 'moment'

import { currentParams } from '../../modules/router/router.selectors';
import { fields  as contractSearchConfigData } from '../search/searchConfig'


export const contracts = (state) => {
    return state['contracts'].contracts || [];
};

export const contractDetails = (state) => {
    return state['contracts'].details || {};
};

export const contractFilter = (state) => {
    return state['contracts'].filter || {};
};

export const contractSearch = (state) => {
    return state['contracts'].search || {};
};

export const contractsSearchConfig = () => {
    return keyBy(contractSearchConfigData, 'name')
  }

export const isShowingContractSearch = createSelector(
    contractSearch,
    (search) => {
        return some(keys(search), (searchKey) => {
            return search[searchKey] && search[searchKey] !== 'any';
        });
    }
);

export const getReferenceSet = (type, titleKey, valueKey) => {
  return createSelector(
    contactsArray,
    (contracts) => {
      const items = uniqBy(contracts, type);
      return items.map((i) => {
        return {
          ...i,
          title: i[titleKey],
          value: i[valueKey]
        };
      });
    }
  );
};

export const contactsArray = createSelector(
    contracts,
    (contracts) => {
        return toArray(contracts);
    }
);

export const contractsWithFlags = createSelector(
    contactsArray, contractFilter,
    (contracts, filter) => {
        return contracts.map((c) => {
            return {
                ...c,
                isOpen: c.STATUS_ID !== 'C'
            };
        }).filter(c => {
            if (filter.isOpen) {
                return c.isOpen;
            }
            return true;
        });
    }
);

export const visibleContracts = createSelector(
    contractsWithFlags, contractSearch, isShowingContractSearch, contractsSearchConfig,
    (contracts, search, isShowingSearch, searchConfig) => {
        if (isShowingSearch) {
            const searchKeys = keys(search);
            return contracts.filter((lifting) => {
                return every(searchKeys, (key) => {
                    if(!search[key]) {
                        return true
                    } else if(searchConfig[key].type === 'select-date') {
                      const value = search[key]
                      if(value.startDate && moment(lifting[key]).startOf('day').isBefore(moment(value.startDate))) {
                        return false
                      }
                      if(value.endDate && moment(lifting[key]).startOf('day').isAfter(moment(value.endDate))) {
                        return false
                      }
                      return true
                    }
                    return search[key] === lifting[key];
                });
            });
          }
          return contracts;
    }
  )

export const contract = createSelector(
    contractDetails, currentParams,
    (contractDetails, currentParams) => {
        const { id: contractId } = currentParams;
        return contractDetails[contractId];
    }
);
