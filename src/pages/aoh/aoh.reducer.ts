import { sortBy, uniqBy } from 'lodash';

export const INIT_AOH = 'INIT_AOH';
export const GET_AOH = 'GET_AOH';
export const GET_AOH_SUCCESS = 'GET_AOH_SUCCESS';
export const GET_AOH_FAILURE = 'GET_AOH_FAILURE';

export const GET_FLEXI_CONTRACTS = 'GET_FLEXI_CONTRACTS';
export const GET_FLEXI_CONTRACTS_SUCCESS = 'GET_FLEXI_CONTRACTS_SUCCESS';
export const GET_FLEXI_CONTRACTS_FAILURE = 'GET_FLEXI_CONTRACTS_FAILURE';

export const GET_TIMER = 'GET_TIMER';
export const GET_TIMER_SUCCESS = 'GET_TIMER_SUCCESS';



export const GET_CREDIT_POSITION = 'GET_CREDIT_POSITION';
export const GET_CREDIT_POSITION_SUCCESS = 'GET_CREDIT_POSITION_SUCCESS';

export const SET_AOH_FILTER = 'SET_AOH_FILTER';

export const GET_REFERENCE_STATUSES_TYPES = 'GET_REFERENCE_STATUSES_TYPES';
export const GET_REFERENCE_GROUPS = 'GET_REFERENCE_GROUPS';
export const GET_REFERENCE_PLANTS = 'GET_REFERENCE_PLANTS';

export const SET_REFERENCE_DATA = 'SET_REFERENCE_DATA';

const createAction = (type) => {
    return (payload = {}) => ({
        type,
        payload,
    });
};

export const initAoh = createAction(INIT_AOH);
export const getAoh = createAction(GET_AOH);
export const getAohSuccess = createAction(GET_AOH_SUCCESS);
export const getFlexiContracts = createAction(GET_FLEXI_CONTRACTS);
export const getFlexiContractsSuccess = createAction(GET_FLEXI_CONTRACTS_SUCCESS);

export const getTimer = createAction(GET_TIMER);
export const getTimerSuccess = createAction(GET_TIMER_SUCCESS);

export const getCreditPosition = createAction(GET_CREDIT_POSITION);
export const getCreditPositionSuccess = createAction(GET_CREDIT_POSITION_SUCCESS);

export const setAohFilter = createAction(SET_AOH_FILTER);

export const getReferenceStatusesTypes = createAction(GET_REFERENCE_STATUSES_TYPES);
export const getReferenceGroups = createAction(GET_REFERENCE_GROUPS);
export const getReferencePlants = createAction(GET_REFERENCE_PLANTS);
export const setReferenceData = createAction(SET_REFERENCE_DATA);

const initialState = ({
    prices: [],
    contracts: [],
    selectedFilter: null,
    recentFilters: [],
    creditPosition: null
});

export const removeDuplicateRecentFilters = (filters) => {
    // Construct a comp key from the recent filters
    filters = filters.filter(f => f);
    filters = filters.map((filterItem = { plants: [], products: [] }) => {
        filterItem.plants = sortBy(filterItem.plants, 'value') || [];
        filterItem.products = sortBy(filterItem.products, 'value') || [];
        const compKey = [...filterItem.plants, ...filterItem.products].reduce((prev, next) => {
            return prev + '-' + next['value'];
        }, '');
        return {
            ...filterItem,
            compKey,
        };
    });
    filters = uniqBy(filters, 'compKey');

    // Restrict length to 5
    return filters.slice(0, 5);
};

export function aohReducer(state = initialState, action) {
  switch (action.type) {
    case GET_AOH_SUCCESS:
        action.payload.T_MATERIAL_PRICES = action.payload.T_MATERIAL_PRICES.map((m) => {
            return {
                ...m,
                id: m.PLANT + '-' + m.MATERIAL_ID
            };
        });
        return {
            ...state,
            materials: action.payload.T_MATERIAL_PRICES
        };
    case GET_FLEXI_CONTRACTS_SUCCESS:
        const summary = action.payload.T_CONTRACT_SUMMARY.map((i) => {
            return {
                ...i,
                id: i.CONTRACT_NUMBER,
                PLANT_DESCRIPTION: i.PLANT_SHORT_ADDRESS,
                TARGET_QUANTITY: i.TOTAL_VOLUME,
                OPEN_QUANTITY: i.OPEN_VOLUME,
                LIFTED_PERC: (+i.OPEN_VOLUME/+i.TOTAL_VOLUME)*100,
                isOpen: i.OPEN_VOLUME !== i.TOTAL_VOLUME
            }
        })
        return {
            ...state,
            contracts: summary
        };
    case GET_TIMER_SUCCESS:
        return {
            ...state,
            timer: action.payload
        };
    case GET_CREDIT_POSITION_SUCCESS: {
        return {
            ...state,
            creditPosition: action.payload,
        };
    }
    case SET_AOH_FILTER:
        let newRecentFilters = state.recentFilters;
        // If there are no filters selected then don't push to recent list
        // If there are more than 5 filters in the history then remove the last
        if (action.payload.storeAsRecent && (action.payload.products.length || action.payload.plants.length)) {
            newRecentFilters = [action.payload, ...newRecentFilters];
        }
        newRecentFilters = removeDuplicateRecentFilters(newRecentFilters);
        return {
            ...state,
            selectedFilter: action.payload,
            recentFilters: newRecentFilters
        };
    default:
      return state;
  }
}
