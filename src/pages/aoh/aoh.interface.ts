export interface OFFERPRICE_RESPONSE {
    data: OFFERPRICE;
}

export interface MATERIAL {
        'MIN_QUANTITY': string;
        'ZS99_CONDITION_UNIT': string;
        'CUSTOMER_CONDITION_UNIT': string;
        'MARKET_CONDITION_UNIT': string;
        'ADDITIVE_MATERIAL': string;
        'END_DATE': string;
        'SHIP_TO_NUMBER': string;
        'ZD03_CONDITION_UNIT': string;
        'CUSTOMER_CONDITION_UNIT_EXT': string;
        'MAX_QUANTITY': string;
        'DEFAULT_LOCATION': string;
        'PLANT_SHORT_ADDRESS': string;
        'MATERIAL_ID': string;
        'PLANT': string;
        'CUSTOMER_PRICE_UNIT': string;
        'ZS99_PRICE_UNIT': string;
        'SHIP_TO_NAME': string;
        'LIFTING_PERIOD': string;
        'MATERIAL_DESCRIPTION': string;
        'ZD03_CONDITION_UNIT_DESC': string;
        'ZD03_PRICE': string;
        'VALID_PRICE': string;
        'LIMIT_TEXT': string;
        'MARKET_CONDITION_UNIT_DESC': string;
        'MARKET_PRICE': string;
        'ROUNDING_QUANTITY': string;
        'CUSTOMER_PRICE': string;
        'UOM_DESCRIPTION': string;
        'ZS99_CONDITION_UNIT_DESC': string;
        'START_DATE': string;
        'ZS99_PRICE': string;
        'PLANT_NAME': string;
        'MARKET_CONDITION_UNIT_EXT': string;
        'CUSTOMER_CONDITION_UNIT_DESC': string;
        'UOM_EXT': string;
        'CURRENCY': string;
        'ZD03_PRICE_UNIT': string;
        'ZD03_CONDITION_UNIT_EXT': string;
        'UOM': string;
        'MARKET_PRICE_UNIT': string;
        'ZS99_CONDITION_UNIT_EXT': string;
}
export interface OFFERPRICE {
    t_material_prices_count: number;
    T_MATERIAL_PRICES: MATERIAL[];
}
