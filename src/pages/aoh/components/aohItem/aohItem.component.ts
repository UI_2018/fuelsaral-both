import { Component, Input } from '@angular/core';
import { Store } from '@ngrx/store';

import { ModalController, NavController } from 'ionic-angular';

import { actions } from '../../../basket/basket.reducer';

import { MATERIAL } from '../../aoh.interface';

import { AddItemModal } from '../../../basket/modals/addItem/addItem';

@Component({
    selector: 'aoh-item',
    templateUrl: './aohItem.component.html',
    styles: ['./aohItem.component.scss']
})
export class AOHItem {
    @Input() material: MATERIAL;
    constructor(private store: Store<any>, public modalCtrl: ModalController, private navCtrl: NavController) {}

    addItem(itemToAdd) {
        this.store.dispatch(new actions.AddItem(itemToAdd));
    }


    openAddItem(material) {
        const updateItemModal = this.modalCtrl.create(AddItemModal, {
            material,
            onConfirm: this.addItem.bind(this),
            labels: {
                title: 'BASKET.ADD_ITEM',
                confirm: 'BASKET.ADD_ITEM_CONFIRM'
            }
        });
        updateItemModal.present();
    }

    viewOpenContracts() {
        this.navCtrl.push('aoh-details', this.material);
    }
}
