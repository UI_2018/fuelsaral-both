import { Component, ViewChild  } from '@angular/core';
import { Content, IonicPage, ModalController, NavController } from 'ionic-angular';


import { Store } from '@ngrx/store';
import { getAoh, setAohFilter } from './aoh.reducer';

import { LoadingService } from '../../modules/loading.provider';

import { aohFilterOptions, aohListings, aohRecentFilters, aohSelectedFilter } from './aoh.selectors';

import { virtualList } from '../../modules/device/device.selectors';

import { FilterPage } from './subPages/filter/filter';

@IonicPage({
  name: 'aoh'
})
@Component({
  selector: 'page-aoh',
  templateUrl: 'aoh.html'
})
export class AOHPage {
  materials$;
  selectedFilter;
  @ViewChild(Content) content: Content;
  @ViewChild('navBar') navBar;
  // @ViewChild('virtualScroll', { read: VirtualScroll }) virtualScroll: VirtualScroll;

  @ViewChild('masonaryContainer') masonaryContainer;

  constructor(public navCtrl: NavController, private store: Store<any>, private loadingService: LoadingService, private modalCtrl: ModalController) {
    this.materials$ = this.store.select(virtualList(aohListings));
    // this.materials$.subscribe((v) => {
      // if(this.virtualScroll) {
      //   setTimeout(() => {
      //     this.virtualScroll.readUpdate(true)
      //     this.virtualScroll.writeUpdate(true)
      //   }, 25)
      // }
    // })
    this.store.select(aohSelectedFilter)
      .subscribe((v) => {
        this.selectedFilter = v;
      });
  }

  ionViewDidLoad() {
    this.store.dispatch(getAoh());
  }

  openFilter() {
    this.modalCtrl.create(FilterPage, {
      options: aohFilterOptions,
      recents: aohRecentFilters,
      onSetFilter: setAohFilter,
      currentFilter: this.selectedFilter
    }, {
      cssClass: 'modal-right'
      // enterAnimation: 'ios-transition',
      // leaveAnimation: 'slide-in-right',

    }).present();
    // profileModal.present();
    // this.navCtrl.push('filter', {
    //   options: aohFilterOptions,
    //   recents: aohRecentFilters,
    //   onSetFilter: setAohFilter,
    // })
  }

  removePlantFilter(item) {
    const newFilters = {
      plants: this.selectedFilter.plants.filter((p) => p.title !== item.title),
      products: this.selectedFilter.products
    };
    this.store.dispatch(setAohFilter(newFilters));
  }

  removeProductFilter(item) {
    const newFilters = {
      products: this.selectedFilter.products.filter((p) => p.title !== item.title),
      plants: this.selectedFilter.plants
    };
    this.store.dispatch(setAohFilter(newFilters));
  }

  onFooterUpdate() {
    this.content.resize();
  }

  onCountdownComplete() {
    this.ionViewDidLoad();
  }
}
