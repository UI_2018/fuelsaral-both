import { createSelector } from '@ngrx/store';
import { keyBy, some, uniqBy, groupBy } from 'lodash';

import { basketItemIds } from '../basket/basket.selectors';

export const aohMaterials = (state) => {
    return state['aoh'].materials || [];
    // return state['prices'].prices || [];
};

export const aohFlexiContracts = (state) => {
    return state['aoh'].contracts || [];
};

export const aohSelectedFilter = (state) => {
    return state['aoh'].selectedFilter || null;
};

export const aohTimer = (state) => {
    return state['aoh'].timer || null;
};

export const aohRecentFilters = (state) => {
    return state['aoh'].recentFilters || null;
};


export const creditPosition = (state) => {
    return state['aoh'].creditPosition;
};

export const getOptions = (options, key, titleKey, valueKey) => {
    const items = uniqBy(options, key);
    const r =  items.map((i) => ({
        ...i,
        title: i[titleKey],
        value: i[valueKey]
    }));
    return r;
};

export const aohFilterOptions = createSelector(
    aohMaterials,
    (materials) => {
        if (!materials.length) {
            return {};
        }
        return {
            PLANTS: getOptions(materials, 'PLANT_NAME', 'PLANT_NAME', 'PLANT'),
            PRODUCTS: getOptions(materials, 'MATERIAL_DESCRIPTION', 'MATERIAL_DESCRIPTION', 'MATERIAL_ID')
        };
    }
);

export const aohListings = createSelector(
    aohMaterials, aohFlexiContracts, aohSelectedFilter, basketItemIds,
    (materials, contracts, aohSelectedFilter, basketItemIds) => {
        if (!materials.length || !contracts.length) {
            return [];

        }
        const contractMap = keyBy(contracts, (i) => `${i.MATERIAL_ID}-${i.PLANT}`);

        let materialsEnhanced = materials.map((m) => {
            return {
                ...m,
                contract: contractMap[`${m.MATERIAL_ID}-${m.PLANT}`],
            }
        });

        if (aohSelectedFilter) {
            materialsEnhanced = materialsEnhanced.filter((material) => {
                let matchesPlant = true;
                let matchesProduct = true;
                if (aohSelectedFilter.plants.length) {
                    matchesPlant = some(aohSelectedFilter.plants, (plant) => {
                        return material.PLANT === plant.PLANT;
                    });
                }
                if (aohSelectedFilter.products.length) {
                    matchesProduct = some(aohSelectedFilter.products, (product) => {
                        return material.MATERIAL_ID === product.MATERIAL_ID;
                    });
                }
                return matchesPlant && matchesProduct;
            });
        }

        materialsEnhanced = materialsEnhanced.map((material) => {
            return {
                ...material,
                inBasket: basketItemIds[material.id]
            };
        });

        return materialsEnhanced;
    }
);
