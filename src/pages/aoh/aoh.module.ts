import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { AOHPage } from './aoh';

import { ComponentModule } from '../../components';
import { AOHItem } from './components/aohItem/aohItem.component';

import { BasketModule } from '../basket/basket.module';
import { FilterModule } from './subPages/filter/filter.module';
import { DetailsModule } from './subPages/details/details.module';

@NgModule({
    declarations: [
        AOHPage,
        AOHItem,
    ],
    imports: [
        IonicPageModule.forChild(AOHPage),
        ComponentModule,
        BasketModule,
        FilterModule,
        DetailsModule,
    ],
    entryComponents: [
        AOHPage
    ]
})
export class AOHModule {}
