import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Action, Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import moment from 'moment';

import { App, LoadingController, NavController } from 'ionic-angular';

import { APIService } from '../../modules/serviceProvider';


import {
  GET_AOH,
  GET_CREDIT_POSITION,
  GET_FLEXI_CONTRACTS,
  GET_TIMER,
  getAohSuccess,
  getCreditPosition,
  getCreditPositionSuccess,
  getFlexiContracts,
  getFlexiContractsSuccess,
  getTimer,
  getTimerSuccess
} from './aoh.reducer';

// import { selectedCustomer } from './aoh.selectors'

import { OFFERPRICE_RESPONSE } from './aoh.interface';

@Injectable()
export class AOHEffects {
  private loader: any;

  @Effect()
  aohList$: Observable<Action> = this.actions$.pipe(
    ofType(GET_AOH),
    mergeMap(action => {
        return this.api.aoh()
            .mergeMap((resp: OFFERPRICE_RESPONSE) => {
              return [
                getAohSuccess(resp.data),
                getTimer(),
                // getFlexiContracts(),
                // getCreditPosition(),
              ];
            });
    })
  );

  @Effect()
  flexiContracts$: Observable<Action> = this.actions$.pipe(
    ofType(GET_FLEXI_CONTRACTS),
    mergeMap(action => {
        return this.api.flexiContracts()
          .pipe(
            map((resp: OFFERPRICE_RESPONSE) => {
              return getFlexiContractsSuccess(resp.data);
            })
          );
    })
  );

  @Effect()
  timer$: Observable<Action> = this.actions$.pipe(
    ofType(GET_TIMER),
    mergeMap(action => {
        return this.api.timer()
          .pipe(
            map((resp: any) => {

              // tweaking system and period end time for demo, todo: revert
              (function () {

                const systemTime = moment(new Date().getTime() % (24 * 60 * 60 * 1000));
                const periodEndTime = moment(systemTime)
                  .add(15, 'minutes')
                  .minute(45)
                  .second(0)
                  .millisecond(0);

                resp.data.EV_SYSTEM_TIME = systemTime.utc().toString();
                resp.data.EV_PERIOD_END_TIME = periodEndTime.utc().toString();

              })();

              const msDate = new Date(resp.data.EV_SYSTEM_DATE).getTime();
              const msToAdd = new Date(resp.data.EV_SYSTEM_TIME).getTime();

              const offset = msDate + msToAdd - Date.now();

              moment['now'] = function() {
                return moment(Date.now() + offset).valueOf();
              };
              return getTimerSuccess(resp.data);
            })
          );
    })
  );

  @Effect()
  creditPosition$: Observable<Action> = this.actions$.pipe(
    ofType(GET_CREDIT_POSITION),
    mergeMap(action => {
      return this.api.creditPosition()
        .pipe(
          map((resp: any) => {
            return getCreditPositionSuccess(resp.data);
          })
        );
    })
  );


  constructor(
    private http: HttpClient,
    private api: APIService,
    private actions$: Actions,
    private app: App,
    private store: Store<any>,
    public loadingCtrl: LoadingController) {

  }
  get navCtrl(): NavController {
    return this.app.getRootNav();
  }

  changeRouteIfRequired(target, customerCount) {
    const activePage = this.navCtrl.getActive();
    if (target !== activePage.id) {
      if (target !== 'customers' && customerCount) {
        this.navCtrl.setPages([{ page: 'customers' }, { page: target }]);
      } else {
        this.navCtrl.setRoot(target);
      }
    }
  }
}
