import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { ComponentModule } from '../../../../components';
import { DetailsPage } from './details';

@NgModule({
    declarations: [
        DetailsPage,
    ],
    imports: [
        IonicPageModule.forChild(DetailsPage),
        ComponentModule,
    ],
    entryComponents: [
        DetailsPage
    ]
})
export class DetailsModule {}
