import { Component } from '@angular/core';

import { Store } from '@ngrx/store';
import { currentParams } from '../../../../modules/router/router.selectors';

import { IonicPage } from 'ionic-angular';

@IonicPage({
    name: 'aoh-details',
    segment: 'aoh/details',
    defaultHistory: ['aoh'],
})
@Component({
    templateUrl: './details.html'
})
export class DetailsPage {
    material;
    constructor(private store: Store<any>) {
        this.store.select(currentParams)
            .subscribe((v) => {
                this.material = v;
            });
    }
}
