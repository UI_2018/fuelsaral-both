import { Component, Input } from '@angular/core';
import { keys } from 'lodash';

const template = `
    <div *ngIf="filter">
        <div class="row" *ngFor="let item of rowsToDisplay">
            <div class="filter-title flex-1">{{item.title}}</div>
            <div class="additional-count" *ngIf="item.additional">+{{item.additional}}</div>
        </div>
    </div>
`;

const isUniqueTypeOfFilter = (filters) => {
    const filterKeys = keys(filters);
    filterKeys.reduce((prev, key) => {
        if (Array.isArray(filters[key]) && filters[key].length) {
            prev = prev;
        }
        return prev;
    }, []);
};

@Component({
    selector: 'recent-filter',
    styles: ['./recentFilter.component.scss'],
    template
})
export class RecentFilter {
    @Input() filter;

    get rowsToDisplay() {
        const filterKeys = keys(this.filter);
        const groupsToShow = filterKeys.reduce((prev, next) => {
            if (Array.isArray(this.filter[next]) && this.filter[next].length) {
                prev = [...prev, this.filter[next]];
            }
            return prev;
        }, []);

        if (groupsToShow.length === 1 && groupsToShow[0].length >= 2) {
            return [
                { title: groupsToShow[0][0].title, value: groupsToShow[0][0].value },
                { title: groupsToShow[0][1].title, value: groupsToShow[0][1].value, additional:  groupsToShow[0].length - 2 }
            ];
        }

        return groupsToShow.reduce((prev, next) => {

            return [...prev, {
                title: next[0].title, value: next[0].value, additional: next.length - 1
            }];
        }, []);
    }

}
