import { Component } from '@angular/core';
import { FormBuilder, FormControl }   from '@angular/forms';

import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Store } from '@ngrx/store';


@IonicPage({
    name: 'filter',
    segment: 'aoh/filter',
    defaultHistory: ['aoh'],
})
@Component({
    templateUrl: './filter.html',
    styles: ['./filter.scss']
})
export class FilterPage {
    newStory;
    myForm;

    someForm;
    value = [{ title: 'option1' }];

    filterOptions;
    recentFilters;

    options = [{ title: 'option1' }, { title: 'option2' }];

    constructor(private store: Store<any>, private fb: FormBuilder, private navCtrl: NavController, private navParams: NavParams, private viewCtrl: ViewController) {}
    ngOnInit() {
        this.filterOptions = this.store.select(this.navParams.get('options'));
        this.recentFilters = this.store.select(this.navParams.get('recents'));
        const selectedFilter: any = this.navParams.get('currentFilter');
        this.buildForm(selectedFilter);
    }
    buildForm(data: any) {
        const form = this.fb.group({
            plants: new FormControl([], null),
            products: new FormControl([], null)
        });
        if (!this.myForm) {
            this.myForm = form;
            if (data) {
                this.myForm.patchValue(data);
            }
        }
    }

    handleRecentFilter(filter) {
        this.myForm.patchValue(filter);
    }
    submit() {
        // this.store.dispatch(setAohFilter({
        this.store.dispatch(this.navParams.get('onSetFilter')({
            ...this.myForm.value,
            storeAsRecent: true,
        }));
        this.navCtrl.pop();
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
}
