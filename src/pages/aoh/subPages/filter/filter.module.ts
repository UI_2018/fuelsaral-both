import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { FilterPage } from './filter';
import { ComponentModule } from '../../../../components';

import { RecentFilter } from './components/recentFilter.component';

import { ConnectFormDirective } from '../../../../modules/forms/connectForm.directive';

@NgModule({
    declarations: [
        FilterPage,
        RecentFilter,
        ConnectFormDirective,
    ],
    imports: [
        IonicPageModule.forChild(FilterPage),
        ComponentModule,
    ],
    entryComponents: [
        FilterPage
    ]
})
export class FilterModule {}
