import { Component } from '@angular/core';
import {  IonicPage, ModalController } from 'ionic-angular';
import { Store } from '@ngrx/store';

import { SearchPage } from '../search/search';
import { isShowingLoadIdsSearch, loadIdsSearch as loadIdsSearchSelector, visibleLoadIds } from './loadIds.selectors';
import { getLoadIds, setLoadIdsSearch } from './loadids.reducer';

import { loadIdsSearch } from '../search/searchConfig';
import { virtualList } from '../../modules/device/device.selectors';

@IonicPage({
    name: 'loadids'
})
@Component({
  selector: 'page-loadids',
  templateUrl: 'loadids.html',
  styles: ['./loadids.scss']
})
export class LoadIdsPage {
  searchValues = {};
  showingSearchResults$;
  loadIdsSearch = loadIdsSearch;
  loadIds$;

  constructor(
    public store: Store<{}>,
    private modalCtrl: ModalController
  ) {
    this.loadIds$ = store.select(virtualList(visibleLoadIds));
    this.store.select(loadIdsSearchSelector).subscribe((v) => {
      this.searchValues = v;
    });
    this.showingSearchResults$ = this.store.select(isShowingLoadIdsSearch);

    this.store.dispatch(getLoadIds());
  }

  openSearch() {
    this.modalCtrl.create(SearchPage, {
      fields: this.loadIdsSearch.fields,
      onSearch: this.onSearch.bind(this),
      values: this.searchValues
    }).present();
  }

  onSearch(values) {
    this.store.dispatch(setLoadIdsSearch(values));
  }

  identify(index, item) {
    return item.LOAD_ID;
  }

}
