import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { LoadIdsPage } from './loadids';

import { ComponentModule } from '../../components';

@NgModule({
    declarations: [
        LoadIdsPage
    ],
    imports: [
        IonicPageModule.forChild(LoadIdsPage),
        ComponentModule
    ],
    entryComponents: [
        LoadIdsPage
    ]
})
export class LoadIdsModule {}
