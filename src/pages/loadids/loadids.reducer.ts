import { createAction } from '../../utils/storeUtils';

const initialState = {
    loadIds: [
    ],
    search: {}
};

export const GET_LOAD_IDS = 'GET_LOAD_IDS';
export const GET_LOAD_IDS_SUCCESS = 'GET_LOAD_IDS_SUCCESS';

export const SET_LOADIDS_SEARCH = 'SET_LOAD_IDS_SEARCH';

export const getLoadIds = createAction(GET_LOAD_IDS);
export const getLoadIdsSuccess = createAction(GET_LOAD_IDS_SUCCESS);
export const setLoadIdsSearch = createAction(SET_LOADIDS_SEARCH);


export function loadIdsReducer(state = initialState, { type, payload }) {
    switch (type) {
        case GET_LOAD_IDS_SUCCESS:
            return {
                ...state,
                loadIds: payload.T_LOAD_ID_LIST
            };
        case SET_LOADIDS_SEARCH:
            return {
                ...state,
                search: payload,
            };
        default:
            return state;
    }
}
