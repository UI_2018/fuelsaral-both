import { createSelector } from '@ngrx/store';
import { keys, some, uniqBy, every } from 'lodash';

export const loadIds = (state) => {
    return state['loadids'].loadIds;
};

export const loadIdsSearch = (state) => {
    return state['loadids'].search || {};
};

export const isShowingLoadIdsSearch = createSelector(
    loadIdsSearch,
    (search) => {
        return some(keys(search), (searchKey) => {
            return search[searchKey] && search[searchKey] !== 'any';
        });
    }
);

export const visibleLoadIds = createSelector(
    loadIds, loadIdsSearch, isShowingLoadIdsSearch,
    (loadIds, search, isShowingLoadIdsSearch) => {
        if (isShowingLoadIdsSearch) {
            const searchKeys = keys(search);
            return loadIds.filter((loadId) => {
                return every(searchKeys, (key) => {
                    if(!search[key]) {
                        return true
                    }
                    return search[key] === loadId[key];
                });
            });
        }
        return loadIds;
    }
);

export const getReferenceSet = (type, titleKey, valueKey) => {
    return createSelector(
        loadIds,
        (loadIds) => {
            const items = uniqBy(loadIds, type);
            return items.map((i) => {
                return {
                    ...i,
                    title: i[titleKey],
                    value: i[valueKey]
                };
            });
        }
    );
};
