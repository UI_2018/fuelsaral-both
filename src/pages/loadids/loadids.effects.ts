import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';


import { APIService } from '../../modules/serviceProvider';

import { GET_LOAD_IDS, getLoadIdsSuccess } from './loadids.reducer';


@Injectable()
export class LoadIdsEffects {

  @Effect()
  loadIds$: Observable<Action> = this.actions$.pipe(
    ofType(GET_LOAD_IDS),
    mergeMap(action => {
        return this.api.loadIds()
          .pipe(
            map((resp: any) => {
              return getLoadIdsSuccess(resp.data);
            })
          );
    })
  );

  constructor(
    private api: APIService,
    private actions$: Actions,
    private store: Store<any>,) {

  }
}
