import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { PlaygroundPage } from './playground';
import { ComponentModule } from '../../components';


@NgModule({
    declarations: [
        PlaygroundPage,
    ],
    imports: [
        IonicPageModule.forChild(PlaygroundPage),
        ComponentModule,
    ],
    entryComponents: [
        PlaygroundPage
    ]
})
export class PlaygroundModule {}
