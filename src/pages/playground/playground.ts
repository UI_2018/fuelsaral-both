import { Component } from '@angular/core';

import { IonicPage } from 'ionic-angular';

@IonicPage({
    name: 'playground'
})
@Component({
    templateUrl: './playground.html',
})
export class PlaygroundPage {

}
