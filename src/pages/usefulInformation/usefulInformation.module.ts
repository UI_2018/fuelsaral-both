import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { UsefulInformationPage } from './usefulInformation';
import { ComponentModule } from '../../components';

import { ContactsModule } from './subPages/contacts/contacts.module';
import { ProductCatalogueModule } from './subPages/productCatalogue/productCatalogue.module';
import { ProductDetailsModule } from './subPages/productDetails/productDetails.module';

@NgModule({
    declarations: [
        UsefulInformationPage
    ],
    imports: [
        IonicPageModule.forChild(UsefulInformationPage),
        ContactsModule,
        ProductCatalogueModule,
        ProductDetailsModule,
        ComponentModule,
    ],
    entryComponents: [
        UsefulInformationPage
    ]
})
export class UsefulInformationModule {}
