import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { EmailComposer } from '@ionic-native/email-composer';
import { CallNumber } from '@ionic-native/call-number';

import { NavigationService } from '../../app/navigation';

import { getResourceUrl } from '../../utils/cordova.helpers.js';

const PAGE = 'PAGE';
const PDF = 'PDF';
const XLS = 'XLS';
const EXTERNAL_URL = 'EXTERNAL_URL';

const list = [
    // {
    //     title: 'USEFUL_INFORMATION.CONTACTS',
    //     items: [
    //         { title: 'USEFUL_INFORMATION.SALES', type: PAGE, path: 'contacts', params: { type: 'sales' } },
    //         { title: 'USEFUL_INFORMATION.B2B', type: PAGE, path: 'contacts', params: { type: 'business' } },
    //     ]
    // },
    {
        title: 'USEFUL_INFORMATION.MARKET_INFORMATION',
        items: [
            { title: 'USEFUL_INFORMATION.MARKET_INFORMATION', icon: 'pdf', path: 'market information.pdf', type: PDF }
        ]
    },
    {
        title: 'USEFUL_INFORMATION.PRODUCT',
        items: [
            { title: 'USEFUL_INFORMATION.CARGO', icon: 'pdf', path: 'previous cargo.pdf', description: 'USEFUL_INFORMATION.CARGO_DESCRIPTION', type: XLS },
            { title: 'USEFUL_INFORMATION.DISTRIBUTION_CHANNELS', icon: 'pdf', path: 'distribution channels.pdf', type: XLS },
            { title: 'USEFUL_INFORMATION.TERMINALS', icon: 'pdf', description: 'USEFUL_INFORMATION.TERMINALS_DESCRIPTION', type: PDF },
            { title: 'USEFUL_INFORMATION.PRODUCT_CATALOGUE', icon: 'arrow-blue', description: 'USEFUL_INFORMATION.PRODUCT_CATALOGUE_DESCRIPTION', type: PAGE, path: 'product-catalogue' },
        ]
    },
    {
        title: 'USEFUL_INFORMATION.CLAIMS',
        items: [
            { title: 'USEFUL_INFORMATION.PRODUCT_CLAIMS', icon: 'pdf', path: 'product claims handbook.pdf', type: PDF },
            { title: 'USEFUL_INFORMATION.PRODUCT_CLAIMS_HEATING_OIL', icon: 'pdf', path: 'product claims heating.pdf', type: PDF },
            { title: 'USEFUL_INFORMATION.PRODUCT_CLAIMS_FUEL', icon: 'pdf', path: 'product claims fuels.pdf', type: PDF }
        ]
    },
    {
        title: 'USEFUL_INFORMATION.MARKETING',
        items: [
            { title: 'USEFUL_INFORMATION.WEB_SHOP', icon: 'liftings', type: EXTERNAL_URL, path: 'http://aral-webshop.fabs.global' },
        ]
    },
];

@IonicPage({
    name: 'useful-information',
    segment: 'useful-information'
})
@Component({
    templateUrl: './usefulInformation.html',
    styles: ['./usefulInformation.scss']
})
export class UsefulInformationPage {
    private list = list;
    contacts = [{ 'title': 'Arne Fischer', 'email': 'arne.fischer@de.bp.com', 'phone': '02343150' }];
    constructor(
        private document: DocumentViewer,
        private navService: NavigationService,
        private inAppBrowser: InAppBrowser,
        private emailComposer: EmailComposer,
        private callNumber: CallNumber, ) { }

    openDocument() {
        console.log('OPEN');
        const options: DocumentViewerOptions = {
            title: 'My PDF'
        };
        console.log('OPENING');
        const path = getResourceUrl('/assets/pdf/example.pdf');
        this.document.canViewDocument(
            path,
            'application/pdf',
            options,
            () => (console.log('SHOW')),
            () => (console.log('CLOSE')),
            () => (console.log('MISSING')),
            () => (console.log('ERROR'))
        );
        this.document.viewDocument(path, 'application/pdf', options);
    }

    onClick(item) {
        switch (item.type) {
            case 'PDF':
                this.navService.openFile('/assets/useful/' + item.path, item.title);
                break;
            case 'XLS':
                this.navService.openFile('/assets/useful/' + item.path, item.title);
                break;
            case 'EXTERNAL_URL':
                const browser = this.inAppBrowser.create(item.path, '_system');
                break;
            case 'PAGE':
                this.navService.push(item.path, item.params);
                break;
            default:
                break;
        }
    }

    dial(contact) {
        this.callNumber.isCallSupported()
            .then(function(response) {
                if (response == true) {
                    this.callNumber.callNumber(contact.phone, true);
                } else {
                    alert('Not supported');
                }
            });
    }

    sendEmail(contact) {
        const email = {
            to: contact.email,
            isHtml: true
        };

        this.emailComposer.open(email);
    }
}
