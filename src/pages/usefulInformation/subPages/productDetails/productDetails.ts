import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';

import { NavigationService } from '../../../../app/navigation';

const list = [
    { title: 'PRODUCT_DETAILS.TECHINCAL', icon: 'pdf', link: 'product' },
    { title: 'PRODUCT_DETAILS.MSDS', icon: 'pdf', link: 'msds' },
];

@IonicPage({
    name: 'product-details',
    segment: 'product-details',
    defaultHistory: ['useful-information', 'product-catalogue']
})
@Component({
    templateUrl: './productDetails.html',
})
export class ProductDetailsPage {
    list = list;

    product;
    constructor(private navService: NavigationService, private navParams: NavParams) {
        this.product = navParams.get('product');
    }

    onClick(link) {
        if (this.product[link]) {
            const title = this.product.title + ' - ' + (link === 'product' ? 'Technische Kundeninformation' : 'Sicherheitsdatenblatt')
            this.navService.openFile('/assets/useful/'+this.product[link], title );
        }
    }

}
