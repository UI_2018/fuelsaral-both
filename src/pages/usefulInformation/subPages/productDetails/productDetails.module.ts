import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { ProductDetailsPage } from './productDetails';
import { ComponentModule } from '../../../../components';


@NgModule({
    declarations: [
        ProductDetailsPage
    ],
    imports: [
        IonicPageModule.forChild(ProductDetailsPage),
        ComponentModule,
    ],
    entryComponents: [
        ProductDetailsPage
    ]
})
export class ProductDetailsModule {}
