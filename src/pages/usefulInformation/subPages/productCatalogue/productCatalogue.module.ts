import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { ProductCataloguePage } from './productCatalogue';
import { ComponentModule } from '../../../../components';


@NgModule({
    declarations: [
        ProductCataloguePage
    ],
    imports: [
        IonicPageModule.forChild(ProductCataloguePage),
        ComponentModule,
    ],
    entryComponents: [
        ProductCataloguePage
    ]
})
export class ProductCatalogueModule {}
