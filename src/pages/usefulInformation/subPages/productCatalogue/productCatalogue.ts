import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

import { NavigationService } from '../../../../app/navigation';

const products = [

    { title: 'Aral HeizölEco', product: 'Aral HeizoelEco_productsheet.pdf', msds: 'Aral HeizolEco_MSDS.pdf' },
    { title: 'Aral HeizölEcoPlus', product: 'Aral HeizolEcoPlus_productsheet.pdf', msds: 'Aral HeizolEcoPlus_MSDS.pdf' },
    { title: 'Aral Super', product: 'Aral Super 95_productsheet.pdf', msds: 'Aral Super 95_MSDS.pdf' },
    { title: 'Aral SuperDiesel', product: 'Aral SuperDiesel_productsheet.pdf', msds: 'Aral SuperDiesel_MSDS.pdf' },
    { title: 'Aral SuperPlus', product: 'Aral SuperPlus_productsheet.pdf', msds: 'Aral SuperPlus_MSDS.pdf' },
    { title: 'Aral Super 95 E10', product: 'Aral Super 95 E10_productsheet.pdf', msds: 'Aral Super 95 E10_MSDS.pdf' },
    {title: "Heizöl EL", product: "Heizîl EL.pdf", msds: "Heizîl EL msds.pdf"},
    {title: "Heizöl schwefelarm", product: "Heizîl schwefelarm.pdf", msds: "Heizîl schwefelarm msds.pdf"},
    {title: "Super ohne Additiv", product: "Super ohne Additiv.pdf", msds: "Super ohne Additiv msds.pdf"},
    {title: "Super mit Additiv", product: "Super mit Additiv.pdf", msds: "Super mit Additiv msds.pdf"},
    {title: "Super Plus ohne Additiv", product: "Super Plus ohne Additiv.pdf", msds: "Super Plus ohne Additiv msds.pdf"},
    {title: "Super Plus mit Additiv", product: "Super Plus mit Additiv.pdf", msds: "Super Plus mit Additiv msds.pdf"},
    {title: "Diesel ohne Additiv", product: "Diesel ohne Additiv.pdf", msds: "Diesel ohne Additiv msds.pdf"},
    {title: "Diesel mit Additiv", product: "Diesel mit Additiv.pdf", msds: "Diesel mit Additiv msds.pdf"},
    {title: "Petroleum", product: "Petroleum.pdf", msds: "Petroleum msds.pdf"},
    {title: "Super 95 E10 ohne Additiv", product: "Super 95 E10 ohne Additiv.pdf", msds: "Super 95 E10 ohne Additiv msds.pdf"},
    {title: "Super 95 E10 mit Additiv", product: "Super 95 E10 mit Additiv.pdf", msds: "Super 95 E10 mit Additiv msds.pdf"},
    {title: "Seehafendiesel", product: "Seehafendiesel.pdf", msds: "Seehafendiesel msds.pdf"},
    {title: "Schiffsbetriebsstoff", product: "Schiffsbetriebsstoff.pdf", msds: "Schiffsbetriebsstoff msds.pdf"}
];

@IonicPage({
    name: 'product-catalogue',
    segment: 'product-catalogue',
    defaultHistory: ['useful-information']
})
@Component({
    templateUrl: './productCatalogue.html',
})
export class ProductCataloguePage {
    products = products;
    constructor(private navService: NavigationService) {

    }

    openProduct(product) {
        this.navService.push('product-details', { product });
    }

}
