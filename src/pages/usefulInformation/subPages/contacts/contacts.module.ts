import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { ContactsPage } from './contacts';
import { ComponentModule } from '../../../../components';


@NgModule({
    declarations: [
        ContactsPage
    ],
    imports: [
        IonicPageModule.forChild(ContactsPage),
        ComponentModule,
    ],
    entryComponents: [
        ContactsPage
    ]
})
export class ContactsModule {}
