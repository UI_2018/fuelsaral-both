import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';

import { Store } from '@ngrx/store';

import { currentParams } from '../../../../modules/router/router.selectors';

const contacts = {
    sales: [
        { 'title': 'Arne Fischer', 'email': 'arne.fischer@de.bp.com', 'phone': '02343150' },
    ],
    business: [
        { 'title': 'Arne Fischer', 'email': 'arne.fischer@de.bp.com', 'phone': '02343150' }
    ]
};
const titles = {
    sales: 'PAGE_TITLE.CONTACTS_SALES',
    business: 'PAGE_TITLE.CONTACTS_BUSINESS'
};

@IonicPage({
    name: 'contacts',
    segment: 'contacts/:type',
    defaultHistory: ['useful-information']
})
@Component({
    templateUrl: './contacts.html',
})
export class ContactsPage {
    contacts = [];
    titleKey = null;
    constructor(private callNumber: CallNumber, private store: Store<any>) {
        this.store.select(currentParams)
            .subscribe((v) => {
                this.contacts = contacts[v.type];
                this.titleKey = titles[v.type];
            });
    }

    dial(contact) {
        this.callNumber.isCallSupported()
            .then(function(response) {
                if (response == true) {
                    this.callNumber.callNumber(contact.phone, true);
                } else {
                    alert('Not supported');
                }
            });
    }

}
