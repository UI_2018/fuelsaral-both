import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';

import { NavigationService } from '../../app/navigation';

import { getResourceUrl } from '../../utils/cordova.helpers.js';

const list = [
    {
        title: 'PAGE_TITLE.HSSE',
        items: [
            { title: 'HSSE.TRAFFIC_SAFETY', icon: 'arrow-blue', type: 'PAGE', path: 'hsse-item', params: {id: 'group1'} },
            { title: 'HSSE.INCIDENT', icon: 'arrow-blue', type: 'PAGE', path: 'hsse-item', params: {id: 'group2'} },
        ]
    }
];

@IonicPage({
    name: 'hsse',
})
@Component({
    templateUrl: './hsse.html',
    styles: ['./hsse.scss']
})
export class HSSEPage {
    private list = list;

    constructor(private document: DocumentViewer, private navService: NavigationService) { }

    openDocument() {
        const options: DocumentViewerOptions = {
            title: 'My PDF'
        };
        const path = getResourceUrl('/assets/pdf/example.pdf');
        this.document.canViewDocument(
            path,
            'application/pdf',
            options,
            () => (console.log('SHOW')),
            () => (console.log('CLOSE')),
            () => (console.log('MISSING')),
            () => (console.log('ERROR'))
        );
        this.document.viewDocument(path, 'application/pdf', options);
    }

    onClick(item) {
        switch (item.type) {
            case 'PDF':
                this.navService.openFile('/assets/useful/' + item.path);
                break;
            case 'XLS':
                this.navService.openFile('/assets/useful/' + item.path);
                break;
            case 'EXTERNAL_URL':
                // const browser = this.inAppBrowser.create(item.path, '_system');
                break;
            case 'PAGE':
                this.navService.push(item.path, item.params);
                break;
            default:
                break;
        }
    }
}
