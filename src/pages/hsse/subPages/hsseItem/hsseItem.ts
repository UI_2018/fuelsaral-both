import { Component } from '@angular/core';

import { Store } from '@ngrx/store';

import { IonicPage, NavParams } from 'ionic-angular';

import { NavigationService } from '../../../../app/navigation';

const lists = {
    group2: [
        { 
            title: 'Arbeits- und Verkehrssicherheit',
            items: [
                {title: 'Notfallplan MVP'},
                {title: 'Meldeformular Vorfallbericht'}
            ]
        }
    ],
    group1 : [
    {
        title: 'Allgemein',
        items: [
            {
                "title": "Arbeitsschutz Verantwortung Haftung",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            },
            {
                "title": "Arbeitssicherheitsfachkraft Warum Info",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitssicherheitsfachkraft_Warum_Info.pdf"
            },
            {
                "title": "Beratung des Unternehmers nach dem Asig",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Beratung_des_Unternehmers_nach_dem_Asig.pdf"
            },
            {
                "title": "BGV A1 Praeventation",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/BGV_A1_Praeventation.pdf"
            }
        ]
    },{
        title: 'Allgemein',
        items: [{
                "title": "15 Grundregeln für den Strassentransport",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/15%20Regeln%20ST%20Deutsch.pdf"
            },
            {
                "title": "Die sichere Belieferung-Mindestanforderungen-Stand 14Mz08",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Fuhrpark/Allgemein/Die_sichere_Belieferung_-_Mindestanforderungen_-_Stand_14Mz08.pdf"
            },
            {
                "title": "TKW Fahrer Handbuch",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Beratung_des_Unternehmers_nach_dem_Asig.pdf"
            }
        ]
    },{
        title: 'Fahrer', 
        items: [{
                "title": "Fahrergesundheit Herzinfarkt Flyer",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            },
            {
                "title": "Herzinfarkt Fahrergesundheit",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitssicherheitsfachkraft_Warum_Info.pdf"
            },
            {
                "title": "Sturzgefahren auf Tankfahrzeugen",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Beratung_des_Unternehmers_nach_dem_Asig.pdf"
            }
        ]
    }, {
        title: 'Fahrzeug',
        items: [{
                "title": "Sicheres Kuppeln von Fahrzeugen",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            },
            {
                "title": "Explosionsschutz Tkw",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitssicherheitsfachkraft_Warum_Info.pdf"
            },
            {
                "title": "BGV D29 Fahrzeuge",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Beratung_des_Unternehmers_nach_dem_Asig.pdf"
            }
        ]
    }, {
        title: 'Ladung',
        items: [{
                "title": "Betriebsanweisung Fahrzeuge",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            },
            {
                "title": "Die sichere Belieferung - Mindestanforderungen",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitssicherheitsfachkraft_Warum_Info.pdf"
            }
        ]
    }, {
        title: 'Schulungsunterlagen',
        items: [
            {
                "title": "Sicherheit täglich 12 Regeln",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            }
        ]
    }, {
        title: 'Blickpunkt Sicherheit',
        items: [{
                "title": "Batterietanklagen",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            },
            {
                "title": "Bergung",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            },
            {
                "title": "Blind GWG",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            },
            {
                "title": "Erschöpfung",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            },
            {
                "title": "Füllleitung",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            },
            {
                "title": "Gaspendeln",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            },
            {
                "title": "Hilfsbereitschaft",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            },
            {
                "title": "Kabelbrand",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            },
            {
                "title": "Ladungssicherung",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            },
            {
                "title": "Pylone",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            },
            {
                "title": "Rechtskataster",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            },
            {
                "title": "Rollover",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            },
            {
                "title": "Schlauchleitungen / Schlauchprüfungen",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            },
            {
                "title": "Sicherheitsgerechte Belieferung",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            },
            {
                "title": "Überblick behalten",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            },
            {
                "title": "Umpumpen Hänger",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            },
            {
                "title": "Wetter",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            },
            {
                "title": "Winter",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            },
            {
                "title": "Sicheres Verhalten",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            },
            {
                "title": "Unachtsamkeit",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            }
        ]
    }, {
        title: 'Vordrucke, Muster, Checklisten',
        items: [{
                "title": "7 Punkte damit es gut läuft DIN A4",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitsschutz_Verantwortung_Haftung.pdf"
            },
            {
                "title": "Checkliste Verbandskasten mz07",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Arbeitssicherheitsfachkraft_Warum_Info.pdf"
            },
            {
                "title": "Muster Gefahrstoffverzeichnis",
                "link": "https://sapefswd.bpweb.bp.com:8131/irj/go/km/docs//pccdesign/Aral/MVP/MVP/HSSE/Shared%20Content/Arbeits%20und%20Verkehrssicherheit/Allgemein/Beratung_des_Unternehmers_nach_dem_Asig.pdf"
            }
        ]
    }
]};

@IonicPage({
    name: 'hsse-item',
    segment: 'hsse/:id',
    // defaultHistory: ['hsse'],
})
@Component({
    templateUrl: './hsseItem.html',
    // styles: ['./contractDetails.scss']
})
export class HSSEItemPage {
    private list
    constructor(private store: Store<any>, private navService: NavigationService, private navParams: NavParams) {
        const group = navParams.get('id');
        this.list = lists[group]
    }
    onClick(item) {
        this.navService.openFile('/assets/hsse/' + item.title + '.pdf');

        // switch (item.type) {
        //     // case 'PDF':
        //         // break;
        //     case 'XLS':
        //         this.navService.openFile('/assets/useful/' + item.path);
        //         break;
        //     case 'EXTERNAL_URL':
        //         // const browser = this.inAppBrowser.create(item.path, '_system');
        //         break;
        //     case 'PAGE':
        //         this.navService.push(item.path, item.params);
        //         break;
        //     default:
        //         break;
        // }
    }
}
