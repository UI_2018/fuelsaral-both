import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { ComponentModule } from '../../../../components';
import { HSSEItemPage } from './hsseItem';

@NgModule({
    declarations: [
        HSSEItemPage,
    ],
    imports: [
        IonicPageModule.forChild(HSSEItemPage),
        ComponentModule,
    ],
    entryComponents: [
        HSSEItemPage
    ]
})
export class HSSEItemModule {}
