import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { HSSEPage } from './hsse';
import { ComponentModule } from '../../components';
import { HSSEItemModule } from './subPages/hsseItem/hsseItem.module'

@NgModule({
    declarations: [
        HSSEPage
    ],
    imports: [
        IonicPageModule.forChild(HSSEPage),
        HSSEItemModule,
        ComponentModule,
    ],
    entryComponents: [
        HSSEPage
    ]
})
export class HSSEModule {}
