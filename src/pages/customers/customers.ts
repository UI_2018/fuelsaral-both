import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Store } from '@ngrx/store';

import { customersAsArray } from './customers.selectors';
import { setCustomer } from './customers.reducer';

import { getCustomers } from './customers.reducer';

@IonicPage({
    name: 'customers',
})
@Component({
    templateUrl: './customers.html'
})
export class CustomersPage {
    target;
    customers$;
    constructor(private store: Store<any>, private navCtrl: NavController, private navParams: NavParams) {
        this.customers$ = this.store.select(customersAsArray);

        this.target = this.navParams.get('target');

        this.customers$.subscribe(v => {
            if (v.length === 1) {
                this.store.dispatch(setCustomer(v[0]));
                this.navCtrl.setRoot(this.target);
            }
        });
    }
    ngOnInit() {
        this.store.dispatch(getCustomers());
    }
    selectCustomer(customer) {
        this.store.dispatch(setCustomer(customer));
        this.navCtrl.push(this.target);
    }
}
