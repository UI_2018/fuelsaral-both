import { createSelector } from '@ngrx/store';

import { values } from 'lodash';

export const customers = (state) => {
    return state['customers'].customers || null;
};

export const selectedCustomer = (state) => {
    return state['customers'].selectedCustomer;
};
export const customersAsArray = createSelector(
    customers,
    (customers) => {
        if (!customers) {
            return [];
        }
        return values(customers);
    }
);
