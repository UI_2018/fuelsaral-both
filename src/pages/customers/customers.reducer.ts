const initialState = {
    customers: {},
    selectedCustomer: null,
};

export const GET_CUSTOMERS = 'GET_CUSTOMERS';

export const GET_CUSTOMERS_SUCCESS = 'GET_CUSTOMERS_SUCCESS';
export const SET_CUSTOMER = 'SET_CUSTOMER';
const createAction = (type) => {
    return (payload = {}) => ({
        type,
        payload,
    });
};
export const getCustomers = createAction(GET_CUSTOMERS);
export const getCustomersSuccess = createAction(GET_CUSTOMERS_SUCCESS);
export const setCustomer = createAction(SET_CUSTOMER);

export function customersReducer(state = initialState, { type, payload }) {
    switch (type) {
        case GET_CUSTOMERS_SUCCESS:
            return {
                ...state,
                customers: payload,
            };
        case SET_CUSTOMER:
            return {
                ...state,
                selectedCustomer: payload.BP_NUMBER
            };
        default:
            return state;
    }
}
