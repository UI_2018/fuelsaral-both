import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { CustomersPage } from './customers';

import { ComponentModule } from '../../components';


@NgModule({
    declarations: [
        CustomersPage,
    ],
    imports: [
        IonicPageModule.forChild(CustomersPage),
        ComponentModule,
    ],
    entryComponents: [
        CustomersPage,
    ]
})
export class CustomersModule {}
