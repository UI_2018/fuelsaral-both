import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Action, Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import { App, LoadingController, NavController } from 'ionic-angular';

import { APIService } from '../../modules/serviceProvider';


import { GET_CUSTOMERS, getCustomersSuccess } from './customers.reducer';

// import { selectedCustomer } from './aoh.selectors'


@Injectable()
export class CustomersEffects {

  @Effect()
  flexiContracts$: Observable<Action> = this.actions$.pipe(
    ofType(GET_CUSTOMERS),
    mergeMap(action => {
        return this.api.customers()
          .pipe(
            map((resp: any) => {
              return getCustomersSuccess(resp);
            })
          );
    })
  );

  constructor(
    private http: HttpClient,
    private api: APIService,
    private actions$: Actions,
    private app: App,
    private store: Store<any>,
    public loadingCtrl: LoadingController) {

  }
  get navCtrl(): NavController {
    return this.app.getRootNav();
  }

//   changeRouteIfRequired(target, customerCount) {
//     const activePage = this.navCtrl.getActive()
//     if(target !== activePage.id) {
//       if(target !== 'customers' && customerCount) {
//         this.navCtrl.setPages([{page:'customers'}, {page:target}])
//       } else {
//         this.navCtrl.setRoot(target)
//       }
//     }
//   }
}
