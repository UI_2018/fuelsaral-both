webpackJsonp([2],{

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export SET_SEARCH */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CONTRACTS_SEARCH; });
/* unused harmony export CONTRACTS_SEARCH_SUCCESS */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return GET_CONTRACTS; });
/* unused harmony export GET_CONTRACTS_SUCCESS */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return GET_CONTRACT; });
/* unused harmony export GET_CONTRACT_SUCCESS */
/* unused harmony export SET_FILTER */
/* unused harmony export setSearch */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "i", function() { return getContracts; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "j", function() { return getContractsSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return getContract; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return getContractSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return contractsSearch; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return contractsSearchSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "k", function() { return setFilter; });
/* harmony export (immutable) */ __webpack_exports__["d"] = contractsReducer;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_lodash__);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};

var initialState = {
    contracts: {},
    details: {},
    search: {},
    filter: {
        isOpen: true
    },
    searchReference: {
        groups: [{ title: 'Something', value: 1 }],
        types: [
            {
                'title': 'BP Cr Qty Spt Cntrct',
                'value': 'ZCS1'
            },
            {
                'title': 'BP Flexi Order Cntr',
                'value': 'ZCS3'
            },
            {
                'title': 'BP Fuel Qty Inv Cntr',
                'value': 'ZCT1'
            },
            {
                'title': 'BP Fuel Qty Trm Cntr',
                'value': 'ZCT0'
            },
            {
                'title': 'BP OMR Ord Day Cntr',
                'value': 'ZCS2'
            },
            {
                'title': 'BP OMR OrdDay Portal',
                'value': 'ZCS4'
            }
        ],
        statuses: [{
                'title': '0',
                'value': 'Created'
            },
            {
                'title': 'A',
                'value': 'Not yet processed'
            },
            {
                'title': 'B',
                'value': 'Partially processed'
            },
            {
                'title': 'C',
                'value': 'Completely processed'
            }],
        plants: [{ title: 'Something', value: 1 }],
    }
};
var createAction = function (type) {
    return function (payload) {
        if (payload === void 0) { payload = {}; }
        return ({
            type: type,
            payload: payload,
        });
    };
};
var SET_SEARCH = 'SET_SEARCH';
var CONTRACTS_SEARCH = 'CONTRACTS_SEARCH';
var CONTRACTS_SEARCH_SUCCESS = 'CONTRACTS_SEARCH_SUCCESS';
var GET_CONTRACTS = 'GET_CONTRACTS';
var GET_CONTRACTS_SUCCESS = 'GET_CONTRACTS_SUCCESS';
var GET_CONTRACT = 'GET_CONTRACT';
var GET_CONTRACT_SUCCESS = 'GET_CONTRACT_SUCCESS';
var SET_FILTER = 'SET_FILTER';
var setSearch = createAction(SET_SEARCH);
var getContracts = createAction(GET_CONTRACTS);
var getContractsSuccess = createAction(GET_CONTRACTS_SUCCESS);
var getContract = createAction(GET_CONTRACT);
var getContractSuccess = createAction(GET_CONTRACT_SUCCESS);
var contractsSearch = createAction(CONTRACTS_SEARCH);
var contractsSearchSuccess = createAction(CONTRACTS_SEARCH_SUCCESS);
var setFilter = createAction(SET_FILTER);
function contractsReducer(state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case CONTRACTS_SEARCH_SUCCESS:
            return __assign({}, state, { search: action.payload });
        case GET_CONTRACT_SUCCESS:
            var _a = action.payload, details = _a.ES_CONTRACT_ITEM, delivery = _a.ES_DELIVERY_POINT, soldTo = _a.ES_SOLD_TO, liftings = _a.T_LIFTINGS;
            var contractId = details.CONTRACT_NUMBER;
            return __assign({}, state, { details: __assign({}, state.details, (_b = {}, _b[contractId] = {
                    details: details,
                    delivery: delivery,
                    soldTo: soldTo,
                    liftings: liftings
                }, _b)) });
        case GET_CONTRACTS_SUCCESS:
            var contracts = action.payload.T_CONTRACT_ITEMS.map(function (contract) {
                return __assign({}, contract, { id: contract.CONTRACT_NUMBER });
            });
            return __assign({}, state, { contracts: Object(__WEBPACK_IMPORTED_MODULE_0_lodash__["keyBy"])(contracts, 'id') });
        case SET_FILTER:
            return __assign({}, state, { filter: action.payload });
        default:
            return state;
    }
    var _b;
}
//# sourceMappingURL=contracts.reducer.js.map

/***/ }),

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__searchConfig__ = __webpack_require__(103);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SearchPage = /** @class */ (function () {
    function SearchPage(alertCtrl, fb, viewCtrl, store, navParams) {
        this.alertCtrl = alertCtrl;
        this.fb = fb;
        this.viewCtrl = viewCtrl;
        this.store = store;
        this.navParams = navParams;
        this.group = 'any';
        this.fields = __WEBPACK_IMPORTED_MODULE_4__searchConfig__["b" /* fields */];
        this.fields = this.navParams.get('fields');
        this.onSearch = this.navParams.get('onSearch');
        var values = this.navParams.get('values');
        this.form = this.fb.group(this.fields.reduce(function (prev, field) {
            // If there is no value passed in then use the default for that field
            var value = values[field.name] || field.value;
            prev[field.name] = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](value, null);
            return prev;
        }, {}));
    }
    SearchPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    SearchPage.prototype.performSearch = function () {
        var _this = this;
        this.onSearch(this.form.value);
        setTimeout(function () {
            _this.viewCtrl.dismiss();
        }, 400);
    };
    SearchPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\search\search.html"*/'\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n      <ion-buttons start>\n\n        <button ion-button (click)="dismiss()">Close</button>\n\n      </ion-buttons>\n\n      <ion-title>Search</ion-title>\n\n    </ion-navbar>\n\n  \n\n  </ion-header>\n\n  \n\n\n\n<ion-content class="background-white" padding>\n\n    <form [formGroup]="form">\n\n\n\n        <div *ngFor="let field of fields" [ngSwitch]="field.type">\n\n            \n\n            <div *ngSwitchCase="\'text\'">\n\n                <ion-input  class="aoh-input" [placeholder]="field.label" [formControlName]="field.name"></ion-input>\n\n            </div>\n\n            <div *ngSwitchCase="\'select\'">\n\n                <filter-select [formControlName]="field.name" [field]="field"></filter-select>\n\n            </div>\n\n        </div>\n\n    </form>\n\n</ion-content>\n\n\n\n<ion-footer>\n\n    <ion-toolbar>\n\n        <button class="btn-primary filled" (click)="performSearch()">Search</button>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\search\search.html"*/,
            styles: ['./search.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["p" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["h" /* Store */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["n" /* NavParams */]])
    ], SearchPage);
    return SearchPage;
}());

//# sourceMappingURL=search.js.map

/***/ }),

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export dateOptions */
/* unused harmony export groupOptions */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return fields; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return contractsSearch; });
/* unused harmony export liftingsFields */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return liftingsSearch; });
/* unused harmony export loadIdsFields */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return loadIdsSearch; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__modals_dateRangeSelector_dateRangeSelector__ = __webpack_require__(395);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__loadids_loadIds_selectors__ = __webpack_require__(396);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contracts_contracts_selectors__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__liftings_liftings_selectors__ = __webpack_require__(397);




var dateOptions = [
    { title: 'DATERANGE.ANY_TIME', value: 'any' },
    { title: 'DATERANGE.YESTERDAY', value: 'yesterday' },
    { title: 'DATERANGE.LAST_WEEK', value: 'last week' },
    { title: 'DATERANGE.LAST_MONTH', value: 'last month' },
    { title: 'DATERANGE.CUSTOM_RANGE', modal: __WEBPACK_IMPORTED_MODULE_0__modals_dateRangeSelector_dateRangeSelector__["a" /* DateRangeSelector */] }
];
var groupOptions = [
    { value: 'any', title: 'Any product group' },
    { value: 'group1', title: 'Group 1' },
    { value: 'group2', title: 'Group 2' }
];
var returnStatic = function (set) {
    return function () { return set; };
};
var fields = [
    { type: 'text', label: 'Contract no', name: 'contractNo', value: '' },
    { type: 'text', label: 'Customer reference', name: 'customerReference', value: '' },
    { type: 'select', label: 'Product groups', placeholder: 'Any product group', name: 'contractGroup', value: null, options: Object(__WEBPACK_IMPORTED_MODULE_2__contracts_contracts_selectors__["d" /* getReferenceSet */])('groups') },
    { type: 'select', label: 'Contract Type', placeholder: 'Any contract type', name: 'contractType', value: null, options: Object(__WEBPACK_IMPORTED_MODULE_2__contracts_contracts_selectors__["d" /* getReferenceSet */])('types') },
    { type: 'select', label: 'Contract Status', placeholder: 'Any contract status', name: 'contractStatus', value: null, options: Object(__WEBPACK_IMPORTED_MODULE_2__contracts_contracts_selectors__["d" /* getReferenceSet */])('statuses') },
    { type: 'select', label: 'Terminal', placeholder: 'Any terminal', name: 'Terminal', value: null, options: Object(__WEBPACK_IMPORTED_MODULE_2__contracts_contracts_selectors__["d" /* getReferenceSet */])('plants') },
    { type: 'select', label: 'Date', name: 'date', value: 'any', options: returnStatic(dateOptions), translate: true, displayFn: function (value) {
            return [value.startDate, value.endDate].join(' - ');
        } }
];
var contractsSearch = {
    fields: fields,
};
var liftingsFields = [
    { type: 'text', label: 'Contract no', name: 'contractNo', value: '' },
    // { type: 'text', label: 'Invoice no', name: 'invoiceNo', value: '' },
    // { type: 'text', label: 'Order no', name: 'orderNo', value: '' },
    { type: 'text', label: 'Customer reference', name: 'customerReference', value: '' },
    { type: 'select', label: 'Product group', placeholder: 'Any product group', name: 'productGroup', value: null, options: Object(__WEBPACK_IMPORTED_MODULE_3__liftings_liftings_selectors__["a" /* getReferenceSet */])('PRODUCT_ID', 'MATERIAL_DESCRIPTION', 'PRODUCT_ID') },
    { type: 'select', label: 'Terminal', placeholder: 'Any terminal', name: 'terminal', value: null, options: Object(__WEBPACK_IMPORTED_MODULE_3__liftings_liftings_selectors__["a" /* getReferenceSet */])('PLANT_ID', 'DELIVERY_POINT_NAME', 'PLANT_ID') },
    { type: 'select', label: 'Date', name: 'date', value: 'any', options: returnStatic(dateOptions), translate: true, displayFn: function (value) {
            return [value.startDate, value.endDate].join(' - ');
        } }
];
var liftingsSearch = {
    fields: liftingsFields,
};
var loadIdsFields = [
    { type: 'select', label: 'Material', placeholder: 'Any material', name: 'MATERIAL_NUMBER', value: null, options: Object(__WEBPACK_IMPORTED_MODULE_1__loadids_loadIds_selectors__["a" /* getReferenceSet */])('MATERIAL_NUMBER', 'MATERIAL_DESCRIPTION', 'MATERIAL_NUMBER') },
    { type: 'select', label: 'Term type', placeholder: 'Any term type', name: 'SHIPPING_COND', value: null, options: Object(__WEBPACK_IMPORTED_MODULE_1__loadids_loadIds_selectors__["a" /* getReferenceSet */])('SHIPPING_COND', 'MATERIAL_DESCRIPTION', 'SHIPPING_COND') },
    { type: 'select', label: 'Terminal', placeholder: 'Any terminal', name: 'PLANT_NUMBER', value: null, options: Object(__WEBPACK_IMPORTED_MODULE_1__loadids_loadIds_selectors__["a" /* getReferenceSet */])('PLANT_NUMBER', 'PLANT_NAME', 'PLANT_NUMBER') },
];
var loadIdsSearch = {
    fields: loadIdsFields,
};
//# sourceMappingURL=searchConfig.js.map

/***/ }),

/***/ 143:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export aohMaterials */
/* unused harmony export aohFlexiContracts */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return aohSelectedFilter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return aohTimer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return aohRecentFilters; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return creditPosition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return getOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return aohFilterOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return aohListings; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__basket_basket_selectors__ = __webpack_require__(144);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};



var aohMaterials = function (state) {
    return state['aoh'].materials || [];
    // return state['prices'].prices || [];
};
var aohFlexiContracts = function (state) {
    return state['aoh'].contracts || [];
};
var aohSelectedFilter = function (state) {
    return state['aoh'].selectedFilter || null;
};
var aohTimer = function (state) {
    return state['aoh'].timer || null;
};
var aohRecentFilters = function (state) {
    return state['aoh'].recentFilters || null;
};
var creditPosition = function (state) {
    return state['aoh'].creditPosition;
};
var getOptions = function (options, key, titleKey, valueKey) {
    var items = Object(__WEBPACK_IMPORTED_MODULE_1_lodash__["uniqBy"])(options, key);
    var r = items.map(function (i) { return (__assign({}, i, { title: i[titleKey], value: i[valueKey] })); });
    return r;
};
var aohFilterOptions = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(aohMaterials, function (materials) {
    if (!materials.length) {
        return {};
    }
    return {
        PLANTS: getOptions(materials, 'PLANT_NAME', 'PLANT_NAME', 'PLANT'),
        PRODUCTS: getOptions(materials, 'MATERIAL_DESCRIPTION', 'MATERIAL_DESCRIPTION', 'MATERIAL_ID')
    };
});
var aohListings = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(aohMaterials, aohFlexiContracts, aohSelectedFilter, __WEBPACK_IMPORTED_MODULE_2__basket_basket_selectors__["b" /* basketItemIds */], function (materials, contracts, aohSelectedFilter, basketItemIds) {
    if (!materials.length || !contracts.length) {
        return [];
    }
    var contractMap = Object(__WEBPACK_IMPORTED_MODULE_1_lodash__["keyBy"])(contracts, 'MATERIAL_ID');
    var materialsEnhanced = materials.map(function (m) { return (__assign({}, m, { contract: contractMap[m.MATERIAL_ID] })); });
    if (aohSelectedFilter) {
        materialsEnhanced = materialsEnhanced.filter(function (material) {
            var matchesPlant = true;
            var matchesProduct = true;
            if (aohSelectedFilter.plants.length) {
                matchesPlant = Object(__WEBPACK_IMPORTED_MODULE_1_lodash__["some"])(aohSelectedFilter.plants, function (plant) {
                    return material.PLANT === plant.PLANT;
                });
            }
            if (aohSelectedFilter.products.length) {
                matchesProduct = Object(__WEBPACK_IMPORTED_MODULE_1_lodash__["some"])(aohSelectedFilter.products, function (product) {
                    return material.MATERIAL_ID === product.MATERIAL_ID;
                });
            }
            return matchesPlant && matchesProduct;
        });
    }
    materialsEnhanced = materialsEnhanced.map(function (material) {
        return __assign({}, material, { inBasket: basketItemIds[material.id] });
    });
    return materialsEnhanced;
});
//# sourceMappingURL=aoh.selectors.js.map

/***/ }),

/***/ 144:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export basketItems */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return basketItemsWithSubTotal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return basketItemIds; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return basketTotal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return basketCount; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};


var basketItems = function (state) {
    return state['basket'].items;
};
var basketItemsWithSubTotal = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(basketItems, function (basketItems) {
    if (basketItems === void 0) { basketItems = []; }
    return basketItems.map(function (item) {
        return __assign({}, item, { subTotal: (+item.quantity / 100) * +item.material.CUSTOMER_PRICE });
    });
});
var basketItemIds = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(basketItems, function (basketItems) {
    return basketItems.reduce(function (prev, item) {
        prev[item.id] = true;
        return prev;
    }, {});
});
var basketTotal = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(basketItemsWithSubTotal, function (basketItems) {
    var total = Object(__WEBPACK_IMPORTED_MODULE_1_lodash__["sumBy"])(basketItems, 'subTotal');
    return total;
});
var basketCount = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(basketItems, function (basketItems) {
    if (!basketItems) {
        return 0;
    }
    return basketItems.length;
});
// export const selectFeatureCount = createSelector(
//   selectFeature,
//   (state: FeatureState) => state.counter
// );
//# sourceMappingURL=basket.selectors.js.map

/***/ }),

/***/ 149:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShareModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shareConfirmation_shareConfirmation_component__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngrx_store__ = __webpack_require__(6);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var dummyContracts = [
    { title: 'Some contract', value: 'value1' },
    { title: 'Some contract 2', value: 'value2' }
];
var validateEmail = function (control) {
    var email = control.value;
    if (!email) {
        return { email: true };
    }
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var isValid = re.test(control.value.toLowerCase());
    return isValid ? null : { email: true };
};
var atLeastOneSelected = function (control) {
    return control.value.length ? null : { noItemSelected: true };
};
var ShareModal = /** @class */ (function () {
    function ShareModal(viewCtrl, store, fb, modalCtrl, navParams) {
        this.viewCtrl = viewCtrl;
        this.store = store;
        this.fb = fb;
        this.modalCtrl = modalCtrl;
        this.navParams = navParams;
        this.contracts = dummyContracts;
        var contracts = navParams.get('contracts');
        this.contracts = contracts.map(function (contract) {
            return __assign({}, contract, { title: contract.MATERIAL_DESCRIPTION, value: contract.MATERIAL_DESCRIPTION });
        });
        this.form = this.form = this.fb.group({
            email: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](null, [validateEmail]),
            contracts: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]([], [atLeastOneSelected])
        }, {});
        if (this.contracts.length === 1) {
            this.form.patchValue({
                contracts: this.contracts.slice()
            });
        }
    }
    ShareModal.prototype.share = function () {
        debugger;
        this.viewCtrl.dismiss();
        this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__shareConfirmation_shareConfirmation_component__["a" /* ShareConfirmationModal */], this.form.value).present();
    };
    ShareModal = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'share',template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\modals\share\share.component.html"*/'<ion-header>\n\n\n\n        <dismiss-page-header pageTitle="CONTRACT.SHARE_CONTRACT_DETAILS"></dismiss-page-header>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n    <form [formGroup]="form">\n\n        <div class="share-container">\n\n            <ion-input  class="aoh-input" placeholder="Email" formControlName="email"></ion-input>\n\n            <div class="select-to-share">{{\'CONTRACT.SELECT_TO_SHARE\' | i18nPlural: contracts.length | translate}}</div> \n\n        </div>\n\n        <selection-list [ngClass]="{\'stop-pointer\': contracts.length <=1}" label="" [options]="contracts" formControlName="contracts"></selection-list>\n\n    </form>\n\n</ion-content>\n\n\n\n<ion-footer>\n\n    <ion-toolbar>\n\n        <button class="btn-primary filled" (click)="share()" [disabled]="form.invalid">{{\'SHARE\' | translate}}</button>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\modals\share\share.component.html"*/,
            styles: ['./share.component.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_4__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* NavParams */]])
    ], ShareModal);
    return ShareModal;
}());

//# sourceMappingURL=share.component.js.map

/***/ }),

/***/ 150:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return user; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getMenuOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getHomeTileOptions; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngrx_store__ = __webpack_require__(6);

var user = function (state) {
    return state['userReducer'];
};
var menuConfig = [
    { title: 'NAVIGATION.HOME', icon: 'home', link: 'home', tile: false },
    { title: 'NAVIGATION.AOH', icon: 'prices', link: 'aoh', tile: true },
    { title: 'NAVIGATION.PRICES', icon: 'prices', link: 'prices', tile: true },
    { title: 'NAVIGATION.CONTRACTS', icon: 'contract', link: 'contracts', tile: true },
    { title: 'NAVIGATION.LIFTINGS', icon: 'liftings', link: 'liftings', tile: true },
    { title: 'NAVIGATION.NOTIFICATIONS', notification: true, icon: 'notification', link: 'notifications', tile: true },
    { title: 'NAVIGATION.HSSE', icon: 'hsse', link: 'hsse', divider: true, tile: true },
    { title: 'NAVIGATION.LOAD_IDS', icon: 'load-ids', link: 'loadids', tile: false },
    { title: 'NAVIGATION.USEFUL_INFORMATION', icon: 'ui', link: 'useful-information', divider: true, tile: false },
];
var getMenuOptions = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(user, function (user) {
    return menuConfig;
});
var getHomeTileOptions = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(user, function (user) {
    return menuConfig.filter(function (i) { return i.tile; });
});
//# sourceMappingURL=user.selectors.js.map

/***/ }),

/***/ 151:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export notificationItems */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return notificationItemsAsArray; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return notification; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return notificationCount; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modules_router_router_selectors__ = __webpack_require__(77);



var notificationItems = function (state) {
    return state['notifications'].items;
};
var notificationItemsAsArray = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(notificationItems, function (items) {
    return Object(__WEBPACK_IMPORTED_MODULE_1_lodash__["toArray"])(items).filter(function (i) { return !i.hidden; });
});
var notification = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(__WEBPACK_IMPORTED_MODULE_2__modules_router_router_selectors__["a" /* currentParams */], notificationItems, function (params, items) {
    return items[params.id];
});
var notificationCount = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(notificationItemsAsArray, function (n) {
    return n.reduce(function (prev, next) {
        if (!next.read) {
            prev = prev + 1;
        }
        return prev;
    }, 0);
});
//# sourceMappingURL=notifications.selectors.js.map

/***/ }),

/***/ 152:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddItemModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var quantityValidator = function (min, max, rounding) {
    return function (control) {
        var value = control.value;
        value = +value;
        var errors = {};
        if (value < min) {
            errors['tooSmall'] = true;
        }
        if (value > max && max !== 0) {
            errors['tooLarge'] = true;
        }
        if ((value % rounding) !== 0) {
            errors['rounding'] = true;
        }
        return Object.keys(errors).length ? errors : null;
    };
};
var AddItemModal = /** @class */ (function () {
    function AddItemModal(viewCtrl, params, fb) {
        this.viewCtrl = viewCtrl;
        this.params = params;
        this.fb = fb;
        this.material = this.params.get('material');
        this.onConfirm = this.params.get('onConfirm');
        this.labels = this.params.get('labels');
        var item = this.params.get('item');
        var initialQuantity = item ? item.quantity : null;
        this.materialForm = this.fb.group({
            quantity: this.fb.control(initialQuantity, quantityValidator(+this.material.MIN_QUANTITY, +this.material.MAX_QUANTITY, +this.material.ROUNDING_QUANTITY)),
        });
    }
    AddItemModal.prototype.break = function () {
        debugger;
    };
    AddItemModal.prototype.addItem = function () {
        var quantity = this.materialForm.value.quantity;
        this.onConfirm({
            material: this.material,
            quantity: quantity,
        });
        this.dismiss();
    };
    AddItemModal.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AddItemModal = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\basket\modals\addItem\addItem.html"*/'<ion-header>\n\n    <dismiss-page-header [pageTitle]="labels.title | translate"></dismiss-page-header>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    <form [formGroup]="materialForm" *ngIf="materialForm">\n\n        <div class="form-label" [ngClass]="{error: materialForm?.invalid}">{{\'BASKET.ENTER_VOLUME\' | translate}}</div>\n\n        <input class="input-lg" type="number" formControlName="quantity" formFocus/>\n\n        \n\n        <div>\n\n            <error-message \n\n                [error]="materialForm?.controls.quantity.errors?.tooSmall"\n\n                message="{{\'BASKET.MIN\' | translate}} {{material.MIN_QUANTITY  | aohNumber: 0}} {{ material.UOM_EXT }}"></error-message>\n\n            <error-message\n\n                [error]="materialForm?.controls.quantity.errors?.tooLarge"\n\n                message="{{\'BASKET.MAX\'|translate}} {{material.MAX_QUANTITY  | aohNumber: 0}} {{ material.UOM_EXT }}"></error-message>\n\n            <error-message\n\n                [error]="materialForm?.controls.quantity.errors?.rounding"\n\n                message="{{\'BASKET.MULTIPLE_OF\' | translate}} {{ material.ROUNDING_QUANTITY | aohNumber: 0 }}"></error-message>\n\n        </div>\n\n    </form>\n\n</ion-content>\n\n\n\n<ion-footer>\n\n    <ion-toolbar>\n\n        <button class="btn-primary" (click)="addItem()" [disabled]="materialForm.invalid">{{labels.confirm | translate}}</button>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\basket\modals\addItem\addItem.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */]])
    ], AddItemModal);
    return AddItemModal;
}());

//# sourceMappingURL=addItem.js.map

/***/ }),

/***/ 153:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BasketModule", function() { return BasketModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__basket__ = __webpack_require__(669);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_basketItem_basketItem__ = __webpack_require__(670);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_basketIcon_basketIcon_component__ = __webpack_require__(671);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__modals_updateItem_updateItem__ = __webpack_require__(672);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__modals_addItem_addItem__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var BasketModule = /** @class */ (function () {
    function BasketModule() {
    }
    BasketModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__basket__["a" /* BasketPage */],
                __WEBPACK_IMPORTED_MODULE_3__components_basketItem_basketItem__["a" /* BasketItem */],
                __WEBPACK_IMPORTED_MODULE_4__components_basketIcon_basketIcon_component__["a" /* BasketIcon */],
                __WEBPACK_IMPORTED_MODULE_5__modals_updateItem_updateItem__["a" /* UpdateItemModal */],
                __WEBPACK_IMPORTED_MODULE_6__modals_addItem_addItem__["a" /* AddItemModal */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__basket__["a" /* BasketPage */]),
                __WEBPACK_IMPORTED_MODULE_7__components__["a" /* ComponentModule */],
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__basket__["a" /* BasketPage */],
                __WEBPACK_IMPORTED_MODULE_5__modals_updateItem_updateItem__["a" /* UpdateItemModal */],
                __WEBPACK_IMPORTED_MODULE_6__modals_addItem_addItem__["a" /* AddItemModal */],
                __WEBPACK_IMPORTED_MODULE_4__components_basketIcon_basketIcon_component__["a" /* BasketIcon */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_4__components_basketIcon_basketIcon_component__["a" /* BasketIcon */],
                __WEBPACK_IMPORTED_MODULE_5__modals_updateItem_updateItem__["a" /* UpdateItemModal */]
            ]
        })
    ], BasketModule);
    return BasketModule;
}());

//# sourceMappingURL=basket.module.js.map

/***/ }),

/***/ 155:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GET_LOAD_IDS; });
/* unused harmony export GET_LOAD_IDS_SUCCESS */
/* unused harmony export SET_LOADIDS_SEARCH */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getLoadIds; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getLoadIdsSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return setLoadIdsSearch; });
/* harmony export (immutable) */ __webpack_exports__["d"] = loadIdsReducer;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils_storeUtils__ = __webpack_require__(76);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};

var initialState = {
    loadIds: [],
    search: {}
};
var GET_LOAD_IDS = 'GET_LOAD_IDS';
var GET_LOAD_IDS_SUCCESS = 'GET_LOAD_IDS_SUCCESS';
var SET_LOADIDS_SEARCH = 'SET_LOAD_IDS_SEARCH';
var getLoadIds = Object(__WEBPACK_IMPORTED_MODULE_0__utils_storeUtils__["a" /* createAction */])(GET_LOAD_IDS);
var getLoadIdsSuccess = Object(__WEBPACK_IMPORTED_MODULE_0__utils_storeUtils__["a" /* createAction */])(GET_LOAD_IDS_SUCCESS);
var setLoadIdsSearch = Object(__WEBPACK_IMPORTED_MODULE_0__utils_storeUtils__["a" /* createAction */])(SET_LOADIDS_SEARCH);
function loadIdsReducer(state, _a) {
    if (state === void 0) { state = initialState; }
    var type = _a.type, payload = _a.payload;
    switch (type) {
        case GET_LOAD_IDS_SUCCESS:
            return __assign({}, state, { loadIds: payload.T_LOAD_ID_LIST });
        case SET_LOADIDS_SEARCH:
            return __assign({}, state, { search: payload });
        default:
            return state;
    }
}
//# sourceMappingURL=loadids.reducer.js.map

/***/ }),

/***/ 157:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConfigService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(405);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ConfigService = /** @class */ (function () {
    function ConfigService() {
        this.envs = Object.keys(__WEBPACK_IMPORTED_MODULE_1__config__["a" /* Environments */]);
        this.setEnv('test_internal');
    }
    ConfigService.prototype.setEnv = function (env) {
        if (!__WEBPACK_IMPORTED_MODULE_1__config__["a" /* Environments */][env]) {
            console.error('INVALID ENVIRONMENT');
        }
        this.envName = env;
        this.env = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* Environments */][env];
    };
    Object.defineProperty(ConfigService.prototype, "isMock", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    ConfigService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], ConfigService);
    return ConfigService;
}());

//# sourceMappingURL=configProvider.js.map

/***/ }),

/***/ 158:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export DISMISS_NOTIFICATION */
/* unused harmony export READ_NOTIFICATION */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return dismissNotification; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return readNotification; });
/* harmony export (immutable) */ __webpack_exports__["b"] = notificationsReducer;
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var initialState = {
    items: {
        '1': {
            id: '1',
            createdAt: 'Sun Jul 29 2018 16:46:45 GMT+0100',
            title: 'Opening hours change in ....',
            read: false,
            content: "From the 15th of August 'Refinery' will change its opening hours to:\n\nMonday:  <span class=\"blue\">05:00-23:00</span>\n\nTuesday: <span class=\"blue\">0500-23:00</span>\n\nWednesday: <span class=\"blue\">0500-23:00</span>\n\nThursday: <span class=\"blue\">0500-23:00</span>\n\nFriday: <span class=\"blue\">0500-23:00</span>\n\nSaturday: <span class=\"blue\">0500-23:00</span>\n\nSunday: <span class=\"blue\">0500-23:00</span>\n            "
        },
        '2': {
            id: '2',
            createdAt: 'Sun Jul 29 2018 16:46:45 GMT+0100',
            read: false,
            title: 'BP offers - June 2018'
        },
        '3': {
            id: '3',
            hidden: true,
            createdAt: 'Sun Jul 29 2018 16:46:45 GMT+0100',
            title: 'You just created a new contract'
        }
    }
};
var DISMISS_NOTIFICATION = 'DISMISS_NOTIFICATION';
var READ_NOTIFICATION = 'READ_NOTIFICATION';
var dismissNotification = function (payload) { return ({
    type: DISMISS_NOTIFICATION,
    payload: payload,
}); };
var readNotification = function (payload) { return ({
    type: READ_NOTIFICATION,
    payload: payload
}); };
function notificationsReducer(state, _a) {
    if (state === void 0) { state = initialState; }
    var type = _a.type, payload = _a.payload;
    switch (type) {
        case DISMISS_NOTIFICATION:
            return __assign({}, state, { items: __assign({}, state.items, (_b = {}, _b[payload] = __assign({}, state.items[payload], { hidden: true }), _b)) });
        case READ_NOTIFICATION:
            return __assign({}, state, { items: __assign({}, state.items, (_c = {}, _c[payload] = __assign({}, state.items[payload], { read: true }), _c)) });
        default:
            return state;
    }
    var _b, _c;
}
//# sourceMappingURL=notifications.reducer.js.map

/***/ }),

/***/ 159:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SET_VIEWING; });
/* unused harmony export SET_PRICE_FILTER */
/* unused harmony export PRICES_LOAD_SUCCESS */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return setViewing; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return pricesLoadSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return setPriceFilter; });
/* harmony export (immutable) */ __webpack_exports__["b"] = priceReducer;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils_storeUtils__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__aoh_aoh_reducer__ = __webpack_require__(92);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};


var initialState = {
    viewing: 'history',
    prices: [],
    recentFilters: [],
    selectedFilter: null,
};
var SET_VIEWING = 'SET_VIEWING';
var SET_PRICE_FILTER = 'SET_PRICE_FILTER';
var PRICES_LOAD_SUCCESS = 'PRICES_LOAD_SUCCESS';
var setViewing = Object(__WEBPACK_IMPORTED_MODULE_0__utils_storeUtils__["a" /* createAction */])(SET_VIEWING);
var pricesLoadSuccess = Object(__WEBPACK_IMPORTED_MODULE_0__utils_storeUtils__["a" /* createAction */])(PRICES_LOAD_SUCCESS);
var setPriceFilter = Object(__WEBPACK_IMPORTED_MODULE_0__utils_storeUtils__["a" /* createAction */])(SET_PRICE_FILTER);
function priceReducer(state, _a) {
    if (state === void 0) { state = initialState; }
    var type = _a.type, payload = _a.payload;
    switch (type) {
        case SET_VIEWING:
            return __assign({}, state, { viewing: payload });
        case PRICES_LOAD_SUCCESS:
            return __assign({}, state, { prices: payload });
        case SET_PRICE_FILTER:
            var newRecentFilters = state.recentFilters;
            // If there are no filters selected then don't push to recent list
            // If there are more than 5 filters in the history then remove the last
            if (payload.storeAsRecent && (payload.products.length || payload.plants.length)) {
                newRecentFilters = [payload].concat(newRecentFilters);
            }
            newRecentFilters = Object(__WEBPACK_IMPORTED_MODULE_1__aoh_aoh_reducer__["n" /* removeDuplicateRecentFilters */])(newRecentFilters);
            return __assign({}, state, { selectedFilter: payload, recentFilters: newRecentFilters });
        default:
            return state;
    }
}
//# sourceMappingURL=prices.reducer.js.map

/***/ }),

/***/ 17:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__homeTile_homeTile_component__ = __webpack_require__(625);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__aohIcon_aohIcon_component__ = __webpack_require__(633);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__errorMessage_errorMessage_component__ = __webpack_require__(634);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__formFocus_formFocus_directive__ = __webpack_require__(635);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__autoResize_autoResize_directive__ = __webpack_require__(636);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__filterPill_filterPill_component__ = __webpack_require__(637);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__detailsBox_detailsBox__ = __webpack_require__(638);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__detailsItem_detailsItem__ = __webpack_require__(639);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__dismissPageHeader_dismissPageHeader__ = __webpack_require__(640);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__cardSection_cardSection__ = __webpack_require__(641);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__contractCard_contractCard__ = __webpack_require__(642);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__badge_badge__ = __webpack_require__(643);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__liftingBar_liftingBar__ = __webpack_require__(644);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__tabBar_tabBar__ = __webpack_require__(645);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__homeContainerResize_homeContainerResize__ = __webpack_require__(646);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__masonaryContainer_masonaryContainer__ = __webpack_require__(647);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pricingCountDown_pricingCountDown_component__ = __webpack_require__(648);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__liftingCard_liftingCard_component__ = __webpack_require__(650);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__selectionList_selectionList_component__ = __webpack_require__(651);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__confirmationPage_confirmationPage_component__ = __webpack_require__(652);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__iconSidebar_iconSidebar_component__ = __webpack_require__(653);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__filters_masonaryPipe__ = __webpack_require__(654);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__searchDetails_searchDetails__ = __webpack_require__(655);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__loadingIndicator_loadingIndicator__ = __webpack_require__(656);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__backButton_backButton_directive__ = __webpack_require__(657);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__notificationIcon_notificationIcon__ = __webpack_require__(658);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__progressStepper_progressStepper__ = __webpack_require__(659);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ngx_translate_core__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__filters_countDown_pipe__ = __webpack_require__(660);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__filters_timeFromDate_pips__ = __webpack_require__(661);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__filters_daysToCollect_pipe__ = __webpack_require__(662);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__filters_dayDiff__ = __webpack_require__(663);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__filters_aohDate_pipe__ = __webpack_require__(664);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__filters_aohTime__ = __webpack_require__(665);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__filters_aohNumber_pipe__ = __webpack_require__(666);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__filters_i18nPlural__ = __webpack_require__(667);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};































0;








var ComponentModule = /** @class */ (function () {
    function ComponentModule() {
    }
    ComponentModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [__WEBPACK_IMPORTED_MODULE_29__ngx_translate_core__["b" /* TranslateModule */], __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */], __WEBPACK_IMPORTED_MODULE_30_ionic_angular__["g" /* IonicModule */]],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__homeTile_homeTile_component__["a" /* HomeTile */],
                __WEBPACK_IMPORTED_MODULE_3__aohIcon_aohIcon_component__["a" /* AOHIcon */],
                __WEBPACK_IMPORTED_MODULE_31__filters_countDown_pipe__["a" /* CountDownPipe */],
                __WEBPACK_IMPORTED_MODULE_32__filters_timeFromDate_pips__["a" /* TimeFromDatePipe */],
                __WEBPACK_IMPORTED_MODULE_4__errorMessage_errorMessage_component__["a" /* ErrorMessage */],
                __WEBPACK_IMPORTED_MODULE_5__formFocus_formFocus_directive__["a" /* FormFocusDirective */],
                __WEBPACK_IMPORTED_MODULE_6__autoResize_autoResize_directive__["a" /* Autoresize */],
                __WEBPACK_IMPORTED_MODULE_7__filterPill_filterPill_component__["a" /* FilterPill */],
                __WEBPACK_IMPORTED_MODULE_8__detailsBox_detailsBox__["a" /* DetailsBox */],
                __WEBPACK_IMPORTED_MODULE_9__detailsItem_detailsItem__["a" /* DetailsItem */],
                __WEBPACK_IMPORTED_MODULE_10__dismissPageHeader_dismissPageHeader__["a" /* DismissPageHeader */],
                __WEBPACK_IMPORTED_MODULE_11__cardSection_cardSection__["a" /* CardSection */],
                __WEBPACK_IMPORTED_MODULE_12__contractCard_contractCard__["a" /* ContractCard */],
                __WEBPACK_IMPORTED_MODULE_13__badge_badge__["a" /* Badge */],
                __WEBPACK_IMPORTED_MODULE_14__liftingBar_liftingBar__["a" /* LiftingBar */],
                __WEBPACK_IMPORTED_MODULE_15__tabBar_tabBar__["a" /* TabBar */],
                __WEBPACK_IMPORTED_MODULE_16__homeContainerResize_homeContainerResize__["a" /* HomeContainerResize */],
                __WEBPACK_IMPORTED_MODULE_33__filters_daysToCollect_pipe__["a" /* DaysToCollectPipe */],
                __WEBPACK_IMPORTED_MODULE_18__pricingCountDown_pricingCountDown_component__["a" /* PricingCountDown */],
                __WEBPACK_IMPORTED_MODULE_34__filters_dayDiff__["a" /* DayDiffPipe */],
                __WEBPACK_IMPORTED_MODULE_35__filters_aohDate_pipe__["a" /* AOHDate */],
                __WEBPACK_IMPORTED_MODULE_19__liftingCard_liftingCard_component__["a" /* LiftingCard */],
                __WEBPACK_IMPORTED_MODULE_20__selectionList_selectionList_component__["a" /* SelectionList */],
                __WEBPACK_IMPORTED_MODULE_21__confirmationPage_confirmationPage_component__["a" /* ConfirmationPage */],
                __WEBPACK_IMPORTED_MODULE_24__searchDetails_searchDetails__["a" /* SearchDetails */],
                __WEBPACK_IMPORTED_MODULE_25__loadingIndicator_loadingIndicator__["a" /* LoadingIndicator */],
                __WEBPACK_IMPORTED_MODULE_22__iconSidebar_iconSidebar_component__["a" /* IconSidebar */],
                __WEBPACK_IMPORTED_MODULE_23__filters_masonaryPipe__["a" /* MasonaryPipe */],
                __WEBPACK_IMPORTED_MODULE_17__masonaryContainer_masonaryContainer__["a" /* MasonaryContainer */],
                __WEBPACK_IMPORTED_MODULE_26__backButton_backButton_directive__["a" /* BackButton */],
                __WEBPACK_IMPORTED_MODULE_36__filters_aohTime__["a" /* AOHTime */],
                __WEBPACK_IMPORTED_MODULE_37__filters_aohNumber_pipe__["a" /* AOHNumber */],
                __WEBPACK_IMPORTED_MODULE_38__filters_i18nPlural__["a" /* TranslatePlural */],
                __WEBPACK_IMPORTED_MODULE_27__notificationIcon_notificationIcon__["a" /* NotificationIcon */],
                __WEBPACK_IMPORTED_MODULE_28__progressStepper_progressStepper__["a" /* ProgressStepper */],
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__homeTile_homeTile_component__["a" /* HomeTile */],
                __WEBPACK_IMPORTED_MODULE_3__aohIcon_aohIcon_component__["a" /* AOHIcon */],
                __WEBPACK_IMPORTED_MODULE_29__ngx_translate_core__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_31__filters_countDown_pipe__["a" /* CountDownPipe */],
                __WEBPACK_IMPORTED_MODULE_32__filters_timeFromDate_pips__["a" /* TimeFromDatePipe */],
                __WEBPACK_IMPORTED_MODULE_4__errorMessage_errorMessage_component__["a" /* ErrorMessage */],
                __WEBPACK_IMPORTED_MODULE_5__formFocus_formFocus_directive__["a" /* FormFocusDirective */],
                __WEBPACK_IMPORTED_MODULE_6__autoResize_autoResize_directive__["a" /* Autoresize */],
                __WEBPACK_IMPORTED_MODULE_7__filterPill_filterPill_component__["a" /* FilterPill */],
                __WEBPACK_IMPORTED_MODULE_8__detailsBox_detailsBox__["a" /* DetailsBox */],
                __WEBPACK_IMPORTED_MODULE_9__detailsItem_detailsItem__["a" /* DetailsItem */],
                __WEBPACK_IMPORTED_MODULE_10__dismissPageHeader_dismissPageHeader__["a" /* DismissPageHeader */],
                __WEBPACK_IMPORTED_MODULE_11__cardSection_cardSection__["a" /* CardSection */],
                __WEBPACK_IMPORTED_MODULE_12__contractCard_contractCard__["a" /* ContractCard */],
                __WEBPACK_IMPORTED_MODULE_13__badge_badge__["a" /* Badge */],
                __WEBPACK_IMPORTED_MODULE_14__liftingBar_liftingBar__["a" /* LiftingBar */],
                __WEBPACK_IMPORTED_MODULE_15__tabBar_tabBar__["a" /* TabBar */],
                __WEBPACK_IMPORTED_MODULE_16__homeContainerResize_homeContainerResize__["a" /* HomeContainerResize */],
                __WEBPACK_IMPORTED_MODULE_33__filters_daysToCollect_pipe__["a" /* DaysToCollectPipe */],
                __WEBPACK_IMPORTED_MODULE_18__pricingCountDown_pricingCountDown_component__["a" /* PricingCountDown */],
                __WEBPACK_IMPORTED_MODULE_34__filters_dayDiff__["a" /* DayDiffPipe */],
                __WEBPACK_IMPORTED_MODULE_35__filters_aohDate_pipe__["a" /* AOHDate */],
                __WEBPACK_IMPORTED_MODULE_19__liftingCard_liftingCard_component__["a" /* LiftingCard */],
                __WEBPACK_IMPORTED_MODULE_20__selectionList_selectionList_component__["a" /* SelectionList */],
                __WEBPACK_IMPORTED_MODULE_21__confirmationPage_confirmationPage_component__["a" /* ConfirmationPage */],
                __WEBPACK_IMPORTED_MODULE_22__iconSidebar_iconSidebar_component__["a" /* IconSidebar */],
                __WEBPACK_IMPORTED_MODULE_23__filters_masonaryPipe__["a" /* MasonaryPipe */],
                __WEBPACK_IMPORTED_MODULE_24__searchDetails_searchDetails__["a" /* SearchDetails */],
                __WEBPACK_IMPORTED_MODULE_25__loadingIndicator_loadingIndicator__["a" /* LoadingIndicator */],
                __WEBPACK_IMPORTED_MODULE_17__masonaryContainer_masonaryContainer__["a" /* MasonaryContainer */],
                __WEBPACK_IMPORTED_MODULE_26__backButton_backButton_directive__["a" /* BackButton */],
                __WEBPACK_IMPORTED_MODULE_36__filters_aohTime__["a" /* AOHTime */],
                __WEBPACK_IMPORTED_MODULE_37__filters_aohNumber_pipe__["a" /* AOHNumber */],
                __WEBPACK_IMPORTED_MODULE_38__filters_i18nPlural__["a" /* TranslatePlural */],
                __WEBPACK_IMPORTED_MODULE_27__notificationIcon_notificationIcon__["a" /* NotificationIcon */],
                __WEBPACK_IMPORTED_MODULE_28__progressStepper_progressStepper__["a" /* ProgressStepper */],
            ]
        })
    ], ComponentModule);
    return ComponentModule;
}());

//# sourceMappingURL=index.js.map

/***/ }),

/***/ 181:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GET_CUSTOMERS; });
/* unused harmony export GET_CUSTOMERS_SUCCESS */
/* unused harmony export SET_CUSTOMER */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getCustomers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getCustomersSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return setCustomer; });
/* harmony export (immutable) */ __webpack_exports__["b"] = customersReducer;
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var initialState = {
    customers: {},
    selectedCustomer: null,
};
var GET_CUSTOMERS = 'GET_CUSTOMERS';
var GET_CUSTOMERS_SUCCESS = 'GET_CUSTOMERS_SUCCESS';
var SET_CUSTOMER = 'SET_CUSTOMER';
var createAction = function (type) {
    return function (payload) {
        if (payload === void 0) { payload = {}; }
        return ({
            type: type,
            payload: payload,
        });
    };
};
var getCustomers = createAction(GET_CUSTOMERS);
var getCustomersSuccess = createAction(GET_CUSTOMERS_SUCCESS);
var setCustomer = createAction(SET_CUSTOMER);
function customersReducer(state, _a) {
    if (state === void 0) { state = initialState; }
    var type = _a.type, payload = _a.payload;
    switch (type) {
        case GET_CUSTOMERS_SUCCESS:
            return __assign({}, state, { customers: payload });
        case SET_CUSTOMER:
            return __assign({}, state, { selectedCustomer: payload.BP_NUMBER });
        default:
            return state;
    }
}
//# sourceMappingURL=customers.reducer.js.map

/***/ }),

/***/ 182:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export contracts */
/* unused harmony export contractDetails */
/* unused harmony export contractFilter */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return contractSearch; });
/* unused harmony export contractReferenceData */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return isShowingContractSearch; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getReferenceSet; });
/* unused harmony export contactsArray */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return contractsWithFlags; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return contract; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modules_router_router_selectors__ = __webpack_require__(77);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};



var contracts = function (state) {
    return state['contracts'].contracts || [];
};
var contractDetails = function (state) {
    return state['contracts'].details || {};
};
var contractFilter = function (state) {
    return state['contracts'].filter || {};
};
var contractSearch = function (state) {
    return state['contracts'].search || {};
};
var contractReferenceData = function (state) {
    return state['contracts'].searchReference;
};
var isShowingContractSearch = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(contractSearch, function (search) {
    return Object(__WEBPACK_IMPORTED_MODULE_1_lodash__["some"])(Object(__WEBPACK_IMPORTED_MODULE_1_lodash__["keys"])(search), function (searchKey) {
        return search[searchKey] && search[searchKey] !== 'any';
    });
});
var getReferenceSet = function (key) {
    return Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(contractReferenceData, function (ref) {
        return ref[key] || [];
    });
};
var contactsArray = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(contracts, function (contracts) {
    return Object(__WEBPACK_IMPORTED_MODULE_1_lodash__["toArray"])(contracts);
});
var contractsWithFlags = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(contactsArray, contractFilter, function (contracts, filter) {
    return contracts.map(function (c) {
        return __assign({}, c, { isOpen: c.STATUS_ID !== 'C' });
    }).filter(function (c) {
        if (filter.isOpen) {
            return c.isOpen;
        }
        return true;
    });
});
var contract = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(contractDetails, __WEBPACK_IMPORTED_MODULE_2__modules_router_router_selectors__["a" /* currentParams */], function (contractDetails, currentParams) {
    var contractId = currentParams.id;
    return contractDetails[contractId];
});
//# sourceMappingURL=contracts.selectors.js.map

/***/ }),

/***/ 216:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 216;

/***/ }),

/***/ 261:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/aoh/aoh.module": [
		262
	],
	"../pages/aoh/subPages/details/details.module": [
		393
	],
	"../pages/aoh/subPages/filter/filter.module": [
		392
	],
	"../pages/basket/basket.module": [
		153
	],
	"../pages/contracts/contracts.module": [
		767,
		1
	],
	"../pages/contracts/subPages/contractDetails/contractDetails.module": [
		394
	],
	"../pages/customers/customers.module": [
		768,
		0
	],
	"../pages/home/home.module": [
		398
	],
	"../pages/hsse/hsse.module": [
		399
	],
	"../pages/liftings/liftings.module": [
		400
	],
	"../pages/loadids/loadids.module": [
		402
	],
	"../pages/login/login.module": [
		403
	],
	"../pages/notifications/notifications.module": [
		407
	],
	"../pages/notifications/subPages/notificationDetails/notificationDetails.module": [
		408
	],
	"../pages/notifications/subPages/notificationSettings/notificationSettings.module": [
		409
	],
	"../pages/playground/playground.module": [
		410
	],
	"../pages/prices/prices.module": [
		411
	],
	"../pages/search/search.module": [
		412
	],
	"../pages/usefulInformation/subPages/contacts/contacts.module": [
		414
	],
	"../pages/usefulInformation/subPages/productCatalogue/productCatalogue.module": [
		413
	],
	"../pages/usefulInformation/subPages/productDetails/productDetails.module": [
		416
	],
	"../pages/usefulInformation/usefulInformation.module": [
		417
	],
	"../pages/user/user.module": [
		418
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 261;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 262:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AOHModule", function() { return AOHModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__aoh__ = __webpack_require__(621);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_aohItem_aohItem_component__ = __webpack_require__(668);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__basket_basket_module__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__subPages_filter_filter_module__ = __webpack_require__(392);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__subPages_details_details_module__ = __webpack_require__(393);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var AOHModule = /** @class */ (function () {
    function AOHModule() {
    }
    AOHModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__aoh__["a" /* AOHPage */],
                __WEBPACK_IMPORTED_MODULE_4__components_aohItem_aohItem_component__["a" /* AOHItem */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__aoh__["a" /* AOHPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components__["a" /* ComponentModule */],
                __WEBPACK_IMPORTED_MODULE_5__basket_basket_module__["BasketModule"],
                __WEBPACK_IMPORTED_MODULE_6__subPages_filter_filter_module__["FilterModule"],
                __WEBPACK_IMPORTED_MODULE_7__subPages_details_details_module__["DetailsModule"],
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__aoh__["a" /* AOHPage */]
            ]
        })
    ], AOHModule);
    return AOHModule;
}());

//# sourceMappingURL=aoh.module.js.map

/***/ }),

/***/ 264:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__(6);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FilterPage = /** @class */ (function () {
    function FilterPage(store, fb, navCtrl, navParams, viewCtrl) {
        this.store = store;
        this.fb = fb;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.value = [{ title: 'option1' }];
        this.options = [{ title: 'option1' }, { title: 'option2' }];
    }
    FilterPage.prototype.ngOnInit = function () {
        this.filterOptions = this.store.select(this.navParams.get('options'));
        this.recentFilters = this.store.select(this.navParams.get('recents'));
        this.buildForm({ items: [] });
    };
    FilterPage.prototype.buildForm = function (data) {
        var form = this.fb.group({
            plants: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]([], null),
            products: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]([], null)
        });
        if (!this.myForm) {
            this.myForm = form;
        }
    };
    FilterPage.prototype.handleRecentFilter = function (filter) {
        this.myForm.patchValue(filter);
    };
    FilterPage.prototype.submit = function () {
        // this.store.dispatch(setAohFilter({
        this.store.dispatch(this.navParams.get('onSetFilter')(__assign({}, this.myForm.value, { storeAsRecent: true })));
        this.navCtrl.pop();
    };
    FilterPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    FilterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\aoh\subPages\filter\filter.html"*/'<!-- <ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>Filter</ion-title>\n\n    </ion-navbar>\n\n    \n\n</ion-header> -->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-buttons start>\n\n        <button ion-button (click)="dismiss()">{{\'CLOSE\' | translate}}</button>\n\n        </ion-buttons>\n\n        <ion-title>{{\'PAGE_TITLE.FILTER\' | translate}}</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content class="background-white">\n\n       \n\n    <ion-scroll *ngIf="(recentFilters | async)?.length" scrollX="true" class="recent-filters">\n\n        <div class="recent-filters--container">\n\n\n\n            <recent-filter \n\n                *ngFor="let recentFilter of recentFilters | async"\n\n                [filter]="recentFilter"\n\n                (click)="handleRecentFilter(recentFilter)"></recent-filter>\n\n        </div>\n\n       \n\n    </ion-scroll> \n\n\n\n    <div>\n\n        <form [formGroup]="myForm" (buildForm)="buildForm($event)">\n\n            <selection-list [label]="\'AOH.TERMINALS\' | translate" [options]="(filterOptions | async)?.PLANTS" formControlName="plants"></selection-list>\n\n            <selection-list [label]="\'AOH.PRODUCTS\' | translate" [options]="(filterOptions | async)?.PRODUCTS" formControlName="products"></selection-list>\n\n        </form>\n\n    </div>\n\n    <!-- <ion-list>\n\n        <ion-list-header>\n\n            Terminals\n\n        </ion-list-header>\n\n        <ion-item>\n\n            <ion-label>Terminal 1</ion-label>\n\n            <ion-checkbox checked="true" value="go"></ion-checkbox>\n\n        </ion-item>\n\n        \n\n        <ion-item>\n\n            <ion-label>Terminal 2</ion-label>\n\n            <ion-checkbox value="rust"></ion-checkbox>\n\n        </ion-item>\n\n        \n\n        <ion-item>\n\n            <ion-label>Terminal 3</ion-label>\n\n            <ion-checkbox value="python" disabled="true"></ion-checkbox>\n\n        </ion-item>\n\n    </ion-list> -->\n\n    <!-- <ion-list>\n\n        <ion-list-header>\n\n            Terminals\n\n        </ion-list-header>\n\n        <ion-item>\n\n            <ion-label>Terminal 1</ion-label>\n\n            <ion-checkbox checked="true" value="go"></ion-checkbox>\n\n        </ion-item>\n\n        \n\n        <ion-item>\n\n            <ion-label>Terminal 2</ion-label>\n\n            <ion-checkbox value="rust"></ion-checkbox>\n\n        </ion-item>\n\n        \n\n        <ion-item>\n\n            <ion-label>Terminal 3</ion-label>\n\n            <ion-checkbox value="python" disabled="true"></ion-checkbox>\n\n        </ion-item>\n\n    </ion-list> -->\n\n    <!-- <form [formGroup]="newStory" connectForm="newStory" (submit)="submit($event)" >\n\n        <input name="title" formControlName="title"/>\n\n        <button >SUBMIT</button>\n\n    </form> -->\n\n\n\n\n\n\n\n\n\n        \n\n</ion-content>\n\n\n\n<ion-footer>\n\n    <ion-toolbar>\n\n        <button class="btn-primary filled" (click)="submit()">Filter</button>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\aoh\subPages\filter\filter.html"*/,
            styles: ['./filter.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ngrx_store__["h" /* Store */], __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ViewController */]])
    ], FilterPage);
    return FilterPage;
}());

//# sourceMappingURL=filter.js.map

/***/ }),

/***/ 266:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShareConfirmationModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ShareConfirmationModal = /** @class */ (function () {
    function ShareConfirmationModal(viewCtrl, navParams) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.email = navParams.get('email');
    }
    ShareConfirmationModal.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ShareConfirmationModal = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'share-confirmation',template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\modals\shareConfirmation\shareConfirmation.component.html"*/'<ion-header>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n    <confirmation-page>\n\n        <div class="confirmation-title">{{\'CONTRACT.DETAILS_SENT\' | translate}}</div>\n\n        <div class="confirmation-subtitle">{{email}}</div>\n\n    </confirmation-page>\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\modals\shareConfirmation\shareConfirmation.component.html"*/,
            styles: ['./shareConfirmation.component.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */]])
    ], ShareConfirmationModal);
    return ShareConfirmationModal;
}());

//# sourceMappingURL=shareConfirmation.component.js.map

/***/ }),

/***/ 36:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavigationService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_file_transfer__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_file__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_opener__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__utils_storeUtils__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_customers_customers_selectors__ = __webpack_require__(468);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__modules_loading_provider__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__utils_cordova_helpers_js__ = __webpack_require__(94);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var NavigationService = /** @class */ (function () {
    function NavigationService(store, loadingService, transfer, file, fileOpener) {
        this.store = store;
        this.loadingService = loadingService;
        this.transfer = transfer;
        this.file = file;
        this.fileOpener = fileOpener;
    }
    NavigationService.prototype.setNav = function (nav) {
        this.nav = nav;
    };
    NavigationService.prototype.followLink = function (link, params) {
        if (params === void 0) { params = {}; }
        if (['contracts', 'aoh'].indexOf(link) !== -1) {
            if (!Object(__WEBPACK_IMPORTED_MODULE_5__utils_storeUtils__["b" /* select */])(this.store, __WEBPACK_IMPORTED_MODULE_6__pages_customers_customers_selectors__["b" /* selectedCustomer */])) {
                this.nav.setRoot('customers', { target: link });
                return;
            }
            else if (Object(__WEBPACK_IMPORTED_MODULE_5__utils_storeUtils__["b" /* select */])(this.store, __WEBPACK_IMPORTED_MODULE_6__pages_customers_customers_selectors__["a" /* customersAsArray */]).length > 1) {
                this.nav.setPages([{ page: 'customers', params: { target: link } }, { page: link }]);
                return;
            }
            else {
                this.nav.setRoot(link, params);
            }
        }
        else {
            this.nav.setRoot(link, params);
        }
    };
    NavigationService.prototype.push = function (link, params) {
        if (params === void 0) { params = {}; }
        this.nav.push(link, params);
    };
    NavigationService.prototype.openRemotePDF = function (url, name) {
        var _this = this;
        if (!window['cordova']) {
            this.loadingService.showLoader();
            setTimeout(function () {
                _this.loadingService.hideLoader();
                alert('Only supported on device');
            }, 1000);
        }
        else {
            this.loadingService.showLoader();
            var fileTransfer = this.transfer.create();
            fileTransfer.download(url, this.file.dataDirectory + name).then(function (entry) {
                _this.loadingService.hideLoader();
                _this.fileOpener.open(entry.toURL(), 'application/pdf')
                    .then(function () { return console.log('File is opened'); })
                    .catch(function (e) { return console.log('Error opening file', e); });
            }, function (error) {
                _this.loadingService.hideLoader();
                // handle error
            });
        }
    };
    NavigationService.prototype.openPDF = function (url) {
        this.fileOpener.open(Object(__WEBPACK_IMPORTED_MODULE_8__utils_cordova_helpers_js__["a" /* getResourceUrl */])(url), 'application/pdf');
    };
    NavigationService.prototype.openFile = function (url) {
        this.fileOpener.open(Object(__WEBPACK_IMPORTED_MODULE_8__utils_cordova_helpers_js__["a" /* getResourceUrl */])(url), 'application/pdf');
    };
    NavigationService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_7__modules_loading_provider__["a" /* LoadingService */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_opener__["a" /* FileOpener */]])
    ], NavigationService);
    return NavigationService;
}());

//# sourceMappingURL=navigation.js.map

/***/ }),

/***/ 390:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MakeAClaimModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__makeAClaimConfirmation_makeAClaimConfirmation_component__ = __webpack_require__(391);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngrx_store__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var claimReasons = [
    { title: 'Reason 1', value: 1 },
    { title: 'Reason 2', value: 2 },
    { title: 'Reason 3', value: 3 },
    { title: 'Reason 4', value: 4 },
];
var MakeAClaimModal = /** @class */ (function () {
    function MakeAClaimModal(viewCtrl, store, fb, modalCtrl) {
        this.viewCtrl = viewCtrl;
        this.store = store;
        this.fb = fb;
        this.modalCtrl = modalCtrl;
        this.claimReasons = claimReasons;
        this.activeStep = 0;
        this.form = this.fb.group({
            claimReason: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]([], null),
            justification: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](null, null),
            email: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('max.mustermann@b2b.de', null),
            phone: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('+49 234 12345', null) // todo: replace static dummy value
        });
    }
    MakeAClaimModal.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    Object.defineProperty(MakeAClaimModal.prototype, "hasNextStep", {
        get: function () {
            return this.activeStep <= 1;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MakeAClaimModal.prototype, "hasPreviousStep", {
        get: function () {
            return this.activeStep > 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MakeAClaimModal.prototype, "formIsValid", {
        get: function () {
            var value = this.form.value;
            if (this.activeStep === 0) {
                return value.claimReason.length;
            }
            if (this.activeStep === 1) {
                return value.justification;
            }
            if (this.activeStep === 2) {
                return value.email && value.phone;
            }
        },
        enumerable: true,
        configurable: true
    });
    MakeAClaimModal.prototype.onNextStep = function () {
        this.activeStep++;
    };
    MakeAClaimModal.prototype.onPreviousStep = function () {
        this.activeStep--;
    };
    MakeAClaimModal.prototype.onSubmit = function () {
        this.viewCtrl.dismiss();
        this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__makeAClaimConfirmation_makeAClaimConfirmation_component__["a" /* MakeAClaimConfirmationModal */]).present();
    };
    MakeAClaimModal = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'make-a-claim',template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\modals\makeAClaim\makeAClaim.component.html"*/'<ion-header>\n\n\n\n    <dismiss-page-header class="phone" pageTitle="{{\'MAKE_A_CLAIM.TITLE\' | translate}} - {{\'MAKE_A_CLAIM.STEP\' | translate}} {{activeStep + 1}} {{\'MAKE_A_CLAIM.OF\' | translate}} 3"></dismiss-page-header>\n\n    <dismiss-page-header class="tablet" pageTitle="{{\'MAKE_A_CLAIM.TITLE\' | translate}}"></dismiss-page-header>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content class="make-a-claim--container">\n\n    <progress-stepper class="tablet" [activeStep]="activeStep" [steps]="[\n\n        {title: \'MAKE_A_CLAIM.ADD_A_REASON\'},\n\n        {title: \'MAKE_A_CLAIM.ENTER_JUSTIFICATION\'},\n\n        {title: \'MAKE_A_CLAIM.OVERVIEW\'}\n\n    ]"></progress-stepper>\n\n    <form [formGroup]="form" (buildForm)="buildForm($event)">\n\n        <div *ngIf="activeStep === 0">\n\n            <selection-list [label]="\'MAKE_A_CLAIM.REASON\' | translate" [options]="claimReasons" formControlName="claimReason"></selection-list>\n\n        </div>\n\n\n\n        <div *ngIf="activeStep === 1" class="step">\n\n            <div class="step-title">{{\'MAKE_A_CLAIM.JUSTIFICATION\' | translate}}</div>\n\n            <div class="step-content">\n\n                <ion-textarea formFocus autoresize class="aoh-input" formControlName="justification"></ion-textarea>\n\n            </div>\n\n        </div>\n\n\n\n        <div *ngIf="activeStep === 2" class="step">\n\n            <div class="step-content review">\n\n                <div class="field-label">{{\'MAKE_A_CLAIM.REASON\' | translate}}</div>\n\n                <div class="field-value">\n\n                    <div *ngFor="let reason of form.value.claimReason">{{reason.title}}</div>\n\n                </div>\n\n                <div class="field-label">{{\'MAKE_A_CLAIM.JUSTIFICATION\' | translate}}</div>\n\n                <div class="field-value">{{form.value.justification}}</div>\n\n                <div class="field-label">{{\'MAKE_A_CLAIM.CONTACT_INFORMATION\' | translate}}</div>\n\n\n\n                <ion-input  class="aoh-input" [placeholder]="\'MAKE_A_CLAIM.PHONE\' | translate" formControlName="phone"></ion-input>\n\n                <ion-input  class="aoh-input" [placeholder]="\'MAKE_A_CLAIM.EMAIL\' | translate" formControlName="email"></ion-input>        \n\n            </div>\n\n        </div>\n\n\n\n\n\n\n\n    </form>\n\n</ion-content>\n\n\n\n<ion-footer class="make-a-claim--footer">\n\n    <ion-toolbar>\n\n        <div class="row flex-end">\n\n            <button class="btn-primary" *ngIf="hasPreviousStep" (click)="onPreviousStep()">{{\'MAKE_A_CLAIM.BACK\' | translate}}</button>\n\n            <button class="btn-primary filled" [disabled]="!formIsValid" *ngIf="hasNextStep" (click)="onNextStep()">{{\'MAKE_A_CLAIM.NEXT\' | translate}}</button> \n\n            <button class="btn-primary filled" [disabled]="!formIsValid" *ngIf="activeStep === 2" (click)="onSubmit()">{{\'MAKE_A_CLAIM.SUBMIT\' | translate}}</button> \n\n        </div>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\modals\makeAClaim\makeAClaim.component.html"*/,
            styles: ['./makeAClaim.component.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_4__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ModalController */]])
    ], MakeAClaimModal);
    return MakeAClaimModal;
}());

//# sourceMappingURL=makeAClaim.component.js.map

/***/ }),

/***/ 391:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MakeAClaimConfirmationModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MakeAClaimConfirmationModal = /** @class */ (function () {
    function MakeAClaimConfirmationModal(viewCtrl, store, fb) {
        this.viewCtrl = viewCtrl;
        this.store = store;
        this.fb = fb;
    }
    MakeAClaimConfirmationModal.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    MakeAClaimConfirmationModal = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'make-a-claim-confirmation',template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\modals\makeAClaimConfirmation\makeAClaimConfirmation.component.html"*/'<ion-header>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n    <confirmation-page>\n\n        <div class="confirmation-title">{{\'MAKE_A_CLAIM.CONFIRMATION\' | translate}}</div>\n\n        <div class="confirmation-subtitle">{{\'MAKE_A_CLAIM.CONFIRMATION_NUMBER\' | translate}} 12345678</div>\n\n        <div>\n\n            <button class="btn-primary filled" (click)="dismiss()">{{\'MAKE_A_CLAIM.VIEW_MY_LIFTINGS\' | translate}}</button>\n\n        </div>\n\n    </confirmation-page>\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\modals\makeAClaimConfirmation\makeAClaimConfirmation.component.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_3__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */]])
    ], MakeAClaimConfirmationModal);
    return MakeAClaimConfirmationModal;
}());

//# sourceMappingURL=makeAClaimConfirmation.component.js.map

/***/ }),

/***/ 392:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterModule", function() { return FilterModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__filter__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_recentFilter_component__ = __webpack_require__(673);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__modules_forms_connectForm_directive__ = __webpack_require__(674);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var FilterModule = /** @class */ (function () {
    function FilterModule() {
    }
    FilterModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__filter__["a" /* FilterPage */],
                __WEBPACK_IMPORTED_MODULE_4__components_recentFilter_component__["a" /* RecentFilter */],
                __WEBPACK_IMPORTED_MODULE_5__modules_forms_connectForm_directive__["a" /* ConnectFormDirective */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__filter__["a" /* FilterPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components__["a" /* ComponentModule */],
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__filter__["a" /* FilterPage */]
            ]
        })
    ], FilterModule);
    return FilterModule;
}());

//# sourceMappingURL=filter.module.js.map

/***/ }),

/***/ 393:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailsModule", function() { return DetailsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__details__ = __webpack_require__(675);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var DetailsModule = /** @class */ (function () {
    function DetailsModule() {
    }
    DetailsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__details__["a" /* DetailsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__details__["a" /* DetailsPage */]),
                __WEBPACK_IMPORTED_MODULE_2__components__["a" /* ComponentModule */],
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__details__["a" /* DetailsPage */]
            ]
        })
    ], DetailsModule);
    return DetailsModule;
}());

//# sourceMappingURL=details.module.js.map

/***/ }),

/***/ 394:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContractDetailsModule", function() { return ContractDetailsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__contractDetails__ = __webpack_require__(676);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ContractDetailsModule = /** @class */ (function () {
    function ContractDetailsModule() {
    }
    ContractDetailsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__contractDetails__["a" /* ContractDetailsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__contractDetails__["a" /* ContractDetailsPage */]),
                __WEBPACK_IMPORTED_MODULE_2__components__["a" /* ComponentModule */],
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__contractDetails__["a" /* ContractDetailsPage */]
            ]
        })
    ], ContractDetailsModule);
    return ContractDetailsModule;
}());

//# sourceMappingURL=contractDetails.module.js.map

/***/ }),

/***/ 395:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DateRangeSelector; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DateRangeSelector = /** @class */ (function () {
    function DateRangeSelector(translateService, viewCtrl, fb) {
        this.translateService = translateService;
        this.viewCtrl = viewCtrl;
        this.fb = fb;
        this.pickerOptions = {
            buttons: [{
                    text: this.translateService.instant('CLEAR'),
                    handler: function () {
                        //   this.onClick()
                    }
                }]
        };
        var today = __WEBPACK_IMPORTED_MODULE_2_moment___default()().format('YYYY-MM-DD');
        this.selection = this.fb.group({
            startDate: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["b" /* FormControl */](today, null),
            endDate: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["b" /* FormControl */](today, null)
        });
    }
    DateRangeSelector.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () {
            _this.startDateInput.open();
        }, 20);
    };
    DateRangeSelector.prototype.submit = function () {
        this.viewCtrl.dismiss(this.selection.value);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('startDate'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* DateTime */])
    ], DateRangeSelector.prototype, "startDateInput", void 0);
    DateRangeSelector = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'date-range-selector',template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\modals\dateRangeSelector\dateRangeSelector.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>{{\'CUSTOM_DATE_RANGE_TITLE\' | translate}}</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n    <form [formGroup]="selection">\n\n        <ion-list>\n\n            <ion-item>\n\n                <ion-label>{{\'FROM_DATE\' | translate}}</ion-label>\n\n                <ion-datetime #startDate\n\n                    [pickerOptions]="pickerOptions"\n\n                    displayFormat="DDDD MMM D, YYYY"\n\n                    min="2005"\n\n                    max="2019"\n\n                    formControlName="startDate"\n\n                    ></ion-datetime>\n\n                \n\n            </ion-item>\n\n            <ion-item>\n\n                <ion-datetime #startDate\n\n                    [pickerOptions]="pickerOptions"\n\n                    displayFormat="DDDD MMM D, YYYY"\n\n                    min="2005"\n\n                    max="2019"\n\n                    formControlName="endDate"\n\n                    ></ion-datetime>\n\n\n\n                <ion-label>{{\'TO_DATE\' | translate}}</ion-label>\n\n                <!-- <ion-datetime [pickerOptions]="customOptions" displayFormat="DDDD MMM D, YYYY" min="2005" max="2019" [(ngModel)]="selection.endDate"></ion-datetime> -->\n\n            </ion-item>\n\n        </ion-list>\n\n    </form>\n\n\n\n</ion-content>\n\n\n\n<ion-footer>\n\n    <ion-toolbar>\n\n        <button class="btn-primary filled" (click)="submit()">{{\'SET_DATE_RANGE\' | translate}}</button>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\modals\dateRangeSelector\dateRangeSelector.html"*/,
            styles: ['./dateRangeSelector.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */], __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormBuilder */]])
    ], DateRangeSelector);
    return DateRangeSelector;
}());

//# sourceMappingURL=dateRangeSelector.js.map

/***/ }),

/***/ 396:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export loadIds */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return loadIdsSearch; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return isShowingLoadIdsSearch; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return visibleLoadIds; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getReferenceSet; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};


var loadIds = function (state) {
    return state['loadids'].loadIds;
};
var loadIdsSearch = function (state) {
    return state['loadids'].search || {};
};
var isShowingLoadIdsSearch = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(loadIdsSearch, function (search) {
    return Object(__WEBPACK_IMPORTED_MODULE_1_lodash__["some"])(Object(__WEBPACK_IMPORTED_MODULE_1_lodash__["keys"])(search), function (searchKey) {
        return search[searchKey] && search[searchKey] !== 'any';
    });
});
var visibleLoadIds = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(loadIds, loadIdsSearch, isShowingLoadIdsSearch, function (loadIds, search, isShowingLoadIdsSearch) {
    if (isShowingLoadIdsSearch) {
        var searchKeys_1 = Object(__WEBPACK_IMPORTED_MODULE_1_lodash__["keys"])(search);
        return loadIds.filter(function (loadId) {
            return Object(__WEBPACK_IMPORTED_MODULE_1_lodash__["some"])(searchKeys_1, function (key) {
                return search[key] === loadId[key];
            });
        });
    }
    return loadIds;
});
var getReferenceSet = function (type, titleKey, valueKey) {
    return Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(loadIds, function (loadIds) {
        var items = Object(__WEBPACK_IMPORTED_MODULE_1_lodash__["uniqBy"])(loadIds, type);
        return items.map(function (i) {
            return __assign({}, i, { title: i[titleKey], value: i[valueKey] });
        });
    });
};
//# sourceMappingURL=loadIds.selectors.js.map

/***/ }),

/***/ 397:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return liftings; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return liftingsSearch; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return isShowingLiftingsSearch; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getReferenceSet; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};


var liftings = function (state) {
    return state.liftings.items;
};
var liftingsSearch = function (state) {
    return state['liftings'].search || {};
};
var isShowingLiftingsSearch = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(liftingsSearch, function (search) {
    return Object(__WEBPACK_IMPORTED_MODULE_1_lodash__["some"])(Object(__WEBPACK_IMPORTED_MODULE_1_lodash__["keys"])(search), function (searchKey) {
        return search[searchKey] && search[searchKey] !== 'any';
    });
});
var getReferenceSet = function (type, titleKey, valueKey) {
    return Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(liftings, function (liftings) {
        var items = Object(__WEBPACK_IMPORTED_MODULE_1_lodash__["uniqBy"])(liftings, type);
        return items.map(function (i) {
            return __assign({}, i, { title: i[titleKey], value: i[valueKey] });
        });
    });
};
//# sourceMappingURL=liftings.selectors.js.map

/***/ }),

/***/ 398:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeModule", function() { return HomeModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home__ = __webpack_require__(677);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var HomeModule = /** @class */ (function () {
    function HomeModule() {
    }
    HomeModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */]),
                __WEBPACK_IMPORTED_MODULE_3__components__["a" /* ComponentModule */],
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */]
            ]
        })
    ], HomeModule);
    return HomeModule;
}());

//# sourceMappingURL=home.module.js.map

/***/ }),

/***/ 399:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HSSEModule", function() { return HSSEModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__hsse__ = __webpack_require__(678);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var HSSEModule = /** @class */ (function () {
    function HSSEModule() {
    }
    HSSEModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__hsse__["a" /* HSSEPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__hsse__["a" /* HSSEPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components__["a" /* ComponentModule */],
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__hsse__["a" /* HSSEPage */]
            ]
        })
    ], HSSEModule);
    return HSSEModule;
}());

//# sourceMappingURL=hsse.module.js.map

/***/ }),

/***/ 400:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LiftingsModule", function() { return LiftingsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__liftings__ = __webpack_require__(679);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var LiftingsModule = /** @class */ (function () {
    function LiftingsModule() {
    }
    LiftingsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__liftings__["a" /* LiftingsPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__liftings__["a" /* LiftingsPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components__["a" /* ComponentModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__liftings__["a" /* LiftingsPage */]
            ]
        })
    ], LiftingsModule);
    return LiftingsModule;
}());

//# sourceMappingURL=liftings.module.js.map

/***/ }),

/***/ 401:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return setLiftingsSearch; });
/* harmony export (immutable) */ __webpack_exports__["a"] = liftingsReducer;
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var initialState = {
    items: [
        {
            'INVOICE_NO': 6306571629,
            'BILLING_DOC_ITEMNO': 10,
            'BILLING_DOC_TYPE': 'M',
            'BILLING_DOC_TYPE_DESC': 'Rechnung',
            'PRODUCT_ID': 100003017,
            'MATERIAL_DESCRIPTION': 'Diesel mit Additiv',
            'PLANT_ID': 2101,
            'DELIVERY_POINT_NAME': 'Transtank TL Gelsenkirchen',
            'LIFTING_DATE': 'Thu Dec 12 00:00:00 UTC 2013',
            'LIFTING_TIME': '',
            'LIFTED_QUANTITY': '20,000',
            'UOM': 'L15',
            'STATUS_ID': 904,
            'STATUS': 'Offenstehende Rechnung',
            'CLAIM_CREATED': '',
            'CLAIM_CREATED_DATE': ''
        },
        {
            'INVOICE_NO': 6306571109,
            'BILLING_DOC_ITEMNO': 10,
            'BILLING_DOC_TYPE': 'M',
            'BILLING_DOC_TYPE_DESC': 'Rechnung',
            'PRODUCT_ID': 100003017,
            'MATERIAL_DESCRIPTION': 'Diesel mit Additiv',
            'PLANT_ID': 5531,
            'DELIVERY_POINT_NAME': 'Raffinerie Holborn Hamburg',
            'LIFTING_DATE': 'Thu Dec 12 00:00:00 UTC 2013',
            'LIFTING_TIME': '',
            'LIFTED_QUANTITY': '31,988',
            'UOM': 'L15',
            'STATUS_ID': 904,
            'STATUS': 'Offenstehende Rechnung',
            'CLAIM_CREATED': '',
            'CLAIM_CREATED_DATE': ''
        },
        {
            'INVOICE_NO': 6306571109,
            'BILLING_DOC_ITEMNO': 11,
            'BILLING_DOC_TYPE': 'M',
            'BILLING_DOC_TYPE_DESC': 'Rechnung',
            'PRODUCT_ID': 100003017,
            'MATERIAL_DESCRIPTION': 'Diesel mit Additiv',
            'PLANT_ID': 5531,
            'DELIVERY_POINT_NAME': 'Raffinerie Holborn Hamburg',
            'LIFTING_DATE': 'Thu Dec 12 00:00:00 UTC 2013',
            'LIFTING_TIME': '',
            'LIFTED_QUANTITY': '31,928',
            'UOM': 'L15',
            'STATUS_ID': 904,
            'STATUS': 'Offenstehende Rechnung',
            'CLAIM_CREATED': '',
            'CLAIM_CREATED_DATE': ''
        },
        {
            'INVOICE_NO': 6306571109,
            'BILLING_DOC_ITEMNO': 12,
            'BILLING_DOC_TYPE': 'M',
            'BILLING_DOC_TYPE_DESC': 'Rechnung',
            'PRODUCT_ID': 100003017,
            'MATERIAL_DESCRIPTION': 'Diesel mit Additiv',
            'PLANT_ID': 5531,
            'DELIVERY_POINT_NAME': 'Raffinerie Holborn Hamburg',
            'LIFTING_DATE': 'Thu Dec 12 00:00:00 UTC 2013',
            'LIFTING_TIME': '',
            'LIFTED_QUANTITY': '31,985',
            'UOM': 'L15',
            'STATUS_ID': 904,
            'STATUS': 'Offenstehende Rechnung',
            'CLAIM_CREATED': '',
            'CLAIM_CREATED_DATE': ''
        },
        {
            'INVOICE_NO': 6306571109,
            'BILLING_DOC_ITEMNO': 13,
            'BILLING_DOC_TYPE': 'M',
            'BILLING_DOC_TYPE_DESC': 'Rechnung',
            'PRODUCT_ID': 100003017,
            'MATERIAL_DESCRIPTION': 'Diesel mit Additiv',
            'PLANT_ID': 5531,
            'DELIVERY_POINT_NAME': 'Raffinerie Holborn Hamburg',
            'LIFTING_DATE': 'Thu Dec 12 00:00:00 UTC 2013',
            'LIFTING_TIME': '',
            'LIFTED_QUANTITY': '33,224',
            'UOM': 'L15',
            'STATUS_ID': 904,
            'STATUS': 'Offenstehende Rechnung',
            'CLAIM_CREATED': '',
            'CLAIM_CREATED_DATE': ''
        },
        {
            'INVOICE_NO': 6306571109,
            'BILLING_DOC_ITEMNO': 14,
            'BILLING_DOC_TYPE': 'M',
            'BILLING_DOC_TYPE_DESC': 'Rechnung',
            'PRODUCT_ID': 100003017,
            'MATERIAL_DESCRIPTION': 'Diesel mit Additiv',
            'PLANT_ID': 5531,
            'DELIVERY_POINT_NAME': 'Raffinerie Holborn Hamburg',
            'LIFTING_DATE': 'Thu Dec 12 00:00:00 UTC 2013',
            'LIFTING_TIME': '',
            'LIFTED_QUANTITY': '29,470',
            'UOM': 'L15',
            'STATUS_ID': 904,
            'STATUS': 'Offenstehende Rechnung',
            'CLAIM_CREATED': '',
            'CLAIM_CREATED_DATE': ''
        },
        {
            'INVOICE_NO': 6306570468,
            'BILLING_DOC_ITEMNO': 590,
            'BILLING_DOC_TYPE': 'M',
            'BILLING_DOC_TYPE_DESC': 'Rechnung',
            'PRODUCT_ID': 100002384,
            'MATERIAL_DESCRIPTION': 'Diesel ohne Additiv',
            'PLANT_ID': 1514,
            'DELIVERY_POINT_NAME': 'Raffinerie Gelsenkirchen-Horst',
            'LIFTING_DATE': 'Thu Dec 12 00:00:00 UTC 2013',
            'LIFTING_TIME': '',
            'LIFTED_QUANTITY': '30,871',
            'UOM': 'L15',
            'STATUS_ID': 901,
            'STATUS': 'Überfällige Rechnung',
            'CLAIM_CREATED': '',
            'CLAIM_CREATED_DATE': ''
        },
        {
            'INVOICE_NO': 6306570468,
            'BILLING_DOC_ITEMNO': 591,
            'BILLING_DOC_TYPE': 'M',
            'BILLING_DOC_TYPE_DESC': 'Rechnung',
            'PRODUCT_ID': 100002384,
            'MATERIAL_DESCRIPTION': 'Diesel ohne Additiv',
            'PLANT_ID': 1514,
            'DELIVERY_POINT_NAME': 'Raffinerie Gelsenkirchen-Horst',
            'LIFTING_DATE': 'Thu Dec 12 00:00:00 UTC 2013',
            'LIFTING_TIME': '',
            'LIFTED_QUANTITY': '30,812',
            'UOM': 'L15',
            'STATUS_ID': 901,
            'STATUS': 'Überfällige Rechnung',
            'CLAIM_CREATED': '',
            'CLAIM_CREATED_DATE': ''
        },
        {
            'INVOICE_NO': 6306570509,
            'BILLING_DOC_ITEMNO': 10,
            'BILLING_DOC_TYPE': 'M',
            'BILLING_DOC_TYPE_DESC': 'Rechnung',
            'PRODUCT_ID': 100002374,
            'MATERIAL_DESCRIPTION': 'DK Schiffsbetriebsstoff mit Add. o. Bio',
            'PLANT_ID': 5579,
            'DELIVERY_POINT_NAME': 'Oiltanking TL Duisburg',
            'LIFTING_DATE': 'Thu Dec 12 00:00:00 UTC 2013',
            'LIFTING_TIME': '',
            'LIFTED_QUANTITY': 32.277,
            'UOM': 'L15',
            'STATUS_ID': 901,
            'STATUS': 'Überfällige Rechnung',
            'CLAIM_CREATED': '',
            'CLAIM_CREATED_DATE': ''
        },
        {
            'INVOICE_NO': 6306570275,
            'BILLING_DOC_ITEMNO': 695,
            'BILLING_DOC_TYPE': 'O',
            'BILLING_DOC_TYPE_DESC': 'Gutschrift',
            'PRODUCT_ID': 100003017,
            'MATERIAL_DESCRIPTION': 'Diesel mit Additiv',
            'PLANT_ID': 2104,
            'DELIVERY_POINT_NAME': 'Transtank TL Gustavsburg',
            'LIFTING_DATE': 'Thu Dec 12 00:00:00 UTC 2013',
            'LIFTING_TIME': '',
            'LIFTED_QUANTITY': 1,
            'UOM': 'L15',
            'STATUS_ID': 902,
            'STATUS': 'Überfällige Gutschrift',
            'CLAIM_CREATED': '',
            'CLAIM_CREATED_DATE': ''
        }
    ],
    search: {}
};
var SET_LIFTINGS_SEARCH = 'SET_LIFTINGS_SEARCH';
var setLiftingsSearch = function (payload) { return ({
    type: SET_LIFTINGS_SEARCH,
    payload: payload
}); };
function liftingsReducer(state, _a) {
    if (state === void 0) { state = initialState; }
    var type = _a.type, payload = _a.payload;
    switch (type) {
        case SET_LIFTINGS_SEARCH:
            return __assign({}, state, { search: payload });
        default:
            return state;
    }
}
//# sourceMappingURL=liftings.reducer.js.map

/***/ }),

/***/ 402:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadIdsModule", function() { return LoadIdsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__loadids__ = __webpack_require__(680);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var LoadIdsModule = /** @class */ (function () {
    function LoadIdsModule() {
    }
    LoadIdsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__loadids__["a" /* LoadIdsPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__loadids__["a" /* LoadIdsPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components__["a" /* ComponentModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__loadids__["a" /* LoadIdsPage */]
            ]
        })
    ], LoadIdsModule);
    return LoadIdsModule;
}());

//# sourceMappingURL=loadids.module.js.map

/***/ }),

/***/ 403:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginModule", function() { return LoginModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(681);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var LoginModule = /** @class */ (function () {
    function LoginModule() {
    }
    LoginModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components__["a" /* ComponentModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]
            ]
        })
    ], LoginModule);
    return LoginModule;
}());

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 404:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EnvironmentModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_configProvider__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__modules_config_reducer__ = __webpack_require__(406);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var EnvironmentModal = /** @class */ (function () {
    function EnvironmentModal(viewCtrl, configService, store) {
        var _this = this;
        this.viewCtrl = viewCtrl;
        this.configService = configService;
        this.store = store;
        this.store.select('config', 'environment')
            .subscribe(function (v) { return _this.selectedEnv = v; });
    }
    EnvironmentModal.prototype.dismiss = function () {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_4__modules_config_reducer__["b" /* setEnvironment */](this.selectedEnv));
        this.viewCtrl.dismiss();
    };
    EnvironmentModal = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\modals\environment\environmentModal.component.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-buttons start>\n\n      <button ion-button (click)="dismiss()">Close</button>\n\n    </ion-buttons>\n\n    <ion-title>Select environment</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    <ion-list radio-group [(ngModel)]="selectedEnv">\n\n      <ion-item *ngFor="let env of configService.envs">\n\n        <ion-label>{{env}}</ion-label>\n\n        <ion-radio [value]="env"></ion-radio>\n\n      </ion-item>\n\n    </ion-list>\n\n  <button class="btn-primary" (click)="dismiss()">Save</button>\n\n\n\n</ion-content>\n\n\n\n<ion-footer>\n\n    <ion-toolbar>\n\n        <button class="btn-primary filled" (click)="dismiss()">{{\'SET_ENVIRONMENT\' | translate}}</button>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\modals\environment\environmentModal.component.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_3__config_configProvider__["a" /* ConfigService */],
            __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["h" /* Store */]])
    ], EnvironmentModal);
    return EnvironmentModal;
}());

//# sourceMappingURL=environmentModal.component.js.map

/***/ }),

/***/ 405:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Environments; });
var Environments = {
    local: {
        mock: true,
        // show/hide debug messages / options
        debug: true,
        pdfUrl: '/mocks/test.pdf'
    },
    dev: {
        api: {
            protocol: 'https://',
            uri: 'mobile-a59a70163.hana.ondemand.com:443/',
            appId: 'com.bp.csp.fuels/',
            authUrl: 'odata/applications/latest/',
            service: 'irj/servlet/prt/portal/prtroot/bp.com~bpdc~fvc~css~data~service.',
            // length ajax call
            timeout: 5 * 60 * 1000,
            authTimeout: 15 * 1000
        },
        analyticsId: 'UA-44971642-30',
        // show/hide debug messages / options
        debug: true,
        pdfUrl: 'irj/servlet/prt/portal/prtroot/bp.com~bpdc~fvc~css~data~service.GetPDFData'
    },
    test_internal: {
        api: {
            protocol: 'https://',
            uri: 'mobile-ac9181cf2.hana.ondemand.com:443/',
            appId: 'com.bp.csp.fuels.internal/',
            authUrl: 'odata/applications/latest/',
            service: 'irj/servlet/prt/portal/prtroot/bp.com~bpdc~fvc~css~data~service.',
            // length ajax call
            timeout: 5 * 60 * 1000,
            authTimeout: 15 * 1000
        },
        analyticsId: 'UA-44971642-31',
        // show/hide debug messages / options
        debug: true,
        pdfUrl: 'irj/servlet/prt/portal/prtroot/bp.com~bpdc~fvc~css~data~service.GetPDFData'
    },
    test_external: {
        default: true,
        api: {
            protocol: 'https://',
            uri: 'mobile-ac9181cf2.hana.ondemand.com:443/',
            appId: 'com.bp.csp.fuels.external/',
            authUrl: 'odata/applications/latest/',
            service: 'irj/servlet/prt/portal/prtroot/bp.com~bpdc~fvc~css~data~service.',
            // length ajax call
            timeout: 5 * 60 * 1000,
            authTimeout: 15 * 1000
        },
        analyticsId: 'UA-44971642-31',
        // show/hide debug messages / options
        debug: false,
        pdfUrl: 'irj/servlet/prt/portal/prtroot/bp.com~bpdc~fvc~css~data~service.GetPDFData'
    },
    prod_internal: {
        api: {
            protocol: 'https://',
            uri: 'mobile-a292390f8.hana.ondemand.com:443/',
            appId: 'com.bp.csp.fuels.internal/',
            authUrl: 'odata/applications/latest/',
            service: 'irj/servlet/prt/portal/prtroot/bp.com~bpdc~fvc~css~data~service.',
            // length ajax call
            timeout: 5 * 60 * 1000,
            authTimeout: 15 * 1000
        },
        analyticsId: 'UA-44971642-32',
        // show/hide debug messages / options
        debug: false,
        pdfUrl: 'irj/servlet/prt/portal/prtroot/bp.com~bpdc~fvc~css~data~service.GetPDFData'
    },
    prod_external: {
        api: {
            protocol: 'https://',
            uri: 'mobile-a292390f8.hana.ondemand.com:443/',
            appId: 'com.bp.csp.fuels.external/',
            authUrl: 'odata/applications/latest/',
            service: 'irj/servlet/prt/portal/prtroot/bp.com~bpdc~fvc~css~data~service.',
            // length ajax call
            timeout: 5 * 60 * 1000,
            authTimeout: 15 * 1000
        },
        analyticsId: 'UA-44971642-32',
        // show/hide debug messages / options
        debug: false,
        pdfUrl: 'irj/servlet/prt/portal/prtroot/bp.com~bpdc~fvc~css~data~service.GetPDFData'
    }
};
//# sourceMappingURL=config.js.map

/***/ }),

/***/ 406:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export SET_ENVIRONMENT */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return setEnvironment; });
/* unused harmony export Actions */
/* harmony export (immutable) */ __webpack_exports__["a"] = configReducer;
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var SET_ENVIRONMENT = 'SET_ENVIRONMENT';
var setEnvironment = /** @class */ (function () {
    function setEnvironment(payload) {
        this.payload = payload;
        this.type = SET_ENVIRONMENT;
    }
    return setEnvironment;
}());

var Actions = {
    setEnvironment: setEnvironment
};
var initialState = ({
    environment: 'local'
});
function configReducer(state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case SET_ENVIRONMENT:
            return __assign({}, state, { environment: action.payload });
        default:
            return state;
    }
}
//# sourceMappingURL=config.reducer.js.map

/***/ }),

/***/ 407:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationsModule", function() { return NotificationsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__notifications__ = __webpack_require__(683);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__subPages_notificationDetails_notificationDetails_module__ = __webpack_require__(408);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__subPages_notificationSettings_notificationSettings_module__ = __webpack_require__(409);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var NotificationsModule = /** @class */ (function () {
    function NotificationsModule() {
    }
    NotificationsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__notifications__["a" /* NotificationsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__notifications__["a" /* NotificationsPage */]),
                __WEBPACK_IMPORTED_MODULE_4__subPages_notificationDetails_notificationDetails_module__["NotificationDetailsModule"],
                __WEBPACK_IMPORTED_MODULE_5__subPages_notificationSettings_notificationSettings_module__["NotificationSettingsModule"],
                __WEBPACK_IMPORTED_MODULE_3__components__["a" /* ComponentModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__notifications__["a" /* NotificationsPage */]
            ]
        })
    ], NotificationsModule);
    return NotificationsModule;
}());

//# sourceMappingURL=notifications.module.js.map

/***/ }),

/***/ 408:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationDetailsModule", function() { return NotificationDetailsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__notificationDetails__ = __webpack_require__(684);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var NotificationDetailsModule = /** @class */ (function () {
    function NotificationDetailsModule() {
    }
    NotificationDetailsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__notificationDetails__["a" /* NotificationDetailsPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__notificationDetails__["a" /* NotificationDetailsPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components__["a" /* ComponentModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__notificationDetails__["a" /* NotificationDetailsPage */]
            ]
        })
    ], NotificationDetailsModule);
    return NotificationDetailsModule;
}());

//# sourceMappingURL=notificationDetails.module.js.map

/***/ }),

/***/ 409:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationSettingsModule", function() { return NotificationSettingsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__notificationSettings__ = __webpack_require__(685);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var NotificationSettingsModule = /** @class */ (function () {
    function NotificationSettingsModule() {
    }
    NotificationSettingsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__notificationSettings__["a" /* NotificationSettingsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__notificationSettings__["a" /* NotificationSettingsPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components__["a" /* ComponentModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__notificationSettings__["a" /* NotificationSettingsPage */]
            ]
        })
    ], NotificationSettingsModule);
    return NotificationSettingsModule;
}());

//# sourceMappingURL=notificationSettings.module.js.map

/***/ }),

/***/ 410:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlaygroundModule", function() { return PlaygroundModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__playground__ = __webpack_require__(686);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var PlaygroundModule = /** @class */ (function () {
    function PlaygroundModule() {
    }
    PlaygroundModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__playground__["a" /* PlaygroundPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__playground__["a" /* PlaygroundPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components__["a" /* ComponentModule */],
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__playground__["a" /* PlaygroundPage */]
            ]
        })
    ], PlaygroundModule);
    return PlaygroundModule;
}());

//# sourceMappingURL=playground.module.js.map

/***/ }),

/***/ 411:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PricesModule", function() { return PricesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__prices__ = __webpack_require__(687);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_priceItem_priceItem__ = __webpack_require__(689);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var PricesModule = /** @class */ (function () {
    function PricesModule() {
    }
    PricesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__prices__["a" /* PricesPage */],
                __WEBPACK_IMPORTED_MODULE_4__components_priceItem_priceItem__["a" /* PriceItem */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__prices__["a" /* PricesPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components__["a" /* ComponentModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__prices__["a" /* PricesPage */]
            ]
        })
    ], PricesModule);
    return PricesModule;
}());

//# sourceMappingURL=prices.module.js.map

/***/ }),

/***/ 412:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchModule", function() { return SearchModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__search__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_filterSelect_filterSelect_component__ = __webpack_require__(690);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SearchModule = /** @class */ (function () {
    function SearchModule() {
    }
    SearchModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__search__["a" /* SearchPage */],
                __WEBPACK_IMPORTED_MODULE_3__components_filterSelect_filterSelect_component__["a" /* FilterSelect */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__search__["a" /* SearchPage */])
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__search__["a" /* SearchPage */]
            ]
        })
    ], SearchModule);
    return SearchModule;
}());

//# sourceMappingURL=search.module.js.map

/***/ }),

/***/ 413:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductCatalogueModule", function() { return ProductCatalogueModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__productCatalogue__ = __webpack_require__(691);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ProductCatalogueModule = /** @class */ (function () {
    function ProductCatalogueModule() {
    }
    ProductCatalogueModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__productCatalogue__["a" /* ProductCataloguePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__productCatalogue__["a" /* ProductCataloguePage */]),
                __WEBPACK_IMPORTED_MODULE_3__components__["a" /* ComponentModule */],
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__productCatalogue__["a" /* ProductCataloguePage */]
            ]
        })
    ], ProductCatalogueModule);
    return ProductCatalogueModule;
}());

//# sourceMappingURL=productCatalogue.module.js.map

/***/ }),

/***/ 414:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactsModule", function() { return ContactsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contacts__ = __webpack_require__(692);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ContactsModule = /** @class */ (function () {
    function ContactsModule() {
    }
    ContactsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__contacts__["a" /* ContactsPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__contacts__["a" /* ContactsPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components__["a" /* ComponentModule */],
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__contacts__["a" /* ContactsPage */]
            ]
        })
    ], ContactsModule);
    return ContactsModule;
}());

//# sourceMappingURL=contacts.module.js.map

/***/ }),

/***/ 416:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailsModule", function() { return ProductDetailsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__productDetails__ = __webpack_require__(693);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ProductDetailsModule = /** @class */ (function () {
    function ProductDetailsModule() {
    }
    ProductDetailsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__productDetails__["a" /* ProductDetailsPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__productDetails__["a" /* ProductDetailsPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components__["a" /* ComponentModule */],
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__productDetails__["a" /* ProductDetailsPage */]
            ]
        })
    ], ProductDetailsModule);
    return ProductDetailsModule;
}());

//# sourceMappingURL=productDetails.module.js.map

/***/ }),

/***/ 417:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsefulInformationModule", function() { return UsefulInformationModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__usefulInformation__ = __webpack_require__(694);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__subPages_contacts_contacts_module__ = __webpack_require__(414);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__subPages_productCatalogue_productCatalogue_module__ = __webpack_require__(413);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__subPages_productDetails_productDetails_module__ = __webpack_require__(416);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var UsefulInformationModule = /** @class */ (function () {
    function UsefulInformationModule() {
    }
    UsefulInformationModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__usefulInformation__["a" /* UsefulInformationPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__usefulInformation__["a" /* UsefulInformationPage */]),
                __WEBPACK_IMPORTED_MODULE_4__subPages_contacts_contacts_module__["ContactsModule"],
                __WEBPACK_IMPORTED_MODULE_5__subPages_productCatalogue_productCatalogue_module__["ProductCatalogueModule"],
                __WEBPACK_IMPORTED_MODULE_6__subPages_productDetails_productDetails_module__["ProductDetailsModule"],
                __WEBPACK_IMPORTED_MODULE_3__components__["a" /* ComponentModule */],
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__usefulInformation__["a" /* UsefulInformationPage */]
            ]
        })
    ], UsefulInformationModule);
    return UsefulInformationModule;
}());

//# sourceMappingURL=usefulInformation.module.js.map

/***/ }),

/***/ 418:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserModule", function() { return UserModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user__ = __webpack_require__(695);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var UserModule = /** @class */ (function () {
    function UserModule() {
    }
    UserModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__user__["a" /* UserPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__user__["a" /* UserPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components__["a" /* ComponentModule */],
            ]
        })
    ], UserModule);
    return UserModule;
}());

//# sourceMappingURL=user.module.js.map

/***/ }),

/***/ 461:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export LOADING_ITEM */
/* unused harmony export LOADING_ITEM_COMPLETE */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return loadingItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return loadingItemComplete; });
/* harmony export (immutable) */ __webpack_exports__["c"] = loadingReducer;
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var initialState = {
    count: 0,
};
var LOADING_ITEM = 'LOADING_ITEM';
var LOADING_ITEM_COMPLETE = 'LOADING_ITEM_COMPLETE';
var loadingItem = function (url) {
    return {
        type: LOADING_ITEM,
        payload: url,
    };
};
var loadingItemComplete = function (url) {
    return {
        type: LOADING_ITEM_COMPLETE,
        payload: url,
    };
};
function loadingReducer(state, _a) {
    if (state === void 0) { state = initialState; }
    var type = _a.type, payload = _a.payload;
    switch (type) {
        case LOADING_ITEM:
            console.log('LOADING STARTING:', payload);
            return __assign({}, state, { count: state.count + 1 });
        case LOADING_ITEM_COMPLETE:
            console.log('LOADING COMPLETE:', payload);
            var newCount = state.count - 1;
            return __assign({}, state, { count: newCount <= 0 ? 0 : newCount });
        default:
            return state;
    }
}
//# sourceMappingURL=loading.reducer.js.map

/***/ }),

/***/ 464:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return viewDidEnter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routerReducer; });
var initialState = {
    currentParams: {},
    currentRoute: ''
};
var VIEW_DID_ENTER = 'VIEW_DID_ENTER';
var viewDidEnter = function (payload) {
    return {
        type: VIEW_DID_ENTER,
        payload: payload
    };
};
var routerReducer = function (state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case VIEW_DID_ENTER:
            return {
                currentRoute: action.payload.name,
                currentParams: action.payload.data
            };
        default:
            return state;
    }
};
//# sourceMappingURL=router.reducer.js.map

/***/ }),

/***/ 465:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return rootReducer; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__modules_counter_reducer__ = __webpack_require__(724);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user_reducer__ = __webpack_require__(466);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_reducer__ = __webpack_require__(406);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_basket_basket_reducer__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__forms_forms_reducer__ = __webpack_require__(467);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_aoh_aoh_reducer__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_contracts_contracts_reducer__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__router_router_reducer__ = __webpack_require__(464);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__loading_reducer__ = __webpack_require__(461);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_notifications_notifications_reducer__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_liftings_liftings_reducer__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_customers_customers_reducer__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_loadids_loadids_reducer__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_prices_prices_reducer__ = __webpack_require__(159);














var rootReducer = {
    router: __WEBPACK_IMPORTED_MODULE_7__router_router_reducer__["a" /* routerReducer */],
    counterReducer: __WEBPACK_IMPORTED_MODULE_0__modules_counter_reducer__["a" /* counterReducer */],
    userReducer: __WEBPACK_IMPORTED_MODULE_1__user_reducer__["c" /* userReducer */],
    forms: __WEBPACK_IMPORTED_MODULE_4__forms_forms_reducer__["c" /* forms */],
    config: __WEBPACK_IMPORTED_MODULE_2__config_reducer__["a" /* configReducer */],
    aoh: __WEBPACK_IMPORTED_MODULE_5__pages_aoh_aoh_reducer__["e" /* aohReducer */],
    contracts: __WEBPACK_IMPORTED_MODULE_6__pages_contracts_contracts_reducer__["d" /* contractsReducer */],
    basket: __WEBPACK_IMPORTED_MODULE_3__pages_basket_basket_reducer__["b" /* basketReducer */],
    loading: __WEBPACK_IMPORTED_MODULE_8__loading_reducer__["c" /* loadingReducer */],
    notifications: __WEBPACK_IMPORTED_MODULE_9__pages_notifications_notifications_reducer__["b" /* notificationsReducer */],
    liftings: __WEBPACK_IMPORTED_MODULE_10__pages_liftings_liftings_reducer__["a" /* liftingsReducer */],
    customers: __WEBPACK_IMPORTED_MODULE_11__pages_customers_customers_reducer__["b" /* customersReducer */],
    loadids: __WEBPACK_IMPORTED_MODULE_12__pages_loadids_loadids_reducer__["d" /* loadIdsReducer */],
    prices: __WEBPACK_IMPORTED_MODULE_13__pages_prices_prices_reducer__["b" /* priceReducer */],
};
//# sourceMappingURL=root.reducer.js.map

/***/ }),

/***/ 466:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LOGIN; });
/* unused harmony export LOGIN_SUCCESS */
/* unused harmony export LOGIN_FAILURE */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return LOGOUT; });
/* harmony export (immutable) */ __webpack_exports__["c"] = userReducer;
// counter.ts
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var LOGIN = 'LOGIN';
var LOGIN_SUCCESS = 'LOGIN_SUCCESS';
var LOGIN_FAILURE = 'LOGIN_FAILURE';
var LOGOUT = 'LOGOUT';
var initialState = {
    loggedIn: false,
};
function userReducer(state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case LOGIN:
            return state;
        case LOGIN_SUCCESS:
            return __assign({}, action.payload);
        case LOGIN_FAILURE:
            return 0;
        default:
            return state;
    }
}
//# sourceMappingURL=user.reducer.js.map

/***/ }),

/***/ 467:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return formErrorAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return formSuccessAction; });
/* harmony export (immutable) */ __webpack_exports__["c"] = forms;
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var FORM_SUBMIT_SUCCESS = 'FORM_SUBMIT_SUCCESS';
var FORM_SUBMIT_ERROR = 'FORM_SUBMIT_ERROR';
var UPDATE_FORM = 'UPDATE_FORM';
var updateForm = function (payload) {
    return {
        type: UPDATE_FORM,
        payload: payload,
    };
};
var formErrorAction = function (payload, err) {
    return {
        type: 'FORM_SUBMIT_ERROR',
        payload: payload
    };
};
var formSuccessAction = function (payload) {
    return {
        type: 'FORM_SUBMIT_SUCCESS',
        payload: payload
    };
};
var initialState = {
    someForm: {
        test: 'fromStore',
        items: [
            { name: 'billy' },
            { name: 'billy' },
            { name: 'something' }
        ],
        terminals: [
            { title: 'option1' }
        ]
    },
    newStory: {
        title: 'asdasd',
        description: ''
    },
    contactUs: {
        email: '',
        message: ''
    }
};
function forms(state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case UPDATE_FORM:
            return __assign({}, state, (_a = {}, _a[action.payload.path] = action.payload.value, _a));
        default:
            return state;
    }
    var _a;
}
//# sourceMappingURL=forms.reducer.js.map

/***/ }),

/***/ 468:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export customers */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return selectedCustomer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return customersAsArray; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);


var customers = function (state) {
    return state['customers'].customers || null;
};
var selectedCustomer = function (state) {
    return state['customers'].selectedCustomer;
};
var customersAsArray = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(customers, function (customers) {
    if (!customers) {
        return [];
    }
    return Object(__WEBPACK_IMPORTED_MODULE_1_lodash__["values"])(customers);
});
//# sourceMappingURL=customers.selectors.js.map

/***/ }),

/***/ 469:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(470);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(592);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(737);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_take__ = __webpack_require__(739);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_take___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_take__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_filter__ = __webpack_require__(742);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_filter___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_filter__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_switchMap__ = __webpack_require__(744);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_switchMap___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_switchMap__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_mergeMap__ = __webpack_require__(747);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_mergeMap___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_mergeMap__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_catch__ = __webpack_require__(750);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_first__ = __webpack_require__(753);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_first___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_first__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_do__ = __webpack_require__(756);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_do___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_do__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs_add_operator_debounce__ = __webpack_require__(759);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs_add_operator_debounce___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_rxjs_add_operator_debounce__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_rxjs_add_operator_delay__ = __webpack_require__(762);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_rxjs_add_operator_delay___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_rxjs_add_operator_delay__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_rxjs_add_observable_interval__ = __webpack_require__(765);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_rxjs_add_observable_interval___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_rxjs_add_observable_interval__);













Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 53:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return APIService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_qs__ = __webpack_require__(458);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_qs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_qs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__config_configProvider__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__config_storageProvider__ = __webpack_require__(96);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var APIService = /** @class */ (function () {
    function APIService(http, configService, storage) {
        this.http = http;
        this.configService = configService;
        this.storage = storage;
    }
    APIService.prototype.getUsername = function () {
        if (this.configService.isMock) {
            return this.userName || 'f1';
        }
        return this.userName;
    };
    APIService.prototype.getServiceUrl = function (serviceCall, mode) {
        var queryString = __WEBPACK_IMPORTED_MODULE_3_qs___default.a.stringify({
            mode: mode,
        });
        // return this.configService.env.api.protocol + this.configService.env.api.uri + '/' + this.configService.env.api.appId + this.configService.env.api.service + serviceCall + '?' + queryString
        return serviceCall + '?' + queryString;
    };
    APIService.prototype.getAuthUrl = function () {
        return 'Connections';
    };
    APIService.prototype.getDeAuthUrl = function (connectionId) {
        return 'Connections(\'' + connectionId + '\')';
    };
    APIService.prototype.getHeaders = function (opts) {
        if (opts === void 0) { opts = {}; }
        return new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["d" /* HttpHeaders */](__assign({ Accept: 'application/json', 'Content-Type': 'application/json' }, opts, { 'Authorization': this.baseAuth }));
    };
    APIService.prototype.authenticate = function (_a) {
        var _this = this;
        var username = _a.username, password = _a.password;
        this.userName = username;
        var url = this.getAuthUrl();
        this.baseAuth = 'Basic ' + btoa(username + ":" + password);
        var headers = this.getHeaders();
        return this.http.post(url, {
            'DeviceType': 'ios'
        }, { headers: headers }).pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["tap"])(function (resp) {
            _this.ApplicationConnectionId = resp['d'].ApplicationConnectionId;
        }));
    };
    APIService.prototype.login = function () {
        var url = this.getServiceUrl('Read', 'GETUSER');
        var headers = this.getHeaders({
            'X-SMP-APPCID': this.ApplicationConnectionId,
        });
        return this.http.post(url, {}, { headers: headers })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["map"])(function (d) {
            // debugger
            var data = d.data;
            data.flags = data.T_ROLES.reduce(function (prev, role) {
                var name = role.name.split('_cpr_').pop();
                prev[name] = true;
                return prev;
            }, {});
            d.data = data;
            d.data.fullName = d.data.E_FIRSTNAME + "  " + d.data.E_LASTNAME;
            return d;
        }));
    };
    APIService.prototype.logout = function () {
        var _this = this;
        var url = this.getDeAuthUrl(this.ApplicationConnectionId);
        var headers = this.getHeaders();
        return this.http.delete(url, { headers: headers })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["tap"])(function (resp) {
            _this.ApplicationConnectionId = undefined;
        }));
    };
    APIService.prototype.aoh = function () {
        var url = this.getServiceUrl('Read', 'OFFERPRICE');
        // const headers = this.getHeaders()
        return this.http.get(url);
    };
    APIService.prototype.flexiContracts = function () {
        var url = this.getServiceUrl('Search', 'FLEXICONTRACT');
        return this.http.get(url);
    };
    APIService.prototype.contractDetails = function () {
        // Note this should go to flexicontract instead
        var url = this.getServiceUrl('Search', 'FLEXICONTRACT_DETAILS');
        return this.http.get(url);
    };
    APIService.prototype.timer = function () {
        var url = this.getServiceUrl('Write', 'CHECKTIME');
        return this.http.get(url);
    };
    APIService.prototype.customers = function () {
        var url = this.getServiceUrl('Search', 'CUSTOMER');
        return this.http.get(url).pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["map"])(function (resp) {
            var out = Object(__WEBPACK_IMPORTED_MODULE_4_lodash__["keyBy"])(resp.data.T_BUSINESS_PARTNERS, 'BP_NUMBER');
            return out;
        }));
    };
    APIService.prototype.creditPosition = function () {
        var url = this.getServiceUrl('Read', 'CREDITPOSITION');
        return this.http.get(url);
    };
    APIService.prototype.getContracts = function () {
        var url = this.getServiceUrl('Search', 'CONTRACTITEMS');
        return this.http.get(url);
    };
    APIService.prototype.getContractDetails = function () {
        var url = this.getServiceUrl('Read', 'CONTRACTITEMS');
        return this.http.get(url);
    };
    APIService.prototype.loadIds = function () {
        var url = this.getServiceUrl('Search', 'LOADIDS');
        return this.http.get(url);
    };
    APIService.prototype.getContractSearchStatusTypes = function () {
        var url = this.getServiceUrl('Search', 'LOADIDS');
        return this.http.get(url)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["tap"])(function (resp) {
            return {
                statuses: [{ title: 'remote', value: 1 }],
                types: [{ title: 'renite', value: 1 }]
            };
        }));
    };
    APIService.prototype.getContractSearchGroups = function () {
        var url = this.getServiceUrl('Search', 'LOADIDS');
        return this.http.get(url)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["tap"])(function (resp) {
            return {
                groups: [{ title: 'remote', value: 1 }],
            };
        }));
    };
    APIService.prototype.getContractSearchPlants = function () {
        var url = this.getServiceUrl('Search', 'LOADIDS');
        return this.http.get(url)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["tap"])(function (resp) {
            return {
                plants: [{ title: 'remote', value: 1 }],
            };
        }));
    };
    APIService.prototype.getCurrentPrices = function () {
        var url = this.getServiceUrl('Report', 'CURRENTPRICE');
        return this.http.get(url);
    };
    APIService.prototype.getHistoricPrices = function () {
        var url = this.getServiceUrl('Report', 'HISTORICPRICE');
        return this.http.get(url);
    };
    APIService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_5__config_configProvider__["a" /* ConfigService */], __WEBPACK_IMPORTED_MODULE_6__config_storageProvider__["a" /* StorageService */]])
    ], APIService);
    return APIService;
}());

//# sourceMappingURL=serviceProvider.js.map

/***/ }),

/***/ 592:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export createTranslateLoader */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(597);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ngrx_store_devtools__ = __webpack_require__(713);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_in_app_browser__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_document_viewer__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_secure_storage__ = __webpack_require__(714);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_file__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_file_transfer__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_file_opener__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_call_number__ = __webpack_require__(415);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__interceptors_auth_interceptor__ = __webpack_require__(715);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__interceptors_loading_interceptor__ = __webpack_require__(718);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ngrx_effects__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ngx_translate_core__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ngx_translate_http_loader__ = __webpack_require__(719);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__app_component__ = __webpack_require__(720);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__components_test_test_component__ = __webpack_require__(721);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__components__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__modals__ = __webpack_require__(722);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_notifications_notifications_module__ = __webpack_require__(407);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__config_configProvider__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__modules_loading_provider__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__config_storageProvider__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__modules_serviceProvider__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__navigation__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_login_login_module__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_home_home_module__ = __webpack_require__(398);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_aoh_aoh_module__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_basket_basket_module__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_search_search_module__ = __webpack_require__(412);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_liftings_liftings_module__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_playground_playground_module__ = __webpack_require__(410);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_contracts_subPages_contractDetails_contractDetails_module__ = __webpack_require__(394);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_loadids_loadids_module__ = __webpack_require__(402);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__pages_usefulInformation_usefulInformation_module__ = __webpack_require__(417);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__pages_prices_prices_module__ = __webpack_require__(411);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__pages_user_user_module__ = __webpack_require__(418);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__ionic_native_status_bar__ = __webpack_require__(462);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__ionic_native_splash_screen__ = __webpack_require__(463);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__modules_root_reducer__ = __webpack_require__(465);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__modules_storeEnchancers__ = __webpack_require__(725);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__modules_effects__ = __webpack_require__(727);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__pages_hsse_hsse_module__ = __webpack_require__(399);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















































function createTranslateLoader(http) {
    return new __WEBPACK_IMPORTED_MODULE_19__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, './assets/locales/', '.json');
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_20__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_21__components_test_test_component__["a" /* TestComponent */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_22__components__["a" /* ComponentModule */],
                __WEBPACK_IMPORTED_MODULE_23__modals__["a" /* ModalModule */],
                __WEBPACK_IMPORTED_MODULE_30__pages_login_login_module__["LoginModule"],
                __WEBPACK_IMPORTED_MODULE_31__pages_home_home_module__["HomeModule"],
                __WEBPACK_IMPORTED_MODULE_32__pages_aoh_aoh_module__["AOHModule"],
                __WEBPACK_IMPORTED_MODULE_33__pages_basket_basket_module__["BasketModule"],
                __WEBPACK_IMPORTED_MODULE_34__pages_search_search_module__["SearchModule"],
                __WEBPACK_IMPORTED_MODULE_47__pages_hsse_hsse_module__["HSSEModule"],
                __WEBPACK_IMPORTED_MODULE_35__pages_liftings_liftings_module__["LiftingsModule"],
                __WEBPACK_IMPORTED_MODULE_36__pages_playground_playground_module__["PlaygroundModule"],
                __WEBPACK_IMPORTED_MODULE_37__pages_contracts_subPages_contractDetails_contractDetails_module__["ContractDetailsModule"],
                __WEBPACK_IMPORTED_MODULE_24__pages_notifications_notifications_module__["NotificationsModule"],
                __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["c" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_24__pages_notifications_notifications_module__["NotificationsModule"],
                __WEBPACK_IMPORTED_MODULE_38__pages_loadids_loadids_module__["LoadIdsModule"],
                __WEBPACK_IMPORTED_MODULE_39__pages_usefulInformation_usefulInformation_module__["UsefulInformationModule"],
                __WEBPACK_IMPORTED_MODULE_40__pages_prices_prices_module__["PricesModule"],
                __WEBPACK_IMPORTED_MODULE_41__pages_user_user_module__["UserModule"],
                __WEBPACK_IMPORTED_MODULE_18__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                    loader: {
                        provide: __WEBPACK_IMPORTED_MODULE_18__ngx_translate_core__["a" /* TranslateLoader */],
                        useFactory: (createTranslateLoader),
                        deps: [__WEBPACK_IMPORTED_MODULE_4__angular_common_http__["b" /* HttpClient */]]
                    }
                }),
                __WEBPACK_IMPORTED_MODULE_6__ngrx_store__["j" /* StoreModule */].forRoot(__WEBPACK_IMPORTED_MODULE_44__modules_root_reducer__["a" /* rootReducer */], { metaReducers: __WEBPACK_IMPORTED_MODULE_45__modules_storeEnchancers__["a" /* metaReducers */] }),
                __WEBPACK_IMPORTED_MODULE_17__ngrx_effects__["c" /* EffectsModule */].forRoot(__WEBPACK_IMPORTED_MODULE_46__modules_effects__["a" /* effects */]),
                __WEBPACK_IMPORTED_MODULE_7__ngrx_store_devtools__["a" /* StoreDevtoolsModule */].instrument({
                    maxAge: 25,
                }),
                __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["g" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_20__app_component__["a" /* MyApp */], {
                    mode: 'md'
                }, {
                    links: [
                        { loadChildren: '../pages/aoh/aoh.module#AOHModule', name: 'aoh', segment: 'aoh', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/aoh/subPages/details/details.module#DetailsModule', name: 'aoh-details', segment: 'aoh/details', priority: 'low', defaultHistory: ['aoh'] },
                        { loadChildren: '../pages/aoh/subPages/filter/filter.module#FilterModule', name: 'filter', segment: 'aoh/filter', priority: 'low', defaultHistory: ['aoh'] },
                        { loadChildren: '../pages/basket/basket.module#BasketModule', name: 'basket', segment: 'basket', priority: 'low', defaultHistory: ['aoh'] },
                        { loadChildren: '../pages/contracts/subPages/contractDetails/contractDetails.module#ContractDetailsModule', name: 'contract-details', segment: 'contracts/:id', priority: 'low', defaultHistory: ['contracts'] },
                        { loadChildren: '../pages/contracts/contracts.module#ContractsModule', name: 'contracts', segment: 'contracts', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/customers.module#CustomersModule', name: 'customers', segment: 'customers', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/home/home.module#HomeModule', name: 'home', segment: 'home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/hsse/hsse.module#HSSEModule', name: 'hsse', segment: 'hsse', priority: 'low', defaultHistory: ['aoh'] },
                        { loadChildren: '../pages/liftings/liftings.module#LiftingsModule', name: 'liftings', segment: 'liftings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/loadids/loadids.module#LoadIdsModule', name: 'loadids', segment: 'loadids', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginModule', name: 'login-page', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/notifications/notifications.module#NotificationsModule', name: 'notifications', segment: 'notifications', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/notifications/subPages/notificationSettings/notificationSettings.module#NotificationSettingsModule', name: 'notification-settings', segment: 'notificationSettings', priority: 'low', defaultHistory: ['notifications'] },
                        { loadChildren: '../pages/playground/playground.module#PlaygroundModule', name: 'playground', segment: 'playground', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/prices/prices.module#PricesModule', name: 'prices', segment: 'prices', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/search/search.module#SearchModule', name: 'search', segment: 'search', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/usefulInformation/subPages/productCatalogue/productCatalogue.module#ProductCatalogueModule', name: 'product-catalogue', segment: 'product-catalogue', priority: 'low', defaultHistory: ['useful-information'] },
                        { loadChildren: '../pages/usefulInformation/subPages/contacts/contacts.module#ContactsModule', name: 'contacts', segment: 'contacts/:type', priority: 'low', defaultHistory: ['useful-information'] },
                        { loadChildren: '../pages/usefulInformation/subPages/productDetails/productDetails.module#ProductDetailsModule', name: 'product-details', segment: 'product-details', priority: 'low', defaultHistory: ['useful-information', 'product-catalogue'] },
                        { loadChildren: '../pages/usefulInformation/usefulInformation.module#UsefulInformationModule', name: 'useful-information', segment: 'useful-information', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/user/user.module#UserModule', name: 'user', segment: 'user', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/notifications/subPages/notificationDetails/notificationDetails.module#NotificationDetailsModule', name: 'notification-details', segment: 'notifcations/:id', priority: 'low', defaultHistory: ['notifications'] }
                    ]
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["e" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_20__app_component__["a" /* MyApp */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_27__config_storageProvider__["a" /* StorageService */],
                __WEBPACK_IMPORTED_MODULE_42__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_43__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_25__config_configProvider__["a" /* ConfigService */],
                __WEBPACK_IMPORTED_MODULE_29__navigation__["a" /* NavigationService */],
                __WEBPACK_IMPORTED_MODULE_26__modules_loading_provider__["a" /* LoadingService */],
                __WEBPACK_IMPORTED_MODULE_28__modules_serviceProvider__["a" /* APIService */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_in_app_browser__["a" /* InAppBrowser */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_document_viewer__["a" /* DocumentViewer */],
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_secure_storage__["a" /* SecureStorage */],
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_file_opener__["a" /* FileOpener */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormGroupDirective */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_call_number__["a" /* CallNumber */],
                {
                    provide: __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["a" /* HTTP_INTERCEPTORS */],
                    useClass: __WEBPACK_IMPORTED_MODULE_15__interceptors_auth_interceptor__["a" /* AuthInterceptor */],
                    multi: true
                },
                {
                    provide: __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["a" /* HTTP_INTERCEPTORS */],
                    useClass: __WEBPACK_IMPORTED_MODULE_16__interceptors_loading_interceptor__["a" /* LoadingInterceptor */],
                    multi: true
                },
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["f" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 621:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AOHPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__aoh_reducer__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__modules_loading_provider__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__aoh_selectors__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__subPages_filter_filter__ = __webpack_require__(264);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AOHPage = /** @class */ (function () {
    function AOHPage(navCtrl, store, loadingService, modalCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.store = store;
        this.loadingService = loadingService;
        this.modalCtrl = modalCtrl;
        this.materials$ = this.store.select(__WEBPACK_IMPORTED_MODULE_5__aoh_selectors__["b" /* aohListings */]);
        this.store.select(__WEBPACK_IMPORTED_MODULE_5__aoh_selectors__["d" /* aohSelectedFilter */])
            .subscribe(function (v) {
            _this.selectedFilter = v;
        });
    }
    AOHPage.prototype.ionViewDidLoad = function () {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_3__aoh_reducer__["f" /* getAoh */])());
    };
    AOHPage.prototype.openFilter = function () {
        this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__subPages_filter_filter__["a" /* FilterPage */], {
            options: __WEBPACK_IMPORTED_MODULE_5__aoh_selectors__["a" /* aohFilterOptions */],
            recents: __WEBPACK_IMPORTED_MODULE_5__aoh_selectors__["c" /* aohRecentFilters */],
            onSetFilter: __WEBPACK_IMPORTED_MODULE_3__aoh_reducer__["o" /* setAohFilter */],
        }, {}).present();
        // profileModal.present();
        // this.navCtrl.push('filter', {
        //   options: aohFilterOptions,
        //   recents: aohRecentFilters,
        //   onSetFilter: setAohFilter,
        // })
    };
    AOHPage.prototype.removePlantFilter = function (item) {
        var newFilters = {
            plants: this.selectedFilter.plants.filter(function (p) { return p.title !== item.title; }),
            products: this.selectedFilter.products
        };
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_3__aoh_reducer__["o" /* setAohFilter */])(newFilters));
    };
    AOHPage.prototype.removeProductFilter = function (item) {
        var newFilters = {
            products: this.selectedFilter.products.filter(function (p) { return p.title !== item.title; }),
            plants: this.selectedFilter.plants
        };
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_3__aoh_reducer__["o" /* setAohFilter */])(newFilters));
    };
    AOHPage.prototype.onFooterUpdate = function () {
        this.content.resize();
    };
    AOHPage.prototype.onCountdownComplete = function () {
        this.ionViewDidLoad();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], AOHPage.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('navBar'),
        __metadata("design:type", Object)
    ], AOHPage.prototype, "navBar", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('masonaryContainer'),
        __metadata("design:type", Object)
    ], AOHPage.prototype, "masonaryContainer", void 0);
    AOHPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-aoh',template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\aoh\aoh.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>{{\'PAGE_TITLE.AOH\' | translate}}</ion-title>\n\n    <ion-buttons end>\n\n      <aoh-icon (click)="openFilter()" icon="filter"></aoh-icon>\n\n      <basket-icon></basket-icon>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n<loading-indicator [show]="loadingService.isLoading | async"></loading-indicator>\n\n\n\n\n\n<ion-content padding class="background-blue">\n\n  <div class="filter-pill-container">\n\n    <filter-pill *ngFor="let item of selectedFilter?.plants" [label]="item.title" [item]="item" (onDismiss)="removePlantFilter(item)"></filter-pill>\n\n    <filter-pill *ngFor="let item of selectedFilter?.products" [label]="item.title" [item]="item" (onDismiss)="removeProductFilter(item)"></filter-pill>      \n\n  </div>\n\n  <div class="masonary-container" masonaryContainer="340">\n\n    <div *ngFor="let material of materials$ | async" class="masonary-item">\n\n        <aoh-item [material]="material"></aoh-item>\n\n    </div>\n\n  </div>\n\n</ion-content>\n\n\n\n<ion-footer>\n\n    <pricing-count-down (onSizeChange)="onFooterUpdate()" (onCountdownComplete)="onCountdownComplete()"></pricing-count-down>    \n\n</ion-footer>\n\n'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\aoh\aoh.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["h" /* Store */], __WEBPACK_IMPORTED_MODULE_4__modules_loading_provider__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */]])
    ], AOHPage);
    return AOHPage;
}());

//# sourceMappingURL=aoh.js.map

/***/ }),

/***/ 624:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return loadingCount; });
var loadingCount = function (state) {
    return state['loading'].count;
};
//# sourceMappingURL=loading.selector.js.map

/***/ }),

/***/ 625:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeTile; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_navigation__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import headerStyle    from './header.component.css';
var HomeTile = /** @class */ (function () {
    function HomeTile(navCtrl, navService) {
        this.navCtrl = navCtrl;
        this.navService = navService;
    }
    HomeTile.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], HomeTile.prototype, "tile", void 0);
    HomeTile = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'home-tile',template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\components\homeTile\homeTile.component.html"*/'<div class="home-tile" (click)="navService.followLink(tile.link)">\n\n    <div class="home-tile--icon">\n\n        <aoh-icon *ngIf="!tile.notification" icon="{{tile.icon}} home-tile"></aoh-icon>\n\n        <notification-icon *ngIf="tile.notification" size="lg"></notification-icon>\n\n    </div>\n\n    <div class="home-tile--title">{{ tile.title | translate }}</div>\n\n    <aoh-icon icon="arrow"></aoh-icon>\n\n</div>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\components\homeTile\homeTile.component.html"*/,
            styles: ['./homeTile.component.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__app_navigation__["a" /* NavigationService */]])
    ], HomeTile);
    return HomeTile;
}());

//# sourceMappingURL=homeTile.component.js.map

/***/ }),

/***/ 633:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AOHIcon; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var template = "\n<div class=\"aoh-icon {{icon}}\"></div>\n";
var AOHIcon = /** @class */ (function () {
    function AOHIcon() {
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], AOHIcon.prototype, "icon", void 0);
    AOHIcon = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            template: template,
            selector: 'aoh-icon',
            styles: ['./aohIcon.component.scss']
        }),
        __metadata("design:paramtypes", [])
    ], AOHIcon);
    return AOHIcon;
}());

//# sourceMappingURL=aohIcon.component.js.map

/***/ }),

/***/ 634:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ErrorMessage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var template = "\n<div [ngClass]=\"{error: error}\">{{message}}</div>\n";
var ErrorMessage = /** @class */ (function () {
    function ErrorMessage() {
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], ErrorMessage.prototype, "error", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], ErrorMessage.prototype, "message", void 0);
    ErrorMessage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'error-message',
            template: template,
        })
    ], ErrorMessage);
    return ErrorMessage;
}());

//# sourceMappingURL=errorMessage.component.js.map

/***/ }),

/***/ 635:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormFocusDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FormFocusDirective = /** @class */ (function () {
    function FormFocusDirective(el) {
        this.el = el;
    }
    FormFocusDirective.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () {
            if (_this.el.nativeElement.children[0]) {
                _this.el.nativeElement.children[0].focus();
            }
            else {
                _this.el.nativeElement.focus();
            }
        }, 530);
    };
    FormFocusDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* Directive */])({
            selector: '[formFocus]'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], FormFocusDirective);
    return FormFocusDirective;
}());

//# sourceMappingURL=formFocus.directive.js.map

/***/ }),

/***/ 636:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Autoresize; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
// An autoresize directive that works with ion-textarea in Ionic 2
// Usage example: <ion-textarea autoresize [(ngModel)]="body"></ion-textarea>
// Based on https://www.npmjs.com/package/angular2-autosize
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var Autoresize = /** @class */ (function () {
    function Autoresize(element) {
        this.element = element;
    }
    Autoresize.prototype.onInput = function (textArea) {
        this.adjust();
    };
    Autoresize.prototype.ngOnInit = function () {
        this.adjust();
    };
    Autoresize.prototype.adjust = function () {
        var el = this.element.nativeElement;
        var ta = el.querySelector('textarea');
        if (!ta) {
            return;
        }
        ta.style.overflow = 'hidden';
        ta.style.height = 'auto';
        ta.style.height = ta.scrollHeight + 'px';
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* HostListener */])('input', ['$event.target']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [HTMLTextAreaElement]),
        __metadata("design:returntype", void 0)
    ], Autoresize.prototype, "onInput", null);
    Autoresize = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* Directive */])({
            selector: 'ion-textarea[autoresize]' // Attribute selector
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], Autoresize);
    return Autoresize;
}());

//# sourceMappingURL=autoResize.directive.js.map

/***/ }),

/***/ 637:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterPill; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var template = "\n<div (click)=\"handleDismiss()\" class=\"row align-center\">\n    <div>{{label}}</div>\n    <aoh-icon icon=\"close\"></aoh-icon>\n<div>\n";
var FilterPill = /** @class */ (function () {
    function FilterPill() {
        this.onDismiss = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
    }
    FilterPill.prototype.handleDismiss = function () {
        this.onDismiss.emit(this.item);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], FilterPill.prototype, "label", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], FilterPill.prototype, "item", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["O" /* Output */])(),
        __metadata("design:type", Object)
    ], FilterPill.prototype, "onDismiss", void 0);
    FilterPill = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'filter-pill',
            template: template,
            styles: ['./filterPill.scss']
        })
    ], FilterPill);
    return FilterPill;
}());

//# sourceMappingURL=filterPill.component.js.map

/***/ }),

/***/ 638:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailsBox; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var template = "\n<ng-content></ng-content>\n<div class=\"row flex-end\">\n    <button class=\"btn btn-secondary\" *ngFor=\"let item of footerActions\" (click)=\"item.handler($event, item)\">{{item.label}}</button>\n</div>\n";
var DetailsBox = /** @class */ (function () {
    function DetailsBox() {
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('footerActions'),
        __metadata("design:type", Object)
    ], DetailsBox.prototype, "footerActions", void 0);
    DetailsBox = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'details-box',
            template: template,
            styles: ['./detailsBox.scss']
        })
    ], DetailsBox);
    return DetailsBox;
}());

//# sourceMappingURL=detailsBox.js.map

/***/ }),

/***/ 639:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailsItem; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var template = "\n<div class=\"details-label {{labelClass}}\">{{label | translate}}</div>\n<div class=\"details-value\">{{value}}<div>\n";
var DetailsItem = /** @class */ (function () {
    function DetailsItem() {
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], DetailsItem.prototype, "label", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], DetailsItem.prototype, "labelClass", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], DetailsItem.prototype, "value", void 0);
    DetailsItem = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'details-item',
            template: template,
            styles: ['./detailsItem.scss']
        })
    ], DetailsItem);
    return DetailsItem;
}());

//# sourceMappingURL=detailsItem.js.map

/***/ }),

/***/ 640:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DismissPageHeader; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var template = "\n<aoh-icon icon=\"close\" (click)=\"onClose()\"></aoh-icon>\n<div>\n    <div class=\"page-title\">{{pageTitle | translate}}</div>\n    <div class=\"page-subtitle\">{{pageSubtitle}}</div>\n</div>\n";
var DismissPageHeader = /** @class */ (function () {
    function DismissPageHeader(navCtrl) {
        this.navCtrl = navCtrl;
    }
    DismissPageHeader.prototype.onClose = function () {
        this.navCtrl.pop();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], DismissPageHeader.prototype, "pageTitle", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], DismissPageHeader.prototype, "pageSubtitle", void 0);
    DismissPageHeader = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'dismiss-page-header',
            template: template,
            styles: ['./dismissPageHeader.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */]])
    ], DismissPageHeader);
    return DismissPageHeader;
}());

//# sourceMappingURL=dismissPageHeader.js.map

/***/ }),

/***/ 641:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardSection; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var template = "\n\n";
var CardSection = /** @class */ (function () {
    function CardSection() {
    }
    CardSection = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'card-section',
            template: template,
            styles: ['./cardSection.scss']
        })
    ], CardSection);
    return CardSection;
}());

//# sourceMappingURL=cardSection.js.map

/***/ }),

/***/ 642:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContractCard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modals_share_share_component__ = __webpack_require__(149);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var template = "\n<div class=\"row align-center\">\n    <div class=\"flex-1\">\n        <div class=\"contract-header\">{{contract.MATERIAL_DESCRIPTION}}</div>\n        <div class=\"contract-location\">\n            <aoh-icon icon=\"location\"></aoh-icon>\n            <span>{{contract.PLANT_DESCRIPTION}}</span>\n        </div>\n    </div>\n    <div>\n        <badge *ngIf=\"contract.isOpen\" [label]=\"'CONTRACT_ITEM.BADGE_OPEN' | translate\"></badge>\n        <badge *ngIf=\"!contract.isOpen\" [label]=\"'CONTRACT_ITEM.BADGE_CLOSED' | translate\"></badge>\n    </div>\n</div>\n\n<div class=\"contract-details\">\n    <details-item label=\"CONTRACT_ITEM.CONTRACT_ID\" value=\"{{contract.CONTRACT_NUMBER}}\"></details-item>\n    <details-item label=\"CONTRACT_ITEM.CONTRACT_VOLUME\" value=\"{{contract.TARGET_QUANTITY | aohNumber}} {{contract.UOM_EXT}}\"></details-item>\n    <details-item label=\"CONTRACT_ITEM.OPEN_VOLUME\" value=\"{{contract.OPEN_QUANTITY | aohNumber}} {{contract.UOM_EXT}}\"></details-item>\n</div>\n\n<lifting-bar [value]=\"contract.LIFTED_PERC\"></lifting-bar>\n\n<div class=\"btn-footer\">\n    <button class=\"btn-primary filled\" (click)=\"openDetails()\">{{'CONTRACT_ITEM.VIEW_DETAILS' | translate}}</button>\n    <button class=\"btn-primary filled\" (click)=\"openShare()\">{{'CONTRACT_ITEM.SHARE' | translate}}</button>\n</div>\n";
var ContractCard = /** @class */ (function () {
    function ContractCard(navCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
    }
    ContractCard.prototype.openDetails = function (contract) {
        this.navCtrl.push('contract-details', this.contract);
    };
    ContractCard.prototype.openShare = function () {
        this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__modals_share_share_component__["a" /* ShareModal */], { contracts: [this.contract] }).present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('contract'),
        __metadata("design:type", Object)
    ], ContractCard.prototype, "contract", void 0);
    ContractCard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'contract-card',
            template: template,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */]])
    ], ContractCard);
    return ContractCard;
}());

//# sourceMappingURL=contractCard.js.map

/***/ }),

/***/ 643:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Badge; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var template = "\n<div>{{label}}</div>\n";
var Badge = /** @class */ (function () {
    function Badge() {
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], Badge.prototype, "label", void 0);
    Badge = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'badge',
            template: template,
            styles: ['./badge.scss']
        })
    ], Badge);
    return Badge;
}());

//# sourceMappingURL=badge.js.map

/***/ }),

/***/ 644:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LiftingBar; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var template = "\n<div class=\"row align-center\">\n<div class=\"flex-1\">\n<div class=\"bar\" [ngStyle]=\"{width: percentage}\" [ngClass]=\"{tooLarge: this.isTooLarge}\"></div>\n</div>\n<div class=\"percentage\" [ngClass]=\"{tooLarge: this.isTooLarge}\">{{value | aohNumber: 0}} %</div>\n</div>\n";
var LiftingBar = /** @class */ (function () {
    function LiftingBar() {
    }
    Object.defineProperty(LiftingBar.prototype, "percentage", {
        get: function () {
            return this.value <= 100 ? (this.value) + '%' : '100%';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiftingBar.prototype, "isTooLarge", {
        get: function () {
            return this.value >= 100;
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], LiftingBar.prototype, "value", void 0);
    LiftingBar = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'lifting-bar',
            template: template,
        })
    ], LiftingBar);
    return LiftingBar;
}());

//# sourceMappingURL=liftingBar.js.map

/***/ }),

/***/ 645:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabBar; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var template = "\n<ion-segment [(ngModel)]=\"selected\" (ionChange)=\"segmentChanged($event)\">\n    <ion-segment-button *ngFor=\"let option of options\" [value]=\"option.value\">\n        {{option.title | translate}}\n    </ion-segment-button>\n</ion-segment>\n";
var TabBar = /** @class */ (function () {
    function TabBar() {
        this.onChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
        this.options = [];
        this.selected = true;
    }
    TabBar.prototype.ngOnInit = function () {
        if (this.value) {
            this.selected = this.value;
        }
    };
    TabBar.prototype.segmentChanged = function () {
        this.onChange.emit(this.selected);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["O" /* Output */])('onChange'),
        __metadata("design:type", Object)
    ], TabBar.prototype, "onChange", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('options'),
        __metadata("design:type", Object)
    ], TabBar.prototype, "options", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('value'),
        __metadata("design:type", Object)
    ], TabBar.prototype, "value", void 0);
    TabBar = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'tab-bar',
            template: template,
            styles: ['./tabBar.scss']
        })
    ], TabBar);
    return TabBar;
}());

//# sourceMappingURL=tabBar.js.map

/***/ }),

/***/ 646:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeContainerResize; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var getWidth = function (element) {
    var style = element.currentStyle || window.getComputedStyle(element), width = element.offsetWidth, // or use style.width
    margin = parseFloat(style.marginLeft) + parseFloat(style.marginRight), padding = parseFloat(style.paddingLeft) + parseFloat(style.paddingRight), border = parseFloat(style.borderLeftWidth) + parseFloat(style.borderRightWidth);
    return width + margin - padding + border;
};
var HomeContainerResize = /** @class */ (function () {
    function HomeContainerResize(el) {
        this.el = el;
    }
    HomeContainerResize.prototype.ngAfterViewInit = function () {
        this.parent = this.el.nativeElement.parentElement;
        this.element = this.el.nativeElement;
        this.onResize({});
    };
    HomeContainerResize.prototype.onResize = function (event) {
        var availableWidth = this.parent.clientWidth;
        var firstChild = this.element.firstElementChild;
        var itemWidth = getWidth(firstChild);
        var numItems = Math.floor(availableWidth / itemWidth);
        this.element.style.width = numItems * itemWidth + "px";
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('homeContainerResize'),
        __metadata("design:type", Object)
    ], HomeContainerResize.prototype, "itemWidth", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* HostListener */])('window:resize', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], HomeContainerResize.prototype, "onResize", null);
    HomeContainerResize = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* Directive */])({
            selector: '[homeContainerResize]'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], HomeContainerResize);
    return HomeContainerResize;
}());

//# sourceMappingURL=homeContainerResize.js.map

/***/ }),

/***/ 647:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MasonaryContainer; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MasonaryContainer = /** @class */ (function () {
    function MasonaryContainer(el) {
        this.el = el;
        this.numberOfColumns = 1;
    }
    MasonaryContainer.prototype.ngAfterViewInit = function () {
        this.parent = this.el.nativeElement.parentElement;
        this.element = this.el.nativeElement;
        this.onResize({});
    };
    MasonaryContainer.prototype.onResize = function (event) {
        if (!this.parent) {
            return;
        }
        var availableWidth = this.parent.clientWidth;
        var numItems = Math.floor(availableWidth / this.itemWidth);
        numItems = numItems <= 4 ? numItems : 4;
        numItems = numItems <= 0 ? 1 : numItems;
        this.numberOfColumns = numItems;
        var newClassName = "columns-" + numItems;
        if (this.currentClass !== newClassName) {
            if (this.currentClass) {
                this.element.classList.remove(this.currentClass);
            }
            this.element.classList.add(newClassName);
            this.currentClass = newClassName;
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('masonaryContainer'),
        __metadata("design:type", Object)
    ], MasonaryContainer.prototype, "itemWidth", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* HostListener */])('window:resize', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], MasonaryContainer.prototype, "onResize", null);
    MasonaryContainer = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* Directive */])({
            selector: '[masonaryContainer]',
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], MasonaryContainer);
    return MasonaryContainer;
}());

//# sourceMappingURL=masonaryContainer.js.map

/***/ }),

/***/ 648:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PricingCountDown; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_aoh_aoh_selectors__ = __webpack_require__(143);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PricingCountDown = /** @class */ (function () {
    function PricingCountDown(store) {
        var _this = this;
        this.store = store;
        this.timeRemaining = 100;
        this.onSizeChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
        this.onCountdownComplete = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
        this.isOpen = false;
        this.hasEmittedComplete = false;
        this.remainingListener = this.store.select(__WEBPACK_IMPORTED_MODULE_4__pages_aoh_aoh_selectors__["e" /* aohTimer */])
            .subscribe(function (timer) {
            if (timer) {
                var time = __WEBPACK_IMPORTED_MODULE_3_moment___default()(timer.EV_PERIOD_END_TIME);
                var date = __WEBPACK_IMPORTED_MODULE_3_moment___default()(timer.EV_PERIOD_END_DATE).hour(time.hour()).minute(time.minute()).second(time.second());
                _this.hasEmittedComplete = false;
                _this.setTimeRemaining(date.toDate(), __WEBPACK_IMPORTED_MODULE_3_moment___default()().toDate());
            }
        });
        this.creditPosition$ = this.store.select(__WEBPACK_IMPORTED_MODULE_4__pages_aoh_aoh_selectors__["f" /* creditPosition */]);
    }
    PricingCountDown.prototype.setTimeRemaining = function (endTime, systemTime) {
        this.periodEndTime = endTime;
        var msRemaining = endTime.getTime() - systemTime.getTime();
        this.timeRemaining = msRemaining;
    };
    PricingCountDown.prototype.handleInterval = function () {
        // this.timeRemaining
        this.timeRemaining = this.timeRemaining - 1000;
        // When we reach zero stop the countdown
        if (this.timeRemaining <= 0) {
            if (!this.hasEmittedComplete) {
                this.hasEmittedComplete = true;
                this.onCountdownComplete.emit();
            }
            // this.interval.unsubscribe()
            this.timeRemaining = 0;
        }
        else {
            this.hasEmittedComplete = false;
        }
    };
    PricingCountDown.prototype.toggleOpen = function () {
        this.isOpen = !this.isOpen;
        if (this.onSizeChange) {
            this.onSizeChange.emit();
        }
    };
    PricingCountDown.prototype.ngOnInit = function () {
        this.interval = __WEBPACK_IMPORTED_MODULE_2_rxjs__["Observable"].interval(1000)
            .subscribe(this.handleInterval.bind(this));
    };
    PricingCountDown.prototype.ngOnDestroy = function () {
        if (this.interval) {
            clearInterval(this.interval);
        }
        this.remainingListener.unsubscribe();
    };
    PricingCountDown.prototype.onResize = function ($event) {
        this.onSizeChange.emit();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["O" /* Output */])(),
        __metadata("design:type", Object)
    ], PricingCountDown.prototype, "onSizeChange", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["O" /* Output */])(),
        __metadata("design:type", Object)
    ], PricingCountDown.prototype, "onCountdownComplete", void 0);
    PricingCountDown = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'pricing-count-down',template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\components\pricingCountDown\pricingCountDown.component.html"*/'<div (window:resize)="onResize($event)">\n\n<div class="pricing-countdown-container phone" (click)="toggleOpen()">\n\n    <div>\n\n        <span class="title">{{\'COUNT_DOWN.PRICES_VALID\' | translate}}</span>\n\n        <span>{{\'COUNT_DOWN.UNTIL\' | translate}} {{periodEndTime | timeFromDate}} ({{timeRemaining | countdown}})</span>\n\n    </div>\n\n    <div *ngIf="isOpen">\n\n        <span class="title">{{\'COUNT_DOWN.OUTSTANDING_BALANCE\' | translate}}</span>\n\n        <span>{{(creditPosition$ | async)?.EV_CREDIT_BALANCE | number : \'1.2-2\'}} {{(creditPosition$ | async)?.EV_CURRENCY}}</span>\n\n    </div>\n\n    <div *ngIf="isOpen">\n\n        <span class="title">{{\'COUNT_DOWN.CREDIT_LIMIT\' | translate}}</span>\n\n        <span>{{(creditPosition$ | async)?.EV_CREDIT_LIMIT | number : \'1.2-2\'}} {{(creditPosition$ | async)?.EV_CURRENCY}}</span>\n\n    </div>\n\n</div>\n\n\n\n<div class="pricing-countdown-container tablet">\n\n    <div class="row">\n\n        <div class="flex-1">\n\n            <span class="title">{{\'COUNT_DOWN.PRICES_VALID\' | translate}}</span>\n\n            <span>{{\'COUNT_DOWN.UNTIL\' | translate}} {{periodEndTime | timeFromDate}}</span>\n\n            <div class="time-remaining">{{timeRemaining | countdown}} {{ \'COUNT_DOWN.LEFT\' | translate }}</div>\n\n        </div>\n\n        <div class="right-side">\n\n            <div class="row align-center">\n\n                <div class="title flex-1">{{\'COUNT_DOWN.OUTSTANDING_BALANCE\' | translate}}</div>\n\n                <div>{{(creditPosition$ | async)?.EV_CREDIT_BALANCE | number : \'1.2-2\'}} {{(creditPosition$ | async)?.EV_CURRENCY}}</div>\n\n            </div>\n\n            <div class="row align-center">\n\n                <div class="title flex-1">{{\'COUNT_DOWN.CREDIT_LIMIT\' | translate}}</div>\n\n                <div>{{(creditPosition$ | async)?.EV_CREDIT_LIMIT | number : \'1.2-2\'}} {{(creditPosition$ | async)?.EV_CURRENCY}}</div>\n\n            </div>\n\n        </div>\n\n    </div>\n\n\n\n</div>\n\n</div>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\components\pricingCountDown\pricingCountDown.component.html"*/,
            styles: ['./pricingCountDown.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */]])
    ], PricingCountDown);
    return PricingCountDown;
}());

//# sourceMappingURL=pricingCountDown.component.js.map

/***/ }),

/***/ 649:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 267,
	"./af.js": 267,
	"./ar": 268,
	"./ar-dz": 269,
	"./ar-dz.js": 269,
	"./ar-kw": 270,
	"./ar-kw.js": 270,
	"./ar-ly": 271,
	"./ar-ly.js": 271,
	"./ar-ma": 272,
	"./ar-ma.js": 272,
	"./ar-sa": 273,
	"./ar-sa.js": 273,
	"./ar-tn": 274,
	"./ar-tn.js": 274,
	"./ar.js": 268,
	"./az": 275,
	"./az.js": 275,
	"./be": 276,
	"./be.js": 276,
	"./bg": 277,
	"./bg.js": 277,
	"./bm": 278,
	"./bm.js": 278,
	"./bn": 279,
	"./bn.js": 279,
	"./bo": 280,
	"./bo.js": 280,
	"./br": 281,
	"./br.js": 281,
	"./bs": 282,
	"./bs.js": 282,
	"./ca": 283,
	"./ca.js": 283,
	"./cs": 284,
	"./cs.js": 284,
	"./cv": 285,
	"./cv.js": 285,
	"./cy": 286,
	"./cy.js": 286,
	"./da": 287,
	"./da.js": 287,
	"./de": 288,
	"./de-at": 289,
	"./de-at.js": 289,
	"./de-ch": 290,
	"./de-ch.js": 290,
	"./de.js": 288,
	"./dv": 291,
	"./dv.js": 291,
	"./el": 292,
	"./el.js": 292,
	"./en-au": 293,
	"./en-au.js": 293,
	"./en-ca": 294,
	"./en-ca.js": 294,
	"./en-gb": 295,
	"./en-gb.js": 295,
	"./en-ie": 296,
	"./en-ie.js": 296,
	"./en-il": 297,
	"./en-il.js": 297,
	"./en-nz": 298,
	"./en-nz.js": 298,
	"./eo": 299,
	"./eo.js": 299,
	"./es": 300,
	"./es-do": 301,
	"./es-do.js": 301,
	"./es-us": 302,
	"./es-us.js": 302,
	"./es.js": 300,
	"./et": 303,
	"./et.js": 303,
	"./eu": 304,
	"./eu.js": 304,
	"./fa": 305,
	"./fa.js": 305,
	"./fi": 306,
	"./fi.js": 306,
	"./fo": 307,
	"./fo.js": 307,
	"./fr": 308,
	"./fr-ca": 309,
	"./fr-ca.js": 309,
	"./fr-ch": 310,
	"./fr-ch.js": 310,
	"./fr.js": 308,
	"./fy": 311,
	"./fy.js": 311,
	"./gd": 312,
	"./gd.js": 312,
	"./gl": 313,
	"./gl.js": 313,
	"./gom-latn": 314,
	"./gom-latn.js": 314,
	"./gu": 315,
	"./gu.js": 315,
	"./he": 316,
	"./he.js": 316,
	"./hi": 317,
	"./hi.js": 317,
	"./hr": 318,
	"./hr.js": 318,
	"./hu": 319,
	"./hu.js": 319,
	"./hy-am": 320,
	"./hy-am.js": 320,
	"./id": 321,
	"./id.js": 321,
	"./is": 322,
	"./is.js": 322,
	"./it": 323,
	"./it.js": 323,
	"./ja": 324,
	"./ja.js": 324,
	"./jv": 325,
	"./jv.js": 325,
	"./ka": 326,
	"./ka.js": 326,
	"./kk": 327,
	"./kk.js": 327,
	"./km": 328,
	"./km.js": 328,
	"./kn": 329,
	"./kn.js": 329,
	"./ko": 330,
	"./ko.js": 330,
	"./ky": 331,
	"./ky.js": 331,
	"./lb": 332,
	"./lb.js": 332,
	"./lo": 333,
	"./lo.js": 333,
	"./lt": 334,
	"./lt.js": 334,
	"./lv": 335,
	"./lv.js": 335,
	"./me": 336,
	"./me.js": 336,
	"./mi": 337,
	"./mi.js": 337,
	"./mk": 338,
	"./mk.js": 338,
	"./ml": 339,
	"./ml.js": 339,
	"./mn": 340,
	"./mn.js": 340,
	"./mr": 341,
	"./mr.js": 341,
	"./ms": 342,
	"./ms-my": 343,
	"./ms-my.js": 343,
	"./ms.js": 342,
	"./mt": 344,
	"./mt.js": 344,
	"./my": 345,
	"./my.js": 345,
	"./nb": 346,
	"./nb.js": 346,
	"./ne": 347,
	"./ne.js": 347,
	"./nl": 348,
	"./nl-be": 349,
	"./nl-be.js": 349,
	"./nl.js": 348,
	"./nn": 350,
	"./nn.js": 350,
	"./pa-in": 351,
	"./pa-in.js": 351,
	"./pl": 352,
	"./pl.js": 352,
	"./pt": 353,
	"./pt-br": 354,
	"./pt-br.js": 354,
	"./pt.js": 353,
	"./ro": 355,
	"./ro.js": 355,
	"./ru": 356,
	"./ru.js": 356,
	"./sd": 357,
	"./sd.js": 357,
	"./se": 358,
	"./se.js": 358,
	"./si": 359,
	"./si.js": 359,
	"./sk": 360,
	"./sk.js": 360,
	"./sl": 361,
	"./sl.js": 361,
	"./sq": 362,
	"./sq.js": 362,
	"./sr": 363,
	"./sr-cyrl": 364,
	"./sr-cyrl.js": 364,
	"./sr.js": 363,
	"./ss": 365,
	"./ss.js": 365,
	"./sv": 366,
	"./sv.js": 366,
	"./sw": 367,
	"./sw.js": 367,
	"./ta": 368,
	"./ta.js": 368,
	"./te": 369,
	"./te.js": 369,
	"./tet": 370,
	"./tet.js": 370,
	"./tg": 371,
	"./tg.js": 371,
	"./th": 372,
	"./th.js": 372,
	"./tl-ph": 373,
	"./tl-ph.js": 373,
	"./tlh": 374,
	"./tlh.js": 374,
	"./tr": 375,
	"./tr.js": 375,
	"./tzl": 376,
	"./tzl.js": 376,
	"./tzm": 377,
	"./tzm-latn": 378,
	"./tzm-latn.js": 378,
	"./tzm.js": 377,
	"./ug-cn": 379,
	"./ug-cn.js": 379,
	"./uk": 380,
	"./uk.js": 380,
	"./ur": 381,
	"./ur.js": 381,
	"./uz": 382,
	"./uz-latn": 383,
	"./uz-latn.js": 383,
	"./uz.js": 382,
	"./vi": 384,
	"./vi.js": 384,
	"./x-pseudo": 385,
	"./x-pseudo.js": 385,
	"./yo": 386,
	"./yo.js": 386,
	"./zh-cn": 387,
	"./zh-cn.js": 387,
	"./zh-hk": 388,
	"./zh-hk.js": 388,
	"./zh-tw": 389,
	"./zh-tw.js": 389
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 649;

/***/ }),

/***/ 650:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LiftingCard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_file_transfer__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_file__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_opener__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__modules_loading_provider__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_navigation__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__modals_makeAClaim_makeAClaim_component__ = __webpack_require__(390);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var LiftingCard = /** @class */ (function () {
    function LiftingCard(modalCtrl, transfer, file, fileOpener, navService, loadingService) {
        this.modalCtrl = modalCtrl;
        this.transfer = transfer;
        this.file = file;
        this.fileOpener = fileOpener;
        this.navService = navService;
        this.loadingService = loadingService;
    }
    LiftingCard.prototype.openMakeAClaim = function () {
        this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__modals_makeAClaim_makeAClaim_component__["a" /* MakeAClaimModal */]).present();
    };
    LiftingCard.prototype.openInvoice = function () {
        this.navService.openPDF('/assets/pdf/invoice_6306570275.pdf');
        // if(!window['cordova']) {
        //     this.loadingService.showLoader()
        //     setTimeout(()=> {
        //         this.loadingService.hideLoader()
        //         alert('Only supported on device')
        //     }, 1000)
        // } else {
        //     this.loadingService.showLoader()
        //     const url = 'http://www.pdf995.com/samples/pdf.pdf';
        //     const fileTransfer: FileTransferObject = this.transfer.create();
        //     fileTransfer.download(url, this.file.dataDirectory + 'file.pdf').then((entry) => {
        //       console.log('download complete: ' + entry.toURL());
        //       this.loadingService.hideLoader()
        //       this.fileOpener.open(entry.toURL(), 'application/pdf')
        //         .then(() => console.log('File is opened'))
        //         .catch(e => console.log('Error opening file', e));
        //     }, (error) => {
        //         this.loadingService.hideLoader()
        //       // handle error
        //     });
        // }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('lifting'),
        __metadata("design:type", Object)
    ], LiftingCard.prototype, "lifting", void 0);
    LiftingCard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'lifting-card',template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\components\liftingCard\liftingCard.component.html"*/'<div class="lifting-header">{{ lifting.MATERIAL_DESCRIPTION }}</div>\n\n<div class="lifting-location">\n\n    <aoh-icon icon="location"></aoh-icon>\n\n    <span>{{lifting.DELIVERY_POINT_NAME}}</span>\n\n</div>\n\n<div class="lifting-details">\n\n    <details-item label="LIFTING_ITEM.DATE" value="{{lifting.LIFTING_DATE | aohDate}}"></details-item>\n\n    <details-item label="LIFTING_ITEM.VOLUME" value="{{lifting.LIFTED_QUANTITY | aohNumber}} {{lifting.UOM}}"></details-item>\n\n</div>\n\n<div class="invoice-container row" *ngIf="lifting.INVOICE_NO" (click)="openInvoice()">\n\n    <aoh-icon icon="pdf"></aoh-icon>\n\n    <div class="flex-1 invoice-number">{{lifting.INVOICE_NO}}</div>\n\n    <div class="view-invoice">{{ \'LIFTING_ITEM.VIEW_INVOICE\' | translate }}</div>\n\n</div>\n\n\n\n<div class="btn-footer">\n\n    <button class="btn-primary filled" (click)="openMakeAClaim()">{{ \'LIFTINGS.MAKE_A_CLAIM\' | translate }}</button>\n\n</div>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\components\liftingCard\liftingCard.component.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_opener__["a" /* FileOpener */],
            __WEBPACK_IMPORTED_MODULE_6__app_navigation__["a" /* NavigationService */],
            __WEBPACK_IMPORTED_MODULE_5__modules_loading_provider__["a" /* LoadingService */]])
    ], LiftingCard);
    return LiftingCard;
}());

//# sourceMappingURL=liftingCard.component.js.map

/***/ }),

/***/ 651:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export EPANDED_TEXTAREA_VALUE_ACCESSOR */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectionList; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EPANDED_TEXTAREA_VALUE_ACCESSOR = {
    provide: __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* NG_VALUE_ACCESSOR */],
    useExisting: Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* forwardRef */])(function () { return SelectionList; }),
    multi: true,
};
var SelectionList = /** @class */ (function () {
    function SelectionList(renderer, alertCtrl) {
        this.renderer = renderer;
        this.alertCtrl = alertCtrl;
        this.innerValue = [];
        this.selectedKeys = {};
    }
    SelectionList.prototype.writeValue = function (value) {
        this.innerValue = value;
        if (value) {
            this.setSelectedKeys();
        }
    };
    SelectionList.prototype.setSelectedKeys = function () {
        this.selectedKeys = this.innerValue.reduce(function (prev, next) {
            prev[next.title] = true;
            return prev;
        }, {});
    };
    SelectionList.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    SelectionList.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    SelectionList.prototype.setDisabledState = function (isDisabled) {
        // const div = this.textarea.nativeElement;
        // const action = isDisabled ? 'addClass' : 'removeClass';
        // this.renderer[action](div, 'disabled');
    };
    SelectionList.prototype.onFieldChange = function ($event) {
        this.setFieldValue($event.target.value);
    };
    SelectionList.prototype.onCheck = function ($event, option, checked) {
        var newState = $event.value;
        // Make sure that the checkbox has actually changed state
        if (newState === this.isChecked(option)) {
            return;
        }
        if (newState) {
            this.setFieldValue(this.innerValue.concat([
                option,
            ]));
        }
        else {
            this.setFieldValue(this.innerValue.filter(function (i) {
                return i.title !== option.title;
            }));
        }
    };
    SelectionList.prototype.setFieldValue = function (value) {
        this.innerValue = value;
        this.setSelectedKeys();
        this.onChange(value);
        this.onTouched(value);
    };
    SelectionList.prototype.isChecked = function (option) {
        if (!this.innerValue) {
            return false;
        }
        return this.selectedKeys[option.title] || false;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], SelectionList.prototype, "options", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], SelectionList.prototype, "label", void 0);
    SelectionList = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'selection-list',
            providers: [EPANDED_TEXTAREA_VALUE_ACCESSOR],template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\components\selectionList\selectionList.component.html"*/'<ion-list *ngIf="options">\n\n    <ion-list-header *ngIf="label">\n\n        {{ label }}\n\n    </ion-list-header>\n\n    <ion-item *ngFor="let option of options" >\n\n        <ion-label>{{option.title}}</ion-label>\n\n        <ion-checkbox [checked]="isChecked(option)" (ionChange)="onCheck($event, option)"></ion-checkbox>\n\n    </ion-item>\n\n</ion-list>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\components\selectionList\selectionList.component.html"*/,
            styles: ['./selectionList.component.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["W" /* Renderer2 */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */]])
    ], SelectionList);
    return SelectionList;
}());

// <textarea-expanded [formControl]="control"></textarea-expanded>
//# sourceMappingURL=selectionList.component.js.map

/***/ }),

/***/ 652:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConfirmationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var template = "\n<aoh-icon *ngIf=\"showClose\" icon=\"close\" (click)=\"onClose()\"></aoh-icon>\n<div class=\"tick\"></div>\n<ng-content></ng-content>\n";
var ConfirmationPage = /** @class */ (function () {
    function ConfirmationPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.showClose = true;
    }
    ConfirmationPage.prototype.onClose = function () {
        this.navCtrl.pop();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], ConfirmationPage.prototype, "pageSubtitle", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], ConfirmationPage.prototype, "showClose", void 0);
    ConfirmationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'confirmation-page',
            template: template,
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */]])
    ], ConfirmationPage);
    return ConfirmationPage;
}());

//# sourceMappingURL=confirmationPage.component.js.map

/***/ }),

/***/ 653:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IconSidebar; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modules_user_selectors__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_navigation__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var IconSidebar = /** @class */ (function () {
    function IconSidebar(store, navService) {
        this.store = store;
        this.navService = navService;
        this.navigation$ = this.store.select(__WEBPACK_IMPORTED_MODULE_2__modules_user_selectors__["b" /* getMenuOptions */]);
    }
    IconSidebar.prototype.openPage = function (p) {
        this.navService.followLink(p.link);
    };
    IconSidebar = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'icon-sidebar',template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\components\iconSidebar\iconSidebar.component.html"*/'<div class="sidebar--item" *ngFor="let p of navigation$ | async" (click)="openPage(p)">\n\n    <aoh-icon *ngIf="!p.notification" [icon]="p.icon"></aoh-icon>\n\n    <notification-icon *ngIf="p.notification"></notification-icon>\n\n</div>\n\n\n\n\n\n'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\components\iconSidebar\iconSidebar.component.html"*/,
            styles: ['./iconSidebar.component.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */], __WEBPACK_IMPORTED_MODULE_3__app_navigation__["a" /* NavigationService */]])
    ], IconSidebar);
    return IconSidebar;
}());

//# sourceMappingURL=iconSidebar.component.js.map

/***/ }),

/***/ 654:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MasonaryPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
var MasonaryPipe = /** @class */ (function () {
    function MasonaryPipe() {
    }
    MasonaryPipe.prototype.transform = function (list, numColumns) {
        if (numColumns === void 0) { numColumns = 2; }
        var v = numColumns > 1 ? Object(__WEBPACK_IMPORTED_MODULE_1_lodash__["range"])(numColumns) : [0];
        return v.reduce(function (prev, col) {
            var newItems = list.reduce(function (prev, item, i) {
                if (i % numColumns === col) {
                    return prev.concat([item]);
                }
                return prev;
            }, []);
            return prev.concat(newItems);
        }, []);
    };
    MasonaryPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({ name: 'masonary' })
    ], MasonaryPipe);
    return MasonaryPipe;
}());

//# sourceMappingURL=masonaryPipe.js.map

/***/ }),

/***/ 655:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchDetails; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utils_storeUtils__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var template = "\n<details-box [footerActions]=\"footerActions\">\n    <details-item *ngFor=\"let field of fieldsToShow\" [label]=\"field.label\" [value]=\"getValue(field)\"></details-item>\n</details-box>\n";
// import { fields } from '../../pages/search/searchConfig'
var values = {
    'contractNo': '',
    'customerReference': '',
    'contractType': 'any',
    'contractStatus': 'any',
    'terminal': 'any',
    'date': {
        'startDate': '2018-07-23',
        'endDate': '2018-07-23'
    }
};
var SearchDetails = /** @class */ (function () {
    function SearchDetails(store) {
        var _this = this;
        this.store = store;
        this.footerActions = [{
                label: 'CONTRACTS_SEARCH.CHANGE',
                handler: function () {
                    _this.onChange.emit();
                }
            }];
        this.onChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
        this.setFieldsToShow();
    }
    SearchDetails.prototype.ngOnChanges = function (changes) {
        if (changes.values) {
            this.setFieldsToShow();
        }
        // changes.prop contains the old and the new value...
    };
    SearchDetails.prototype.setFieldsToShow = function () {
        var _this = this;
        if (!this.values) {
            return;
        }
        this.fieldsToShow = this.fields.filter(function (field) {
            var value = _this.values[field.name];
            return value && value !== 'any';
        });
    };
    SearchDetails.prototype.getValue = function (field) {
        var value = this.values[field.name];
        if (field.type === 'select') {
            var options = Object(__WEBPACK_IMPORTED_MODULE_1__utils_storeUtils__["b" /* select */])(this.store, field.options);
            var selectedValue = options.find(function (option) {
                return option.value === value;
            });
            return field.displayFn ? field.displayFn(value) : selectedValue.title;
        }
        return value;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('values'),
        __metadata("design:type", Object)
    ], SearchDetails.prototype, "values", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('fields'),
        __metadata("design:type", Object)
    ], SearchDetails.prototype, "fields", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["O" /* Output */])(),
        __metadata("design:type", Object)
    ], SearchDetails.prototype, "onChange", void 0);
    SearchDetails = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'search-details',
            template: template,
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ngrx_store__["h" /* Store */]])
    ], SearchDetails);
    return SearchDetails;
}());

//# sourceMappingURL=searchDetails.js.map

/***/ }),

/***/ 656:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadingIndicator; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var template = "\n<div *ngIf=\"show\" class=\"loading-indicator\">\n<svg class=\"spinner\" width=\"65px\" height=\"65px\" viewBox=\"0 0 66 66\" xmlns=\"http://www.w3.org/2000/svg\">\n<circle class=\"path\" fill=\"none\" stroke-width=\"6\" stroke-linecap=\"round\" cx=\"33\" cy=\"33\" r=\"30\"></circle>\n</svg>\n    <div>{{'LOADING.TEXT' | translate}}</div>\n</div>\n";
var LoadingIndicator = /** @class */ (function () {
    function LoadingIndicator() {
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('show'),
        __metadata("design:type", Object)
    ], LoadingIndicator.prototype, "show", void 0);
    LoadingIndicator = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'loading-indicator',
            styles: ['./loadingIndicator.scss'],
            template: template,
        })
    ], LoadingIndicator);
    return LoadingIndicator;
}());

//# sourceMappingURL=loadingIndicator.js.map

/***/ }),

/***/ 657:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BackButton; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
// An autoresize directive that works with ion-textarea in Ionic 2
// Usage example: <ion-textarea autoresize [(ngModel)]="body"></ion-textarea>
// Based on https://www.npmjs.com/package/angular2-autosize
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BackButton = /** @class */ (function () {
    function BackButton(element) {
        this.element = element;
    }
    BackButton.prototype.ngOnInit = function () {
        this.adjust();
    };
    BackButton.prototype.adjust = function () {
        var _this = this;
        setTimeout(function () {
            var el = _this.element.nativeElement;
            var backButton = el.querySelector('.back-button');
            backButton.classList.add('show-back-button');
            var menuButton = el.querySelector('.bar-button-menutoggle');
            menuButton.classList.add('hidden');
        }, 100);
    };
    BackButton = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* Directive */])({
            selector: '[backButton]' // Attribute selector
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], BackButton);
    return BackButton;
}());

//# sourceMappingURL=backButton.directive.js.map

/***/ }),

/***/ 658:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationIcon; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_notifications_notifications_selectors__ = __webpack_require__(151);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var template = "\n<div class=\"notification-icon {{size}}\">\n<aoh-icon [icon]=\"icon\"></aoh-icon>\n<div class=\"count\" *ngIf=\"count$ | async\">{{count$ | async}}</div>\n</div>\n";
var NotificationIcon = /** @class */ (function () {
    function NotificationIcon(store) {
        this.store = store;
        this.count$ = this.store.select(__WEBPACK_IMPORTED_MODULE_2__pages_notifications_notifications_selectors__["b" /* notificationCount */]);
    }
    Object.defineProperty(NotificationIcon.prototype, "icon", {
        get: function () {
            return this.size == 'lg' ? 'notification home-tile' : 'notification';
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('size'),
        __metadata("design:type", Object)
    ], NotificationIcon.prototype, "size", void 0);
    NotificationIcon = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'notification-icon',
            template: template,
            styles: ['./notification.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */]])
    ], NotificationIcon);
    return NotificationIcon;
}());

//# sourceMappingURL=notificationIcon.js.map

/***/ }),

/***/ 659:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgressStepper; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var template = "\n<ng-container *ngFor=\"let step of steps; let i = index\">\n<div class=\"step\">\n<div class=\"step--details\">\n    <div class=\"step--number\" [ngClass]=\"{active: i <= activeStep }\"><div class=\"step--number-inner\">{{i + 1}}</div></div>\n    <div class=\"step--title\" [ngClass]=\"{active: i <= activeStep }\">{{step.title | translate}}</div>\n</div>\n</div>\n<div class=\"step--bar\" *ngIf=\"i !== steps.length - 1\" [ngClass]=\"{active: i < activeStep }\"></div>\n\n\n</ng-container>\n";
var steps = [
    { title: 'Stepa' },
    { title: 'Step b' },
    { title: 'Step c' },
];
var ProgressStepper = /** @class */ (function () {
    function ProgressStepper() {
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('steps'),
        __metadata("design:type", Object)
    ], ProgressStepper.prototype, "steps", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('activeStep'),
        __metadata("design:type", Object)
    ], ProgressStepper.prototype, "activeStep", void 0);
    ProgressStepper = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'progress-stepper',
            styles: ['./progressStepper.scss'],
            template: template,
        })
    ], ProgressStepper);
    return ProgressStepper;
}());

//# sourceMappingURL=progressStepper.js.map

/***/ }),

/***/ 660:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CountDownPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
var CountDownPipe = /** @class */ (function () {
    function CountDownPipe() {
    }
    CountDownPipe.prototype.transform = function (value) {
        var fiveMins = 1000 * 60 * 5;
        var oneMin = 1000 * 60;
        var duration = __WEBPACK_IMPORTED_MODULE_1_moment___default.a.duration(value, 'ms');
        if (value <= oneMin) {
            return __WEBPACK_IMPORTED_MODULE_1_moment___default.a.utc(duration.asMilliseconds()).format('s') + ' seconds';
        }
        if (value <= fiveMins) {
            return __WEBPACK_IMPORTED_MODULE_1_moment___default.a.utc(duration.asMilliseconds()).format('mm:s') + ' mins';
        }
        return duration.humanize();
    };
    CountDownPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({ name: 'countdown' })
    ], CountDownPipe);
    return CountDownPipe;
}());

//# sourceMappingURL=countDown.pipe.js.map

/***/ }),

/***/ 661:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TimeFromDatePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
var TimeFromDatePipe = /** @class */ (function () {
    function TimeFromDatePipe() {
    }
    TimeFromDatePipe.prototype.transform = function (date) {
        return __WEBPACK_IMPORTED_MODULE_1_moment___default()(date).format('HH:mm');
    };
    TimeFromDatePipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({ name: 'timeFromDate' })
    ], TimeFromDatePipe);
    return TimeFromDatePipe;
}());

//# sourceMappingURL=timeFromDate.pips.js.map

/***/ }),

/***/ 662:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DaysToCollectPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
var DaysToCollectPipe = /** @class */ (function () {
    function DaysToCollectPipe() {
    }
    DaysToCollectPipe.prototype.transform = function (date) {
        var input = __WEBPACK_IMPORTED_MODULE_1_moment___default()(date);
        var today = __WEBPACK_IMPORTED_MODULE_1_moment___default()();
        var diff = input.diff(today, 'days');
        return diff;
    };
    DaysToCollectPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({ name: 'daysToCollect' })
    ], DaysToCollectPipe);
    return DaysToCollectPipe;
}());

//# sourceMappingURL=daysToCollect.pipe.js.map

/***/ }),

/***/ 663:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DayDiffPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
var DayDiffPipe = /** @class */ (function () {
    function DayDiffPipe() {
    }
    DayDiffPipe.prototype.transform = function (date, date2) {
        var input = __WEBPACK_IMPORTED_MODULE_1_moment___default()(date);
        var date2Moment = __WEBPACK_IMPORTED_MODULE_1_moment___default()(date2);
        var diff = input.diff(date2Moment, 'days');
        return Math.abs(diff);
    };
    DayDiffPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({ name: 'dayDiff' })
    ], DayDiffPipe);
    return DayDiffPipe;
}());

//# sourceMappingURL=dayDiff.js.map

/***/ }),

/***/ 664:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AOHDate; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
var AOHDate = /** @class */ (function () {
    function AOHDate() {
    }
    AOHDate.prototype.transform = function (date) {
        if (!date) {
            return '';
        }
        return __WEBPACK_IMPORTED_MODULE_1_moment___default()(date).format('DD MMM YYYY');
    };
    AOHDate = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({ name: 'aohDate' })
    ], AOHDate);
    return AOHDate;
}());

//# sourceMappingURL=aohDate.pipe.js.map

/***/ }),

/***/ 665:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AOHTime; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
var AOHTime = /** @class */ (function () {
    function AOHTime() {
    }
    AOHTime.prototype.transform = function (date) {
        if (!date) {
            return '';
        }
        return __WEBPACK_IMPORTED_MODULE_1_moment___default()(date).format('hh:mm');
    };
    AOHTime = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({ name: 'aohTime' })
    ], AOHTime);
    return AOHTime;
}());

//# sourceMappingURL=aohTime.js.map

/***/ }),

/***/ 666:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AOHNumber; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

function formatNumber(number, options) {
    /** @type {Array.<string>} */
    var result = (options['fraction'] ? number.toFixed(options['fraction']) :
        '' + number).split('.');
    return options['prefix'] +
        result[0].replace(/\B(?=(\d{3})+(?!\d))/g, options['grouping']) +
        (result[1] && options.fraction != 0 ? options['decimal'] + result[1] : '');
}
var AOHNumber = /** @class */ (function () {
    function AOHNumber() {
    }
    AOHNumber.prototype.transform = function (number, decimalPlaces) {
        if (!number) {
            return '';
        }
        return formatNumber(number, {
            'decimal': ',',
            'grouping': '.',
            'fraction': decimalPlaces == null ? decimalPlaces : decimalPlaces,
            'prefix': '',
        });
    };
    AOHNumber = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({ name: 'aohNumber' })
    ], AOHNumber);
    return AOHNumber;
}());

//# sourceMappingURL=aohNumber.pipe.js.map

/***/ }),

/***/ 667:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TranslatePlural; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var TranslatePlural = /** @class */ (function () {
    function TranslatePlural() {
    }
    TranslatePlural.prototype.transform = function (key, count) {
        if (count === void 0) { count = 0; }
        if (count == 0) {
            key += '.none';
        }
        else if (count == 1) {
            key += '.singular';
        }
        else if (count > 1) {
            key += '.plural';
        }
        return key;
    };
    TranslatePlural = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({ name: 'i18nPlural' })
    ], TranslatePlural);
    return TranslatePlural;
}());

//# sourceMappingURL=i18nPlural.js.map

/***/ }),

/***/ 668:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AOHItem; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__basket_basket_reducer__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__basket_modals_addItem_addItem__ = __webpack_require__(152);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AOHItem = /** @class */ (function () {
    function AOHItem(store, modalCtrl, navCtrl) {
        this.store = store;
        this.modalCtrl = modalCtrl;
        this.navCtrl = navCtrl;
    }
    AOHItem.prototype.addItem = function (itemToAdd) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_3__basket_basket_reducer__["a" /* actions */].AddItem(itemToAdd));
    };
    AOHItem.prototype.openAddItem = function (material) {
        var updateItemModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__basket_modals_addItem_addItem__["a" /* AddItemModal */], {
            material: material,
            onConfirm: this.addItem.bind(this),
            labels: {
                title: 'BASKET.ADD_ITEM',
                confirm: 'BASKET.ADD_ITEM_CONFIRM'
            }
        });
        updateItemModal.present();
    };
    AOHItem.prototype.viewOpenContracts = function () {
        this.navCtrl.push('aoh-details', this.material);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], AOHItem.prototype, "material", void 0);
    AOHItem = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'aoh-item',template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\aoh\components\aohItem\aohItem.component.html"*/'<div class="aoh-item--container">\n\n\n\n    <div class="aoh-item--title">{{ material.MATERIAL_DESCRIPTION }}</div>\n\n    <div class="aoh-item--location">\n\n        <aoh-icon icon="location"></aoh-icon>\n\n        <div class="aoh-item--location-label">{{ material.PLANT_NAME }}</div>\n\n    </div>\n\n\n\n    <div class="aoh-item--price-row">\n\n        <div class="price">\n\n            {{ material.CUSTOMER_PRICE | aohNumber }}\n\n        </div>\n\n        <div>\n\n\n\n            <div class="price-specifics">{{ \'AOH.MAX\' | translate }} {{material.MAX_QUANTITY | aohNumber: 0}} {{material.contract?.UOM_EXT}}</div>\n\n            <div class="price-specifics">{{material.END_DATE | daysToCollect}} {{ \'AOH.DAYS_TO_COLLECT\' | translate }}</div>\n\n        </div>\n\n    </div>\n\n\n\n    <div class="price-unit"> {{material.CURRENCY}} {{\'AOH.PER\' | translate}} {{material.CUSTOMER_PRICE_UNIT}}/{{material.CUSTOMER_CONDITION_UNIT_EXT}}</div>\n\n\n\n    <details-box class="tablet" *ngIf="material.contract?.NO_OF_CONTRACTS">\n\n        <details-item label="AOH_DETAILS.OPEN_CONTRACTS" [value]="material.contract?.NO_OF_CONTRACTS"></details-item>\n\n        <details-item label="AOH_DETAILS.OPEN_VOLUME" [value]="material.contract?.OPEN_VOLUME | aohNumber"></details-item>\n\n    </details-box>\n\n\n\n    <div class="add-to-basket">\n\n        <button class="btn-primary filled" (click)="openAddItem(material)" *ngIf="!material.inBasket">{{\'AOH.ADD_TO_BASKET\' | translate}}</button>\n\n        <div *ngIf="material.inBasket" class="added-to-basket">{{ \'AOH.ADDED_TO_BASKET\'| translate}}</div>\n\n    </div>\n\n    <button *ngIf="material.contract?.NO_OF_CONTRACTS" class="btn-primary view-contracts no-icon" (click)="viewOpenContracts()">\n\n        <span>{{ \'AOH.VIEW_OPEN_CONTRACTS\' | translate}}</span>\n\n        <span class="btn-badge">{{material.contract?.NO_OF_CONTRACTS}}</span> \n\n    </button>\n\n\n\n    \n\n</div>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\aoh\components\aohItem\aohItem.component.html"*/,
            styles: ['./aohItem.component.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ModalController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavController */]])
    ], AOHItem);
    return AOHItem;
}());

//# sourceMappingURL=aohItem.component.js.map

/***/ }),

/***/ 669:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BasketPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__basket_reducer__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__basket_selectors__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__modals_share_share_component__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_navigation__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var mustBeTrue = function (control) {
    return control.value ? null : { mustBeTrue: true };
};
var BasketPage = /** @class */ (function () {
    function BasketPage(store, fb, modalCtrl, navService) {
        this.store = store;
        this.fb = fb;
        this.modalCtrl = modalCtrl;
        this.navService = navService;
        this.isSubmitted = false;
        this.items$ = store.pipe(Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_store__["o" /* select */])(__WEBPACK_IMPORTED_MODULE_5__basket_selectors__["c" /* basketItemsWithSubTotal */]));
        this.basketTotal$ = store.pipe(Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_store__["o" /* select */])(__WEBPACK_IMPORTED_MODULE_5__basket_selectors__["d" /* basketTotal */]));
        this.basketCount$ = store.pipe(Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_store__["o" /* select */])(__WEBPACK_IMPORTED_MODULE_5__basket_selectors__["a" /* basketCount */]));
        this.basketConfirmationForm = this.fb.group({
            confirmed: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */](false, mustBeTrue)
        });
    }
    BasketPage.prototype.addItem = function () {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_4__basket_reducer__["a" /* actions */].AddItem({}));
    };
    BasketPage.prototype.onFooterUpdate = function () {
        this.content.resize();
    };
    BasketPage.prototype.onSubmit = function () {
        var _this = this;
        this.isSubmitted = true;
        setTimeout(function () {
            _this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_4__basket_reducer__["a" /* actions */].EmptyBasket({}));
        }, 10);
        // this.modalCtrl.create(BasketConfirmationModal).present()
    };
    BasketPage.prototype.navigate = function (link) {
        this.navService.followLink(link);
    };
    BasketPage.prototype.openShare = function () {
        this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__modals_share_share_component__["a" /* ShareModal */], { contracts: [] }).present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], BasketPage.prototype, "content", void 0);
    BasketPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\basket\basket.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{\'PAGE_TITLE.BASKET\'| translate}}</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding class="background-blue basket">\n  <div *ngIf="!isSubmitted">\n    <div *ngIf="basketCount$ | async">\n        <div class="total-label">{{\'BASKET.TOTAL\' | translate}} ({{ basketCount$ | async | aohNumber }} {{\'BASKET.CONTRACTS\' | translate}})</div>\n        <div class="total-amount--container">\n          <div class="total-amount">{{(basketTotal$ | async) | aohNumber}}</div>\n          <div class="total-unit">EUR</div>\n        </div>\n        <div *ngFor=\'let item of items$ | async\'>\n            <basket-item [item]="item"></basket-item>\n        </div>\n    </div>\n\n    <div *ngIf="(basketCount$ | async) == 0" class="no-items-container">\n      <aoh-icon icon="contract-big"></aoh-icon>\n      <div class="no-items-message">{{\'BASKET.NO_CONTRACTS\' | translate}}</div>\n    </div>\n  </div>\n\n  <confirmation-page *ngIf="isSubmitted" [showClose]="false">\n    <div class="confirmation-title">{{\'BASKET.CONTRACT_CREATED\' | translate}}</div>\n    <div class="confirmation-subtitle">{{\'BASKET.CONTRACT_CREATED_DESCRIPTION\' | translate}}</div>\n    <!-- todo: replace static dummy text -->\n    <div class="confirmation-subtitle">max.mustermann@b2b.de</div>\n  \n    <div>\n        <button class="btn-primary filled" (click)="navigate(\'contracts\')">{{\'BASKET.VIEW_MY_CONTRACTS\' | translate}}</button>\n        <button class="btn-primary" (click)="navigate(\'aoh\')">{{\'BASKET.VIEW_MY_PRICES\' | translate}}</button>\n        <button class="btn-primary" (click)="openShare()">{{\'BASKET.SHARE_CONTRACTS\' | translate}}</button>\n    </div>\n  </confirmation-page>\n\n</ion-content>\n  \n\n<ion-footer class="basket-footer" *ngIf="!isSubmitted">\n  <form [formGroup]="basketConfirmationForm" *ngIf="basketCount$ | async">\n    <pricing-count-down (onSizeChange)="onFooterUpdate()"></pricing-count-down>    \n    <ion-toolbar>\n        <ion-list>\n            <ion-item>\n                <ion-label>{{\'BASKET.AGREE_TO\' | translate}} <span class="terms-conditions-link">XXXXXXXX{{\'BASKET.SALES_AND_DELIVER\' | translate}}</span></ion-label>\n                <ion-checkbox formControlName="confirmed"></ion-checkbox>\n            </ion-item>\n        </ion-list>\n        <div class="btn-footer">\n          <button class="btn-primary filled" [disabled]="basketConfirmationForm.invalid" (click)="onSubmit()">{{ \'BASKET.CREATE_CONTRACTS\' | i18nPlural: (basketCount$ | async) | translate }}</button>\n        </div>\n    </ion-toolbar>\n  </form>\n</ion-footer>\n'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\basket\basket.html"*/,
            styles: ['./basket.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ngrx_store__["h" /* Store */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */], __WEBPACK_IMPORTED_MODULE_7__app_navigation__["a" /* NavigationService */]])
    ], BasketPage);
    return BasketPage;
}());

//# sourceMappingURL=basket.js.map

/***/ }),

/***/ 670:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BasketItem; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__modals_addItem_addItem__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__basket_reducer__ = __webpack_require__(95);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BasketItem = /** @class */ (function () {
    function BasketItem(store, modalCtrl, alertCtrl) {
        this.store = store;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
    }
    BasketItem.prototype.removeItem = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Remove contract from basket',
            message: 'Are you sure you want to remove this contract from your basket?',
            buttons: [
                {
                    text: 'Cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Remove',
                    handler: function () {
                        _this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_4__basket_reducer__["a" /* actions */].RemoveItem(_this.item.material.id));
                    }
                }
            ]
        });
        alert.present();
    };
    BasketItem.prototype.openUpdateItem = function (item) {
        var updateItemModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__modals_addItem_addItem__["a" /* AddItemModal */], {
            material: this.item.material,
            item: this.item,
            onConfirm: this.updateItem.bind(this),
            labels: {
                title: 'BASKET.UPDATE_ITEM',
                confirm: 'BASKET.UPDATE_CONFIRM'
            }
        });
        updateItemModal.present();
    };
    BasketItem.prototype.updateItem = function (_a) {
        var quantity = _a.quantity, material = _a.material;
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_4__basket_reducer__["a" /* actions */].UpdateItem({
            id: this.item.material.id,
            quantity: quantity,
        }));
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], BasketItem.prototype, "item", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Number)
    ], BasketItem.prototype, "index", void 0);
    BasketItem = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'basket-item',template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\basket\components\basketItem\basketItem.component.html"*/'<div>\n\n    <div class="item-title">{{ item.material.MATERIAL_DESCRIPTION }}</div>\n\n\n\n    <div class="item-detail">\n\n        <div class="item-detail--title">{{ \'BASKET_ITEM.LOCATION\' | translate }}</div>\n\n        <div class="item-detail--value">{{item.material.PLANT_SHORT_ADDRESS}}</div>\n\n    </div>\n\n    <div class="item-detail">\n\n        <div class="item-detail--title">{{ \'BASKET_ITEM.VALIDITY\' | translate }}</div>\n\n        <div class="item-detail--value">{{item.material.START_DATE | aohDate}} - {{item.material.END_DATE | aohDate}} ({{item.material.START_DATE | dayDiff: item.material.END_DATE}} days)</div>\n\n    </div>\n\n    <div class="item-box">\n\n        <div class="item-column">\n\n            <div class="item-column--title">{{ \'BASKET_ITEM.VOLUME\' | translate }}</div>\n\n            <div class="item-value--container">\n\n                <span class="item-value--amount">{{item.quantity | aohNumber}}</span>\n\n                <span class="item-value--unit">{{item.material.UOM_EXT}}</span>\n\n            </div>\n\n        </div>\n\n        <div class="item-column">\n\n            <div class="item-column--title">{{ \'BASKET_ITEM.PRICE\' | translate }}</div>\n\n            <div class="item-value--container">\n\n                <span class="item-value--amount">{{item.material.CUSTOMER_PRICE | aohNumber}}</span>\n\n                <span class="item-value--unit">{{item.material.CURRENCY}}/{{item.material.CUSTOMER_CONDITION_UNIT_EXT}}</span>\n\n            </div>\n\n        </div>\n\n    </div>\n\n     <div class="item-box">\n\n        <div class="item-column">\n\n            <div class="item-column--title">{{ \'BASKET_ITEM.SUBTOTAL\' | translate }}</div>\n\n            <div class="item-value--container">\n\n                <span class="item-value--amount">{{item.subTotal | aohNumber}}</span>\n\n                <span class="item-value--unit">{{item.material.CURRENCY}}</span>\n\n            </div>\n\n        </div>\n\n    </div>\n\n    <div class="btn-row">\n\n        <button class="btn-primary" (click)="removeItem(item)">{{ \'BASKET_ITEM.REMOVE\' | translate }}</button>\n\n        <button class="btn-primary filled" (click)="openUpdateItem(item)">{{ \'BASKET_ITEM.UPDATE\' | translate }}</button>\n\n    </div>\n\n    \n\n</div>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\basket\components\basketItem\basketItem.component.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ModalController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */]])
    ], BasketItem);
    return BasketItem;
}());

//# sourceMappingURL=basketItem.js.map

/***/ }),

/***/ 671:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BasketIcon; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__basket_selectors__ = __webpack_require__(144);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var template = "\n<div>\n    <aoh-icon icon=\"basket\" (click)=\"showBasket()\"></aoh-icon>\n    <span *ngIf=\"itemCount$ | async\" class=\"count\" (click)=\"showBasket()\">{{itemCount$ | async}}</span>\n</div>\n";
var BasketIcon = /** @class */ (function () {
    function BasketIcon(navCtrl, store) {
        this.navCtrl = navCtrl;
        this.store = store;
        this.itemCount$ = this.store.pipe(Object(__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["o" /* select */])(__WEBPACK_IMPORTED_MODULE_3__basket_selectors__["a" /* basketCount */]));
    }
    BasketIcon.prototype.showBasket = function () {
        this.navCtrl.push('basket');
    };
    BasketIcon = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'basket-icon',
            template: template
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */]])
    ], BasketIcon);
    return BasketIcon;
}());

//# sourceMappingURL=basketIcon.component.js.map

/***/ }),

/***/ 672:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdateItemModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UpdateItemModal = /** @class */ (function () {
    function UpdateItemModal(viewCtrl, params) {
        this.viewCtrl = viewCtrl;
        this.params = params;
        this.item = this.params.get('item');
    }
    UpdateItemModal.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    UpdateItemModal = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\basket\modals\updateItem\updateItem.html"*/'<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-buttons start>\n\n        <button ion-button (click)="dismiss()">Close</button>\n\n        </ion-buttons>\n\n        <ion-title>Update item</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    <h1>{{item.price}} {{item.id}}</h1>\n\n    <button class="btn-primary" (click)="dismiss()">Save</button>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\basket\modals\updateItem\updateItem.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */]])
    ], UpdateItemModal);
    return UpdateItemModal;
}());

//# sourceMappingURL=updateItem.js.map

/***/ }),

/***/ 673:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RecentFilter; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var template = "\n    <div *ngIf=\"filter\">\n        <div class=\"row\" *ngFor=\"let item of rowsToDisplay\">\n            <div class=\"filter-title flex-1\">{{item.title}}</div>\n            <div class=\"additional-count\" *ngIf=\"item.additional\">+{{item.additional}}</div>\n        </div>\n    </div>\n";
var isUniqueTypeOfFilter = function (filters) {
    var filterKeys = Object(__WEBPACK_IMPORTED_MODULE_1_lodash__["keys"])(filters);
    filterKeys.reduce(function (prev, key) {
        if (Array.isArray(filters[key]) && filters[key].length) {
            prev = prev;
        }
        return prev;
    }, []);
};
var RecentFilter = /** @class */ (function () {
    function RecentFilter() {
    }
    Object.defineProperty(RecentFilter.prototype, "rowsToDisplay", {
        get: function () {
            var _this = this;
            var filterKeys = Object(__WEBPACK_IMPORTED_MODULE_1_lodash__["keys"])(this.filter);
            var groupsToShow = filterKeys.reduce(function (prev, next) {
                if (Array.isArray(_this.filter[next]) && _this.filter[next].length) {
                    prev = prev.concat([_this.filter[next]]);
                }
                return prev;
            }, []);
            if (groupsToShow.length === 1 && groupsToShow[0].length >= 2) {
                return [
                    { title: groupsToShow[0][0].title, value: groupsToShow[0][0].value },
                    { title: groupsToShow[0][1].title, value: groupsToShow[0][1].value, additional: groupsToShow[0].length - 2 }
                ];
            }
            return groupsToShow.reduce(function (prev, next) {
                return prev.concat([{
                        title: next[0].title, value: next[0].value, additional: next.length - 1
                    }]);
            }, []);
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], RecentFilter.prototype, "filter", void 0);
    RecentFilter = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'recent-filter',
            styles: ['./recentFilter.component.scss'],
            template: template
        })
    ], RecentFilter);
    return RecentFilter;
}());

//# sourceMappingURL=recentFilter.component.js.map

/***/ }),

/***/ 674:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConnectFormDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_effects__ = __webpack_require__(43);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ConnectFormDirective = /** @class */ (function () {
    function ConnectFormDirective(formGroupDirective, store, actions$) {
        this.formGroupDirective = formGroupDirective;
        this.store = store;
        this.actions$ = actions$;
        this.debounce = 300;
        this.error = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
        this.success = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
        this.buildForm = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
    }
    ConnectFormDirective.prototype.ngOnInit = function () {
        var _this = this;
        var sub = this.store.select(function (state) { return state.forms[_this.path]; }).take(1).subscribe(function (formValue) {
            if (formValue) {
                _this.buildForm.emit(formValue);
                _this.formGroupDirective.form.patchValue(formValue);
            }
        });
        this.formChange = this.formGroupDirective.form.valueChanges
            .subscribe(function (value) {
            _this.store.dispatch({
                type: 'UPDATE_FORM',
                payload: {
                    value: value,
                    path: _this.path,
                }
            });
        });
        this.formSuccess = this.actions$
            .ofType('FORM_SUBMIT_SUCCESS')
            .filter(function (_a) {
            var payload = _a.payload;
            return payload.path === _this.path;
        })
            .subscribe(function () {
            _this.formGroupDirective.form.reset();
            _this.success.emit();
        });
        this.formError = this.actions$
            .ofType('FORM_SUBMIT_ERROR')
            .filter(function (_a) {
            var payload = _a.payload;
            return payload.path === _this.path;
        })
            .subscribe(function (_a) {
            var payload = _a.payload;
            return _this.error.emit(payload.error);
        });
        // Update the form value based on the state
    };
    ConnectFormDirective.prototype.ngOnDestroy = function () {
        this.formChange.unsubscribe();
        this.formError.unsubscribe();
        this.formSuccess.unsubscribe();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('connectForm'),
        __metadata("design:type", String)
    ], ConnectFormDirective.prototype, "path", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], ConnectFormDirective.prototype, "debounce", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["O" /* Output */])(),
        __metadata("design:type", Object)
    ], ConnectFormDirective.prototype, "error", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["O" /* Output */])(),
        __metadata("design:type", Object)
    ], ConnectFormDirective.prototype, "success", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["O" /* Output */])(),
        __metadata("design:type", Object)
    ], ConnectFormDirective.prototype, "buildForm", void 0);
    ConnectFormDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* Directive */])({
            selector: '[connectForm]'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormGroupDirective */],
            __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["a" /* Actions */]])
    ], ConnectFormDirective);
    return ConnectFormDirective;
}());

//# sourceMappingURL=connectForm.directive.js.map

/***/ }),

/***/ 675:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modules_router_router_selectors__ = __webpack_require__(77);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DetailsPage = /** @class */ (function () {
    function DetailsPage(store) {
        var _this = this;
        this.store = store;
        this.store.select(__WEBPACK_IMPORTED_MODULE_2__modules_router_router_selectors__["a" /* currentParams */])
            .subscribe(function (v) {
            _this.material = v;
        });
    }
    DetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\aoh\subPages\details\details.html"*/'<ion-header>\n\n    <dismiss-page-header [pageTitle]="material.MATERIAL_DESCRIPTION" [pageSubtitle]="material.PLANT_NAME"></dismiss-page-header>\n\n</ion-header>\n\n\n\n<ion-content>\n\n    <details-box>\n\n        <details-item label="AOH_DETAILS.OPEN_CONTRACTS" [value]="material.contract?.NO_OF_CONTRACTS"></details-item>\n\n        <details-item label="AOH_DETAILS.TOTAL_VOLUME" value="{{material.contract?.TOTAL_VOLUME | aohNumber}} {{material.contract?.UOM_EXT}}"></details-item>\n\n        <details-item label="AOH_DETAILS.VOLUME_LIFTED" labelClass="emphasis" value="{{material.contract?.LIFTED_VOLUME | aohNumber}} {{material.contract?.UOM_EXT}}"></details-item>\n\n        <details-item label="AOH_DETAILS.OPEN_VOLUME" value="{{material.contract?.OPEN_VOLUME | aohNumber}} {{material.contract?.UOM_EXT}}"></details-item>\n\n        <details-item label="AOH_DETAILS.AGREEMENT_ENDS" [value]="material.END_DATE | aohDate"></details-item>\n\n    </details-box>\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\aoh\subPages\details\details.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */]])
    ], DetailsPage);
    return DetailsPage;
}());

//# sourceMappingURL=details.js.map

/***/ }),

/***/ 676:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContractDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contracts_reducer__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__contracts_selectors__ = __webpack_require__(182);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ContractDetailsPage = /** @class */ (function () {
    function ContractDetailsPage(store) {
        var _this = this;
        this.store = store;
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_2__contracts_reducer__["g" /* getContract */])());
        this.store.select(__WEBPACK_IMPORTED_MODULE_3__contracts_selectors__["a" /* contract */])
            .subscribe(function (v) {
            debugger;
            _this.contract = v;
        });
    }
    ContractDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\contracts\subPages\contractDetails\contractDetails.html"*/'<ion-header>\n\n    <dismiss-page-header pageTitle="PAGE_TITLE.CONTRACT_DETAILS" [pageSubtitle]="contract?.details.CONTRACT_NUMBER"></dismiss-page-header>\n\n</ion-header>\n\n\n\n<ion-content>\n\n    <details-box *ngIf="contract?.details.CONTRACT_NUMBER">\n\n        <details-item label="CONTRACTS.CREATED_AT" value="{{contract?.details.DATE_STARTED | aohDate}}"></details-item>\n\n        <details-item label="CONTRACTS.PRICE_AGREED" value="{{contract?.details.ITEM_TOTAL_VALUE | aohNumber}} {{contract?.details.CURRENCY}}"></details-item>\n\n        <details-item label="CONTRACTS.VALIDITY" value="{{contract?.details.DATE_STARTED | aohDate}} - {{contract?.details.DATE_ENDED | aohDate}}"></details-item>\n\n        <details-item label="CONTRACTS.CONTRACT_VOLUME" value="{{contract?.details.TARGET_QUANTITY | aohNumber}} L15"></details-item>\n\n        <details-item label="CONTRACTS.OPEN_VOLUME" value="{{contract?.details.OPEN_QUANTITY | aohNumber}} L15"></details-item>\n\n        <details-item label="CONTRACTS.LIFTINGS_MADE" [value]="contract?.liftings.length | aohNumber: 0"></details-item>\n\n    </details-box>\n\n\n\n    <lifting-card *ngFor="let lifting of contract?.liftings" [lifting]="lifting"></lifting-card>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\contracts\subPages\contractDetails\contractDetails.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */]])
    ], ContractDetailsPage);
    return ContractDetailsPage;
}());

//# sourceMappingURL=contractDetails.js.map

/***/ }),

/***/ 677:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__modules_user_selectors__ = __webpack_require__(150);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var tiles = [
    { title: 'Aoh', icon: 'prices', link: 'aoh' },
    { title: 'Contracts', icon: 'contract', link: 'contracts' },
    { title: 'Liftings', icon: 'liftings', link: 'home' },
    { title: 'Notifications', icon: 'notification', link: 'home' },
    { title: 'HSSE', icon: 'hsse', link: 'hsse' }
];
var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, store) {
        this.navCtrl = navCtrl;
        this.store = store;
        this.tiles = tiles;
        this.tiles$ = this.store.select(__WEBPACK_IMPORTED_MODULE_3__modules_user_selectors__["a" /* getHomeTileOptions */]);
    }
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\home\home.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>{{ \'APP_TITLE\' | translate }}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding class="background-blue">\n\n\n\n  <!-- <button ion-button secondary menuToggle>Toggle Menu</button> -->\n\n  <div class="home-tile-row" homeContainerResize="168">\n\n      <home-tile *ngFor="let tile of tiles$ | async" [tile]="tile"></home-tile>\n\n  </div>\n\n  \n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["h" /* Store */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 678:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HSSEPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_document_viewer__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_navigation__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__utils_cordova_helpers_js__ = __webpack_require__(94);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var list = [
    {
        title: 'Occupational and traffic safety',
        items: [
            { title: 'Item 1', icon: 'pdf' },
            { title: 'Item 2', icon: 'pdf' },
            { title: 'Item 3', icon: 'pdf' }
        ]
    },
    {
        title: 'Fleet',
        items: [
            { title: 'Title', icon: 'pdf' },
            { title: 'Title 2', icon: 'pdf', description: 'Last updated: xxxxx' }
        ]
    }
];
var HSSEPage = /** @class */ (function () {
    function HSSEPage(document, navService) {
        this.document = document;
        this.navService = navService;
        this.list = list;
    }
    HSSEPage.prototype.openDocument = function () {
        console.log('OPEN');
        var options = {
            title: 'My PDF'
        };
        console.log('OPENING');
        var path = Object(__WEBPACK_IMPORTED_MODULE_3__utils_cordova_helpers_js__["a" /* getResourceUrl */])('/assets/pdf/example.pdf');
        this.document.canViewDocument(path, 'application/pdf', options, function () { return (console.log('SHOW')); }, function () { return (console.log('CLOSE')); }, function () { return (console.log('MISSING')); }, function () { return (console.log('ERROR')); });
        this.document.viewDocument(path, 'application/pdf', options);
    };
    HSSEPage.prototype.onClick = function (item) {
        switch (item.type) {
            case 'PDF':
                this.navService.openRemotePDF('http://www.pdf995.com/samples/pdf.pdf', 'SAMPLE DOCUMENT');
                break;
            case 'EXTERNAL_URL':
                // let browser = this.inAppBrowser.create(item.path,'_system')
                break;
            case 'PAGE':
                this.navService.push(item.path, item.params);
                break;
            default:
                break;
        }
    };
    HSSEPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\hsse\hsse.html"*/'<ion-header>\n\n<ion-navbar>\n\n    <button ion-button menuToggle>\n\n        <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>{{ \'PAGE_TITLE.HSSE\' | translate }}</ion-title>\n\n</ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="background-white">\n\n    <div *ngFor="let section of list" class="hsse-sections--container">\n\n        <div class="sections-title-container">\n\n            <div class="sections-title">{{section.title | translate}}</div>\n\n        </div>\n\n        <div *ngFor="let item of section.items" class="section-item" (click)="onClick(item)">\n\n            <aoh-icon *ngIf="item.icon" [icon]="item.icon"></aoh-icon>\n\n            <div class="section-item--title-container">\n\n                <div class="section-item--title">{{item.title | translate}}</div>\n\n                <div *ngIf="item.description" class="section-item--description">{{item.description}}</div>\n\n            </div>\n\n        </div>\n\n    </div>\n\n</ion-content>\n\n      '/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\hsse\hsse.html"*/,
            styles: ['./hsse.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_document_viewer__["a" /* DocumentViewer */], __WEBPACK_IMPORTED_MODULE_2__app_navigation__["a" /* NavigationService */]])
    ], HSSEPage);
    return HSSEPage;
}());

//# sourceMappingURL=hsse.js.map

/***/ }),

/***/ 679:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LiftingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__search_search__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__search_searchConfig__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__liftings_selectors__ = __webpack_require__(397);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__liftings_reducer__ = __webpack_require__(401);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LiftingsPage = /** @class */ (function () {
    function LiftingsPage(store, navCtrl, modalCtrl) {
        var _this = this;
        this.store = store;
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.liftingsSearch = __WEBPACK_IMPORTED_MODULE_4__search_searchConfig__["c" /* liftingsSearch */];
        this.liftings$ = this.store.select(__WEBPACK_IMPORTED_MODULE_5__liftings_selectors__["c" /* liftings */]);
        this.showingSearchResults$ = this.store.select(__WEBPACK_IMPORTED_MODULE_5__liftings_selectors__["b" /* isShowingLiftingsSearch */]);
        this.store.select(__WEBPACK_IMPORTED_MODULE_5__liftings_selectors__["d" /* liftingsSearch */])
            .subscribe(function (v) {
            _this.searchValues = v;
        });
    }
    LiftingsPage.prototype.openSearch = function () {
        this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__search_search__["a" /* SearchPage */], {
            fields: __WEBPACK_IMPORTED_MODULE_4__search_searchConfig__["c" /* liftingsSearch */].fields,
            onSearch: this.onSearch.bind(this),
            values: this.searchValues
        }).present();
    };
    LiftingsPage.prototype.onChangeSearch = function () {
        this.openSearch();
    };
    LiftingsPage.prototype.onSearch = function (values) {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_6__liftings_reducer__["b" /* setLiftingsSearch */])(values));
    };
    LiftingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'liftings',template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\liftings\liftings.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>Liftings</ion-title>\n\n        <ion-buttons end>\n\n            <aoh-icon icon="search" (click)="openSearch()"></aoh-icon>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="background-white">\n\n    <progress-stepper></progress-stepper>\n\n    <search-details *ngIf="(showingSearchResults$ | async) && searchValues" [values]="searchValues" [fields]="liftingsSearch.fields" (onChange)="onChangeSearch()"></search-details>\n\n    <tab-bar *ngIf="!(showingSearchResults$ | async)"></tab-bar>\n\n    <div class="sections-title-container" *ngIf="(showingSearchResults$ | async) && searchValues">\n\n        <div class="sections-title">{{ \'SEARCH.RESULTS_TITLE\' | translate }}</div>\n\n    </div>\n\n    <div class="masonary-container padding" masonaryContainer="340">\n\n        <lifting-card *ngFor="let lifting of liftings$ | async" [lifting]="lifting"></lifting-card>\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\liftings\liftings.html"*/,
            styles: ['./liftings.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */]])
    ], LiftingsPage);
    return LiftingsPage;
}());

//# sourceMappingURL=liftings.js.map

/***/ }),

/***/ 680:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadIdsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__search_search__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__loadIds_selectors__ = __webpack_require__(396);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loadids_reducer__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__search_searchConfig__ = __webpack_require__(103);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LoadIdsPage = /** @class */ (function () {
    function LoadIdsPage(store, modalCtrl) {
        var _this = this;
        this.store = store;
        this.modalCtrl = modalCtrl;
        this.searchValues = {};
        this.loadIdsSearch = __WEBPACK_IMPORTED_MODULE_6__search_searchConfig__["d" /* loadIdsSearch */];
        this.loadIds$ = store.select(__WEBPACK_IMPORTED_MODULE_4__loadIds_selectors__["d" /* visibleLoadIds */]);
        this.store.select(__WEBPACK_IMPORTED_MODULE_4__loadIds_selectors__["c" /* loadIdsSearch */]).subscribe(function (v) {
            _this.searchValues = v;
        });
        this.showingSearchResults$ = this.store.select(__WEBPACK_IMPORTED_MODULE_4__loadIds_selectors__["b" /* isShowingLoadIdsSearch */]);
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_5__loadids_reducer__["b" /* getLoadIds */])());
    }
    LoadIdsPage.prototype.openSearch = function () {
        this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__search_search__["a" /* SearchPage */], {
            fields: this.loadIdsSearch.fields,
            onSearch: this.onSearch.bind(this),
            values: this.searchValues
        }).present();
    };
    LoadIdsPage.prototype.onSearch = function (values) {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_5__loadids_reducer__["e" /* setLoadIdsSearch */])(values));
    };
    LoadIdsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-loadids',template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\loadids\loadids.html"*/'<ion-header>\n\n    <ion-navbar>\n\n      <button ion-button menuToggle>\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n        <ion-title>{{\'PAGE_TITLE.LOAD_IDS\' | translate}}</ion-title>\n\n        <ion-buttons end>\n\n            <aoh-icon icon="search" (click)="openSearch()"></aoh-icon>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n  </ion-header>\n\n  \n\n\n\n<ion-content class="loadids" [ngClass]="{\'background-blue\': !(showingSearchResults$ | async)}">\n\n    <search-details *ngIf="(showingSearchResults$ | async) && searchValues" [values]="searchValues" [fields]="loadIdsSearch.fields" (onChange)="openSearch()"></search-details>\n\n    <div class="sections-title-container" *ngIf="(showingSearchResults$ | async) && searchValues">\n\n        <div class="sections-title">{{ \'SEARCH.RESULTS_TITLE\' | translate }} {{(loadIds$ | async)?.length}}</div>\n\n    </div>\n\n    <div>\n\n        <div class="masonary-container" masonaryContainer="340">\n\n        <div class="loadId-item" *ngFor="let item of loadIds$ | async">\n\n            <div class="loadId-title">{{item.MATERIAL_DESCRIPTION}}</div>\n\n\n\n            <div class="loadId-detail">{{ \'LOAD_IDS.ID\' | translate }}</div>\n\n            <div class="loadId-value">{{item.MATERIAL_NUMBER}}</div>\n\n    \n\n            <div class="loadId-detail">{{ \'LOAD_IDS.TERMINAL\' | translate }}</div>\n\n            <div class="loadId-value">{{item.PLANT_NAME}}</div>\n\n    \n\n            <div class="loadId-detail">{{ \'LOAD_IDS.DESTINATION\' | translate }}</div>\n\n            <div class="loadId-value">{{item.DELIVERY_POINT_ADDRESS}}</div>\n\n    \n\n            <!-- <div class="loadId-detail">Term type</div>\n\n            <div class="loadId-value"></div> -->\n\n    \n\n            <div class="loadId-detail">{{ \'LOAD_IDS.CUSTOMER_HANDLINE_TYPE\' | translate }}</div>\n\n            <div class="loadId-value">{{item.SHIPPING_COND_TEXT}}</div>\n\n        </div>\n\n        </div>\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\loadids\loadids.html"*/,
            styles: ['./loadids.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */]])
    ], LoadIdsPage);
    return LoadIdsPage;
}());

//# sourceMappingURL=loadids.js.map

/***/ }),

/***/ 681:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_in_app_browser__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__modules_user_actions__ = __webpack_require__(682);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__modals_environment_environmentModal_component__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__config_storageProvider__ = __webpack_require__(96);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







// const initialUser = {
//   username: 'PortalTstF02',
//   password: 'welcome2'
// }
var initialUser = {
    username: 'PortalTstF02',
    password: 'welcome2'
};
var LoginPage = /** @class */ (function () {
    function LoginPage(modalCtrl, navCtrl, store, inAppBrowser, storage) {
        this.modalCtrl = modalCtrl;
        this.navCtrl = navCtrl;
        this.store = store;
        this.inAppBrowser = inAppBrowser;
        this.storage = storage;
        this.user = initialUser;
    }
    LoginPage.prototype.login = function () {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_4__modules_user_actions__["a" /* Login */](this.user));
    };
    LoginPage.prototype.swipeEvent = function (e) {
        if (e.overallVelocityX >= 0) {
            return;
        }
        var environmentModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__modals_environment_environmentModal_component__["a" /* EnvironmentModal */]);
        environmentModal.present();
    };
    LoginPage.prototype.forgottenPassword = function () {
        var browser = this.inAppBrowser.create('https://pscuat.bpglobal.com/PwdSelfCare/dialog/appl/bp/wflow/main', '_system');
        // browser.show()
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\login\login.html"*/'<ion-content class="login-content" (swipe)="swipeEvent($event)">\n\n    <div class="login-logo"></div>\n\n    <div class="login-truck"></div>\n\n    <div class="login-oil"></div>\n\n    <div class="login-details">\n\n        <div class="login-title">{{\'LOGIN.TITLE\' | translate}}</div>\n\n        <div>\n\n            <input type="text" class="login-input" [placeholder]="\'LOGIN.USERNAME\' | translate" [(ngModel)]="user.username"/>\n\n            <input type="password" class="login-input" [placeholder]="\'LOGIN.PASSWORD\' | translate" [(ngModel)]="user.password"/>\n\n        </div>\n\n        <div>\n\n            <button class="btn-primary" (click)="login()">{{ \'LOGIN.LOGIN\' | translate }}</button>\n\n        </div>\n\n        <div class="forgotten-password">\n\n            <button class="btn-link" (click)="forgottenPassword()">{{ \'LOGIN.FORGOTTEN\' | translate }}</button>\n\n        </div>\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\login\login.html"*/,
            styles: ['./login.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_6__config_storageProvider__["a" /* StorageService */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 682:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Login; });
/* unused harmony export Logout */
var Login = /** @class */ (function () {
    function Login(payload) {
        this.payload = payload;
        this.type = 'LOGIN';
    }
    return Login;
}());

var Logout = /** @class */ (function () {
    function Logout() {
        this.type = 'LOGOUT';
    }
    return Logout;
}());

//# sourceMappingURL=user.actions.js.map

/***/ }),

/***/ 683:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__notifications_selectors__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__notifications_reducer__ = __webpack_require__(158);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var NotificationsPage = /** @class */ (function () {
    function NotificationsPage(navCtrl, navParams, store) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.store = store;
        this.items$ = store.select(__WEBPACK_IMPORTED_MODULE_3__notifications_selectors__["c" /* notificationItemsAsArray */]);
    }
    NotificationsPage.prototype.itemTapped = function (event, item) {
        this.navCtrl.push('notification-details', item);
    };
    NotificationsPage.prototype.openSettings = function () {
        this.navCtrl.push('notification-settings');
    };
    NotificationsPage.prototype.dismiss = function (event, item) {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_4__notifications_reducer__["a" /* dismissNotification */])(item.id));
    };
    NotificationsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'notifications',template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\notifications\notifications.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>{{\'PAGE_TITLE.NOTIFICATIONS\' | translate}}</ion-title>\n\n    <ion-buttons end>\n\n      <aoh-icon (click)="openSettings()" icon="settings"></aoh-icon>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="notifications-page background-blue">\n\n  <div *ngFor="let item of items$ | async"\n\n    \n\n    [ngClass]="{read: item.read}"\n\n    class="notification-item row">\n\n    <div class="flex-1" (click)="itemTapped($event, item)">\n\n        <div class="notification-title">{{item.title | translate}}</div>\n\n        <div class="notification-subtitle">{{item.createdAt | aohDate}}</div>\n\n    </div>\n\n    <div (click)="dismiss($event, item)">\n\n      <aoh-icon icon="close"></aoh-icon>\n\n    </div>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\notifications\notifications.html"*/,
            styles: ['./notifications.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["h" /* Store */]])
    ], NotificationsPage);
    return NotificationsPage;
}());

//# sourceMappingURL=notifications.js.map

/***/ }),

/***/ 684:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__notifications_selectors__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__notifications_reducer__ = __webpack_require__(158);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var NotificationDetailsPage = /** @class */ (function () {
    function NotificationDetailsPage(navCtrl, store) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.store = store;
        this.details$ = this.store.select(__WEBPACK_IMPORTED_MODULE_3__notifications_selectors__["a" /* notification */]);
        this.details$.first(function (v) { return v; }).subscribe(function (v) {
            _this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_4__notifications_reducer__["c" /* readNotification */])(v.id));
        });
    }
    NotificationDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'notification-details',template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\notifications\subPages\notificationDetails\notificationDetails.html"*/'<ion-header>\n\n    <ion-navbar>\n\n      <button ion-button menuToggle>\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n    </ion-navbar>\n\n  </ion-header>\n\n  \n\n  <ion-content padding class="notification-details">\n\n    <div class="notification-title">{{(details$ | async)?.title}}</div>\n\n    <div class="notification-date">{{(details$ | async)?.createdAt | aohDate}}</div>\n\n    <div class="notification-content" [innerHTML]="(details$ | async)?.content"></div>\n\n  </ion-content>\n\n  '/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\notifications\subPages\notificationDetails\notificationDetails.html"*/,
            styles: ['./notificationDetails.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["h" /* Store */]])
    ], NotificationDetailsPage);
    return NotificationDetailsPage;
}());

//# sourceMappingURL=notificationDetails.js.map

/***/ }),

/***/ 685:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationSettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var fields = [
    { title: 'NOTIFICATIONS.SMS' },
    { title: 'NOTIFICATIONS.EMAIL' },
    { title: 'PUSH', description: 'PUSH_DESCRIPTION' }
];
var config = [
    {
        title: 'PRICE_CHANGE',
        description: 'PRICE_CHANGE_DESCRIPTION',
        fields: fields,
    },
    {
        title: 'CONTRACT_CONFIRMATION',
        description: 'CONTRACT_CONFIRMATION_DESCRIPTION',
        fields: fields,
    },
    {
        title: 'CONTRACT_NOT_FULFILLED',
        description: 'CONTRACT_NOT_FULFILLED_DESCRIPTION',
        fields: fields,
    },
    {
        title: 'LOW_IN_CONTRACT',
        description: 'LOW_IN_CONTRACT_DESCRIPTION',
        fields: fields,
    },
    {
        title: 'TERMINAL_EVENTS',
        description: 'TERMINAL_EVENTS_DESCRIPTION',
        fields: fields,
    }
];
var NotificationSettingsPage = /** @class */ (function () {
    function NotificationSettingsPage(navCtrl, navParams, store) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.store = store;
        this.config = config;
        // If we navigated to this page, we will have an item available as a nav param
    }
    NotificationSettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'notification-settings',template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\notifications\subPages\notificationSettings\notificationSettings.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n        <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>{{\'PAGE_TITLE.NOTIFICATION_SETTINGS\' | translate}}</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n    <ion-list *ngFor="let section of config">\n\n        <ion-list-header>\n\n            <div>{{section.title | translate}}</div>\n\n            <div>{{section.description | translate}}</div>\n\n        </ion-list-header>\n\n        <ion-item *ngFor="let field of section.fields">\n\n            <ion-label>\n\n                <div>{{field.title | translate}}</div>\n\n                <div class="settings-subtitle">{{field.description | translate}}</div>\n\n            </ion-label>\n\n            <ion-checkbox item-right [checked]="false"></ion-checkbox>\n\n        </ion-item>\n\n    </ion-list>\n\n</ion-content>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\notifications\subPages\notificationSettings\notificationSettings.html"*/,
            styles: ['./notificationSettings.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["h" /* Store */]])
    ], NotificationSettingsPage);
    return NotificationSettingsPage;
}());

//# sourceMappingURL=notificationSettings.js.map

/***/ }),

/***/ 686:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlaygroundPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var PlaygroundPage = /** @class */ (function () {
    function PlaygroundPage() {
    }
    PlaygroundPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\playground\playground.html"*/''/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\playground\playground.html"*/,
        })
    ], PlaygroundPage);
    return PlaygroundPage;
}());

//# sourceMappingURL=playground.js.map

/***/ }),

/***/ 687:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PricesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_navigation__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__prices_selectors__ = __webpack_require__(688);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__prices_reducer__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__utils_storeUtils__ = __webpack_require__(76);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var tabOptions = [
    { title: 'CURRENT', value: 'current' },
    { title: 'HISTORY', value: 'history' }
];
var PricesPage = /** @class */ (function () {
    function PricesPage(navCtrl, navParams, store, navService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.store = store;
        this.navService = navService;
        this.tabOptions = tabOptions;
        this.prices$ = this.store.select(__WEBPACK_IMPORTED_MODULE_4__prices_selectors__["b" /* prices */]);
        this.historyPrices$ = this.store.select(__WEBPACK_IMPORTED_MODULE_4__prices_selectors__["a" /* historyPrices */]);
        this.store.select(__WEBPACK_IMPORTED_MODULE_4__prices_selectors__["d" /* pricesSelectedFilter */])
            .subscribe(function (v) {
            _this.selectedFilter = v;
        });
        this.viewing$ = this.store.select(__WEBPACK_IMPORTED_MODULE_4__prices_selectors__["e" /* pricesViewing */]);
    }
    PricesPage.prototype.ionViewDidLoad = function () {
        var viewing = Object(__WEBPACK_IMPORTED_MODULE_6__utils_storeUtils__["b" /* select */])(this.store, __WEBPACK_IMPORTED_MODULE_4__prices_selectors__["e" /* pricesViewing */]) || 'current';
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_5__prices_reducer__["e" /* setViewing */])(viewing));
    };
    PricesPage.prototype.onTabChange = function (value) {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_5__prices_reducer__["e" /* setViewing */])(value));
    };
    PricesPage.prototype.removePlantFilter = function (item) {
        var newFilters = {
            plants: this.selectedFilter.plants.filter(function (p) { return p.title !== item.title; }),
            products: this.selectedFilter.products
        };
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_5__prices_reducer__["d" /* setPriceFilter */])(newFilters));
    };
    PricesPage.prototype.removeProductFilter = function (item) {
        var newFilters = {
            products: this.selectedFilter.products.filter(function (p) { return p.title !== item.title; }),
            plants: this.selectedFilter.plants
        };
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_5__prices_reducer__["d" /* setPriceFilter */])(newFilters));
    };
    PricesPage.prototype.openFilter = function () {
        this.navCtrl.push('filter', {
            options: __WEBPACK_IMPORTED_MODULE_4__prices_selectors__["c" /* pricesFilterOptions */],
            recents: __WEBPACK_IMPORTED_MODULE_4__prices_selectors__["f" /* recents */],
            onSetFilter: __WEBPACK_IMPORTED_MODULE_5__prices_reducer__["d" /* setPriceFilter */],
        });
    };
    PricesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'prices',template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\prices\prices.html"*/'<ion-header>\n\n    <ion-navbar>\n\n      <button ion-button menuToggle>\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n      <ion-title>{{\'PAGE_TITLE.PRICES\' | translate}}</ion-title>\n\n      <ion-buttons end>\n\n        <aoh-icon (click)="openFilter()" icon="filter"></aoh-icon>\n\n      </ion-buttons>\n\n    </ion-navbar>\n\n  </ion-header>\n\n  \n\n  <ion-content class="background-blue">\n\n    <div class="filter-pill-container with-padding">\n\n      <filter-pill *ngFor="let item of selectedFilter?.plants" [label]="item.title" [item]="item" (onDismiss)="removePlantFilter(item)"></filter-pill>\n\n      <filter-pill *ngFor="let item of selectedFilter?.products" [label]="item.title" [item]="item" (onDismiss)="removeProductFilter(item)"></filter-pill>      \n\n    </div>\n\n    <tab-bar [value]="viewing$ | async" [options]="tabOptions" (onChange)="onTabChange($event)"></tab-bar>\n\n    <div *ngFor="let priceGroup of historyPrices$ | async">\n\n      <div *ngIf="(historyPrices$ | async)?.length > 1" class="price-group--title">{{priceGroup.date | aohDate}}</div>\n\n      <price-item *ngFor="let price of priceGroup.prices" [price]="price"></price-item>\n\n    </div>\n\n  </ion-content>\n\n  '/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\prices\prices.html"*/,
            styles: ['./prices.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["h" /* Store */], __WEBPACK_IMPORTED_MODULE_3__app_navigation__["a" /* NavigationService */]])
    ], PricesPage);
    return PricesPage;
}());

//# sourceMappingURL=prices.js.map

/***/ }),

/***/ 688:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return prices; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return recents; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return pricesSelectedFilter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return pricesViewing; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return pricesFilterOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return historyPrices; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__aoh_aoh_selectors__ = __webpack_require__(143);



var prices = function (state) {
    return state['prices'].prices || [];
};
var recents = function (state) {
    return state['prices'].recentFilters || [];
};
var pricesSelectedFilter = function (state) {
    return state['prices'].selectedFilter || null;
};
var pricesViewing = function (state) {
    return state['prices'].viewing;
};
var pricesFilterOptions = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(prices, function (prices) {
    if (!prices.length) {
        return {};
    }
    return {
        PLANTS: Object(__WEBPACK_IMPORTED_MODULE_2__aoh_aoh_selectors__["g" /* getOptions */])(prices, 'PLANT_NAME', 'PLANT_NAME', 'PLANT'),
        PRODUCTS: Object(__WEBPACK_IMPORTED_MODULE_2__aoh_aoh_selectors__["g" /* getOptions */])(prices, 'MATERIAL_DESCRIPTION', 'MATERIAL_DESCRIPTION', 'MATERIAL_ID')
    };
});
var historyPrices = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(prices, pricesSelectedFilter, function (prices, selectedFilter) {
    if (selectedFilter) {
        // If there isn't at least 1 filter selected then nothing to do
        if (selectedFilter.plants.length || selectedFilter.products.length) {
            prices = prices.filter(function (material) {
                return Object(__WEBPACK_IMPORTED_MODULE_1_lodash__["some"])(selectedFilter.plants, function (plant) {
                    return material.PLANT === plant.PLANT;
                }) || Object(__WEBPACK_IMPORTED_MODULE_1_lodash__["some"])(selectedFilter.products, function (product) {
                    return material.MATERIAL_ID === product.MATERIAL_ID;
                });
            });
        }
    }
    var pricesByDate = Object(__WEBPACK_IMPORTED_MODULE_1_lodash__["groupBy"])(prices, 'CREATION_DATE');
    var output = [];
    for (var date in pricesByDate) {
        output.push({
            date: date,
            prices: pricesByDate[date]
        });
    }
    return output;
});
//# sourceMappingURL=prices.selectors.js.map

/***/ }),

/***/ 689:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PriceItem; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PriceItem = /** @class */ (function () {
    function PriceItem() {
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('price'),
        __metadata("design:type", Object)
    ], PriceItem.prototype, "price", void 0);
    PriceItem = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'price-item',template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\prices\components\priceItem\priceItem.html"*/'<div class="price-item--container">\n\n    <div class="price-item--title">{{ price.MATERIAL_DESCRIPTION }} {{price.MATERIAL_ID}}</div>\n    <div class="price-item--location">\n        <aoh-icon icon="location"></aoh-icon>\n        <div class="price-item--location-label">{{ price.PLANT_NAME }}</div>\n    </div>\n    <div class="price">\n        {{ price.PRICE }}\n    </div>\n    <div class="price-unit">{{price.CURRENCY}} {{\'AOH.PER\' | translate}} {{price.PRICING_UNIT}}/{{price.UOM_EXT}}</div>\n\n    <div class="price-created-at">{{ price.CREATION_DATE | aohDate }} {{ price.CREATION_TIME | aohTime }}</div>    \n</div>\n'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\prices\components\priceItem\priceItem.html"*/
        })
    ], PriceItem);
    return PriceItem;
}());

//# sourceMappingURL=priceItem.js.map

/***/ }),

/***/ 690:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export EPANDED_TEXTAREA_VALUE_ACCESSOR */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterSelect; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var EPANDED_TEXTAREA_VALUE_ACCESSOR = {
    provide: __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* NG_VALUE_ACCESSOR */],
    useExisting: Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* forwardRef */])(function () { return FilterSelect; }),
    multi: true,
};
var FilterSelect = /** @class */ (function () {
    function FilterSelect(renderer, alertCtrl, modalCtrl, store, translateService) {
        this.renderer = renderer;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.store = store;
        this.translateService = translateService;
        this.field = { translate: false, displayFn: null, options: null, label: null, placeholder: null };
    }
    FilterSelect.prototype.writeValue = function (value) {
        this.innerValue = value;
    };
    FilterSelect.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    FilterSelect.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    FilterSelect.prototype.setDisabledState = function (isDisabled) {
        // const div = this.textarea.nativeElement;
        // const action = isDisabled ? 'addClass' : 'removeClass';
        // this.renderer[action](div, 'disabled');
    };
    FilterSelect.prototype.ngOnInit = function () {
        var _this = this;
        this.store.select(this.field.options)
            .subscribe(function (v) {
            _this.options = v;
        });
    };
    FilterSelect.prototype.onFieldChange = function ($event) {
        this.setFieldValue($event.target.value);
    };
    FilterSelect.prototype.setFieldValue = function (value) {
        this.innerValue = value;
        this.onChange(value);
        this.onTouched(value);
    };
    Object.defineProperty(FilterSelect.prototype, "displayValue", {
        get: function () {
            var _this = this;
            if (!this.options) {
                return '';
            }
            var selectedValue = this.options.find(function (o) { return o.value === _this.innerValue; });
            if (selectedValue) {
                return this.field.translate ? this.translateService.instant(selectedValue.title) : selectedValue.title;
            }
            if (!selectedValue && this.innerValue && this.field.displayFn) {
                return this.field.displayFn(this.innerValue);
            }
            return this.translateService.instant(this.field.placeholder) || '';
        },
        enumerable: true,
        configurable: true
    });
    FilterSelect.prototype.showSelect = function () {
        var _this = this;
        var alert = this.alertCtrl.create();
        var title = this.translateService.instant(this.field.label);
        alert.setTitle(title);
        if (this.field.placeholder) {
            var placeholderInput = {
                type: 'radio',
                label: this.translateService.instant(this.field.placeholder),
                value: undefined,
                checked: !this.innerValue,
            };
            alert.addInput(placeholderInput);
        }
        var inputs = this.options.map(function (o) {
            return {
                type: 'radio',
                label: _this.field.translate ? _this.translateService.instant(o.title) : o.title,
                value: o.value,
                checked: o.value === _this.innerValue,
                handler: function () {
                    if (o.modal) {
                        var modal = _this.modalCtrl.create(o.modal);
                        modal.onDidDismiss(function (data) {
                            _this.setFieldValue(data);
                            console.log(data);
                            alert.dismiss();
                        });
                        modal.present();
                    }
                }
            };
        });
        inputs.forEach(function (i) {
            alert.addInput(i);
        });
        alert.addButton('Cancel');
        alert.addButton({
            text: 'Ok',
            handler: function (data) {
                _this.setFieldValue(data);
            }
        });
        alert.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], FilterSelect.prototype, "field", void 0);
    FilterSelect = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'filter-select',
            providers: [EPANDED_TEXTAREA_VALUE_ACCESSOR],template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\search\components\filterSelect\filterSelect.component.html"*/'<button class="btn-primary" (click)="showSelect()">{{displayValue}}</button>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\search\components\filterSelect\filterSelect.component.html"*/,
            styles: ['./filterSelect.component.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["W" /* Renderer2 */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["k" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */]])
    ], FilterSelect);
    return FilterSelect;
}());

// <textarea-expanded [formControl]="control"></textarea-expanded>
//# sourceMappingURL=filterSelect.component.js.map

/***/ }),

/***/ 691:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductCataloguePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_navigation__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var products = [
    { title: 'Aral HeizölEco', product: 'Aral HeizoelEco_productsheet.pdf', msds: 'Aral HeizolEco_MSDS.pdf' },
    { title: 'Aral HeizölEcoPlus', product: 'Aral HeizolEcoPlus_productsheet.pdf', msds: 'Aral HeizolEcoPlus_MSDS.pdf' },
    { title: 'Aral Super', product: 'Aral Super 95_productsheet', msds: 'Aral Super 95_MSDS' },
    { title: 'Aral SuperDiesel', product: 'Aral SuperDiesel_productsheet', msds: 'Aral SuperDiesel_MSDS' },
    { title: 'Aral SuperPlus', product: 'Aral SuperPlus_productsheet', msds: 'Aral SuperPlus_MSDS' },
    { title: 'Aral Super 95 E10', product: 'Aral Super 95 E10_productsheet', msds: 'Aral Super 95 E10_MSDS' },
];
var ProductCataloguePage = /** @class */ (function () {
    function ProductCataloguePage(navService) {
        this.navService = navService;
        this.products = products;
    }
    ProductCataloguePage.prototype.openProduct = function (product) {
        this.navService.push('product-details', { product: product });
    };
    ProductCataloguePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\usefulInformation\subPages\productCatalogue\productCatalogue.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>{{ \'PAGE_TITLE.PRODUCT_CATALOGUE\' | translate }}</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="background-white">\n\n    <ion-list>\n\n        <ion-item *ngFor="let product of products" (click)="openProduct(product)">\n\n            <ion-label>{{product.title}}</ion-label>\n\n        </ion-item>\n\n    </ion-list>\n\n</ion-content>\n\n      '/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\usefulInformation\subPages\productCatalogue\productCatalogue.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__app_navigation__["a" /* NavigationService */]])
    ], ProductCataloguePage);
    return ProductCataloguePage;
}());

//# sourceMappingURL=productCatalogue.js.map

/***/ }),

/***/ 692:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_call_number__ = __webpack_require__(415);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__modules_router_router_selectors__ = __webpack_require__(77);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var contacts = {
    sales: [
        { 'title': 'Arne Fischer', 'email': 'arne.fischer@de.bp.com', 'phone': '02343150' },
    ],
    business: [
        { 'title': 'Arne Fischer', 'email': 'arne.fischer@de.bp.com', 'phone': '02343150' }
    ]
};
var titles = {
    sales: 'PAGE_TITLE.CONTACTS_SALES',
    business: 'PAGE_TITLE.CONTACTS_BUSINESS'
};
var ContactsPage = /** @class */ (function () {
    function ContactsPage(callNumber, store) {
        var _this = this;
        this.callNumber = callNumber;
        this.store = store;
        this.contacts = [];
        this.titleKey = null;
        this.store.select(__WEBPACK_IMPORTED_MODULE_3__modules_router_router_selectors__["a" /* currentParams */])
            .subscribe(function (v) {
            _this.contacts = contacts[v.type];
            _this.titleKey = titles[v.type];
        });
    }
    ContactsPage.prototype.dial = function (contact) {
        this.callNumber.isCallSupported()
            .then(function (response) {
            if (response == true) {
                this.callNumber.callNumber(contact.phone, true);
            }
            else {
                alert('Not supported');
            }
        });
    };
    ContactsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\usefulInformation\subPages\contacts\contacts.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>{{ titleKey | translate }}</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="background-blue contacts">\n\n    <div class="contact-card" *ngFor="let contact of contacts">\n\n        <div class="contact-name">{{contact.title}}</div>\n\n\n\n        <div class="contact-details">{{ \'CONTACTS.PHONE\' | translate }}</div>\n\n        <div class="contact-value" (onClick)="dial(contact)">{{contact.phone}}</div>\n\n\n\n        <div class="contact-details">{{ \'CONTACTS.EMAIL\' | translate }}</div>\n\n        <div class="contact-value">{{contact.email}}</div>\n\n    </div>\n\n</ion-content>\n\n      '/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\usefulInformation\subPages\contacts\contacts.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_call_number__["a" /* CallNumber */], __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["h" /* Store */]])
    ], ContactsPage);
    return ContactsPage;
}());

//# sourceMappingURL=contacts.js.map

/***/ }),

/***/ 693:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_navigation__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var list = [
    { title: 'PRODUCT_DETAILS.TECHINCAL', icon: 'pdf', link: 'product' },
    { title: 'PRODUCT_DETAILS.MSDS', icon: 'pdf', link: 'msds' },
];
var ProductDetailsPage = /** @class */ (function () {
    function ProductDetailsPage(navService, navParams) {
        this.navService = navService;
        this.navParams = navParams;
        this.list = list;
        this.product = navParams.get('product');
    }
    ProductDetailsPage.prototype.onClick = function (link) {
        if (this.product[link]) {
            this.navService.openFile(this.product[link]);
        }
    };
    ProductDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\usefulInformation\subPages\productDetails\productDetails.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>{{ \'PAGE_TITLE.PRODUCT_DETAILS\' | translate }}</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="background-white">\n\n    <div *ngFor="let item of list" class="section-item" (click)="onClick(item.link)">\n\n        <aoh-icon *ngIf="item.icon" [icon]="item.icon"></aoh-icon>\n\n        <div class="section-item--title-container">\n\n            <div class="section-item--title">{{item.title | translate}}</div>\n\n            <div *ngIf="item.description" class="section-item--description">{{item.description}}</div>\n\n        </div>\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\usefulInformation\subPages\productDetails\productDetails.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__app_navigation__["a" /* NavigationService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */]])
    ], ProductDetailsPage);
    return ProductDetailsPage;
}());

//# sourceMappingURL=productDetails.js.map

/***/ }),

/***/ 694:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsefulInformationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_document_viewer__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_navigation__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__utils_cordova_helpers_js__ = __webpack_require__(94);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PAGE = 'PAGE';
var PDF = 'PDF';
var XLS = 'XLS';
var EXTERNAL_URL = 'EXTERNAL_URL';
var list = [
    {
        title: 'USEFUL_INFORMATION.CONTACTS',
        items: [
            { title: 'USEFUL_INFORMATION.SALES', type: PAGE, path: 'contacts', params: { type: 'sales' } },
            { title: 'USEFUL_INFORMATION.B2B', type: PAGE, path: 'contacts', params: { type: 'business' } },
        ]
    },
    {
        title: 'USEFUL_INFORMATION.MARKET_INFORMATION',
        items: [
            { title: 'USEFUL_INFORMATION.MARKET_INFORMATION', icon: 'pdf', path: 'market information.pdf', type: PDF }
        ]
    },
    {
        title: 'USEFUL_INFORMATION.MARKETING',
        items: [
            { title: 'USEFUL_INFORMATION.WEB_SHOP', icon: 'liftings', type: EXTERNAL_URL, path: 'http://aral-webshop.fabs.global' },
        ]
    },
    {
        title: 'USEFUL_INFORMATION.PRODUCT',
        items: [
            { title: 'USEFUL_INFORMATION.PRODUCT_CATALOGUE', description: 'USEFUL_INFORMATION.PRODUCT_CATALOGUE_DESCRIPTION', type: PAGE, path: 'product-catalogue' },
            { title: 'USEFUL_INFORMATION.DISTRIBUTION_CHANNELS', icon: 'pdf', path: 'distribution channels.xls', type: XLS },
            { title: 'USEFUL_INFORMATION.PRODUCT_CLAIMS', icon: 'pdf', path: 'product claims handbook.pdf', type: PDF }
        ]
    },
    {
        title: 'USEFUL_INFORMATION.LIFTINGS',
        items: [
            { title: 'USEFUL_INFORMATION.TERMINALS', icon: 'pdf', description: 'Information and opeing hours', type: PDF },
            { title: 'USEFUL_INFORMATION.CARGO', icon: 'pdf', path: 'previous cargo.xls', description: 'Procedure for product change', type: XLS },
        ]
    }
];
var UsefulInformationPage = /** @class */ (function () {
    function UsefulInformationPage(document, navService, inAppBrowser) {
        this.document = document;
        this.navService = navService;
        this.inAppBrowser = inAppBrowser;
        this.list = list;
    }
    UsefulInformationPage.prototype.openDocument = function () {
        console.log('OPEN');
        var options = {
            title: 'My PDF'
        };
        console.log('OPENING');
        var path = Object(__WEBPACK_IMPORTED_MODULE_4__utils_cordova_helpers_js__["a" /* getResourceUrl */])('/assets/pdf/example.pdf');
        this.document.canViewDocument(path, 'application/pdf', options, function () { return (console.log('SHOW')); }, function () { return (console.log('CLOSE')); }, function () { return (console.log('MISSING')); }, function () { return (console.log('ERROR')); });
        this.document.viewDocument(path, 'application/pdf', options);
    };
    UsefulInformationPage.prototype.onClick = function (item) {
        switch (item.type) {
            case 'PDF':
                this.navService.openFile('/useful/' + item.path);
                break;
            case 'XLS':
                this.navService.openFile('/useful/' + item.path);
                break;
            case 'EXTERNAL_URL':
                var browser = this.inAppBrowser.create(item.path, '_system');
                break;
            case 'PAGE':
                this.navService.push(item.path, item.params);
                break;
            default:
                break;
        }
    };
    UsefulInformationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\usefulInformation\usefulInformation.html"*/'<ion-header>\n\n<ion-navbar>\n\n    <button ion-button menuToggle>\n\n        <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>{{ \'PAGE_TITLE.USEFUL_INFORMATION\' | translate }}</ion-title>\n\n</ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="background-white">\n\n    <div *ngFor="let section of list" class="hsse-sections--container">\n\n        <div class="sections-title-container">\n\n            <div class="sections-title">{{section.title | translate}}</div>\n\n        </div>\n\n        <div *ngFor="let item of section.items" class="section-item" (click)="onClick(item)">\n\n            <aoh-icon *ngIf="item.icon" [icon]="item.icon"></aoh-icon>\n\n            <div class="section-item--title-container">\n\n                <div class="section-item--title">{{item.title | translate}}</div>\n\n                <div *ngIf="item.description" class="section-item--description">{{item.description | translate}}</div>\n\n            </div>\n\n        </div>\n\n    </div>\n\n</ion-content>\n\n      '/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\usefulInformation\usefulInformation.html"*/,
            styles: ['./usefulInformation.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_document_viewer__["a" /* DocumentViewer */], __WEBPACK_IMPORTED_MODULE_3__app_navigation__["a" /* NavigationService */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__["a" /* InAppBrowser */]])
    ], UsefulInformationPage);
    return UsefulInformationPage;
}());

//# sourceMappingURL=usefulInformation.js.map

/***/ }),

/***/ 695:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__ = __webpack_require__(60);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var languages = [
    { title: 'English', code: 'en' },
    { title: 'German', code: 'de' }
];
var UserPage = /** @class */ (function () {
    function UserPage(fb, translate) {
        this.fb = fb;
        this.translate = translate;
        this.languages = languages;
        this.selectedLanguage = null;
        this.form = this.fb.group({
            language: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](null, null),
            email: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](null, null),
            phone: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */](null, null),
        });
    }
    UserPage.prototype.onSet = function () {
        var values = this.form.value;
        if (values.language) {
            this.translate.use(values.language);
        }
    };
    UserPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\user\user.html"*/'<ion-header>\n\n<ion-navbar>\n\n    <button ion-button menuToggle>\n\n        <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>{{ \'PAGE_TITLE.USER\' | translate }}</ion-title>\n\n</ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="background-white">\n\n    <form [formGroup]="form">\n\n    <div class="hsse-sections--container">\n\n        <div class="sections-title-container">\n\n            <div class="sections-title">{{ \'USER.LANGUAGE\' | translate }}</div>\n\n        </div>\n\n        <ion-list radio-group formControlName="language">\n\n            <ion-item *ngFor="let lang of languages">\n\n                <ion-label>{{lang.title}}</ion-label>\n\n                <ion-radio [value]="lang.code"></ion-radio>\n\n            </ion-item>\n\n        </ion-list>\n\n    </div>\n\n    <div class="hsse-sections--container">\n\n        <div class="sections-title-container">\n\n            <div class="sections-title">{{ \'USER.CONTACT_INFORMATION\' | translate }}</div>\n\n        </div>\n\n        <div class="section-item">\n\n            <div class="section-item--title-container">\n\n                <ion-input  class="aoh-input" [placeholder]="\'MAKE_A_CLAIM.PHONE\' | translate" formControlName="phone"></ion-input>\n\n            </div>\n\n        </div>\n\n        <div class="section-item">\n\n            <div class="section-item--title-container">\n\n                <ion-input  class="aoh-input" [placeholder]="\'MAKE_A_CLAIM.EMAIL\' | translate" formControlName="email"></ion-input>        \n\n            </div>\n\n        </div>\n\n    </div>\n\n    </form>\n\n</ion-content>\n\n\n\n<ion-footer>\n\n    <ion-toolbar>\n\n        <button class="btn-primary filled" (click)="onSet()">{{\'USER.SET\' | translate}}</button>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\user\user.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["c" /* TranslateService */]])
    ], UserPage);
    return UserPage;
}());

//# sourceMappingURL=user.js.map

/***/ }),

/***/ 715:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthInterceptor; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__utils_cordova_helpers_js__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_config__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_qs__ = __webpack_require__(458);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_qs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_qs__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var alterUrl = function (url, env) {
    if (url.indexOf('locales') !== -1) {
        return url;
    }
    // return url
    if (env.mock) {
        if (url.indexOf('Connections') !== -1) {
            // return window['cordova'].file.applicationDirectory + 'www/assets/mocks/f1/LOGIN.json'
            return Object(__WEBPACK_IMPORTED_MODULE_2__utils_cordova_helpers_js__["a" /* getResourceUrl */])('/assets/mocks/f1/LOGIN.json');
        }
        else {
            var parts = url.split('?');
            var query = __WEBPACK_IMPORTED_MODULE_4_qs___default.a.parse(parts[1]);
            // return window['cordova'].file.applicationDirectory + `www/assets/mocks/f1/${parts[0]}/${query.mode}.json`
            return Object(__WEBPACK_IMPORTED_MODULE_2__utils_cordova_helpers_js__["a" /* getResourceUrl */])("/assets/mocks/f1/" + parts[0] + "/" + query.mode + ".json");
        }
    }
    else {
        if (url.indexOf('Connections') !== -1) {
            return env.api.protocol + env.api.uri + env.api.authUrl + env.api.appId + url;
        }
        //  / Read?mode=GETUSER
        return env.api.protocol + env.api.uri + env.api.appId + env.api.service + url;
    }
};
var AuthInterceptor = /** @class */ (function () {
    function AuthInterceptor(store) {
        var _this = this;
        this.store = store;
        this.store.select('config')
            .subscribe(function (v) {
            _this.config = v;
            _this.env = __WEBPACK_IMPORTED_MODULE_3__config_config__["a" /* Environments */][_this.config.environment];
        });
    }
    AuthInterceptor.prototype.intercept = function (request, next) {
        request = request.clone({
            url: alterUrl(request.url, this.env),
            method: this.env.mock ? 'GET' : request.method,
            setHeaders: {
                Something: this.config.environment
            }
        });
        return next.handle(request);
    };
    AuthInterceptor = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */]])
    ], AuthInterceptor);
    return AuthInterceptor;
}());

//# sourceMappingURL=auth.interceptor.js.map

/***/ }),

/***/ 718:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadingInterceptor; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_operators__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__modules_loading_reducer__ = __webpack_require__(461);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoadingInterceptor = /** @class */ (function () {
    function LoadingInterceptor(store) {
        this.store = store;
    }
    LoadingInterceptor.prototype.intercept = function (request, next) {
        var _this = this;
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_3__modules_loading_reducer__["a" /* loadingItem */])(request.url));
        return next.handle(request).delay(1000)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["tap"])(function (event) {
            _this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_3__modules_loading_reducer__["b" /* loadingItemComplete */])(request.url));
            return event;
        }));
        //   if(event instanceof HttpResponse) {
        //     this.store.dispatch(loadingItemComplete(request.url))
        //   }
        // });
    };
    LoadingInterceptor = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ngrx_store__["h" /* Store */]])
    ], LoadingInterceptor);
    return LoadingInterceptor;
}());

//# sourceMappingURL=loading.interceptor.js.map

/***/ }),

/***/ 720:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(462);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(463);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__config_storageProvider__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__modules_user_selectors__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__modules_router_router_reducer__ = __webpack_require__(464);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__modules_router_router_selectors__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__navigation__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var startingHash = window.location.hash || '#/login';
// window.location.hash = ''
var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, store, splashScreen, menuCtrl, storage, translate, navigate) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.store = store;
        this.splashScreen = splashScreen;
        this.menuCtrl = menuCtrl;
        this.storage = storage;
        this.translate = translate;
        this.navigate = navigate;
        translate.setDefaultLang('en');
        if (window['cordova']) {
            translate.use('de');
        }
        else {
            translate.use('en');
        }
        this.initializeApp();
        this.navigation$ = this.store.select(__WEBPACK_IMPORTED_MODULE_7__modules_user_selectors__["b" /* getMenuOptions */]);
        this.showSidebar$ = this.store.select(__WEBPACK_IMPORTED_MODULE_9__modules_router_router_selectors__["b" /* showSidebar */]);
        this.user = this.store.select(__WEBPACK_IMPORTED_MODULE_7__modules_user_selectors__["c" /* user */]);
        this.count = store.pipe(Object(__WEBPACK_IMPORTED_MODULE_5__ngrx_store__["o" /* select */])('count'));
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.navigate.setNav(_this.nav);
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            if (window['cordova']) {
                _this.nav.setRoot('login-page');
            }
            // this.storage.init()
            //   .then(() => {
            // window.location.hash = startingHash
            //     this.splashScreen.hide();
            //   })
            setTimeout(function () {
                _this.nav.viewWillEnter.subscribe(function (view) {
                    _this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_8__modules_router_router_reducer__["b" /* viewDidEnter */])({
                        data: view.data,
                        name: view.name
                    }));
                });
            }, 10);
        });
    };
    MyApp.prototype.openPage = function (page) {
        this.menuCtrl.close();
        this.navigate.followLink(page.link);
        // if(this.nav.getActive().id !== page.link) {
        // this.nav.setRoot(page.link);
        // }
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
    };
    MyApp.prototype.logout = function () {
        this.menuCtrl.close();
        this.nav.setRoot('login-page');
        // this.store.dispatch(new Logout())
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\app\app.html"*/'<div [ngClass]="{sidebar: (showSidebar$ | async)}">\n\n<ion-menu [content]="content">\n\n  <ion-header>\n\n    <div class="sidebar--header">\n\n      <div class="sidebar--title">{{ (user|async)?.fullName }}</div>\n\n      <div class="sidebar--subtitle hidden">Zherer & Petersen GMBH</div>\n\n    </div>\n\n  </ion-header>\n\n\n\n  <ion-content class="sidebar--content">\n\n    <div *ngFor="let p of navigation$ | async">\n\n      <div class="sidebar--item"  (click)="openPage(p)">\n\n        <aoh-icon class="sidebar--icon" *ngIf="!p.notification" [icon]="p.icon"></aoh-icon>\n\n        <notification-icon class="sidebar--icon" *ngIf="p.notification"></notification-icon>\n\n        <div class="sidebar--label">{{p.title | translate}}</div>\n\n      </div>\n\n      <hr *ngIf="p.divider">\n\n    </div>\n\n\n\n    <div class="sidebar--item" (click)="logout()">\n\n      <aoh-icon icon="logout"></aoh-icon>\n\n      <div class="sidebar--label">{{\'LOGOUT\' | translate}}</div>\n\n    </div>\n\n  </ion-content>\n\n\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n\n\n\n<icon-sidebar></icon-sidebar>\n\n</div>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\app\app.html"*/,
            styles: ['./app.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_5__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_6__config_storageProvider__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_10__navigation__["a" /* NavigationService */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 721:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TestComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TestComponent = /** @class */ (function () {
    function TestComponent() {
    }
    TestComponent.prototype.ngOnInit = function () {
    };
    TestComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'test',template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\components\test\test.component.html"*/'<h1>Hello test component</h1>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\components\test\test.component.html"*/,
        }),
        __metadata("design:paramtypes", [])
    ], TestComponent);
    return TestComponent;
}());

//# sourceMappingURL=test.component.js.map

/***/ }),

/***/ 722:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environment_environmentModal_component__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__share_share_component__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shareConfirmation_shareConfirmation_component__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__makeAClaim_makeAClaim_component__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__makeAClaimConfirmation_makeAClaimConfirmation_component__ = __webpack_require__(391);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__basketConfirmation_basketConfirmation_component__ = __webpack_require__(723);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__dateRangeSelector_dateRangeSelector__ = __webpack_require__(395);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components___ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var ModalModule = /** @class */ (function () {
    function ModalModule() {
    }
    ModalModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__environment_environmentModal_component__["a" /* EnvironmentModal */]),
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_8__dateRangeSelector_dateRangeSelector__["a" /* DateRangeSelector */]),
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__share_share_component__["a" /* ShareModal */]),
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_4__shareConfirmation_shareConfirmation_component__["a" /* ShareConfirmationModal */]),
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_5__makeAClaim_makeAClaim_component__["a" /* MakeAClaimModal */]),
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_6__makeAClaimConfirmation_makeAClaimConfirmation_component__["a" /* MakeAClaimConfirmationModal */]),
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_7__basketConfirmation_basketConfirmation_component__["a" /* BasketConfirmationModal */]),
                __WEBPACK_IMPORTED_MODULE_9__components___["a" /* ComponentModule */],
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__environment_environmentModal_component__["a" /* EnvironmentModal */],
                __WEBPACK_IMPORTED_MODULE_8__dateRangeSelector_dateRangeSelector__["a" /* DateRangeSelector */],
                __WEBPACK_IMPORTED_MODULE_3__share_share_component__["a" /* ShareModal */],
                __WEBPACK_IMPORTED_MODULE_4__shareConfirmation_shareConfirmation_component__["a" /* ShareConfirmationModal */],
                __WEBPACK_IMPORTED_MODULE_5__makeAClaim_makeAClaim_component__["a" /* MakeAClaimModal */],
                __WEBPACK_IMPORTED_MODULE_6__makeAClaimConfirmation_makeAClaimConfirmation_component__["a" /* MakeAClaimConfirmationModal */],
                __WEBPACK_IMPORTED_MODULE_7__basketConfirmation_basketConfirmation_component__["a" /* BasketConfirmationModal */],
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__environment_environmentModal_component__["a" /* EnvironmentModal */],
                __WEBPACK_IMPORTED_MODULE_8__dateRangeSelector_dateRangeSelector__["a" /* DateRangeSelector */],
                __WEBPACK_IMPORTED_MODULE_3__share_share_component__["a" /* ShareModal */],
                __WEBPACK_IMPORTED_MODULE_4__shareConfirmation_shareConfirmation_component__["a" /* ShareConfirmationModal */],
                __WEBPACK_IMPORTED_MODULE_5__makeAClaim_makeAClaim_component__["a" /* MakeAClaimModal */],
                __WEBPACK_IMPORTED_MODULE_6__makeAClaimConfirmation_makeAClaimConfirmation_component__["a" /* MakeAClaimConfirmationModal */],
                __WEBPACK_IMPORTED_MODULE_7__basketConfirmation_basketConfirmation_component__["a" /* BasketConfirmationModal */],
            ],
        })
    ], ModalModule);
    return ModalModule;
}());

//# sourceMappingURL=index.js.map

/***/ }),

/***/ 723:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BasketConfirmationModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BasketConfirmationModal = /** @class */ (function () {
    function BasketConfirmationModal(viewCtrl, navParams) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.email = navParams.get('email');
    }
    BasketConfirmationModal.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    BasketConfirmationModal = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'basketConfirmation-confirmation',template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\modals\basketConfirmation\basketConfirmation.component.html"*/'<ion-header>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n    <confirmation-page>\n\n        <div class="confirmation-title">{{\'BASKET.CONTRACT_CREATED\' | translate}}</div>\n\n    </confirmation-page>\n\n</ion-content>\n\n\n\n    '/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\modals\basketConfirmation\basketConfirmation.component.html"*/,
            styles: ['./basketConfirmation.component.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */]])
    ], BasketConfirmationModal);
    return BasketConfirmationModal;
}());

//# sourceMappingURL=basketConfirmation.component.js.map

/***/ }),

/***/ 724:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export INCREMENT */
/* unused harmony export DECREMENT */
/* unused harmony export RESET */
/* harmony export (immutable) */ __webpack_exports__["a"] = counterReducer;
var INCREMENT = 'INCREMENT';
var DECREMENT = 'DECREMENT';
var RESET = 'RESET';
function counterReducer(state, action) {
    if (state === void 0) { state = 2; }
    switch (action.type) {
        case INCREMENT:
            return state + 1;
        case DECREMENT:
            return state - 1;
        case RESET:
            return 0;
        default:
            return state;
    }
}
//# sourceMappingURL=counter.reducer.js.map

/***/ }),

/***/ 725:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export localStorageSyncReducer */
/* unused harmony export debug */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return metaReducers; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ngrx_store_localstorage__ = __webpack_require__(726);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ngrx_store_localstorage___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_ngrx_store_localstorage__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__root_reducer__ = __webpack_require__(465);


var reducers = __WEBPACK_IMPORTED_MODULE_1__root_reducer__["a" /* rootReducer */];
function localStorageSyncReducer(reducer) {
    return Object(__WEBPACK_IMPORTED_MODULE_0_ngrx_store_localstorage__["localStorageSync"])({
        keys: [
            'userReducer',
            'config',
            'basket',
            // 'aoh',
            // 'customers',
            // {aoh: {filter: ['contracts', 'prices']}},
            'router',
            { prices: { filter: ['viewing'] } },
            // 'liftings',
            // 'contracts',
            'forms'
        ],
        rehydrate: true,
    })(reducer);
}
// console.log all actions
function debug(reducer) {
    return function (state, action) {
        // console.log('state', state);
        // console.log('action', action);
        return reducer(state, action);
    };
}
var metaReducers = [localStorageSyncReducer, debug];
//# sourceMappingURL=storeEnchancers.js.map

/***/ }),

/***/ 727:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return effects; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__user_effects__ = __webpack_require__(728);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__forms_connectForm_effects__ = __webpack_require__(729);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_aoh_aoh_effects__ = __webpack_require__(732);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_contracts_contracts_effects__ = __webpack_require__(733);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_customers_customers_effects__ = __webpack_require__(734);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_loadids_loadids_effects__ = __webpack_require__(735);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_prices_prices_effects__ = __webpack_require__(736);







var effects = [
    __WEBPACK_IMPORTED_MODULE_0__user_effects__["a" /* AuthEffects */],
    __WEBPACK_IMPORTED_MODULE_1__forms_connectForm_effects__["a" /* ConnectFormEffects */],
    __WEBPACK_IMPORTED_MODULE_2__pages_aoh_aoh_effects__["a" /* AOHEffects */],
    __WEBPACK_IMPORTED_MODULE_3__pages_contracts_contracts_effects__["a" /* ContractsEffects */],
    __WEBPACK_IMPORTED_MODULE_4__pages_customers_customers_effects__["a" /* CustomersEffects */],
    __WEBPACK_IMPORTED_MODULE_5__pages_loadids_loadids_effects__["a" /* LoadIdsEffects */],
    __WEBPACK_IMPORTED_MODULE_6__pages_prices_prices_effects__["a" /* PricesEffects */],
];
//# sourceMappingURL=effects.js.map

/***/ }),

/***/ 728:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthEffects; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_effects__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__serviceProvider__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__user_reducer__ = __webpack_require__(466);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var AuthEffects = /** @class */ (function () {
    function AuthEffects(http, api, actions$, app, loadingCtrl) {
        var _this = this;
        this.http = http;
        this.api = api;
        this.actions$ = actions$;
        this.app = app;
        this.loadingCtrl = loadingCtrl;
        // Listen for the 'LOGIN' action
        this.login$ = this.actions$.pipe(Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["d" /* ofType */])(__WEBPACK_IMPORTED_MODULE_7__user_reducer__["a" /* LOGIN */]), Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["mergeMap"])(function (action) {
            _this.loader = _this.loadingCtrl.create({
                content: 'Please wait...'
            });
            _this.loader.present();
            return _this.api.authenticate(action['payload'])
                .pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["mergeMap"])(function (data) { return _this.api.login(); }), Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["map"])(function (data) {
                _this.loader.dismiss();
                _this.navCtrl.setRoot('home');
                return { type: 'LOGIN_SUCCESS', payload: data['data'] };
            }), Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["catchError"])(function (e) {
                _this.loader.dismiss();
                return Object(__WEBPACK_IMPORTED_MODULE_3_rxjs__["of"])({ type: 'LOGIN_FAILED' });
            }));
        }));
        this.logout$ = this.actions$.pipe(Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["d" /* ofType */])(__WEBPACK_IMPORTED_MODULE_7__user_reducer__["b" /* LOGOUT */]), Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["mergeMap"])(function (action) {
            // this.navCtrl.setRoot('home')
            return _this.api.logout().
                pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["map"])(function (data) {
                _this.navCtrl.setRoot('login-page');
                return { type: 'LOGOUT_SUCCESS', payload: data };
            }), 
            // If request fails, dispatch failed action
            Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["catchError"])(function (e) {
                debugger;
                return Object(__WEBPACK_IMPORTED_MODULE_3_rxjs__["of"])({ type: 'LOGOUT_FAILED' });
            }));
        }));
        this.loader = this.loadingCtrl.create({
            content: 'Please wait...'
        });
    }
    Object.defineProperty(AuthEffects.prototype, "navCtrl", {
        get: function () {
            return this.app.getRootNav();
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs__["Observable"])
    ], AuthEffects.prototype, "login$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs__["Observable"])
    ], AuthEffects.prototype, "logout$", void 0);
    AuthEffects = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_6__serviceProvider__["a" /* APIService */], __WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["a" /* Actions */], __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["i" /* LoadingController */]])
    ], AuthEffects);
    return AuthEffects;
}());

//# sourceMappingURL=user.effects.js.map

/***/ }),

/***/ 729:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConnectFormEffects; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_observable_from__ = __webpack_require__(730);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_observable_from___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_observable_from__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__forms_reducer__ = __webpack_require__(467);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_observable_of__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_observable_of___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_observable_of__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngrx_effects__ = __webpack_require__(43);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ConnectFormEffects = /** @class */ (function () {
    function ConnectFormEffects(store, actions$) {
        this.store = store;
        this.actions$ = actions$;
        this.addStory$ = this.actions$
            .ofType('ADD_STORY')
            .switchMap(function (action) {
            return Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_observable_of__["of"])({})
                .switchMap(function (story) { return (Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_observable_from__["from"])([{
                    type: 'ADD_STORY_SUCCESS'
                }, Object(__WEBPACK_IMPORTED_MODULE_2__forms_reducer__["b" /* formSuccessAction */])('newStory')])); })
                .catch(function (err) {
                debugger;
                return Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_observable_of__["of"])(Object(__WEBPACK_IMPORTED_MODULE_2__forms_reducer__["a" /* formErrorAction */])('newStory', err));
            });
        });
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_5__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], ConnectFormEffects.prototype, "addStory$", void 0);
    ConnectFormEffects = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_5__ngrx_effects__["a" /* Actions */]])
    ], ConnectFormEffects);
    return ConnectFormEffects;
}());

//# sourceMappingURL=connectForm.effects.js.map

/***/ }),

/***/ 732:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AOHEffects; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_effects__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_operators__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__modules_serviceProvider__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__aoh_reducer__ = __webpack_require__(92);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var AOHEffects = /** @class */ (function () {
    function AOHEffects(http, api, actions$, app, store, loadingCtrl) {
        var _this = this;
        this.http = http;
        this.api = api;
        this.actions$ = actions$;
        this.app = app;
        this.store = store;
        this.loadingCtrl = loadingCtrl;
        this.aohList$ = this.actions$.pipe(Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["d" /* ofType */])(__WEBPACK_IMPORTED_MODULE_9__aoh_reducer__["a" /* GET_AOH */]), Object(__WEBPACK_IMPORTED_MODULE_5_rxjs_operators__["mergeMap"])(function (action) {
            return _this.api.aoh()
                .mergeMap(function (resp) {
                return [
                    Object(__WEBPACK_IMPORTED_MODULE_9__aoh_reducer__["g" /* getAohSuccess */])(resp.data),
                    Object(__WEBPACK_IMPORTED_MODULE_9__aoh_reducer__["l" /* getTimer */])(),
                    Object(__WEBPACK_IMPORTED_MODULE_9__aoh_reducer__["j" /* getFlexiContracts */])(),
                    Object(__WEBPACK_IMPORTED_MODULE_9__aoh_reducer__["h" /* getCreditPosition */])(),
                ];
            });
        }));
        this.flexiContracts$ = this.actions$.pipe(Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["d" /* ofType */])(__WEBPACK_IMPORTED_MODULE_9__aoh_reducer__["c" /* GET_FLEXI_CONTRACTS */]), Object(__WEBPACK_IMPORTED_MODULE_5_rxjs_operators__["mergeMap"])(function (action) {
            return _this.api.flexiContracts()
                .pipe(Object(__WEBPACK_IMPORTED_MODULE_5_rxjs_operators__["map"])(function (resp) {
                return Object(__WEBPACK_IMPORTED_MODULE_9__aoh_reducer__["k" /* getFlexiContractsSuccess */])(resp.data);
            }));
        }));
        this.timer$ = this.actions$.pipe(Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["d" /* ofType */])(__WEBPACK_IMPORTED_MODULE_9__aoh_reducer__["d" /* GET_TIMER */]), Object(__WEBPACK_IMPORTED_MODULE_5_rxjs_operators__["mergeMap"])(function (action) {
            return _this.api.timer()
                .pipe(Object(__WEBPACK_IMPORTED_MODULE_5_rxjs_operators__["map"])(function (resp) {
                var msDate = new Date(resp.data.EV_SYSTEM_DATE).getTime();
                var msToAdd = new Date(resp.data.EV_SYSTEM_TIME).getTime();
                var offset = msDate + msToAdd - Date.now();
                __WEBPACK_IMPORTED_MODULE_6_moment___default.a['now'] = function () {
                    return __WEBPACK_IMPORTED_MODULE_6_moment___default()(Date.now() + offset).valueOf();
                };
                return Object(__WEBPACK_IMPORTED_MODULE_9__aoh_reducer__["m" /* getTimerSuccess */])(resp.data);
            }));
        }));
        this.creditPosition$ = this.actions$.pipe(Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["d" /* ofType */])(__WEBPACK_IMPORTED_MODULE_9__aoh_reducer__["b" /* GET_CREDIT_POSITION */]), Object(__WEBPACK_IMPORTED_MODULE_5_rxjs_operators__["mergeMap"])(function (action) {
            return _this.api.creditPosition()
                .pipe(Object(__WEBPACK_IMPORTED_MODULE_5_rxjs_operators__["map"])(function (resp) {
                return Object(__WEBPACK_IMPORTED_MODULE_9__aoh_reducer__["i" /* getCreditPositionSuccess */])(resp.data);
            }));
        }));
    }
    Object.defineProperty(AOHEffects.prototype, "navCtrl", {
        get: function () {
            return this.app.getRootNav();
        },
        enumerable: true,
        configurable: true
    });
    AOHEffects.prototype.changeRouteIfRequired = function (target, customerCount) {
        var activePage = this.navCtrl.getActive();
        if (target !== activePage.id) {
            if (target !== 'customers' && customerCount) {
                this.navCtrl.setPages([{ page: 'customers' }, { page: target }]);
            }
            else {
                this.navCtrl.setRoot(target);
            }
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_rxjs__["Observable"])
    ], AOHEffects.prototype, "aohList$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_rxjs__["Observable"])
    ], AOHEffects.prototype, "flexiContracts$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_rxjs__["Observable"])
    ], AOHEffects.prototype, "timer$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_rxjs__["Observable"])
    ], AOHEffects.prototype, "creditPosition$", void 0);
    AOHEffects = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_8__modules_serviceProvider__["a" /* APIService */],
            __WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["a" /* Actions */],
            __WEBPACK_IMPORTED_MODULE_7_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_7_ionic_angular__["i" /* LoadingController */]])
    ], AOHEffects);
    return AOHEffects;
}());

//# sourceMappingURL=aoh.effects.js.map

/***/ }),

/***/ 733:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContractsEffects; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_effects__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__modules_serviceProvider__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__contracts_reducer__ = __webpack_require__(101);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ContractsEffects = /** @class */ (function () {
    function ContractsEffects(http, api, actions$, app, loadingCtrl) {
        var _this = this;
        this.http = http;
        this.api = api;
        this.actions$ = actions$;
        this.app = app;
        this.loadingCtrl = loadingCtrl;
        this.getContracts$ = this.actions$.pipe(Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["d" /* ofType */])(__WEBPACK_IMPORTED_MODULE_7__contracts_reducer__["c" /* GET_CONTRACTS */]), Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["mergeMap"])(function (action) {
            return _this.api.getContracts()
                .pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["map"])(function (resp) {
                return Object(__WEBPACK_IMPORTED_MODULE_7__contracts_reducer__["j" /* getContractsSuccess */])(resp.data);
            }));
        }));
        this.getContract$ = this.actions$.pipe(Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["d" /* ofType */])(__WEBPACK_IMPORTED_MODULE_7__contracts_reducer__["b" /* GET_CONTRACT */]), Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["mergeMap"])(function (action) {
            return _this.api.getContractDetails()
                .pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["map"])(function (resp) {
                return Object(__WEBPACK_IMPORTED_MODULE_7__contracts_reducer__["h" /* getContractSuccess */])(resp.data);
            }));
        }));
        this.contractsSearch$ = this.actions$.pipe(Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["d" /* ofType */])(__WEBPACK_IMPORTED_MODULE_7__contracts_reducer__["a" /* CONTRACTS_SEARCH */]), Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["map"])(function (action) {
            return Object(__WEBPACK_IMPORTED_MODULE_7__contracts_reducer__["f" /* contractsSearchSuccess */])(action.payload);
        }));
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs__["Observable"])
    ], ContractsEffects.prototype, "getContracts$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs__["Observable"])
    ], ContractsEffects.prototype, "getContract$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs__["Observable"])
    ], ContractsEffects.prototype, "contractsSearch$", void 0);
    ContractsEffects = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_6__modules_serviceProvider__["a" /* APIService */], __WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["a" /* Actions */], __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["i" /* LoadingController */]])
    ], ContractsEffects);
    return ContractsEffects;
}());

//# sourceMappingURL=contracts.effects.js.map

/***/ }),

/***/ 734:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomersEffects; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_effects__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_operators__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__modules_serviceProvider__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__customers_reducer__ = __webpack_require__(181);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









// import { selectedCustomer } from './aoh.selectors'
var CustomersEffects = /** @class */ (function () {
    function CustomersEffects(http, api, actions$, app, store, loadingCtrl) {
        var _this = this;
        this.http = http;
        this.api = api;
        this.actions$ = actions$;
        this.app = app;
        this.store = store;
        this.loadingCtrl = loadingCtrl;
        this.flexiContracts$ = this.actions$.pipe(Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["d" /* ofType */])(__WEBPACK_IMPORTED_MODULE_8__customers_reducer__["a" /* GET_CUSTOMERS */]), Object(__WEBPACK_IMPORTED_MODULE_5_rxjs_operators__["mergeMap"])(function (action) {
            return _this.api.customers()
                .pipe(Object(__WEBPACK_IMPORTED_MODULE_5_rxjs_operators__["map"])(function (resp) {
                return Object(__WEBPACK_IMPORTED_MODULE_8__customers_reducer__["d" /* getCustomersSuccess */])(resp);
            }));
        }));
    }
    Object.defineProperty(CustomersEffects.prototype, "navCtrl", {
        get: function () {
            return this.app.getRootNav();
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_rxjs__["Observable"])
    ], CustomersEffects.prototype, "flexiContracts$", void 0);
    CustomersEffects = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_7__modules_serviceProvider__["a" /* APIService */],
            __WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["a" /* Actions */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["i" /* LoadingController */]])
    ], CustomersEffects);
    return CustomersEffects;
}());

//# sourceMappingURL=customers.effects.js.map

/***/ }),

/***/ 735:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadIdsEffects; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_effects__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__modules_serviceProvider__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__loadids_reducer__ = __webpack_require__(155);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LoadIdsEffects = /** @class */ (function () {
    function LoadIdsEffects(api, actions$, store) {
        var _this = this;
        this.api = api;
        this.actions$ = actions$;
        this.store = store;
        this.loadIds$ = this.actions$.pipe(Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["d" /* ofType */])(__WEBPACK_IMPORTED_MODULE_6__loadids_reducer__["a" /* GET_LOAD_IDS */]), Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["mergeMap"])(function (action) {
            return _this.api.loadIds()
                .pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["map"])(function (resp) {
                return Object(__WEBPACK_IMPORTED_MODULE_6__loadids_reducer__["c" /* getLoadIdsSuccess */])(resp.data);
            }));
        }));
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs__["Observable"])
    ], LoadIdsEffects.prototype, "loadIds$", void 0);
    LoadIdsEffects = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__modules_serviceProvider__["a" /* APIService */],
            __WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["a" /* Actions */],
            __WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */]])
    ], LoadIdsEffects);
    return LoadIdsEffects;
}());

//# sourceMappingURL=loadids.effects.js.map

/***/ }),

/***/ 736:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PricesEffects; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_effects__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__prices_reducer__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__modules_serviceProvider__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var PricesEffects = /** @class */ (function () {
    // return this.api.authenticate(action['payload'])
    //   .pipe(
    //     mergeMap(data => this.api.login()),
    //     map(data => {
    //       this.loader.dismiss()
    //       this.navCtrl.setRoot('home')
    //       return { type: 'LOGIN_SUCCESS', payload: data['data'] }
    //     }),
    //     catchError((e) => {
    //       this.loader.dismiss()
    //       return of({ type: 'LOGIN_FAILED' })
    //     })
    //   )
    function PricesEffects(http, api, actions$) {
        var _this = this;
        this.http = http;
        this.api = api;
        this.actions$ = actions$;
        // Listen for the 'LOGIN' action
        this.prices$ = this.actions$.pipe(Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["d" /* ofType */])(__WEBPACK_IMPORTED_MODULE_5__prices_reducer__["a" /* SET_VIEWING */]), Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["mergeMap"])(function (action) {
            if (action.payload === 'current') {
                return _this.api.getCurrentPrices()
                    .pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["map"])(function (resp) {
                    return Object(__WEBPACK_IMPORTED_MODULE_5__prices_reducer__["c" /* pricesLoadSuccess */])(resp.data.T_MATERIAL_DETAILS);
                }));
            }
            return _this.api.getHistoricPrices()
                .pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["map"])(function (resp) {
                return Object(__WEBPACK_IMPORTED_MODULE_5__prices_reducer__["c" /* pricesLoadSuccess */])(resp.data.T_HISTORICAL_MATERIAL_DETAILS);
            }));
        }));
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs__["Observable"])
    ], PricesEffects.prototype, "prices$", void 0);
    PricesEffects = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_6__modules_serviceProvider__["a" /* APIService */], __WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["a" /* Actions */]])
    ], PricesEffects);
    return PricesEffects;
}());

//# sourceMappingURL=prices.effects.js.map

/***/ }),

/***/ 76:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export getState */
/* harmony export (immutable) */ __webpack_exports__["b"] = select;
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return createAction; });
function getState(store) {
    var state;
    store.take(1).subscribe(function (s) { return state = s; });
    return state;
}
function select(store, selector) {
    return selector(getState(store));
}
var createAction = function (type) {
    return function (payload) {
        if (payload === void 0) { payload = {}; }
        return ({
            type: type,
            payload: payload,
        });
    };
};
//# sourceMappingURL=storeUtils.js.map

/***/ }),

/***/ 77:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return currentParams; });
/* unused harmony export currentRouteName */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return showSidebar; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngrx_store__ = __webpack_require__(6);

var currentParams = function (state) {
    return state['router'].currentParams;
};
var currentRouteName = function (state) {
    return state['router'].currentRoute;
};
var showSidebar = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(currentRouteName, function (route) {
    return [
        'ContractDetailsPage',
        'DetailsPage',
        'LoginPage',
        'UserPage',
    ].indexOf(route) == -1;
});
//# sourceMappingURL=router.selectors.js.map

/***/ }),

/***/ 92:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export INIT_AOH */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GET_AOH; });
/* unused harmony export GET_AOH_SUCCESS */
/* unused harmony export GET_AOH_FAILURE */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return GET_FLEXI_CONTRACTS; });
/* unused harmony export GET_FLEXI_CONTRACTS_SUCCESS */
/* unused harmony export GET_FLEXI_CONTRACTS_FAILURE */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return GET_TIMER; });
/* unused harmony export GET_TIMER_SUCCESS */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return GET_CREDIT_POSITION; });
/* unused harmony export GET_CREDIT_POSITION_SUCCESS */
/* unused harmony export SET_AOH_FILTER */
/* unused harmony export GET_REFERENCE_STATUSES_TYPES */
/* unused harmony export GET_REFERENCE_GROUPS */
/* unused harmony export GET_REFERENCE_PLANTS */
/* unused harmony export SET_REFERENCE_DATA */
/* unused harmony export initAoh */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return getAoh; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return getAohSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "j", function() { return getFlexiContracts; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "k", function() { return getFlexiContractsSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "l", function() { return getTimer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "m", function() { return getTimerSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return getCreditPosition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "i", function() { return getCreditPositionSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "o", function() { return setAohFilter; });
/* unused harmony export getReferenceStatusesTypes */
/* unused harmony export getReferenceGroups */
/* unused harmony export getReferencePlants */
/* unused harmony export setReferenceData */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "n", function() { return removeDuplicateRecentFilters; });
/* harmony export (immutable) */ __webpack_exports__["e"] = aohReducer;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_lodash__);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};

var INIT_AOH = 'INIT_AOH';
var GET_AOH = 'GET_AOH';
var GET_AOH_SUCCESS = 'GET_AOH_SUCCESS';
var GET_AOH_FAILURE = 'GET_AOH_FAILURE';
var GET_FLEXI_CONTRACTS = 'GET_FLEXI_CONTRACTS';
var GET_FLEXI_CONTRACTS_SUCCESS = 'GET_FLEXI_CONTRACTS_SUCCESS';
var GET_FLEXI_CONTRACTS_FAILURE = 'GET_FLEXI_CONTRACTS_FAILURE';
var GET_TIMER = 'GET_TIMER';
var GET_TIMER_SUCCESS = 'GET_TIMER_SUCCESS';
var GET_CREDIT_POSITION = 'GET_CREDIT_POSITION';
var GET_CREDIT_POSITION_SUCCESS = 'GET_CREDIT_POSITION_SUCCESS';
var SET_AOH_FILTER = 'SET_AOH_FILTER';
var GET_REFERENCE_STATUSES_TYPES = 'GET_REFERENCE_STATUSES_TYPES';
var GET_REFERENCE_GROUPS = 'GET_REFERENCE_GROUPS';
var GET_REFERENCE_PLANTS = 'GET_REFERENCE_PLANTS';
var SET_REFERENCE_DATA = 'SET_REFERENCE_DATA';
var createAction = function (type) {
    return function (payload) {
        if (payload === void 0) { payload = {}; }
        return ({
            type: type,
            payload: payload,
        });
    };
};
var initAoh = createAction(INIT_AOH);
var getAoh = createAction(GET_AOH);
var getAohSuccess = createAction(GET_AOH_SUCCESS);
var getFlexiContracts = createAction(GET_FLEXI_CONTRACTS);
var getFlexiContractsSuccess = createAction(GET_FLEXI_CONTRACTS_SUCCESS);
var getTimer = createAction(GET_TIMER);
var getTimerSuccess = createAction(GET_TIMER_SUCCESS);
var getCreditPosition = createAction(GET_CREDIT_POSITION);
var getCreditPositionSuccess = createAction(GET_CREDIT_POSITION_SUCCESS);
var setAohFilter = createAction(SET_AOH_FILTER);
var getReferenceStatusesTypes = createAction(GET_REFERENCE_STATUSES_TYPES);
var getReferenceGroups = createAction(GET_REFERENCE_GROUPS);
var getReferencePlants = createAction(GET_REFERENCE_PLANTS);
var setReferenceData = createAction(SET_REFERENCE_DATA);
var initialState = ({
    prices: [],
    contracts: [],
    selectedFilter: null,
    recentFilters: [],
    creditPosition: null
});
var removeDuplicateRecentFilters = function (filters) {
    // Construct a comp key from the recent filters
    filters = filters.filter(function (f) { return f; });
    filters = filters.map(function (filterItem) {
        if (filterItem === void 0) { filterItem = { plants: [], products: [] }; }
        filterItem.plants = Object(__WEBPACK_IMPORTED_MODULE_0_lodash__["sortBy"])(filterItem.plants, 'value') || [];
        filterItem.products = Object(__WEBPACK_IMPORTED_MODULE_0_lodash__["sortBy"])(filterItem.products, 'value') || [];
        var compKey = filterItem.plants.concat(filterItem.products).reduce(function (prev, next) {
            return prev + '-' + next['value'];
        }, '');
        return __assign({}, filterItem, { compKey: compKey });
    });
    filters = Object(__WEBPACK_IMPORTED_MODULE_0_lodash__["uniqBy"])(filters, 'compKey');
    // Restrict length to 5
    return filters.slice(0, 5);
};
function aohReducer(state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case GET_AOH_SUCCESS:
            action.payload.T_MATERIAL_PRICES = action.payload.T_MATERIAL_PRICES.map(function (m) {
                return __assign({}, m, { id: m.PLANT + '-' + m.MATERIAL_ID });
            });
            return __assign({}, state, { materials: action.payload.T_MATERIAL_PRICES });
        case GET_FLEXI_CONTRACTS_SUCCESS:
            return __assign({}, state, { contracts: action.payload.T_CONTRACT_SUMMARY });
        case GET_TIMER_SUCCESS:
            return __assign({}, state, { timer: action.payload });
        case GET_CREDIT_POSITION_SUCCESS: {
            return __assign({}, state, { creditPosition: action.payload });
        }
        case SET_AOH_FILTER:
            var newRecentFilters = state.recentFilters;
            // If there are no filters selected then don't push to recent list
            // If there are more than 5 filters in the history then remove the last
            if (action.payload.storeAsRecent && (action.payload.products.length || action.payload.plants.length)) {
                newRecentFilters = [action.payload].concat(newRecentFilters);
            }
            newRecentFilters = removeDuplicateRecentFilters(newRecentFilters);
            return __assign({}, state, { selectedFilter: action.payload, recentFilters: newRecentFilters });
        default:
            return state;
    }
}
//# sourceMappingURL=aoh.reducer.js.map

/***/ }),

/***/ 93:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadingService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_observable_timer__ = __webpack_require__(622);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_observable_timer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_observable_timer__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loading_selector__ = __webpack_require__(624);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LoadingService = /** @class */ (function () {
    function LoadingService(store, loadingCtrl) {
        var _this = this;
        this.store = store;
        this.loadingCtrl = loadingCtrl;
        this.currentCount = 0;
        this.isLoading = new __WEBPACK_IMPORTED_MODULE_4_rxjs__["Subject"]();
        this.store.select(__WEBPACK_IMPORTED_MODULE_5__loading_selector__["a" /* loadingCount */])
            .debounce(function (v) {
            if (v && !_this.currentCount) {
                return Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_observable_timer__["timer"])(1);
            }
            return Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_observable_timer__["timer"])(200);
        })
            .subscribe(function (v) {
            _this.currentCount = v;
            _this.isLoading.next(!!_this.currentCount);
        });
    }
    LoadingService.prototype.showLoader = function () {
        this.loader = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        this.loader.present();
    };
    LoadingService.prototype.hideLoader = function () {
        if (this.loader) {
            this.loader.dismiss();
        }
    };
    LoadingService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ngrx_store__["h" /* Store */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */]])
    ], LoadingService);
    return LoadingService;
}());

//# sourceMappingURL=loading.provider.js.map

/***/ }),

/***/ 94:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
const getResourceUrl = (url) => {
    if(window['cordova']) {
        return  window['cordova'].file.applicationDirectory + 'www' + url
    } else {
        return url
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = getResourceUrl;


/***/ }),

/***/ 95:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export ADD_ITEM */
/* unused harmony export REMOVE_ITEM */
/* unused harmony export UPDATE_ITEM */
/* unused harmony export EMPTY_BASKET */
/* unused harmony export AddItem */
/* unused harmony export RemoveItem */
/* unused harmony export UpdateItem */
/* unused harmony export EmptyBasket */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return actions; });
/* harmony export (immutable) */ __webpack_exports__["b"] = basketReducer;
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var ADD_ITEM = 'ADD_ITEM';
var REMOVE_ITEM = 'REMOVE_ITEM';
var UPDATE_ITEM = 'UPDATE_ITEM';
var EMPTY_BASKET = 'EMPTY_BASKET';
var AddItem = /** @class */ (function () {
    function AddItem(payload) {
        this.payload = payload;
        this.type = ADD_ITEM;
    }
    return AddItem;
}());

var RemoveItem = /** @class */ (function () {
    function RemoveItem(payload) {
        this.payload = payload;
        this.type = REMOVE_ITEM;
    }
    return RemoveItem;
}());

var UpdateItem = /** @class */ (function () {
    function UpdateItem(payload) {
        this.payload = payload;
        this.type = UPDATE_ITEM;
    }
    return UpdateItem;
}());

var EmptyBasket = /** @class */ (function () {
    function EmptyBasket(payload) {
        this.payload = payload;
        this.type = EMPTY_BASKET;
    }
    return EmptyBasket;
}());

var actions = {
    AddItem: AddItem,
    RemoveItem: RemoveItem,
    UpdateItem: UpdateItem,
    EmptyBasket: EmptyBasket
};
var initialState = {
    items: [],
    // items: [],
    total: 0,
};
function basketReducer(state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case ADD_ITEM:
            action.payload.id = action.payload.material.id;
            return __assign({}, state, { items: (state.items).concat(action.payload) });
        case REMOVE_ITEM:
            var items = state.items;
            var id_1 = action.payload;
            return __assign({}, state, { items: items.filter(function (i) { return i['id'] !== id_1; }) });
        case UPDATE_ITEM:
            var updateItems = state.items;
            var _a = action.payload, itemId_1 = _a.id, quantity_1 = _a.quantity;
            return __assign({}, state, { items: updateItems.map(function (i) {
                    if (i['id'] === itemId_1) {
                        return __assign({}, i, { quantity: quantity_1 });
                    }
                    return i;
                }) });
        case EMPTY_BASKET:
            return __assign({}, state, { items: [] });
        default:
            return state;
    }
}
//# sourceMappingURL=basket.reducer.js.map

/***/ }),

/***/ 96:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StorageService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DummyStorage = /** @class */ (function () {
    function DummyStorage() {
    }
    DummyStorage.prototype.create = function (name) {
        // const self = this;
        // return this;
        return new Promise(function (resolve, reject) {
            setTimeout(function () {
                resolve(self);
            }, 100);
        });
    };
    DummyStorage.prototype.set = function (key, value) {
        return new Promise(function (resolve, reject) {
            setTimeout(function () {
                localStorage.setItem(key, value);
                resolve();
            }, 100);
        });
    };
    DummyStorage.prototype.get = function (key) {
        return new Promise(function (resolve, reject) {
            setTimeout(function () {
                var x = localStorage.getItem(key);
                resolve({ key: key, value: x });
            }, 100);
        });
    };
    DummyStorage.prototype.keys = function () {
        return new Promise(function (resolve, reject) {
            setTimeout(function () {
                resolve(Object.keys(localStorage));
            });
        });
    };
    DummyStorage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])()
    ], DummyStorage);
    return DummyStorage;
}());
var StorageItem = /** @class */ (function () {
    function StorageItem() {
    }
    Object.defineProperty(StorageItem.prototype, "value", {
        get: function () {
            return;
        },
        enumerable: true,
        configurable: true
    });
    return StorageItem;
}());
var StorageService = /** @class */ (function () {
    function StorageService() {
        // this.secureStorage.create('my_store_name')
        //     .then((storage: SecureStorageObject) => {
        //         this.storage = storage
        //         this.storage.set('test', 'waffle').then()
        //     })
    }
    StorageService.prototype.init = function () {
        return new Promise(function (resolve) {
            resolve();
        });
        // return new Promise((resolve, reject) => {
        //     const dummyStorage = new DummyStorage()
        //     this.secureStorage = dummyStorage
        //     dummyStorage.keys()
        //         .then((keys) => {
        //             return Promise.all(
        //                 keys.map((key) => dummyStorage.get(key))
        //             )
        //         })
        //         .then((values) => {
        //             this.storage = values.reduce((prev, next) => {
        //                 prev[next.key] = next.value
        //                 return prev
        //             }, {})
        //             return true
        //         }).then(resolve)
        // })
    };
    StorageService.prototype.getValue = function (key) {
        return this.storage[key];
    };
    StorageService.prototype.setValue = function (key, value) {
        this.secureStorage.set(key, value);
        return this.storage[key] = value;
    };
    StorageService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], StorageService);
    return StorageService;
}());

//# sourceMappingURL=storageProvider.js.map

/***/ })

},[469]);
//# sourceMappingURL=main.js.map