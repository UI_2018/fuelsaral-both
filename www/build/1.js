webpackJsonp([1],{

/***/ 767:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContractsModule", function() { return ContractsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contracts__ = __webpack_require__(769);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ContractsModule = /** @class */ (function () {
    function ContractsModule() {
    }
    ContractsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__contracts__["a" /* ContractsPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__contracts__["a" /* ContractsPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components__["a" /* ComponentModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__contracts__["a" /* ContractsPage */]
            ]
        })
    ], ContractsModule);
    return ContractsModule;
}());

//# sourceMappingURL=contracts.module.js.map

/***/ }),

/***/ 769:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContractsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__search_search__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__contracts_reducer__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__contracts_selectors__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__search_searchConfig__ = __webpack_require__(103);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var tabOptions = [
    { title: 'OPEN', value: true },
    { title: 'ALL', value: false }
];
var ContractsPage = /** @class */ (function () {
    function ContractsPage(store, navCtrl, viewCtrl, modalCtrl) {
        var _this = this;
        this.store = store;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.modalCtrl = modalCtrl;
        this.contractsSearch = __WEBPACK_IMPORTED_MODULE_6__search_searchConfig__["a" /* contractsSearch */];
        this.tabOptions = tabOptions;
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_4__contracts_reducer__["i" /* getContracts */])({}));
        this.store.select(__WEBPACK_IMPORTED_MODULE_5__contracts_selectors__["b" /* contractSearch */]).subscribe(function (v) {
            _this.searchValues = v;
        });
        this.store.select(__WEBPACK_IMPORTED_MODULE_5__contracts_selectors__["c" /* contractsWithFlags */]).subscribe(function (v) {
            _this.contracts = v;
        });
        this.showingSearchResults$ = this.store.select(__WEBPACK_IMPORTED_MODULE_5__contracts_selectors__["e" /* isShowingContractSearch */]);
    }
    ContractsPage.prototype.openSearch = function () {
        this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__search_search__["a" /* SearchPage */], {
            fields: __WEBPACK_IMPORTED_MODULE_6__search_searchConfig__["a" /* contractsSearch */].fields,
            onSearch: this.onSearch.bind(this),
            values: this.searchValues
        }).present();
    };
    ContractsPage.prototype.onChangeSearch = function () {
        this.openSearch();
    };
    ContractsPage.prototype.handleFilterChange = function (value) {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_4__contracts_reducer__["k" /* setFilter */])({ isOpen: value }));
    };
    ContractsPage.prototype.onSearch = function (values) {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_4__contracts_reducer__["e" /* contractsSearch */])(values));
    };
    ContractsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'contracts',template:/*ion-inline-start:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\contracts\contracts.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>{{\'PAGE_TITLE.CONTRACTS\' | translate}}</ion-title>\n\n        <ion-buttons end>\n\n            <aoh-icon icon="search" (click)="openSearch()"></aoh-icon>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="background-white">\n\n    <search-details *ngIf="(showingSearchResults$ | async) && searchValues" [values]="searchValues" [fields]="contractsSearch.fields" (onChange)="onChangeSearch()"></search-details>\n\n    <tab-bar *ngIf="!(showingSearchResults$ | async)" (onChange)="handleFilterChange($event)" [options]="tabOptions"></tab-bar>\n\n    <div class="sections-title-container" *ngIf="(showingSearchResults$ | async) && searchValues">\n\n        <div class="sections-title">{{ \'SEARCH.RESULTS_TITLE\' | translate }}</div>\n\n    </div>\n\n    <div class="masonary-container padding" masonaryContainer="375">\n\n        <contract-card *ngFor="let contract of contracts" [contract]="contract"></contract-card>\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\UR\WebstormProjects\bp-aral-fuels\src\pages\contracts\contracts.html"*/,
            styles: ['./contracts.scss']
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */]])
    ], ContractsPage);
    return ContractsPage;
}());

//# sourceMappingURL=contracts.js.map

/***/ })

});
//# sourceMappingURL=1.js.map